<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ExtraPropertyCharges */

$this->title = 'Create Extra Property Charges';
$this->params['breadcrumbs'][] = ['label' => 'Extra Property Charges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="extra-property-charges-create">
    

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
