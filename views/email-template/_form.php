<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model app\models\EmailTemplate */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="email-template-form">

    <?php $form = ActiveForm::begin ( [
        'type' => ActiveForm::TYPE_VERTICAL,
        'errorCssClass' => 'has-danger',

    ] ); ?>

    <section class="card " id="card-1" data-portlet-item>
        <header class="card-header portlet-handler">

            <?php

            if($model->isNewRecord){?>
            <h4 class="card-title">Add New Email Template</h4>


           <?php }else { ?>

                <h4 class="card-title">Update Email Template</h4>

            <?php }?>

        </header>
        <div class="card-body">

            <?= $form->field($model, 'template_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'template_subject')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'template_body')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'full'
            ]) ?>

            <?php

            if(\Yii::$app->user->can('developer_fields'))
            {

                echo $form->field($model, 'developer')->dropDownList(['0'=>'No','1'=>'Yes', ]);

            }

            ?>

            <?= $form->field($model, 'active')->dropDownList(['1'=>'Active','0'=>'In Active', ]) ?>


        </div>
        <footer class="card-header text-center">



            <a href="<?= Yii::$app->homeUrl?>email-template/index" class="mb-1 mt-1 mr-1 btn btn-danger">Cancel</a>

            <?= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) ?>

        </footer>


        <?php ActiveForm::end(); ?>



    </section>

</div>
