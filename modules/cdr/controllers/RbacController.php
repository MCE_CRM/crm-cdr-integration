<?php

namespace app\controllers;

use Yii;
use app\models\AuthItem;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RbacController implements the CRUD actions for AuthItem model.
 */
class RbacController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AuthItem models.
     * @return mixed
     */
    // Create role
    public function actionAssignrole()
    {
        $auth = Yii::$app->authManager;
        $Manager = $auth->createRole('Manager');
        $frontoffice = $auth->createRole('frontoffice');
        $backoffice = $auth->createRole('backoffice');
        $admin = $auth->createRole('admin');
        $registration = $auth->createRole('registration');
        $cashier = $auth->createRole('cashier');
        $attendance = $auth->createRole('attendance');
        $paymentupdate = $auth->createRole('paymentupdate');
        $registrationdeletion = $auth->createRole('registrationdeletion');
        $Owner = $auth->createRole('Owner');
        // Assign roles to users. 1 and 2 are IDs returned by IdentityInterface::getId()
        // usually implemented in your User model.
        //$auth->assign($registration, 4);
        //$auth->assign($frontoffice, 3);
        //$auth->assign($Manager, 2);
        //$auth->assign($admin, 1);
    }

    // Create role
    public function actionCreate_role()
    {
        $auth = Yii::$app->authManager;
        // add "create" permission for Registeration
        $createregistration = $auth->createPermission('registration/create');
        $updateregistration = $auth->createPermission('registration/update');
        $registration = $auth->createPermission('registration/');
        $viewregistration = $auth->createPermission('registration/view');
        $deleteregistration = $auth->createPermission('registration/delete');
        $indexregistration = $auth->createPermission('registration/index');

        //add "create" Permission for Payments
        $payments = $auth->createPermission('payments/');
        $viewpayments = $auth->createPermission('payments/view');
        $createpayments = $auth->createPermission('payments/create');
        $updatepayments = $auth->createPermission('payments/update');
        $deletepayments = $auth->createPermission('payments/delete');
        $indexpayments = $auth->createPermission('payments/index');
        $viewpendingpayments = $auth->createPermission('payments/viewpending');
        $createvoucherpayments = $auth->createPermission('payments/createvoucher');
        $regformpayments = $auth->createPermission('payments/regform');
        $takepaymentpayments = $auth->createPermission('payments/takepayment');

        //add "create" Permission for Attendance
        $attendance = $auth->createPermission('attendance/');
        $viewattendance = $auth->createPermission('attendance/view');
        $createattendance = $auth->createPermission('attendance/create');
        $indexattendance = $auth->createPermission('attendance/index');
        $updateattendance = $auth->createPermission('attendance/update');
        $deleteattendance = $auth->createPermission('attendance/delete');

        //add "create" permission MOP
        $MOP = $auth->createPermission('mop/');
        $viewMOP = $auth->createPermission('mop/view');
        $createMOP = $auth->createPermission('mop/create');
        $indexMOP = $auth->createPermission('mop/index');
        $updateMOP = $auth->createPermission('mop/update');
        $deleteMOP = $auth->createPermission('mop/delete');

        //add "create" permission newplan
        $newplan = $auth->createPermission('newplan/');
        $viewnewplan = $auth->createPermission('newplan/view');
        $createnewplan = $auth->createPermission('newplan/create');
        $indexnewplan = $auth->createPermission('newplan/index');
        $updatenewplan = $auth->createPermission('newplan/update');
        $deletenewplan = $auth->createPermission('newplan/delete');

        //add "create" permission program
        $program = $auth->createPermission('program/');
        $viewprogram = $auth->createPermission('program/view');
        $createprogram = $auth->createPermission('program/create');
        $indexprogram = $auth->createPermission('program/index');
        $updateprogram = $auth->createPermission('program/update');
        $deleteprogram = $auth->createPermission('program/delete');

        //add "create" permission report
        $dailysalereportreport = $auth->createPermission('reports/dailysalereport');
        $categorywisesalereportreport = $auth->createPermission('reports/categorywisesalereport');
        $attendancereport = $auth->createPermission('reports/attendance');
        $monthlyattendancereport = $auth->createPermission('reports/monthlyattendance');
        $pendingpaymentreportreport = $auth->createPermission('reports/pendingpaymentreport');
        $activecustomerreport = $auth->createPermission('reports/activecustomer');
        $suspendedcustomerreport = $auth->createPermission('reports/suspendedcustomer');
        $categorythermalreportreport = $auth->createPermission('reports/categorywisethermalreport');
        $paymentplanreport = $auth->createPermission('reports/paymentplan');
        $inactivecustomerreport = $auth->createPermission('reports/inactivecustomer');





        // add "author" role and give this role the "createPost" permission
        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $createregistration);
        $auth->addChild($admin, $updateregistration);
        $auth->addChild($admin, $viewregistration);
        $auth->addChild($admin, $registration);
        $auth->addChild($admin, $deleteregistration);
        $auth->addChild($admin, $indexregistration);
        $auth->addChild($admin, $createpayments);
        $auth->addChild($admin, $updatepayments);
        $auth->addChild($admin, $deletepayments);
        $auth->addChild($admin, $indexpayments);
        $auth->addChild($admin, $viewpayments);
        $auth->addChild($admin, $createvoucherpayments);
        $auth->addChild($admin, $viewpendingpayments);
        $auth->addChild($admin, $regformpayments);
        $auth->addChild($admin, $takepaymentpayments);
        $auth->addChild($admin, $viewattendance);
        $auth->addChild($admin, $createattendance);
        $auth->addChild($admin, $indexattendance);
        $auth->addChild($admin, $updateattendance);
        $auth->addChild($admin, $deleteattendance);
        $auth->addChild($admin, $viewMOP);
        $auth->addChild($admin, $createMOP);
        $auth->addChild($admin, $indexMOP);
        $auth->addChild($admin, $updateMOP);
        $auth->addChild($admin, $deleteMOP);
        $auth->addChild($admin, $viewnewplan);
        $auth->addChild($admin, $createnewplan);
        $auth->addChild($admin, $indexnewplan);
        $auth->addChild($admin, $updatenewplan);
        $auth->addChild($admin, $deletenewplan);
        $auth->addChild($admin, $viewprogram);
        $auth->addChild($admin, $createprogram);
        $auth->addChild($admin, $indexprogram);
        $auth->addChild($admin, $updateprogram);
        $auth->addChild($admin, $deleteprogram);
        $auth->addChild($admin, $dailysalereportreport);
        $auth->addChild($admin, $categorywisesalereportreport);
        $auth->addChild($admin, $attendancereport);
        $auth->addChild($admin, $monthlyattendancereport);
        $auth->addChild($admin, $pendingpaymentreportreport);
        $auth->addChild($admin, $activecustomerreport);
        $auth->addChild($admin, $suspendedcustomerreport);
        $auth->addChild($admin, $categorythermalreportreport);
        $auth->addChild($admin, $paymentplanreport);
        $auth->addChild($admin, $inactivecustomerreport);



        $registration = $auth->createRole('registration');
        $auth->add($registration);
        $auth->addChild($registration, $createregistration);
        $auth->addChild($registration, $regformpayments);
        $auth->addChild($registration, $createpayments);
        $auth->addChild($registration, $indexregistration);
        $auth->addChild($registration, $updateregistration);
        $auth->addChild($registration, $viewregistration);
        $auth->addChild($registration, $activecustomerreport);
        $auth->addChild($registration, $suspendedcustomerreport);

        $cashier = $auth->createRole('cashier');
        $auth->add($cashier);
        $auth->addChild($cashier, $viewpendingpayments);
        $auth->addChild($cashier, $takepaymentpayments);
        $auth->addChild($cashier, $dailysalereportreport);
        $auth->addChild($cashier, $categorythermalreportreport);

        $attendance = $auth->createRole('attendance');
        $auth->add($attendance);
        $auth->addChild($attendance, $indexattendance);
        $auth->addChild($attendance, $attendancereport);

        $paymentupdate = $auth->createRole('paymentupdate');
        $auth->add($paymentupdate);
        $auth->addChild($paymentupdate, $indexpayments);
        $auth->addChild($paymentupdate, $updatepayments);
        $auth->addChild($paymentupdate, $paymentplanreport);

        $registrationdeletion = $auth->createRole('registrationdeletion');
        $auth->add($registrationdeletion);
        $auth->addChild($registrationdeletion, $deleteregistration);
        $auth->addChild($registrationdeletion, $viewregistration);
        $auth->addChild($registrationdeletion, $indexregistration);



        // add "author" role and give this role the "createPost" permission
        $manager = $auth->createRole('manager');
        $auth->add($manager);
        $auth->addChild($manager, $indexattendance);
        $auth->addChild($manager, $createattendance);
        $auth->addChild($manager, $dailysalereportreport);
        $auth->addChild($manager, $categorywisesalereportreport);
        $auth->addChild($manager, $attendancereport);
        $auth->addChild($manager, $monthlyattendancereport);
        $auth->addChild($manager, $pendingpaymentreportreport);
        $auth->addChild($manager, $activecustomerreport);
        $auth->addChild($manager, $suspendedcustomerreport);


        $backoffice = $auth->createRole('backoffice');
        $auth->add($backoffice);
        $auth->addChild($backoffice, $indexattendance);
        $auth->addChild($backoffice, $createattendance);
        $auth->addChild($backoffice, $dailysalereportreport);
        $auth->addChild($backoffice, $categorywisesalereportreport);
        $auth->addChild($backoffice, $attendancereport);
        $auth->addChild($backoffice, $monthlyattendancereport);
        $auth->addChild($backoffice, $pendingpaymentreportreport);
        $auth->addChild($backoffice, $activecustomerreport);
        $auth->addChild($backoffice, $suspendedcustomerreport);


        $Owner = $auth->createRole('Owner');
        $auth->add($Owner);
        $auth->addChild($Owner, $indexattendance);
        $auth->addChild($Owner, $registration);
        $auth->addChild($Owner, $createattendance);
        $auth->addChild($Owner, $createregistration);
        $auth->addChild($Owner, $viewregistration);
        $auth->addChild($Owner, $deleteregistration);
        $auth->addChild($Owner, $createpayments);
        $auth->addChild($Owner, $updatepayments);
        $auth->addChild($Owner, $deletepayments);
        $auth->addChild($Owner, $viewpayments);
        $auth->addChild($Owner, $createvoucherpayments);
        $auth->addChild($Owner, $viewpendingpayments);
        $auth->addChild($Owner, $regformpayments);
        $auth->addChild($Owner, $takepaymentpayments);
        $auth->addChild($Owner, $dailysalereportreport);
        $auth->addChild($Owner, $categorywisesalereportreport);
        $auth->addChild($Owner, $attendancereport);
        $auth->addChild($Owner, $monthlyattendancereport);
        $auth->addChild($Owner, $activecustomerreport);
        $auth->addChild($Owner, $suspendedcustomerreport);
        $auth->addChild($Owner, $inactivecustomerreport);



        // add "admin" role and give this role the "updatePost" permission
        // as well as the permissions of the "author" role
        /*$admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $createregistration);
        $auth->addChild($admin, $updateregistration);
        $auth->addChild($admin, $viewregistration);
        $auth->addChild($admin, $deleteregistration);
        $auth->addChild($admin, $indexregistration);*/
    }


	 
	//Create permission
    public function actionCreate_permission()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
		// add "create" permission
        $createregistration = $auth->createPermission('registration/create');
        $createregistration->description = 'Create registration';
        $auth->add($createregistration);

        // add "update" permission
        $updateregistration = $auth->createPermission('registration/update');
        $updateregistration->description = 'Update registration';
        $auth->add($updateregistration);

        // add "view" permission
        $viewregistration = $auth->createPermission('registration/view');
        $viewregistration->description = 'view registration';
        $auth->add($viewregistration);

        // add "view" permission
        $registration = $auth->createPermission('registration/');
        $registration->description = 'header registration';
        $auth->add($registration);

        // add "delete" permission
        $deleteregistration = $auth->createPermission('registration/delete');
        $deleteregistration->description = 'delete registration';
        $auth->add($deleteregistration);

        // add "index" permission
        $indexregistration = $auth->createPermission('registration/index');
        $indexregistration->description = 'index registration';
        $auth->add($indexregistration);

        $createpayments = $auth->createPermission('payments/create');
        $createpayments->description = 'Create payments';
        $auth->add($createpayments);

        // add "update" permission
        $updatepayments = $auth->createPermission('payments/update');
        $updatepayments->description = 'Update payments';
        $auth->add($updatepayments);

        // add "view" permission
        $payments = $auth->createPermission('payments/');
        $payments->description = 'view payments';
        $auth->add($payments);

        // add "view" permission
        $viewpayments = $auth->createPermission('payments/view');
        $viewpayments->description = 'view payments';
        $auth->add($viewpayments);

        // add "delete" permission
        $deletepayments = $auth->createPermission('payments/delete');
        $deletepayments->description = 'delete payments';
        $auth->add($deletepayments);

        // add "index" permission
        $indexpayments = $auth->createPermission('payments/index');
        $indexpayments->description = 'index payments';
        $auth->add($indexpayments);

        // add "$createvoucherpayments" permission
        $createvoucherpayments = $auth->createPermission('payments/createvoucher');
        $createvoucherpayments->description = 'create voucher payments';
        $auth->add($createvoucherpayments);

        // add "$viewpendingpayments" permission
        $viewpendingpayments = $auth->createPermission('payments/viewpending');
        $viewpendingpayments->description = 'view pending payments';
        $auth->add($viewpendingpayments);

        // add "$regformpayments" permission
        $regformpayments = $auth->createPermission('payments/regform');
        $regformpayments->description = 'Registration Form';
        $auth->add($regformpayments);

        // add "$regformpayments" permission
        $takepaymentpayments = $auth->createPermission('payments/takepayment');
        $takepaymentpayments->description = 'take payments';
        $auth->add($takepaymentpayments);

        // add "$viewattendance" permission
        $viewattendance = $auth->createPermission('attendance/view');
        $viewattendance->description = 'View Attendance';
        $auth->add($viewattendance);


        // add "$createattendance" permission
        $createattendance = $auth->createPermission('attendance/create');
        $createattendance->description = 'Create Attendance';
        $auth->add($createattendance);


        // add "$indexattendance" permission
        $indexattendance = $auth->createPermission('attendance/index');
        $indexattendance->description = 'Index view Attendance';
        $auth->add($indexattendance);


        // add "$updateattendance" permission
        $updateattendance = $auth->createPermission('attendance/update');
        $updateattendance->description = 'Update Attendance';
        $auth->add($updateattendance);


        // add "$deleteattendance" permission
        $deleteattendance = $auth->createPermission('attendance/delete');
        $deleteattendance->description = 'Delete Attendance';
        $auth->add($deleteattendance);


        // add "$viewMOP" permission
        $viewMOP = $auth->createPermission('mop/view');
        $viewMOP->description = 'view MOP';
        $auth->add($viewMOP);


        // add "$createMOP" permission
        $createMOP = $auth->createPermission('mop/create');
        $createMOP->description = 'create mop';
        $auth->add($createMOP);


        // add "$indexMOP" permission
        $indexMOP = $auth->createPermission('mop/index');
        $indexMOP->description = 'create mop';
        $auth->add($indexMOP);


        // add "$updateMOP" permission
        $updateMOP = $auth->createPermission('mop/update');
        $updateMOP->description = 'update mop';
        $auth->add($updateMOP);


        // add "$deleteMOP" permission
        $deleteMOP = $auth->createPermission('mop/delete');
        $deleteMOP->description = 'delete mop';
        $auth->add($deleteMOP);


        // add "$viewnewplan" permission
        $viewnewplan = $auth->createPermission('newplan/viewnew');
        $viewnewplan->description = 'delete mop';
        $auth->add($viewnewplan);


        // add "$createnewplan" permission
        $createnewplan = $auth->createPermission('newplan/create');
        $createnewplan->description = 'create newplan';
        $auth->add($createnewplan);


        // add "$indexnewplan" permission
        $indexnewplan = $auth->createPermission('newplan/index');
        $indexnewplan->description = 'index newplan';
        $auth->add($indexnewplan);


        // add "$updatenewplan" permission
        $updatenewplan = $auth->createPermission('newplan/update');
        $updatenewplan->description = 'index newplan';
        $auth->add($updatenewplan);


        // add "$viewnewplan" permission
        $viewnewplan = $auth->createPermission('newplan/view');
        $viewnewplan->description = 'view newplan';
        $auth->add($viewnewplan);


        // add "$deletenewplan" permission
        $deletenewplan = $auth->createPermission('newplan/delete');
        $deletenewplan->description = 'delete newplan';
        $auth->add($deletenewplan);


        // add "$viewprogram" permission
        $viewprogram = $auth->createPermission('program/view');
        $viewprogram->description = 'view program';
        $auth->add($viewprogram);


        // add "$createprogram" permission
        $createprogram = $auth->createPermission('program/create');
        $createprogram->description = 'create program';
        $auth->add($createprogram);


        // add "$indexprogram" permission
        $indexprogram = $auth->createPermission('program/index');
        $indexprogram->description = 'index program';
        $auth->add($indexprogram);


        // add "$updateprogram" permission
        $updateprogram = $auth->createPermission('program/update');
        $updateprogram->description = 'index program';
        $auth->add($updateprogram);


        // add "$deleteprogram" permission
        $deleteprogram = $auth->createPermission('program/delete');
        $deleteprogram->description = 'delete program';
        $auth->add($deleteprogram);


        // add "$dailysalereportreport" permission
        $dailysalereportreport = $auth->createPermission('reports/dailysalereport');
        $dailysalereportreport->description = 'daily sale reports';
        $auth->add($dailysalereportreport);


        // add "$categorywisesalereportreport" permission
        $categorywisesalereportreport = $auth->createPermission('reports/categorywisesalereport');
        $categorywisesalereportreport->description = 'category sale reports';
        $auth->add($categorywisesalereportreport);


        // add "$attendancereport" permission
        $attendancereport = $auth->createPermission('reports/attendance');
        $attendancereport->description = 'Attendance reports';
        $auth->add($attendancereport);


        // add "$monthlyattendancereport" permission
        $monthlyattendancereport = $auth->createPermission('reports/monthlyattendance');
        $monthlyattendancereport->description = 'Attendance reports';
        $auth->add($monthlyattendancereport);


        // add "$pendingpaymentreportreport" permission
        $pendingpaymentreportreport = $auth->createPermission('reports/pendingpaymentreport');
        $pendingpaymentreportreport->description = 'pending payment reports';
        $auth->add($pendingpaymentreportreport);


        // add "$activecustomerreport" permission
        $activecustomerreport = $auth->createPermission('reports/activecustomer');
        $activecustomerreport->description = 'active customer reports';
        $auth->add($activecustomerreport);


        // add "$suspendedcustomerreport" permission
        $suspendedcustomerreport = $auth->createPermission('reports/suspendedcustomer');
        $suspendedcustomerreport->description = 'active customer reports';
        $auth->add($suspendedcustomerreport);


        // add "$categorythermalreportreport" permission
        $categorythermalreportreport = $auth->createPermission('reports/categorythermalreport');
        $categorythermalreportreport->description = 'category wise thermal reports';
        $auth->add($categorythermalreportreport);


        // add "$categorythermalreportreport" permission
        $paymentplanreport = $auth->createPermission('reports/paymentplan');
        $paymentplanreport->description = 'payment wise reports';
        $auth->add($paymentplanreport);

        $inactivecustomerreport = $auth->createPermission('reports/inactivecustomer');
        $inactivecustomerreport->description = 'Inactive customer reports';
        $auth->add($inactivecustomerreport);



	}
	
	
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AuthItem::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AuthItem model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AuthItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AuthItem();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing AuthItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->name]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing AuthItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AuthItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return AuthItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AuthItem::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
