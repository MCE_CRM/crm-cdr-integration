<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\AgentSession */

$this->title = 'Create Agent Session';
$this->params['breadcrumbs'][] = ['label' => 'Agent Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agent-session-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
