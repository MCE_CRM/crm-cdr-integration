<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\SmsTemplate */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    /*#smstemplate-sms_status
    {
        display:none;
    }*/
    .field-smstemplate-sms_status
    {
        display: none;
    }
    .field-smstemplate-projectname
    {
        display: none;
    }
   
</style>
<div class="sms-template-form">

    <?php

    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'sms-template/validate':Yii::$app->homeUrl.'sms-template/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);


    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'message')->textarea(['maxlength' => true]) ?>

    <?= $form->field($model, 'active')->dropDownList(['1'=>'Active','0'=>'InActive', ]) ?>
    <?= $form->field($model, 'type')->dropDownList(['1'=>'Mnuall Sms','0'=>'Auto Sms']);?>
    
      <?php


                    // Normal select with ActiveForm & model
                    echo $form->field($model,'projectname')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Projects::find()->where(['active'=>1])->all(),'id','name'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Project ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);





                    ?>


    <?= $form->field($model, 'sms_status')->dropDownList(['incoming'=>'Incoming Call','outgoing'=>'Outgoing Call']) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success smssave']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script type="text/javascript">
    $(document).ready(function(){
        $("#smstemplate-type").change(function(){
           $value=$(this).val();
           if($value==0)
           {
            $(".field-smstemplate-sms_status").show();
            $(".field-smstemplate-projectname").show();
           }
           else
           {
            $(".field-smstemplate-sms_status").hide();
            $(".field-smstemplate-projectname").hide();
           }
        });
       

    });

 
</script>