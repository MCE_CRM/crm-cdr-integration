<?php

use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\leadsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = $model->contact_name;
$this->params['breadcrumbs'][] = $this->title;
/*echo"<pre>";
echo print_r($dataProvider2->models);
echo"</pre>";exit;*/

/*$roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
$userid=Yii::$app->user->id;

$status= \app\models\Leads::find()->select('lead_assign,created_by')->where(['id'=>$_GET['id']])->one();
  if($roles['Admin']->name=='Admin' || $roles['SuperAdmin']->name=='SuperAdmin')
        {


        }
        else
        {
  if($userid==$status->lead_assign || $userid==$status->created_by)
  {

  }
  else
  {
    return false;
  }
}*/
 

?>

<style>
.panel-custom .panel-heading {
    border-bottom: 2px solid #564aa3;
}

.panel-custom .panel-heading {
    margin-bottom: 10px;
}
.panel .panel-heading {
    border-bottom: 0;
    font-size: 14px;
}
.panel-heading {
    padding: 10px 15px;
    border-bottom: 1px solid transparent;
    border-top-right-radius: 3px;
    border-top-left-radius: 3px;
}

.tab-content>.active {
    display: block;
}
.navbar-custom-nav, .panel-custom {
    box-shadow: 0 3px 12px 0 rgba(0,0,0,.15);
}
.panel {
    margin-bottom: 21px;
    background-color: #fff;
    border: 1px solid transparent;
    -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
    box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel-custom .panel-heading {
    border-bottom: 2px solid #564aa3;
}
.panel-title {
    margin-top: 0;
    font-size: 16px;
}
.panel-title{ margin-bottom: 0;  color: inherit;
}
.ml-sm {
    margin-left: 5px!important;
}

.pull-right {
    float: right!important;
}
.panel .panel-heading a {
    text-decoration: none!important;
}
.panel-body {
    padding: 15px;
}
.small .timer, .task_details .form-group {
    margin-bottom: 0;
}
.form-control-static {
    padding-top: 7px;
    padding-bottom: 7px;
    margin-bottom: 0;
    min-height: 35px;
    line-height: 15px;
}
@media (min-width: 768px){
.form-horizontal .control-label {
    text-align: right;
    margin-bottom: 0;
    padding-top: 7px;
}
.col-sm-5 {
    width: 41.66666667%;
}
.col-sm-6 {
    width: 50%;
}
.col-sm-6{
    float: left;
}
}

* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 30%;
    height: 300px;
}

/* Style the buttons inside the tab */
.tab button {
    display: block;
    background-color: inherit;
    color: black;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    transition: 0.3s;
    font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
    background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    width: 70%;
    border-left: none;
    height: 300px;
}
</style>
<div class="leads-index">
    <div class="row">

       <!-- Left Sidebar-->
       <div class="col-md-2">
            
            
            <div class="list-group" id="list-tab" role="tablist">
                    <a href="#detail" data-toggle="list"  role="tab" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action active">
                        Detail
                    </a>

                <a href="#follow_up" data-toggle="list"  class="list-group-item d-flex justify-content-between align-items-center list-group-item-action"  role="tab">
                    Follow Up
                </a>
                    

                    <a href="#projects"   data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" role="tab" >
                    Projects/Property
                    
                    </a>
                    
                    <a href="#attachment"  data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action"  role="tab" >
                    Attachment
                    </a>
                    
                    <a href="#notes"  data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action"  role="tab" >
                    Notes
                    </a>

                <a href="#sms" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action"  role="tab" >
                    SMS
                </a>

               <!-- <a href="#email"  data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action"  role="tab" >
                    Email
                </a>-->
                    
                    <a href="#activities"  data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action"  role="tab" >
                    Activities                            

                    </a>
                    

                    

            </div>

        </div>
        <!-- Close Left sidebar-->
        <div class="col-md-10" style="padding: 0px">

        <div class="tab-content">
            <div class="tab-pane active" id="detail" style="position: relative; margin-bottom:30px">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <?php echo $model->contact_name;?>
                            <div class="pull-right ml-sm " style="margin-top: -6px">

                            </div>

                            <?php if(\Yii::$app->user->can('leads/update')){ ?>


                            <span class="btn-xs pull-right">
        <a href="<?= Yii::$app->homeUrl?>leads/update?id=<?= $_GET['id'];?>">Edit Leads</a>
                        </span>

                            <?php } ?>
                        </h3>
                    </div>


                    <div class="panel-body row form-horizontal task_details">
                        <div class="form-group col-sm-12">

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Contact Name :</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->contact_name;?></p></div>
                            </div>

                            <div class="col-sm-6">

                                <div class="control-label col-sm-5" style="float: left"><strong>Lead Status :</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="label label-info form-control-static"><?= $model->lead_status;?></p></div>
                            </div>

                        </div>


                        <div class="form-group col-sm-12">

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Phone : </strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->contact_phone;?></p></div>
                            </div>

                            <div class="col-sm-6">

                                <div class="control-label col-sm-5" style="float: left"><strong>Lead Purpose :</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="label label-info form-control-static"><?= $model->lead_type;?></p></div>
                            </div>


                        </div>


                        <div class="form-group col-sm-12">
                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Mobile :</strong> </div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->contact_mobile;?></p></div>
                            </div>
                            <div class="col-sm-6">

                                <div class="control-label col-sm-5" style="float: left"><strong>Lead Source :</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="label label-info form-control-static"><?= $model->lead_source;?></p></div>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">


                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Email : </strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->contact_email;?></p></div>
                            </div>

                            <div class="col-sm-6">

                                <div class="control-label col-sm-5" style="float: left"><strong>Lead Assigned :</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="label label-info form-control-static"><?= app\helpers\Helper::getAssignLeadUser($model->lead_assign);?></p></div>
                            </div>


                        </div>



                        <div class="form-group col-sm-12">
                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Address :</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->contact_address;?></p></div>
                            </div>

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Lead Registered :</strong> </div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= app\helpers\Helper::getCreated($model->created_by);?></p></div>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">


                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>City: </strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static">
                                        <?php $cityname=\app\models\City::find()->select('city')->where(['=','id',$model->contact_city])->one();
                                        echo $cityname->city;
                                        ?>
                                    </p></div>
                            </div>

                             <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Call Status: </strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static">
                                        <?php 
                                        echo $model->sms_status;
                                         
                                        ?>
                                    </p></div>
                            </div>
                        </div>

                        <div class="form-group col-sm-12">


                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Province/State: </strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static">
                                        <?php $country=\app\models\State::find()->select('state')->where(['=','id',$model->contact_state])->one();
                                        echo $country->state;
                                        ?>
                                    </p></div>
                            </div>

                        </div>



                        <div class="form-group col-sm-12">
                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Country: </strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static">
                                        <?php $country=\app\models\Country::find()->select('country')->where(['=','id',$model->contact_country])->one();
                                        echo $country->country;
                                        ?>
                                    </p></div>
                            </div>


                        </div>




                        <div class="form-group col-sm-12">
                <div  class="form-group  col-sm-6">


                                <div class="col-sm-7 ">
                                    <h4 class="form-control-static"><strong>Description</strong>
                                    </h4>

                                </div>
                            </div>
                        </div>


                        <div class="col-sm-12">
                            <blockquote style="font-size: 12px;">
                            <span class="feedText" id="ext-gen19" style="position: relative; color: rgb(22, 50, 92); font-family: sf-font-regular, &quot;Helvetica Neue&quot;, Helvetica, Arial, sans-serif; font-size: 13.008px;">
                                <div class="feedcommenttext" id="ext-gen18" style="overflow: hidden; margin-top: 5px; line-height: 20px; padding-top: 8px;">
                                    <?php echo $model->lead_description;?>
                                </div>


                            </span>
                            </blockquote>
                        </div>
                    </div>

                </div>
            </div>
            <div class="tab-pane" id="info" style="position: relative; margin-bottom:30px">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">Info
                            <div class="pull-right ml-sm " style="margin-top: -6px">
                                <a data-toggle="tooltip" data-placement="top" title="" href="" class="btn-xs btn btn-warning" data-original-title="Added into Pinned">
                                    <i class="fa fa-thumb-tack"></i>
                                </a>
                            </div>
                            <span class="btn-xs pull-right">
                                <a href="#">Edit Info</a>
                            </span>
                        </h3>
                    </div>


                    <div class="panel-body row form-horizontal task_details">
                        <div class="form-group col-sm-12">

                            <div class="col-sm-12">
                                <div class="control-label col-sm-4" style="float: left"><strong>Lead Description :</strong></div>
                                <div class="control-label col-sm-8" style="float: left"><p class="" style="text-align: center"><?= $model->lead_description;?></p></div>

                            </div>


                        </div>


                        <div class="form-group col-sm-12">

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Lead Status:</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->lead_status;?></p></div>
                            </div>

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Purpose :</strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->lead_type;?></p></div>
                            </div>

                        </div>


                        <div class="form-group col-sm-12">
                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Assigned:</strong></div>
                                <div class="control-label col-sm-5" style="float: left">
                                    <div class="pull-right">
                                        <p class="form-control-static"><span class="label label-warning"></span>
                                            <?php $leadasign=\app\models\User::find()->select('first_name')->where(['=','id',$model->lead_assign])->one();
                                            echo $leadasign->first_name;
                                            ?>
                                        </p>
                                    </div>
                                </div>


                            </div>


                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Lead Source: </strong></div>
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?= $model->lead_source;?></p></div>
                            </div>

                        </div>




                    </div>

                </div>
            </div>
            <div class="tab-pane" id="projects" style="position: relative; margin-bottom:30px">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">Project/Property
                            <div class="pull-right ml-sm " style="margin-top: -6px">

                            </div>

                        </h3>
                    </div>


                    <div class="panel-body row form-horizontal task_details">



                        <div class="form-group col-sm-12">

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Project:</strong></div>
                                <div class="control-label col-sm-5" style="float: left"></div>
                            </div>

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><p class="" style="text-align: center"><?php  $projectname= \app\models\Projects::find()->select('name')->where(['=','id',$model->project_id])->all();?>
                                        <?php echo $projectname[0]['name'];?>
                                    </p></div>
                            </div>

                        </div>

                        <div class="form-group col-sm-12">

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Block/Sector:</strong></div>
                                <div class="control-label col-sm-5" style="float: left"></div>
                            </div>

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><p class="" style="text-align: center"><?php  $block= \app\models\Blocksector::find()->select('blocksector')->where(['=','id',$model->block_sector])->all();?>
                                        <?php echo $block[0]['blocksector'];?>
                                    </p></div>
                            </div>

                        </div>


                        <div class="form-group col-sm-12">

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><strong>Property:</strong></div>
                               <!--  <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?php $propertytname= \app\models\Property::find()->select('property_title')->where(['=','id',$model->property_id])->all();
                                     //echo $propertytname[0]['property_title'];
                                ?>
                            </div> -->

                            <div class="col-sm-6">
                                <div class="control-label col-sm-5" style="float: left"><p class="form-control-static"><?php $propertytname= \app\models\Property::find()->select('property_title')->where(['=','id',$model->property_id])->all();?>
                                        <?php echo $propertytname[0]['property_title'];?></p>
                                </div>
                            </div>
                            </div>

                        </div>

                    </div>

                </div>
            </div>


            <div class="tab-pane" id="follow_up">
                <div class="card-body">
                    <div class="text-right">

                        <button type="button" onclick="gridShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-box"></i> Grid View</button>
                      <!-- <button type="button" onclick="calenderShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-calendar-alt"></i> Calender</button> -->
                      <button type="button" onclick="calenderShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-calendar-alt"></i> Follow Up Calender</button>
                    </div>


                    <br>


                    <div class="row" id="gridview-display">
                        <div class="col-md-12">




                            <?php


                            Pjax::begin(['timeout' => '30000']);

                            $create = '';
                            $ordering = '';
                            if(\Yii::$app->user->can('follow-up/create')){
                                $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Set New Follow Up'), [
                                    'follow-up/create?id='.$_GET['id'].''
                                ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                                    'class' => 'btn btn-default btn-sm add-new'
                                ]);
                            }


                            ?>


                            <?= GridView::widget([
                                'dataProvider' => $dataProvider2,
                                'responsiveWrap' => false,
                                'toolbar' =>  [
                                    //'{export}',
                                    '{toggleData}',
                                ],

                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                                    'before'=>$create.' '.$ordering.' ',
                                    'showFooter' => false,

                                ],
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    [
                                        'attribute'=>'follow_date_time',
                                        /*'format' => ['date', 'php:d/m/Y H:i a']*/
                                        'format'=>'raw',
                                          'value' => function($model){
                    if($model->follow_date_time)
                    {
                        return date("d/m/Y h:i A", strtotime($model->follow_date_time));

                    }else
                    {
                        return '';
                    }
                },


                                    ],
                                    'note',
                                    [
                                        'class' => 'kartik\grid\EditableColumn',
                                        'attribute' => 'status',
                                        'label'=>'Status',
                                        'filter'=>false,
                                        'options' => ['style' => 'width: 15%'],

                                        'editableOptions'=> function ($model, $key, $index, $widget) {
                                            $appttypes = ArrayHelper::map(\app\models\FollowUpStatus::find()->orderBy('sort_order')->asArray()->all(), 'status', 'status');
                                            return [
                                                'header' => 'Status',
                                                'attribute' => 'status',
                                                'asPopover' => false,
                                                'inlineSettings' => [
                                                    'closeButton' => '<button class="btn btn-sm btn-danger kv-editable-close kv-editable-submit" title="Cancel Edit"><i class="fa fa-times-circle"></i></button>'
                                                ],

                                                'type' => 'primary',
                                                //'size'=> 'lg',
                                                'size' => 'sm',
                                                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                                'displayValueConfig' => $appttypes,
                                                'data' => $appttypes,
                                                'formOptions'=> ['action' => ['/ajax/update-follow-up-status?leadid='.$_GET['id']]] // point to new action
                                            ];
                                        },

                                    ],
                                    //'created_by',
                                    //'updated_on',
                                    //'updated_by',

                                    [
                                        'class' => '\kartik\grid\ActionColumn',
                                        'template'=>'{update} {delete}',
                                        'buttons' => [
                                            'delete' => function ($url, $model, $key) {
                                                if (\Yii::$app->user->can('follow-up/delete')) {
                                                    return Html::a('<span class="fas fa-trash " onClick="deletefollowup('.$model->id.')"></span>'
                                                    
                                                       /* [
                                                            'title' => Yii::t('app', 'Delete'),
                                                            'data-pjax' => '1',
                                                            'data' => [
                                                                'method' => 'post',
                                                                'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                                                'pjax' => 1,],
                                                        ]*/
                                                    );
                                                }
                                            },
                                            'update' => function ($url, $model)

                                            {
                                                if(\Yii::$app->user->can('follow-up/update')){
                                                    return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'follow-up\',\'Update Follow Up\',event)"></span></a>';

                                                }else
                                                {
                                                    return '';
                                                }
                                            } ,

                                        ],
                                    ],
                                ],
                            ]); ?>
                            <?php Pjax::end(); ?>


                        </div>


                    </div>

                    <div class="row" id="calender-display" style="display: none;">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">

                            <div id="calendar"></div>

                        </div>
                        <div class="col-md-1"></div>


                    </div>





                </div>

            </div>


            <div id="attachment" class="tab-pane" style="margin-bottom:25px">

                <div class="card-body">
                    <div class="text-right">

                        <button type="button"  class="mb-1 mt-1 mr-1 btn btn-default attach-files"><i class="fas fa-attachment"></i> Add New File</button>
                        <!--                        <button type="button" onclick="calenderShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-calendar-alt"></i> Calender</button>
                        -->                    </div>


                    <br>


                <div class="row">

                    <div class="col-md-12">
                        <div class="files-index">
                            <?php Pjax::begin(['id' => 'filesPjax']); ?>

                            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                //'filterModel' => $searchModel,
                                'responsiveWrap' => false,
                                //'filter'=>false,
                                'toolbar' =>  [
                                    //'{export}',
                                    //'{toggleData}',
                                ],

                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'Attachment' ).' </h5>',
                                    'showFooter' => false,

                                ],
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    // 'id',
                                    'file_name',
                                    'custom_name',
                                    'type',
                                    //'size',
                                    [
                                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                        'contentOptions' => ['class' => 'text-center'],
                                        'attribute' =>  'size',
                                        'value' => function($model){
                                            return \app\helpers\Helper::getSize($model->size);
                                        },
                                        //'filter'=>false

                                    ],

                                    //'project_id',
                                    //'inventory_id',
                                    [
                                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                        'contentOptions' => ['class' => 'text-center'],
                                        'attribute' =>  'created_on',
                                        'value' => function($model){
                                            return date("d/m/Y", strtotime($model->created_on));
                                        },
                                        //'filter'=>false

                                    ],
                                    [
                                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                        'attribute' => 'created_by',
                                        'contentOptions' => ['class' => 'text-center'],
                                        'value'=>'user.username',
                                        'format'=>'raw',

                                    ],
                                    //'updated_on',
                                    //'updated_by',
                                    //'custom_name',
                                    //'type',
                                    //'size',

                                    [
                                        'class' => '\kartik\grid\ActionColumn',
                                        'template'=>'{play}{update}{download}{delete}',
                                        'buttons' => [
                                            'play' => function ($url, $model)

                                            {
                                                return '<a href="'.\app\helpers\Helper::getBaseUrl().'files/lead'.$model->lead_id.'/'.$model->file_name.'" class="html5lightbox"><span class="fas fa-play"></span></a>'.'&nbsp;';
                                            } ,
                                            'update' => function ($url, $model)

                                            {
                                                return '<a href="javascript:void(0)"><span class="fa fa-pencil-alt"  onclick="editLeadFile('.$model->id.',\''.$model->custom_name.'\')"></span></a>'.'&nbsp;';
                                            } ,
                                            'download' => function ($url, $model)

                                            {
                                                return '<a href="'.\app\helpers\Helper::getBaseUrl().'files/lead'.$model->lead_id.'/'.$model->file_name.'"  data-pjax =0 download><span class="fas fa-download" ></span></a>'.'&nbsp;';
                                            } ,
                                            'delete' => function ($url, $model) {
                                                return '<a href="javascript:void(0)"><span class="far fa-trash-alt"  onclick="delLeadFile('.$model->id.')"></span></a>'.'&nbsp;';

                                            }

                                        ],
                                    ],
                                ],
                            ]); ?>

                            <?php Pjax::end(); ?>

                        </div>
                    </div>


                </div>

                </div>

            </div>

            <div class="tab-pane" id="notes" style="position: relative; margin-bottom:30px">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">Notes

<span class="btn-xs pull-right">
                    <a href="javascript:void(0)" class="notes" ><i class="fas fa-comment"></i> New Note</a>

                        </span>
                        </h3>
                    </div>


                    <div class="panel-body row form-horizontal task_details">

                        <div id="notes" class="tab-pane active show">
                            <div id="LoadingOverlayApi" data-loading-overlay="">
                                <div class="timeline timeline-simple mt-3 mb-3">
                                    <div class="tm-body">
                                        <div class="tm-title">
                                            <h5 class="m-0 pt-2 pb-2 text-uppercase">Notes</h5>
                                        </div>
                                        <ol class="tm-items">

                                        </ol>
                                    </div>
                                </div>

                            </div>

                        </div>




                    </div>

                </div>
            </div>


            <div id="sms" class="tab-pane" style="margin-bottom:25px">

                <div class="card-body">
                    <div class="text-right">

                        <button type="button" onclick="smsSendShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-mobile"></i> Send New SMS</button>
                        <button type="button" onclick="smsHistoryShow(this)" class="mb-1 mt-1 mr-1 btn btn-default"><i class="fas fa-calendar-alt"></i> SMS History</button>
                    </div>


                    <br>


                    <div class="row">

                        <div class="col-md-12">
                            <div class="row" id="sendSMSShow">
                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <form class="form-horizontal" id="custom_sms"  action="<?php echo Yii::$app->homeUrl?>ajax/sms-send" method="post">
                                        <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                        <input type="hidden" name="lead_id" value="<?= $_GET['id'];?>" />

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="template">Template:</label>
                                            <div class="col-sm-10">
                                                <select id="inputState" onchange="changeFunc(value);" class="form-control">
                                                    <option selected>Select Template</option>
                                                    <?php
                                                    $getTemplateName = \app\models\SmsTemplate::find()->select('id,name')->where(['type'=>1])->all();
                                                    foreach ($getTemplateName as $template_name)
                                                    {?>
                                                        <option value="<?php echo $template_name->id?>"><?php echo $template_name->name?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="pwd">Message:</label>
                                            <div class="col-sm-10">
                                                <textarea class="form-control" rows="5" name="message" id="commentt" onkeyup="countChar(this)" required></textarea>
                                                <em>Character Count : <span id="count">0</span></em>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-sm-2" for="number">Mobiles:</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="number" name="number" placeholder="Multiple Numbers Seprated By Comma's" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <button type="submit" id="smsButton" class="btn btn-success">Submit</button>
                                            </div>
                                        </div>
                                    </form>


                                </div>
                                <div class="col-md-3"></div>

                            </div>
                            <div class="files-index" id="smsHistory" style="display:none">
                                <?php Pjax::begin(['id' => 'smsPjax']); ?>

                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                                <?= GridView::widget([
                                    'dataProvider' => $dataProvider3,
                                    //'filterModel' => $searchModel,
                                    'responsiveWrap' => false,
                                    //'filter'=>false,
                                    'toolbar' =>  [
                                        //'{export}',
                                        //'{toggleData}',
                                    ],

                                    'panel' => [
                                        'type' => GridView::TYPE_PRIMARY,
                                        'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'SMS History' ).' </h5>',
                                        'showFooter' => false,

                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        //'id',
                                        'number',
                                        'message',
                                        [

                                            'attribute' =>  'created_on',
                                            'value' => function($model){
                                                return date("d/m/Y", strtotime($model->created_on));
                                            },

                                            //'filter'=>false

                                        ],
                                        //'created_by',
                                        //'updated_on',
                                        //'updated_by',
                                        //'message',
                                        //'lead_id',

                                        ['class' => 'yii\grid\ActionColumn'],
                                    ],
                                ]); ?>

                                <?php Pjax::end(); ?>

                            </div>
                        </div>


                    </div>

                </div>

            </div>

            <div class="tab-pane" id="activities" style="position: relative; margin-bottom:30px">
                <div class="panel panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">Activities
                            <div class="pull-right ml-sm " style="margin-top: -6px">
                                <a data-toggle="tooltip" data-placement="top" title="" href="" class="btn-xs btn btn-warning" data-original-title="Added into Pinned">
                                    <i class="fa fa-thumb-tack"></i>
                                </a>
                            </div>

                        </h3>
                    </div>


                    <div class="panel-body row form-horizontal task_details">
                        <div class="col-md-12">
                            <div class="files-index">

                                <?php Pjax::begin(['id' => 'activitiesPjax']);
                                ?>
                                <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                                <?= GridView::widget([
                                    'dataProvider' => $dataProviderActivity,
                                    //'filterModel' => $searchModel,
                                    'responsiveWrap' => false,
                                    //'filter'=>false,
                                    'toolbar' =>  [
                                        //'{export}',
                                        //'{toggleData}',
                                    ],

                                    'panel' => [
                                        'type' => GridView::TYPE_PRIMARY,
                                        'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                                        'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                                                'index'
                                            ], [
                                                'class' => 'btn btn-primary btn-sm'
                                            ]),


                                        'showFooter' => false,

                                    ],
                                    'columns' => [
                                        ['class' => 'yii\grid\SerialColumn'],

                                        // 'id',
                                        'activity',
                                        [
                                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                            'contentOptions' => ['class' => 'text-center'],
                                            'attribute' =>  'created_on',
                                            'value' => function($model){
                                                return date("d/m/Y", strtotime($model->created_on));
                                            },
                                            //'filter'=>false

                                        ],
                                        [
                                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                            'attribute' => 'created_by',
                                            'contentOptions' => ['class' => 'text-center'],
                                            'value'=>'user.username',
                                            'format'=>'raw',

                                        ],
                                        //'updated_on',
                                        //'updated_by',
                                        //'custom_name',
                                        //'type',
                                        //'size',

                                    ],
                                ]); ?>
                                <?php Pjax::end(); ?>
                            </div>
                        </div>
                    </div>

                    <!-- Close Project Section -->



                    <!--close Right Side-->


                </div>







            </div>
        </div>
        
        </div>
        



        <!-- get follow up date and note and show in calender events -->
        <?php
        $id=$_GET['id'];
        $user=\app\models\User::find()->where(['id'=>$model->lead_assign])->one();
        $username=$user->first_name." ".$user->last_name;
        $stringdata="LeadId: ".$model->id." Lead Assign: ".$username." Lead Status: ".$model->lead_status."";
        foreach($dataProvider2->models as $value)
            {
                $date= $value->follow_date_time;
                $note=$value->note;
                $newDate = date("Y-m-d", strtotime($date));
                $data[]=[
                    'title'=>''.$note.'',
                    'start'=>$newDate,
                    'description'=>''.$stringdata.''

                ];
            }
           
           ?>

        <Script>
            $( ".attach-files" ).on( "click", function(event) {

                event.preventDefault();
                var url = '<?= Yii::$app->homeUrl?>ajax/attach-files?id=<?= $model->id?>';
                var dialog = bootbox.dialog({
                    title: 'Attach Files',
                    message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
                    size: 'large',
                    onEscape: function() {
                        //$.pjax.reload({container: '#filesPjax', async: false});
                        location.reload();
                    }

                });

                dialog.init(function(){
                    var request = $.ajax({
                        url: url,
                        method: "GET",
                    });

                    request.done(function( msg ) {
                        dialog.find('.bootbox-body').html(msg);
                    });



                });


            });


            function updateComment(id,event) {


                var text = $('#comment'+id+'').text();

                event.preventDefault();
                var html  = '<section class="simple-compose-box mb-3">'+
                    '<form method="get" id="update-note_form" action="<?= Yii::$app->homeUrl?>ajax/update-note?id='+id+'">'+
                    '<textarea name="message-text" data-plugin-textarea-autosize  placeholder="Whats on your mind?" rows="3">'+text+'</textarea>'+
                    '<div class="compose-box-footer">'+
                    '<ul class="compose-btn">'+
                    '<li>'+
                    '<button type="submit" class="btn btn-primary btn-xs">Post</button>'+
                    '</li>'+
                    '</ul>'+
                    '</div>'+
                    '</form>'+
                    '</section>';



                var dialog = bootbox.dialog({
                    title: 'Update Note',
                    message: html,
                });
                dialog.init(function(){

                    $(document).on("submit", "#update-note_form", function (event) {
                        event.preventDefault();
                        event.stopImmediatePropagation();

                        $form = $(this); //wrap this in jQuery

                        var url = '<?= Yii::$app->homeUrl?>ajax/update-note?id='+id+'';

                        $.ajax({
                            type: "GET",
                            url: url,
                            data: $("#update-note_form").serialize(),
                            // serializes the form's elements.
                            success: function(data)
                            {

                                if(data==true)
                                {
                                    reloadComment();
                                    bootbox.hideAll();
                                }
                                else
                                {
                                    //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                                }


                            }
                        });
                        // avoid to execute the actual submit of the form.
                    });

                });



            }

            function reloadComment() {

                var $el = $('#LoadingOverlayApi');
                $el.trigger('loading-overlay:show');

                var url = '<?= Yii::$app->homeUrl?>ajax/get-note?id=<?= $model->id?>';

                $.ajax({
                    url: url,
                    // serializes the form's elements.
                    success: function(data)
                    {
                        $('.tm-items').html('');
                        $('.tm-items').html(data);
                        //$el.trigger('loading-overlay:hide');
                    }
                });



                $.pjax.reload({container: '#activitiesPjax', async: false});

            }

            function deleteComment(id,event) {

                var url = '<?= Yii::$app->homeUrl?>ajax/delete-note?id='+id+'';


                bootbox.confirm({
                    message: "Are You Sure Delete This?",
                    buttons: {
                        confirm: {
                            label: 'OK',
                            className: 'btn btn-primary'
                        },
                        cancel: {
                            label: 'Cancel',
                            className: 'btn btn-default'
                        }
                    },
                    callback: function (result) {
                        if(result==true)
                        {
                            $.ajax({
                                url: url,
                                success: function(result){

                                    if(result==true)
                                    {
                                        reloadComment();
                                    }

                                }});


                        }
                    }
                });





            }

            function editLeadFile(id,custom) {

                bootbox.prompt("Update", function(result){

                    console.log(result);

                    if(result === null)
                    {
                        console.log(result);

                    }else {

                        $.ajax({
                            url: baseUrl+"files/update",
                            type: "POST",
                            data: { name: result,id:id},
                            success: function(result){

                                if(result==true)
                                {
                                    bootbox.hideAll();
                                    $.pjax.reload({container: '#filesPjax', async: false});
                                }

                            }});
                    }

                });

                $('.bootbox-input').val(custom);


            }

            function delLeadFile(id) {

                bootbox.confirm({
                    message: "Are You Sure Delete This?",
                    buttons: {
                        confirm: {
                            label: 'OK',
                            className: 'btn btn-primary'
                        },
                        cancel: {
                            label: 'Cancel',
                            className: 'btn btn-default'
                        }
                    },
                    callback: function (result) {
                        if(result==true)
                        {
                            $.ajax({
                                url: "<?= Yii::$app->homeUrl?>files/delete",
                                type: "POST",
                                data: {id:id},
                                success: function(result){

                                    if(result==true)
                                    {
                                        bootbox.hideAll();
                                        $.pjax.reload({container: '#filesPjax', async: false});
                                    }

                                }});


                        }
                    }
                });


            }

            $( ".notes" ).on( "click", function() {


                var html  = '<section class="simple-compose-box mb-3">'+
                    '<form method="get" id="note_form" action="<?= Yii::$app->homeUrl?>ajax/submit-note?id=<?= $model->id?>">'+
                    '<textarea name="message-text" data-plugin-textarea-autosize placeholder="Whats on your mind?" rows="3"></textarea>'+
                    '<div class="compose-box-footer">'+
                    '<ul class="compose-btn">'+
                    '<li>'+
                    '<button type="submit" class="btn btn-primary btn-xs">Post</button>'+
                    '</li>'+
                    '</ul>'+
                    '</div>'+
                    '</form>'+
                    '</section>';



                var dialog = bootbox.dialog({
                    title: 'Add New Notes',
                    message: html,
                });
                dialog.init(function(){

                    $(document).on("submit", "#note_form", function (event) {
                        event.preventDefault();
                        event.stopImmediatePropagation();

                        $form = $(this); //wrap this in jQuery

                        var url = '<?= Yii::$app->homeUrl?>ajax/submit-note?id=<?= $model->id?>';

                        $.ajax({
                            type: "GET",
                            url: url,
                            data: $("#note_form").serialize(),
                            // serializes the form's elements.
                            success: function(data)
                            {

                                if(data==true)
                                {
                                    reloadComment();
                                    bootbox.hideAll();
                                }
                                else
                                {
                                    //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                                }


                            }
                        });
                        // avoid to execute the actual submit of the form.
                    });

                });



            });


            $( document ).ready(function() {
                reloadComment();
            });



            var getUrl = window.location;
            var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";
            function countChar(val) {
                var len = val.value.length;
                $('#count').text(len);
            }


            function changeFunc($i) {
                var id = $i;
                $.ajax({
                    type: "POST",
                    url: baseUrl+"/ajax/get-template?id="+id,
                    success: function(data)
                    {
                        $('#commentt').val(data);
                        var len = data.length;
                        $('#count').text(len);


                    }
                });
            }


            $("#custom_sms").submit(function(e) {

                $("#smsButton").addClass('disabled');

                $form = $(this);

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#custom_sms").serialize(), // serializes the form's elements.
                    success: function(data)
                    {
                        bootbox.confirm({
                            message: data,
                            buttons: {
                                confirm: {
                                    label: 'OK',
                                    className: 'btn-success'
                                },
                            },
                            callback: function (result) {
                                if(result==true)
                                {
                                    location.reload();
                                }
                                else
                                {
                                    $("#smsButton").removeClass('disabled');
                                    $('#number').val('');
                                }
                            }
                        });

                    }
                });

                e.preventDefault(); // avoid to execute the actual submit of the form.
            });


            function smsSendShow(event) {
                $('#sendSMSShow').show();
                $('#smsHistory').hide();

            }

            function smsHistoryShow(event) {
                $('#smsHistory').show();
                $('#sendSMSShow').hide();
            }
           

           function calenderShow(event) {
        $('#gridview-display').hide();
        $('#calender-display').show();
        showcalender();
        }
          

        function gridShow(event)
        {
             $('#gridview-display').show();
             $('#calender-display').hide();

        }
         
  function showcalender(){
    (function($) {

        'use strict';

        var initCalendarDragNDrop = function() {
            $('#external-events div.external-event').each(function() {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        };

        var initCalendar = function() {
            var $calendar = $('#calendar');
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            $calendar.fullCalendar({
                header: {
                    left: 'title',
                    right: 'prev,today,next,basicDay,basicWeek,month'
                },
                                timeFormat: 'h:mm',

                themeButtonIcons: {
                    prev: 'fas fa-caret-left',
                    next: 'fas fa-caret-right',
                },

                editable: true,
                droppable: true,


                 // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay) { // this function is called when something is dropped
                    var $externalEvent = $(this);
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $externalEvent.data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $externalEvent.attr('data-event-class');

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);


                    // is the "remove after drop" checkbox checked?
                    if ($('#RemoveAfterDrop').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                },
                events:<?php echo json_encode($data);?>,
                eventClick: function(event) {
                       if (event.url) {
                          window.open(event.url, "_blank");
                           return false;
                          }
                       },
                eventRender: function(event, element) {
                    $(element).tooltip({title: event.description});
                }
                
            });

            // FIX INPUTS TO BOOTSTRAP VERSIONS
            var $calendarButtons = $calendar.find('.fc-header-right > span');
            $calendarButtons
                .filter('.fc-button-prev, .fc-button-today, .fc-button-next')
                .wrapAll('<div class="btn-group mt-sm mr-md mb-sm ml-sm"></div>')
                .parent()
                .after('<br class="hidden"/>');

            $calendarButtons
                .not('.fc-button-prev, .fc-button-today, .fc-button-next')
                .wrapAll('<div class="btn-group mb-sm mt-sm"></div>');

            $calendarButtons
                .attr({ 'class': 'btn btn-sm btn-default' });
        };

        $(function() {
            initCalendar();
            initCalendarDragNDrop();
        });

    }).apply(this, [jQuery]);

}
    </Script>
    <script type="text/javascript">
        function deletefollowup(id)
        {
           $.ajax({
            url:"<?php echo Yii::$app->homeUrl;?>ajax/deletefollowup",
            method:"get",
            data:{id,id},
            success:function(res){
                location.reload();
            }
           });
        }
    </script>