<?php

namespace app\modules\cdr\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cdr\models\Cdr;
use DateTime;

/**
 * CdrSearch represents the model behind the search form of `app\models\Cdr`.
 */
class CdrSearch extends Cdr
{

    public $not;
  	public $data;
	public $lastapp;
    public $iorecording;
    public $from;
    public $to;
  	public $durationfrom,$durationto;
   	public $check_3;
   	public $total;
	public $agent_id;
	public $channelsearch ;
    /**
     * {@inheritdoc}
     */


    public function rules()
    {
        return [
            [['id', 'duration', 'billsec', 'amaflags'], 'integer'],
            [['calldate', 'clid', 'src', 'dst', 'dcontext', 'channel', 'dstchannel', 'lastapp', 'lastdata', 'disposition', 'accountcode', 'userfield', 'uniqueid','iorecording'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
	
    public function search($params)
    {

           /* echo '<pre>';
            echo print_r($params);echo '</pre>';
            exit;*/

            if($params['CdrSearch']['iorecording'] == 'Incoming'){
                $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['NOT LIKE','iorecording', 'NULL'])->andWhere(['>','duration', '3'])->andWhere(['LIKE','lastapp', 'Queue'])->orderBy(['id' => SORT_DESC]);

            }else{
                $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['NOT LIKE','iorecording', 'NULL'])->andWhere(['>','duration', '3'])->orderBy(['id' => SORT_DESC]);

            }

			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']!='10' && $_GET['CdrSearch']['channelsearch']!='all')
			{
				$agent_id = $_GET['CdrSearch']['channelsearch'];
				$agent_id = "SIP/".$agent_id;
				$query->andWhere(
					['or',
						['like', 'channel',  $agent_id],
						['like', 'dstchannel',  $agent_id]
			
					]
				)->andWhere(
					['or',
						['Not like', 'dst', 'for'],
						['Not like', 'dst', 'o']
			
					]
				)->andWhere(['!=','dstchannel',''])->all();
				
			}
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='10')
			{
				$query->andWhere(
					['or',
						['like', 'dst', 'for'],
						['like', 'dst', 'o']
			
					]
				)->andWhere(['>','duration', '4'])->andWhere(['=','dstchannel',''])->all();
			}
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='11')
			{
				
				$query->andWhere(['LIKE','dst', 'o'])->andWhere(['=','dstchannel',''])->andWhere(['<=','duration', '4'])->all();
			}
			
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='all')
			{
				
				$query->andWhere(1)->all();
			}
			
		$dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $this->load($params);
         if (!$this->validate()) {
         return $dataProvider;

       }

        $query->andFilterWhere([
            'id' => $this->id,
           // 'calldate' => $this->calldate,
            'duration' => $this->duration,
            'billsec' => $this->billsec,
            'amaflags' => $this->amaflags,
            'lastapp'=>$this->lastapp,
        ]);

        if ( ! is_null($this->calldate) && strpos($this->calldate, ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$this->calldate);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'calldate', $start_date, $end_date]);

        }


        $query->andFilterWhere(['like', 'clid', $this->clid])
            ->andFilterWhere(['like', 'src', $this->src .'%',false])
            ->andFilterWhere(['like', 'dst', $this->dst])
            ->andFilterWhere(['like', 'dcontext', $this->dcontext])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'dstchannel', $this->dstchannel])
            ->andFilterWhere(['like', 'lastapp', $this->lastapp])
            ->andFilterWhere(['like', 'lastdata', $this->lastdata])
            ->andFilterWhere(['like', 'disposition', $this->disposition])
            ->andFilterWhere(['like', 'accountcode', $this->accountcode])
            ->andFilterWhere(['like', 'userfield', $this->userfield])
            ->andFilterWhere(['like', 'uniqueid', $this->uniqueid])
            ->andFilterWhere(['like', 'iorecording', $this->iorecording])
            ->andFilterWhere(['between', 'cdr.calldate', $start_date, $end_date]);
           // echo $query->createCommand()->getRawSql();exit;
			return $dataProvider;
    }
	
	

    public function attendedreportsearch($params)
    {


        $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['LIKE','disposition', 'ANSWERED'])->andWhere(['>','duration', '3'])->orderBy(['id' => SORT_DESC]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'calldate' => $this->calldate,
            'duration' => $this->duration,
            'billsec' => $this->billsec,
            'amaflags' => $this->amaflags,
            'lastapp'=>$this->lastapp,
        ]);


        if ( ! is_null($this->calldate) && strpos($this->calldate, ' - ') !== false ) {


            list($start_date, $end_date) = explode(' - ',$this->calldate);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'calldate', $start_date, $end_date]);

        }


        $query->andFilterWhere(['like', 'clid', $this->clid])
            ->andFilterWhere(['like', 'src', $this->src .'%',false])      
            ->andFilterWhere(['like', 'dst', $this->dst])
            ->andFilterWhere(['like', 'dcontext', $this->dcontext])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'dstchannel', $this->dstchannel])
            ->andFilterWhere(['like', 'lastapp', $this->lastapp])
            ->andFilterWhere(['like', 'lastdata', $this->lastdata])
            ->andFilterWhere(['like', 'disposition', $this->disposition])
            ->andFilterWhere(['like', 'accountcode', $this->accountcode])
            ->andFilterWhere(['like', 'userfield', $this->userfield])
            ->andFilterWhere(['like', 'uniqueid', $this->uniqueid])
            ->andFilterWhere(['between', 'cdr.calldate', $start_date, $end_date]);
        return $dataProvider;

    }
	
	public function allagentreportsearch($params)
    {


        $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->orderBy(['id' => SORT_DESC]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'calldate' => $this->calldate,
            'duration' => $this->duration,
            'billsec' => $this->billsec,
            'amaflags' => $this->amaflags,
            'lastapp'=>$this->lastapp,
        ]);


        if ( ! is_null($this->calldate) && strpos($this->calldate, ' - ') !== false ) {


            list($start_date, $end_date) = explode(' - ',$this->calldate);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'calldate', $start_date, $end_date]);

        }


        $query->andFilterWhere(['like', 'clid', $this->clid])
        ->andFilterWhere(['like', 'src', $this->src .'%',false])
            ->andFilterWhere(['like', 'dst', $this->dst])
            ->andFilterWhere(['like', 'dcontext', $this->dcontext])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'dstchannel', $this->dstchannel])
            ->andFilterWhere(['like', 'lastapp', $this->lastapp])
            ->andFilterWhere(['like', 'lastdata', $this->lastdata])
            ->andFilterWhere(['like', 'disposition', $this->disposition])
            ->andFilterWhere(['like', 'accountcode', $this->accountcode])
            ->andFilterWhere(['like', 'userfield', $this->userfield])
            ->andFilterWhere(['like', 'uniqueid', $this->uniqueid])
            ->andFilterWhere(['between', 'cdr.calldate', $start_date, $end_date]);
        return $dataProvider;

    }
	

    public function disconnectedreportsearch($params)
    {

        $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['NOT LIKE','disposition', 'ANSWERED'])->andWhere(['>','duration', '3'])->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $this->load($params);


        if (!$this->validate()) {
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'calldate' => $this->calldate,
            'duration' => $this->duration,
            'billsec' => $this->billsec,
            'amaflags' => $this->amaflags,
            'lastapp'=>$this->lastapp,
        ]);

        if ( ! is_null($this->calldate) && strpos($this->calldate, ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$this->calldate);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');
            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'calldate', $start_date, $end_date]);
        }


        $query->andFilterWhere(['like', 'clid', $this->clid])
        ->andFilterWhere(['like', 'src', $this->src .'%',false])
            ->andFilterWhere(['like', 'dst', $this->dst])
            ->andFilterWhere(['like', 'dcontext', $this->dcontext])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'dstchannel', $this->dstchannel])
            ->andFilterWhere(['like', 'lastapp', $this->lastapp])
            ->andFilterWhere(['like', 'lastdata', $this->lastdata])
            ->andFilterWhere(['like', 'disposition', $this->disposition])
            ->andFilterWhere(['like', 'accountcode', $this->accountcode])
            ->andFilterWhere(['like', 'userfield', $this->userfield])
            ->andFilterWhere(['like', 'uniqueid', $this->uniqueid])
            ->andFilterWhere(['between', 'cdr.calldate', $start_date, $end_date]);
        return $dataProvider;

    }

    public function transferreportsearch($params)
    {
        $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['>','duration', '3'])->andWhere(['like','internalcall','Tranfered-call-from'])->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([

            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],

        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'calldate' => $this->calldate,
            'duration' => $this->duration,
            'billsec' => $this->billsec,
            'amaflags' => $this->amaflags,
            'lastapp'=>$this->lastapp,
        ]);

        if ( ! is_null($this->calldate) && strpos($this->calldate, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ',$this->calldate);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');
            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'calldate', $start_date, $end_date]);
        }


        $query->andFilterWhere(['like', 'clid', $this->clid])
        ->andFilterWhere(['like', 'src', $this->src .'%',false])
            ->andFilterWhere(['like', 'dst', $this->dst])
            ->andFilterWhere(['like', 'dcontext', $this->dcontext])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'dstchannel', $this->dstchannel])
            ->andFilterWhere(['like', 'lastapp', $this->lastapp])
            ->andFilterWhere(['like', 'lastdata', $this->lastdata])
            ->andFilterWhere(['like', 'disposition', $this->disposition])
            ->andFilterWhere(['like', 'accountcode', $this->accountcode])
            ->andFilterWhere(['like', 'userfield', $this->userfield])
            ->andFilterWhere(['like', 'uniqueid', $this->uniqueid])
            ->andFilterWhere(['between', 'cdr.calldate', $start_date, $end_date]);

        return $dataProvider;
    }

    public function incomingsearch($params)
    {

        $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['LIKE','iorecording', 'incoming'])->andWhere(['>','duration', '3'])->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],

        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;

        }
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']!='10' && $_GET['CdrSearch']['channelsearch']!='all')
			{
				$agent_id = $_GET['CdrSearch']['channelsearch'];
				$agent_id = "SIP/".$agent_id;
				$query->andWhere(
					['or',
						['like', 'channel', $agent_id],
						['like', 'dstchannel', $agent_id]
			
					]
				)->all();
				
			}
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='10')
			{
				$query->andWhere(
					['or',
						['like', 'dst', 'for'],
						['like', 'dst', 'o']
			
					]
				)->andWhere(['>','duration', '4'])->andWhere(['=','dstchannel',''])->all();
			}
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='11')
			{
				
				$query->andWhere(['LIKE','dst', 'o'])->andWhere(['=','dstchannel',''])->andWhere(['<=','duration', '4'])->all();
			}
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='all')
			{
				
				$query->andWhere(1)->all();
			}
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'calldate' => $this->calldate,
            'duration' => $this->duration,
            'billsec' => $this->billsec,
            'amaflags' => $this->amaflags,
            'lastapp'=>$this->lastapp,
        ]);

        if ( ! is_null($this->calldate) && strpos($this->calldate, ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$this->calldate);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');
            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'calldate', $start_date, $end_date]);

        }

        $query->andFilterWhere(['like', 'clid', $this->clid])
        ->andFilterWhere(['like', 'src', $this->src .'%',false])
            ->andFilterWhere(['like', 'dst', $this->dst])
            ->andFilterWhere(['like', 'dcontext', $this->dcontext])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'dstchannel', $this->dstchannel])
            ->andFilterWhere(['like', 'lastapp', $this->lastapp])
            ->andFilterWhere(['like', 'lastdata', $this->lastdata])
            ->andFilterWhere(['like', 'disposition', $this->disposition])
            ->andFilterWhere(['like', 'accountcode', $this->accountcode])
            ->andFilterWhere(['like', 'userfield', $this->userfield])
            ->andFilterWhere(['like', 'uniqueid', $this->uniqueid])
            ->andFilterWhere(['between', 'cdr.calldate', $start_date, $end_date]);
        return $dataProvider;

    }

    public function outgoingsearch($params)
    {
        $query = Cdr::find()->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['like','iorecording','Outgoing'])->andWhere(['>','duration', '3'])->orderBy(['id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [ 'pageSize' => 20 ],
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }
        else{
            $this->load($params);
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            // 'calldate' => $this->calldate,
            'duration' => $this->duration,
            'billsec' => $this->billsec,
            'amaflags' => $this->amaflags,
            'lastapp'=>$this->lastapp,
        ]);


        if( ! is_null($this->calldate) && strpos($this->calldate, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ',$this->calldate);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');
            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'calldate', $start_date, $end_date]);
        }

		if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']!='10' && $_GET['CdrSearch']['channelsearch']!='all')
			{
				$agent_id = $_GET['CdrSearch']['channelsearch'];
				$agent_id = "SIP/".$agent_id;
				$query->andWhere(
					['or',
						['like', 'channel', $agent_id],
						['like', 'dstchannel', $agent_id]
			
					]
				)->all();
				
			}
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='10')
			{
				$query->andWhere(
					['or',
						['like', 'dst', 'for'],
						['like', 'dst', 'o']
			
					]
				)->andWhere(['>','duration', '4'])->andWhere(['=','dstchannel',''])->all();
			}
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='11')
			{
				
				$query->andWhere(['LIKE','dst', 'o'])->andWhere(['=','dstchannel',''])->andWhere(['<=','duration', '4'])->all();
			}
        	if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']) && $_GET['CdrSearch']['channelsearch']=='all')
			{
				
				$query->andWhere(1)->all();
			}
		$query->andFilterWhere(['like', 'clid', $this->clid])
            ->andFilterWhere(['like', 'src', $this->src .'%',false])
            ->andFilterWhere(['like', 'dst', $this->dst])
            ->andFilterWhere(['like', 'dcontext', $this->dcontext])
            ->andFilterWhere(['like', 'channel', $this->channel])
            ->andFilterWhere(['like', 'dstchannel', $this->dstchannel])
            ->andFilterWhere(['like', 'lastapp', $this->lastapp])
            ->andFilterWhere(['like', 'lastdata', $this->lastdata])
            ->andFilterWhere(['like', 'disposition', $this->disposition])
            ->andFilterWhere(['like', 'accountcode', $this->accountcode])
            ->andFilterWhere(['like', 'userfield', $this->userfield])
            ->andFilterWhere(['like', 'uniqueid', $this->uniqueid])
            ->andFilterWhere(['between', 'cdr.calldate', $start_date, $end_date]);
        return $dataProvider;

    }


}