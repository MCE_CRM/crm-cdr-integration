<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "cdr".
 *
 * @property string $calldate
 * @property string $clid
 * @property string $src
 * @property string $dst
 * @property string $dcontext
 * @property string $channel
 * @property string $dstchannel
 * @property string $lastapp
 * @property string $lastdata
 * @property int $duration
 * @property int $billsec
 * @property string $disposition
 * @property int $amaflags
 * @property string $accountcode
 * @property string $userfield
 * @property string $uniqueid
 * @property string $recording_name
 * @property int $rec_status
 */
class Cdr extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cdr';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['calldate'], 'safe'],
            [['duration', 'billsec', 'amaflags', 'rec_status'], 'integer'],
            [['clid', 'src', 'dst', 'dcontext', 'channel', 'dstchannel', 'lastapp', 'lastdata'], 'string', 'max' => 80],
            [['disposition'], 'string', 'max' => 45],
            [['accountcode'], 'string', 'max' => 20],
            [['userfield'], 'string', 'max' => 255],
            [['uniqueid'], 'string', 'max' => 32],
            [['recording_name'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
	
    public function attributeLabels()
    {
        return [
            'calldate' => 'Call Date',
            //'clid' => 'Clid',
            'src' => 'Source',
            'dst' => 'Destination',
            //'dcontext' => 'Dcontext',
            'channel' => 'Source Line',
           'dstchannel' => 'Destination Line',
            'lastapp' => 'Call Response',
            'lastdata' => 'Lastdata',
            'duration' => 'Duration',
            'billsec' => 'Talk Time',
            'disposition' => 'Call Status',
            'amaflags' => 'Amaflags',
            'accountcode' => 'Accountcode',
            'userfield' => 'Userfield',
            'uniqueid' => 'Uniqueid',
            'vmfilename' => 'Vmfilename',
            'internalcall' => 'Internalcall',
            'iorecording' => 'Iorecording',
        ];
    }

}
