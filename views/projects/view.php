<?php

use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="project-view">

        <div class="row">
            <div class="col-lg-12">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="nav-item active">
                            <a class="nav-link" href="#information" data-toggle="tab">Information</a>
                        </li>
                        <?php if($model->additional_form)
                        {
                        $mtest = explode(',', $model->additional_form);
                        foreach ($mtest as $key=>$data){
                        $name = \app\models\FeatureForm::findOne($data);
                        ?>
                        <li class="nav-item">
                            <a class="nav-link" href="#fea<?=$data?>" data-toggle="tab"><?= $name->name?></a>
                        </li>
                            <?php }

                            }?>

                        <li class="nav-item">
                            <a class="nav-link" href="#properties" data-toggle="tab">Properties</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#files" data-toggle="tab">Files</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="information" class="tab-pane active">

                            <div class="row">
                                <div class="col-md-6">

                                    <?= DetailView::widget([
                                        'model' => $model,
                                        'attributes' => [
                                            //'id',
                                            'name',
                                            'countr.country',
                                            'cit.city',
                                            'description',
                                            [
                                                'attribute'=>'created_on',
                                                'label'=>'Added By',
                                                'value'=>function($model)
                                                {
                                                    $user = \app\models\User::find()->where(['id'=>$model->created_by])->one();
                                                    return $user->username;
                                                }
                                            ],

                                            [
                                                'attribute'=>'created_by',
                                                'label'=>'Added On',
                                                'format'=>'raw',
                                                'value'=>function($model)
                                                {
                                                   return app\helpers\Helper::DateTime($model->created_on);
                                                }
                                            ],

                                        ],
                                    ]) ?>

                                </div>
                                <div class="col-md-6">

                                    <?= DetailView::widget([
                                        'model' => $model,
                                        'attributes' => [
                                            //'id',
                                            'category',
                                            'stat.state',
                                            'address',
                                            [
                                                'attribute'=>'active',
                                                'label'=>'Status',
                                                'value'=>function($model)
                                                {
                                                    if ($model->active==1)
                                                    {
                                                        return 'Active';
                                                    }else
                                                    {
                                                        return 'InActive';
                                                    }
                                                }
                                            ],
                                        ],
                                    ]) ?>


                                </div>
                            </div>


                        </div>

                        <?php if($model->additional_form)
                        {
                            $model->additional_form = explode(',', $model->additional_form);
                            foreach ($model->additional_form as $key=>$data){?>
                        <div id="fea<?=$data?>" class="tab-pane">

                            <table id="w0" class="table table-striped table-bordered detail-view">
                                <tbody>


                                <?php

                                $extra_charges = \app\models\ExtraCharges::find()->where(['type'=>1])->andWhere(['project_id'=>$model->id])->andWhere(['feature_form_id'=>$data])->all();
                                foreach ($extra_charges as $charge)
                                {?>
                                    <tr><th><?= $charge->name ?></th><td><?= $charge->value?></td></tr>
                                <?php } ?>

                                </tbody>

                            </table>

                            </div>
                            <?php }

                        }?>


                        <div id="properties" class="tab-pane">


                            <?php  Pjax::begin(['timeout' => '30000']); ?>
                            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                            <?= GridView::widget([
                                'dataProvider' => $dataProvider,
                                'filterModel' => $searchModel,
                                'responsiveWrap' => false,
                                'toolbar' =>  [
                                    //'{export}',
                                    '{toggleData}',
                                ],
                                'panel' => [
                                    'type' => GridView::TYPE_PRIMARY,
                                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',

                                    'before'=>$create.' ',

                                    'showFooter' => false,

                                ],
                                'columns' => [
                                    ['class' => 'yii\grid\SerialColumn'],

                                    // 'id',

                                    [
                                        'attribute'=>'property_title',
                                        'label'=>'Title',
                                    ],

                                    'purpose',
                                    'property_type',

                                    //'description',
                                    //'price',
                                    //'price_range',
                                    [
                                        'attribute'=>'land_area',
                                        'label'=>'Land Area',
                                        'format'=>'raw',
                                        'value'=>function($model)
                                        {
                                            return $model->land_area .' '.$model->land_area_unit;
                                        }
                                    ],
                                    [
                                        'attribute'=>'price_per',
                                        'label'=>'Unit Price',
                                        'value'=>function($model)
                                        {
                                            return number_format($model->price_per);
                                        }
                                    ],
                                    [
                                        'attribute'=>'price',
                                        'value'=>function($model)
                                        {
                                            return number_format($model->price);
                                        }
                                    ],
                                    [
                                        'attribute'=>'extra_charges',
                                        'value'=>function($model)
                                        {
                                            return number_format($model->extra_charges);
                                        }
                                    ],
                                    [
                                        'attribute'=>'total_price',
                                        'value'=>function($model)
                                        {
                                            return number_format($model->total_price);
                                        }
                                    ],
                                    [
                                        'class' => 'kartik\grid\EditableColumn',
                                        'attribute' => 'status',
                                        'label'=>'Status',
                                        'filter'=>false,
                                        //'options' => ['style' => 'width: 8%'],

                                        'editableOptions'=> function ($model, $key, $index, $widget) {
                                            $appttypes = ['Avaliable'=>'Avaliable','Sold'=>'Sold','Occupied'=>'Occupied'];
                                            return [
                                                'header' => 'Status',
                                                'attribute' => 'status',
                                                'asPopover' => false,
                                                'inlineSettings' => [
                                                    'closeButton' => '<button class="btn btn-sm btn-danger kv-editable-close kv-editable-submit" title="Cancel Edit"><i class="fa fa-times-circle"></i></button>'
                                                ],

                                                'type' => 'primary',
                                                //'size'=> 'lg',
                                                'size' => 'md',
                                                'options' => ['class'=>'form-control', 'placeholder'=>'Enter person name...'],
                                                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                                'displayValueConfig' => $appttypes,
                                                'data' => $appttypes,
                                                'formOptions'=> ['action' => ['/ajax/update-property-status']] // point to new action
                                            ];
                                        },

                                    ],

                                    //'land_area_unit',
                                    //'land_area_unit_name',
                                    //'created_on',
                                    //'created_by',
                                    //'updated_on',
                                    //'updated_by',

                                    [
                                        'class' => '\kartik\grid\ActionColumn',
                                        'template'=>'{view}',
                                        'buttons' => [
                                            'update' => function ($url, $model)

                                            {
                                                if(\Yii::$app->user->can('property/update')){
                                                    return '<a href="'.$url.'"><span class="fa fa-pencil-alt" ></span></a>';

                                                }else
                                                {
                                                    return '';
                                                }
                                            } ,
                                            'delete' => function ($url, $model, $key) {
                                                if (\Yii::$app->user->can('property/delete')) {
                                                    return Html::a('<span class="fas fa-trash"></span>', $url,
                                                        [
                                                            'title' => Yii::t('app', 'Delete'),
                                                            'data-pjax' => '1',
                                                            'data' => [
                                                                'method' => 'post',
                                                                'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                                                'pjax' => 1,],
                                                        ]
                                                    );
                                                }
                                            },
                                            'attach' => function ($url, $model)

                                            {
                                                if(\Yii::$app->user->can('property/attach-files')){
                                                    return '<a href="javascript:void(0)" onclick="attachFiles('.$model->id.',event);"><span class="fas fa-paperclip mr-2 "></span></a>';

                                                }else
                                                {
                                                    return '';
                                                }
                                            } ,
                                            'view' => function ($url, $model)

                                            {
                                                if(\Yii::$app->user->can('property/view')){
                                                    return '&nbsp;<a href="'.Yii::$app->homeUrl.'property/view?id='.$model->id.'" data-pjax="0"><span class="fa fa-eye" ></span></a>';

                                                }else
                                                {
                                                    return '';
                                                }
                                            } ,
                                        ],
                                    ],
                                ],
                            ]); ?>
                            <?php Pjax::end(); ?>



                        </div>

                        <div id="files" class="tab-pane">

                            <?php


                            $filesModel = \app\models\Files::find()->where(['project_id'=>$model->id])->all();

                            ?>
                            <style>
                                .table > thead > tr > td.info, .table > tbody > tr > td.info, .table > tfoot > tr > td.info, .table > thead > tr > th.info, .table > tbody > tr > th.info, .table > tfoot > tr > th.info, .table > thead > tr.info > td, .table > tbody > tr.info > td, .table > tfoot > tr.info > td, .table > thead > tr.info > th, .table > tbody > tr.info > th, .table > tfoot > tr.info > th{
                                    color:#000 !important;
                                    background-color: white !important;
                                }
                            </style>

                            <div class="row">
                                <div class="col-md-1">

                                </div>
                                <div class="col-md-10">
                                    <table class="table table-responsive-lg table-bordered table-striped table-sm mb-0">
                                        <thead>
                                        <tr>
                                            <th>Sno</th>
                                            <th>FileName</th>
                                            <th>Title</th>
                                            <th>Type</th>
                                            <th>Size</th>
                                            <th>Added On</th>
                                            <th>Added By</th>
                                            <th class="text-right">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $i = 0;
                                        foreach ($filesModel as $files)
                                        {
                                            $i++;

                                            ?>
                                            <tr>

                                                <td><?= $i?></td>
                                                <td><?= $files->file_name?></td>
                                                <td><?= $files->custom_name?></td>
                                                <td><?= $files->type?></td>
                                                <td><?= \app\helpers\Helper::getSize($files->size) ?></td>
                                                <td><?= \app\helpers\Helper::DateTime($files->created_on) ?></td>
                                                <td><?= \app\helpers\Helper::getCreated($files->created_by)?></td>
                                                <td class="actions">
                                                    <a href="<?= \app\helpers\Helper::getBaseUrl()?>files/project<?= $model->id?>/<?= $files->file_name?>" class="html5lightbox"><i class="fas fa-play"></i></a>

                                                    <a href="<?= \app\helpers\Helper::getBaseUrl()?>files/project<?= $model->id?>/<?= $files->file_name?>" download><i class="fas fa-download"></i></a>
                                                    <a href="javascript:void(0)" onclick="edit(<?= $files->id ?>,'<?= $files->custom_name ?>')" ><i class="fas fa-pencil-alt"></i></a>
                                                    <a href="javascript:void(0)" onclick="del(<?= $files->id ?>)"><i class="far fa-trash-alt"></i></a>
                                                </td>
                                            </tr>

                                        <?php } ?>

                                        </tbody>
                                    </table>

                                </div>
                                <div class="col-md-1">

                                </div>
                            </div>

                            <script>

                                function edit(id,custom ) {
                                    bootbox.prompt("Update", function(result){

                                        if(result !== custom)
                                        {
                                            $.ajax({
                                                url: "<?= Yii::$app->homeUrl?>files/update",
                                                type: "POST",
                                                data: { name: result,id:id},
                                                success: function(result){

                                                    if(result==true)
                                                    {
                                                        bootbox.hideAll();
                                                        window.location.reload();
                                                        //$.pjax.reload({container:'#p0'});
                                                    }

                                                }});

                                        }

                                    });

                                    $('.bootbox-input').val(custom);


                                }

                                function del(id) {

                                    bootbox.confirm({
                                        message: "Are You Sure Delete This?",
                                        buttons: {
                                            confirm: {
                                                label: 'OK',
                                                className: 'btn btn-primary'
                                            },
                                            cancel: {
                                                label: 'Cancel',
                                                className: 'btn btn-default'
                                            }
                                        },
                                        callback: function (result) {
                                            if(result==true)
                                            {
                                                $.ajax({
                                                    url: "<?= Yii::$app->homeUrl?>files/delete",
                                                    type: "POST",
                                                    data: {id:id},
                                                    success: function(result){

                                                        if(result==true)
                                                        {
                                                            bootbox.hideAll();
                                                            window.location.reload();
                                                            //$.pjax.reload({container:'#p0'});
                                                        }

                                                    }});


                                            }
                                        }
                                    });


                                }




                            </script>


                        </div>


                    </div>
                </div>
            </div>

        </div>

    </div>