<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServerStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Server Status';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="round-top">
    

    <!--<div class="search-main"></div>-->

</div>
<div class="server-status-index">

    <div class="panel panel-info">
  <div class="panel-heading">Search</div>
  <div class="panel-body">  <?php echo $this->render('_search', ['model' => $searchModel]); ?></div>
</div>

   
  


    <?= GridView::widget([
         'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}\n{summary}",

        'pager' => [
        'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
        'prevPageLabel' => 'Previous',   // Set the label for the "previous" page button
        'nextPageLabel' => 'Next',   // Set the label for the "next" page button
        'firstPageLabel'=>'First',   // Set the label for the "first" page button
        'lastPageLabel'=>'Last',    // Set the label for the "last" page button
        'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
        'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
        'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
        'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
        'maxButtonCount'=>10,    // Set maximum number of page buttons that can be displayed
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' =>  'username',
                'filter'=>false,


            ],
             [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' =>  'ip',
                'filter'=>false,


            ],
             [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' =>  'type',
                'value' => function($model){
                    if($model->type=="1")
                    {
                        return 'Call Agent';

                    }
                    else
                    {
                        return 'SIP Provider';

                    }
                    
                },
                'filter'=>false,


            ],
             [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' =>  'total_calls',
                'value' => function($model){
                    if(!empty($model->total_calls))
                    return $model->total_calls.' Calls';
                },
                'filter'=>false,


            ],
             [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' =>  'last_call_taken',
                'value' => function($model){
                    if(!empty($model->last_call_taken))
                    return $model->last_call_taken.' Secs';
                },
                'filter'=>false,
            ],
             [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y g:i a", strtotime($model->created_on));
                },
                'filter'=>false,

            ],
             [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' =>  'status',
                'filter'=>false,


            ],
            //'username',
            //'ip',
            //'status',
            //'type',
            //'total_calls',
            //'last_call_taken',
            //'created_on',
            //'latency',

            
        ],
    ]); ?>
</div>
