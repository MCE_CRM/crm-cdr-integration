<?php

use app\models\DefaultValueModule;
use dosamigos\fileupload\FileUpload;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Projects */
/* @var $form yii\widgets\ActiveForm */
?>


<?php

$country = '';

Yii::$app->user->identity->id;

if($model->isNewRecord)
{
    if($default_country = DefaultValueModule::getDefaultValueId('country'))
    {
        $c= \app\models\Country::findOne($default_country);
        $country = $c->country;
        $model->country = $default_country;
    }
}
else
{
    $c= \app\models\Country::findOne($model->country);
    $country = $c->country;

    $s = \app\models\State::findOne($model->state);
    $state = $s->state;

    $ci = \app\models\City::findOne($model->city);
    $city = $ci->city;

}




?>



<div class="content-boody">

    <section class="card">

        <div class="card-body">


        <div class="projects-form" >

            <?php

            if($_GET['id'])
            {
                $form = \yii\widgets\ActiveForm::begin([
                    'id' => 'form',
                    'enableAjaxValidation' => true,
                    'validationUrl' => Yii::$app->homeUrl.'projects/validate?id='.$model->id.'',
                    'errorCssClass' => 'has-danger',

                ]);
            }
            else
            {
                $form = \yii\widgets\ActiveForm::begin([
                    'id' => 'form',
                    'enableAjaxValidation' => true,
                    'validationUrl' => Yii::$app->homeUrl.'projects/validate',
                    'errorCssClass' => 'has-danger',
                ]);
            }



            ?>

            <div class="row">

                <div class="row col-md-6">

                    <div class="col-md-6">
                        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                    </div>

                   <!--    <div class="col-md-6">
                        <?=

                        $form->field($model, 'blocksector')->widget(Select2::classname(), [
                            //'initValueText' => $country, // set the initial display text
                            'options' => ['placeholder' => 'Search for a Block/sector ...'],
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['ajax/blocksector-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],
                        ]);



                        ?>

                    </div> -->
                    <div class="col-md-6">
                        <?php

                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'category')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\ProjectCategory::find()->where(['active'=>1])->orderBy([
                                'sort_order' => SORT_ASC,
                            ])->all(),'name','name'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Select a Category ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);




                        ?>


                    </div>

                    <div class="col-md-6">
                        <?=

                        $form->field($model, 'country')->widget(Select2::classname(), [
                            'initValueText' => $country, // set the initial display text
                            'options' => ['placeholder' => 'Search for a Country ...'],
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['ajax/country-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],
                        ]);



                        ?>

                    </div>
                    <div class="col-md-6">
                        <?php

                        echo $form->field($model, 'state')->widget(DepDrop::classname(), [
                            'options'=>['id'=>'subcat-id'],
                            'data'=> [$model->state =>$state],
                            'type'=>DepDrop::TYPE_SELECT2,
                            'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],

                            'pluginOptions'=>[
                                'depends'=>['projects-country'],
                                'initDepends' => ['projects-country'],
                                'initialize'=>true,
                                'placeholder'=>'Select...',
                                'url'=>Url::to(['/ajax/get-country-state'])
                            ]
                        ]);

                        ?>

                    </div>
                     <div class="col-md-6">
                         <?php

                         echo $form->field($model, 'city')->widget(DepDrop::classname(), [
                             'options'=>['id'=>'subcat2-id'],
                             'data'=> [$model->city =>$city],
                             'type'=>DepDrop::TYPE_SELECT2,
                             'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],


                             'pluginOptions'=>[
                                 'depends'=>['subcat-id'],

                                 'placeholder'=>'Select...',
                                 'url'=>Url::to(['/ajax/get-state-city'])
                             ]
                         ]);

                         ?>

                        </div>

                    <div class="col-md-6">
                        <?= $form->field($model, 'address')->textarea(['rows' => 1]) ?>

                    </div>



                </div>

                <div class="col-md-6">

                    <?= $form->field($model, 'description')->textarea(['rows' => 7]) ?>

                </div>



            </div>


            <hr>

            <div class="col-md-6">

                <div class="form-group row">
                    <label class="col-lg-3 control-label text-lg-right pt-2">Select Form</label>
                    <div class="col-lg-6">

                        <?php $getForm = \app\models\FeatureForm::find()->joinWith('formlist')->where(['show_on'=>'project'])->all(); ?>




                        <?php




                        ?>



                        <select multiple="" id="dynamic_form" name="dynamic_form[]" class="form-control mb-3" >

                            <option value=""></option>

                            <?php

                            foreach ($getForm as $form =>$key) {
                                $selected = '';



                                if($model->additional_form)
                                {

                                    $myString = $model->additional_form;


                                    $myArray = explode(',', $myString);

                                   

                                    if (in_array($key->id, $myArray))
                                    {
                                        $selected = 'selected';
                                    }else
                                    {
                                        $selected = '';
                                    }

                                }

                                ?>

                                <?php echo '<option value="'.$key->id.'"'.$selected.'>' . $key->name . '</option>';

                            }

                            ?>

                        </select>
                    </div>
                </div>


            </div>

            <div class="tabs">
                <ul class="nav nav-tabs">

                    <?php foreach ($getForm  as $form=>$key)
                    {?>

                    <li class="nav-item" id="dyn<?=$key->id?>">
                        <a class="nav-link" id="nav<?=$key->id?>" href="#fea<?=$key->id?>" data-toggle="tab"><?= $key->name?></a>
                    </li>

                   <?php } ?>

                </ul>
                <div class="tab-content">
                    <?php foreach ($getForm  as $form=>$key){?>
                    <div id="fea<?=$key->id?>" class="tab-pane">

                        <?php

                        $featureList = \app\models\FeatureList::find()->where(['feature_form_id'=>$key->id])->all();




                        foreach ($featureList as $list)
                        {
                            if($model->id)
                            {

                                $find = \app\models\ExtraCharges::find()->where(['project_id'=>$model->id])->andWhere(['type'=>'1'])->andWhere(['name'=>$list->name])->one();

                            }
                            if($list->type=="text") {?>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label text-lg-right pt-2" for="inputDefault"><?= $list->name?></label>
                                    <div class="col-lg-6">
                                        <input type="text" class="form-control" name="feature[<?= $list->name?>]" id="inputDefault" value="<?= $find->value?>" <?=$list->required?>>
                                    </div>
                                </div>

                                <?php

                            }else if($list->type=="dropdown"){?>

                                <div class="form-group row">
                                    <label class="col-lg-3 control-label text-lg-right pt-2" for="inputDefault"><?= $list->name?></label>
                                    <div class="col-lg-6">
                                        <select class="form-control   mb-3" name="feature[<?= $list->name?>]" <?=$list->required?>>
                                            <option value="">Select</option>
                                            <?php

                                            $value_list = explode(',', $list->value);
                                            foreach ($value_list as $lis){
                                                if($find->value == $lis)
                                                {
                                                    $selected = 'selected';
                                                }else
                                                {
                                                    $selected = '';
                                                }

                                                ?>
                                                <option value="<?= $lis ?>"<?= $selected?>><?= $lis?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>


                                <?php
                            }else if($list->type=="checkbox"){?>
                                <div class="form-group row">
                                    <label class="col-lg-3 control-label text-lg-right pt-2"><?= $list->name?></label>

                                    <div class="col-sm-6">

                                        <div class="checkbox-custom checkbox-default">
                                            <?php

                                            $checked = '';

                                            if($list->value)
                                            {
                                                $checked = 'checked';

                                            }else
                                            {
                                                $checked = '';

                                            }

                                            if($find->value=='Yes')
                                            {
                                                $checked = 'checked';
                                            }else
                                            {
                                                $checked = '';

                                            }





                                            ?>

                                            <input type='hidden' value='No' name='feature[<?= $list->name?>]'>


                                            <input type="checkbox" value="Yes"  name="feature[<?= $list->name?>]"   id="<?= $list->name?>"  <?= $checked ?>>
                                            <label for="<?= $list->name?>"></label>

                                            </div>


                                    </div>
                                </div>
                                <?php

                            }
                        }


                        ?>



                    </div>

                    <?php } ?>
                </div>
            </div>





            <!--     <?/*= $form->field($model, 'state')->textInput(['maxlength' => true]) */?>

                        <?/*= $form->field($model, 'city')->textInput(['maxlength' => true]) */?>

                        <?/*= $form->field($model, 'address')->textInput(['maxlength' => true]) */?>

                        <?/*= $form->field($model, 'description')->textarea(['rows' => 6]) */?>-->
            <!--
                        <?/*= $form->field($model, 'created_on')->textInput() */?>

                        <?/*= $form->field($model, 'created_by')->textInput() */?>

                        <?/*= $form->field($model, 'updated_on')->textInput() */?>

                        <?/*= $form->field($model, 'updated_by')->textInput() */?>-->

            <!-- <div class="form-group">
                            <?/*= Html::submitButton('Save', ['class' => 'btn btn-success']) */?>
                        </div>-->



        </div>

        </div>

        <footer class="card-header text-center">

            <a href="<?= Yii::$app->homeUrl?>projects" class="mb-1 mt-1 mr-1 btn btn-danger">Cancel</a>

            <?= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) ?>


        </footer>

        </section>



    <?php ActiveForm::end(); ?>



</div>
<script>

    var hideTab = [];




    <?php

    $flag = 0;
    foreach ($getForm  as $form=>$key){

    if($model->additional_form)
    {



        $myString = $model->additional_form;


        $myArray = explode(',', $myString);


        if (in_array($key->id, $myArray))
        {

            if($flag==0){



    ?>

    $('#dyn'+<?=$key->id?>).addClass("active");
    $('#nav'+<?=$key->id?>).addClass("show active");
    $('#fea'+<?=$key->id?>).addClass("active show");

    hideTab.push(<?=$key->id?>);


      <?php } $flag = 1; }else { ?>

    $('#dyn' +<?=$key->id?>).hide();



    hideTab.push(<?=$key->id?>);

     <?php }


    }else {  ?>


    $('#dyn' +<?=$key->id?>).hide();



    hideTab.push(<?=$key->id?>);

  <?php  }
?>







    <?php } ?>


    $(function() {
        $('#dynamic_form').change(function() {

            for (var k=0; k < hideTab.length ; k++)
            {
                $('#dyn'+hideTab[k]).hide();
                $('#dyn'+hideTab[k]).removeClass("active");
                $('#nav'+hideTab[k]).removeClass("show active");
                $('#fea'+hideTab[k]).removeClass("active show");
            }

            var showTab = [];

        console.log($(this).val());

            var value =  $(this).val();
            var arrayLength = value.length;
            for (var i = 0; i < arrayLength; i++) {
                showTab.push(value[i]);
                $('#dyn'+value[i]).show();
                //alert(value[i]);
                //Do something
            }

            for (var j=0; j < showTab.length ; j++)
            {

                $('#dyn'+showTab[j]).show();
                if(j==1)
                {
                    $('#dyn'+showTab[0]).addClass("active");
                    $('#nav'+showTab[0]).addClass("show active");
                    $('#fea'+showTab[0]).addClass("active show");


                }
            }



        });
    });


</script>
