<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\cdr\models\ServerStatus;
/* @var $this yii\web\View */
/* @var $model app\models\ServerStatusSearch */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="server-status-search" style="margin-right: 30px;margin-left: 30px;">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>


    <div class="row">

        <div class="col-md-2">
             <?= $form->field($model, 'username')
     ->dropDownList(
            ArrayHelper::map(ServerStatus::find()->asArray()->all(), 'username', 'username'),['prompt'=>'']
            ); ?>
        </div>

        <div class="col-md-2">
            
    <?= $form->field($model, 'ip') ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'type')->dropDownList(['1' => 'Call Agent', '2' => 'SIP Provider'],['prompt'=>'']);
            ?>
        </div>
        <div class="col-md-2">
            <?= $form->field($model, 'status')
                ->dropDownList(['OK' => 'OK', 'UNREACHABLE' => 'UNREACHABLE', 'LAGGED' => 'LAGGED'],['prompt'=>'']);
 ?>
        </div>


        <div class="col-md-4">

           <?php
           $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;



           echo '<label class="control-label">Date Range</label>';
           echo '<div class="drp-container">';
           echo DateRangePicker::widget([
               'model'=>$model,
               'attribute'=>'created_on',
            
               'convertFormat'=>true,

               'pluginOptions'=>[
                   'opens'=>'left',
                   'ranges' => [

                       "Today" => ["moment().startOf('day')", "moment()"],
                       "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                       "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                   ],

                   'timePicker'=>true,
                   'timePickerIncrement'=>05,
                   'locale'=>['format'=>'d/m/Y h:i A']
               ],
               'presetDropdown'=>false,
               'hideInput'=>true
           ]);
           echo '</div>'; ?>
        </div>

        

    </div>


     
  
    <?php // echo $form->field($model, 'total_calls') ?>

    <?php // echo $form->field($model, 'last_call_taken') ?>

    <?php //echo $form->field($model, 'created_on') ?>

    <?php // echo $form->field($model, 'latency') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default btn-sm']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
