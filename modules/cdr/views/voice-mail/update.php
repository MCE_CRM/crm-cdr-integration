<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\VoiceMail */

$this->title = 'Update Voice Mail: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Voice Mails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="voice-mail-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
