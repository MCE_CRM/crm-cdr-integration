<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 9/23/2018
 * Time: 6:03 PM
 */

use kartik\file\FileInput;
use kartik\password\PasswordInput;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\form\ActiveForm;

$this->title = "Profile";


$role = \Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);


$i=0;
foreach ($role as $key=>$r)
{

    if($i>0)
    {
        echo $key.",";
    }
    $i++;
}


?>


<div class="row animated fadeInRight">
    <div class="col-md-4">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Profile Detail</h5>
            </div>
            <div>
                <div class="ibox-content no-padding border-left-right">
                    <img alt="image" class="img-responsive" src="<?= \app\helpers\Helper::getBaseUrl()?>files/profile_image/<?=Yii::$app->user->identity->image?>">
                </div>
                <div class="ibox-content profile-content">
                    <h4><strong><?= Yii::$app->user->identity->first_name.' '.Yii::$app->user->identity->last_name?></strong> (<?= $key?>)</h4>

                    <h5>
                        About me
                    </h5>
                    <p>
                        <?= Yii::$app->user->identity->about?>
                    </p>

                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="ibox float-e-margins">
            <div class="ibox-content">

                <div class="tabs">
                    <ul class="nav nav-tabs tabs-primary">
                        <li class="nav-item active">
                            
                         
                            <a class="nav-link" href="#edit" data-toggle="tab">Profile</a>
                        }

                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#password" data-toggle="tab">Change Password</a>
                        </li>
                    </ul>
                    <div class="tab-content">

                        <div id="edit" class="tab-pane active">

                            <br>

                            <h3 class="mb-3">Personal Information</h3>

                            <?php

                            $form = ActiveForm::begin([
                                'id' => 'form',

                                'options'=>array('enctype' => 'multipart/form-data')
                            ]);
                            ?>

                            <div class="row">
                                <div class="col-md-6">
                                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                                </div>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <?=$form->field($model, 'email', [
                                        'feedbackIcon' => [
                                            'prefix' => 'fas fa-',
                                            'default' => 'envelope',
                                            'success' => 'check text-success',
                                            'error' => 'exclamation-triangle text-danger',
                                            'defaultOptions' => ['class'=>'text-primary']
                                        ]
                                    ])->textInput(['placeholder'=>'Enter a valid email address...']);?>
                                    <!--   <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?> -->
                                </div>
                                <div class="col-md-6">
                                    <?=$form->field($model, 'phone_no', [
                                        'feedbackIcon' => [
                                            'prefix' => 'fas fa-',
                                            'default' => 'mobile-alt',
                                            'success' => 'check-circle',
                                            'error' => 'exclamation-circle',
                                        ]
                                    ])->widget('yii\widgets\MaskedInput', [
                                        'mask' => '9999-9999999'
                                    ]);?>
                                    <!-- <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?> -->
                                </div>
                            </div>

                            <div class="row">

                                <?php if(Yii::$app->user->can('profile/username')){?>
                                <div class="col-md-6">
                                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                                </div>
                                    <?php
                                }
                                ?>

                                <div class="col-md-6">
                                    <label class="control-label" for="user-image">Image</label>
                                    <?php


                                    echo $form->field($model, 'file')->widget(FileInput::classname(), [
                                        'options' => ['multiple' => false, 'accept' => 'image/*'],
                                        'pluginEvents' => [
                                            'change' => 'function() { showImage(); }',
                                        ],
                                        'pluginOptions' => [
                                            'allowedFileExtensions'=>['jpg', 'gif', 'png', 'bmp'],
                                            'showPreview' => false,
                                            'showCaption' => true,
                                            'showRemove' => false,
                                            'showCancel' => false,
                                            'showUpload' => false,
                                            'initialCaption'=> $model->image,


                                        ]
                                    ])->label(false);


                                    ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form->field($model, 'about')->textarea(['maxlength' => true]) ?>
                                </div>

                            </div>

                            <div class="form-group">
                                <?= Html::submitButton('Update', ['class' => 'btn btn-primary']) ?>
                            </div>



                            <?php ActiveForm::end(); ?>

                        </div>
                        <div id="password" class="tab-pane">

                            <br>

                            <h3 class="mb-3">Change Password</h3>

                            <?php if (Yii::$app->session->hasFlash('success')): ?>

                                <div class="alert alert-success">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    <?= Yii::$app->session->getFlash('success') ?>
                                </div>

                            <?php endif; ?>



                            <?php



                            $form2 = ActiveForm::begin([
                                'id' => 'form2',

                            ]);


                            ?>

                            <div class="row">
                                <div class="col-md-12">
                                    <?= $form2->field($changepassword, 'old_password')->passwordInput(['maxlength' => true]) ?>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <?php
                                    echo $form2->field($changepassword, 'password')->widget(
                                        PasswordInput::classname()
                                    )->label("New Password");

                                    ?>                            </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">

                                    <?php

                                    echo $form2->field($changepassword, 'repeat_password')->widget(
                                        PasswordInput::classname()
                                    );

                                    ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <?= Html::submitButton('Change Password', ['class' => 'btn btn-info']) ?>
                            </div>

                            <?php ActiveForm::end(); ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>


<script>

    $(document).ready(function(e) {


        $('#myTable').on('click', '.checkbox-click', function(){



            var $this = $(this);

            var child = $(this).parent().parent().siblings(":first").text();
            console.log(child);
            var parent = $(this).val();
            if ($(this).is(':checked')) {



                // alert("checked");


                var url = '?child='+child+'&parent='+parent+'';
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (data) {
                        //alert(data);
                        if(!data=="Revoke")
                        {
                            $this.removeAttr("checked");
                        }


                    },
                    error: function(xhr, ajaxOptions, thrownError){

                    },
                    //timeout : 15000//timeout of the ajax call
                });




            }
            else
            {
                var url = '?child='+child+'&parent='+parent+'&remove_child=true';

                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (data) {

                        //alert(data);

                        if(!data=="ASSIGN")
                        {
                            $this.prop('checked');
                        }




                    },
                    error: function(xhr, ajaxOptions, thrownError){

                    },
                    // timeout : 15000//timeout of the ajax call
                });

            }
        });



        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {

            //save the latest tab; use cookies if you like 'em better:

            localStorage.setItem('lastTab_leadview', $(this).attr('href'));

        });
        $('[data-toggle="hover"]').popover({ trigger: "hover" });


        //go to the latest tab, if it exists:

        var lastTab_leadview = localStorage.getItem('lastTab_leadview');

        if ($('a[href="' + lastTab_leadview + '"]').length > 0) {

            $('a[href="' + lastTab_leadview + '"]').tab('show');
        }
        else
        {
            // Set the first tab if cookie do not exist

            $('a[data-toggle="tab"]:first').tab('show');
        }

    });



    function showImage() {

        readURL(this);

    }

    function readURL(input) {


        console.log(input);

        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#showImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }


</script>
