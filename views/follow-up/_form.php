<?php

/*use kartik\date\DatePicker;*/
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */
/* @var $model app\models\FollowUp */
/* @var $form yii\widgets\ActiveForm */

$model->lead_id = $_GET['id'];

?>
<style type="text/css">
    .datetimepicker.datetimepicker-dropdown-bottom-right.dropdown-menu {
    z-index: 1160!important;
}
</style>
<div class="follow-up-form">

    <?php

    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'follow-up/validate':Yii::$app->homeUrl.'follow-up/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);

    ?>
    

    <?php

    // usage without model

    echo $form->field($model, 'follow_date_time')->widget(DateTimePicker::classname(), [
        'options' => ['autocomplete' => 'off',],
        'readonly' => true,
        'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd/mm/yyyy h:ii',
            'startDate'=> date('d-m-Y H:i',time()),
        ]
    ]);



    ?>




    <?= $form->field($model, 'note')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
