<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\VoiceMail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="voice-mail-form">

    <?php $form = ActiveForm::begin([
            'options'=>['class'=>'well-sm',

            'enctype'=>'multipart/form-data']]); ?>


    <div class="row">
        <div class="col-md-8">
            <div class="col-md-12">
                <div class="col-md-6">

                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-6">

                    <?= $form->field($model, 'number_press')->dropDownList([1=>'1',2=>'2',3=>'3',4=>'4',5=>'5',6=>'6' ]) ?>
                </div>

            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <?= $form->field($model, 'time_to')->textInput(['maxlength' => true]) ?>
                </div>

                <div class="col-md-6">
                    <?= $form->field($model, 'time_from')->textInput(['maxlength' => true]) ?>
                </div>

            </div>
            <div class="col-md-12">
                <div class="col-md-6">
                    <?= $form->field($model, 'depart_id')->textInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'InActive', ]) ?>
                </div>
                <div class="col-md-12">
                    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

                </div>

            </div>

        </div>
        <div class="col-md-4">

            <?=  $form->field($model, 'file')->widget(FileInput::classname(), [
                'options' => ['multiple' => true, 'preview'=>true,],
                'pluginOptions' => ['previewFileType' => 'image']
            ])->label(false);?>

        </div>

    </div>
    
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
