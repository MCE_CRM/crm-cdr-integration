<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectsSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style type="text/css">
    .control-label
    {
        display: none;
    }
    #projectssearch-name
    {
        width:60%;
    }
    .form-group:last-child, .form-group:last-of-type
    {
        position: absolute;
        right:334px;
        top:0px;
    }
</style>
<div class="col-md-6">
    <div class="projects-search">

        <?php $form = ActiveForm::begin([
            'action' => ['index'],
            'method' => 'get',
            'options' => [
                'data-pjax' => 1
            ],
        ]); ?>

        <!-- <?= $form->field($model, 'id') ?> -->

        <?= $form->field($model, 'name')->textInput(['placeholder' => "Search Your Record",'style'=>"border-radius:0px;width:40%"])?>

        <!-- <?= $form->field($model, 'state') ?>

    <?= $form->field($model, 'city') ?>

    <?= $form->field($model, 'address') ?> -->

        <?php // echo $form->field($model, 'description') ?>

        <?php // echo $form->field($model, 'created_on') ?>

        <?php // echo $form->field($model, 'created_by') ?>

        <?php // echo $form->field($model, 'updated_on') ?>

        <?php // echo $form->field($model, 'updated_by') ?>

        <div class="form-group">

            <?= Html::submitButton('<i class="fa fa-fw fa-search"></i>', ['class' => 'btn btn-primary','style'=>"border-radius:0px"]) ?>
            <?php // Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>

        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>
<br>
