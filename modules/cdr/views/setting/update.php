<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Setting */

$this->title = 'Update Setting: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="setting-update">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    <style type="text/css">
        .setting-update{
            width: 45%;
            margin: 0 auto;
        }
    </style>
</div>
