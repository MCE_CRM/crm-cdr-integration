<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ForwardingSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="forwarding-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <div class="col-lg-3">
        <?php $form->field($model, 'id') ?>
    </div>

    <?= $form->field($model, 'time_to') ?>

    <?= $form->field($model, 'time_from') ?>

    <?= $form->field($model, 'phone_no') ?>

    <?= $form->field($model, 'created_on') ?>


    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_on') ?>

    <?php // echo $form->field($model, 'updated_by') ?>


    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
