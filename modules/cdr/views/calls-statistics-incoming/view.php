<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CdrCallsStatistics */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cdr Calls Statistics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cdr-calls-statistics-view">

    <h2><?= Html::encode($this->title) ?></h2>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'calldate',
            'total_calls',
            'ans_calls',
            'no_ans_calls',
            'last_updated',
            'entry_date',

            //'call_type_id',
            [
                'label' => 'Call Type',
                'value' => function ($model) {
                    if ($model->call_type_id == 1) {
                        return "Incoming";
                    } else {
                        return "Outgoing";
                    }
                },
                'format'=>'raw',
            ],
        ],
    ]) ?>

</div>
