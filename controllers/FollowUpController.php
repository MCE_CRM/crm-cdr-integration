<?php

namespace app\controllers;

use app\helpers\Helper;
use app\models\DefaultValueModule;
use kartik\widgets\ActiveForm;
use Yii;
use app\models\FollowUp;
use app\models\FollowUpSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use app\models\Activities;
use DateTime;
use app\models\User;

/**
 * FollowUpController implements the CRUD actions for FollowUp model.
 */
class FollowUpController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='follow-up/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all FollowUp models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new FollowUpSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FollowUp model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FollowUp model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
      public function actionCreate()
    {
        $model = new FollowUp();
        $model->created_by = Yii::$app->user->id;
        $model->created_on = date("Y-m-d H:i:s");
        $model->lead_id = $_GET['id'];

        if ($model->load(Yii::$app->request->post())) {

            $christmas = $model->follow_date_time;

             $model->follow_date_time = DateTime::createFromFormat('d/m/Y H:i',$christmas)->format('Y-m-d H:i:s');

            if($default_lead_source = DefaultValueModule::getDefaultValueId('follow-up-status'))
            {
                $lead_source = \app\models\FollowUpStatus::findOne($default_lead_source);
                $model->status = $lead_source->status;
            }else {
                $model->status = '';
            }


            
            /*$parts = explode('/',$christmas);
            $yyyy_mm_dd = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
            $model->follow_date_time = $yyyy_mm_dd;*/

            if($model->save())
            {
                 $date= $_POST['FollowUp']['follow_date_time'];
                 $followupdatetime=DateTime::createFromFormat('d/m/Y H:i',$date)->format('d/m/Y h:i a');
          
                $activities = new Activities();
                $username=\app\models\User::find()->select('first_name')->where(['id'=>Yii::$app->user->id])->one();
                $activities->activity=$username->first_name.' created the follow-up '.$_POST['FollowUp']['note'].' '.$followupdatetime.'';
                $activities->created_on=date("Y-m-d h:i:s");
                $activities->created_by=Yii::$app->user->id;
                $activities->lead_id=$_GET['id'];
                $activities->save();
                return true;
            }

        }

        return $this->renderAjax('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FollowUp model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);
        $model->follow_date_time=date('d/m/Y H:i',strtotime($model->follow_date_time));
        $model->updated_by = Yii::$app->user->id;
        $model->updated_on = date("Y-m-d H:i:s");

        if ($model->load(Yii::$app->request->post())) {

            $christmas = $model->follow_date_time;



            $model->follow_date_time = DateTime::createFromFormat('d/m/Y H:i',$christmas)->format('Y-m-d H:i:s');

          
            
            
            /*$parts = explode('/',$christmas);
            $yyyy_mm_dd = $parts[2] . '-' . $parts[1] . '-' . $parts[0];
            $model->follow_date_time = $yyyy_mm_dd;*/
            
            $leadid=FollowUp::find()->select('lead_id')->where(['id'=>$id])->one();
            if($model->save())
            {
                $date= $_POST['FollowUp']['follow_date_time'];
                 $followupdatetime=DateTime::createFromFormat('d/m/Y H:i',$date)->format('d/m/Y h:i a');
                $activities = new Activities();
                $username=\app\models\User::find()->select('first_name')->where(['id'=>Yii::$app->user->id])->one();
                $activities->activity=$username->first_name.' update the follow up  '.$_POST['FollowUp']['note'].' '.$followupdatetime.'';
                $activities->created_on=date("Y-m-d H:i:s");
                $activities->created_by=Yii::$app->user->id;
                $activities->lead_id=$leadid->lead_id;
                $activities->save();
                return true;
            }

        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FollowUp model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FollowUp model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FollowUp the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FollowUp::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionValidate()
    {

        if($_GET['id'])
        {
            $model = $this->findModel($_GET['id']);
            //$model->scenario = 'update';
        }
        else
        {
            $model = new FollowUp();
            //$model->scenario = 'create';
        }

        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

}
