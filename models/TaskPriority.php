<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task_priority".
 *
 * @property int $id
 * @property string $priority
 * @property int $active
 * @property int $sort_order
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class TaskPriority extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task_priority';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['priority', 'active', 'sort_order', 'created_on', 'created_by'], 'required'],
            [['active', 'sort_order', 'created_by', 'updated_by'], 'integer'],
            [['priority'], 'unique'],
            [['created_on', 'updated_on'], 'safe'],
            [['priority'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'priority' => 'Priority',
            'active' => 'Active',
            'sort_order' => 'Sort Order',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
}
