<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\LeadType */

$this->title = 'Create Lead Type';
$this->params['breadcrumbs'][] = ['label' => 'Lead Types', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-type-create">

   <!-- <h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
