<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "leadresponse".
 *
 * @property int $id
 * @property string $response
 * @property int $active
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Leadresponse extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'leadresponse';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['response', 'active'], 'required'],
            [['active', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['response'], 'string', 'max' => 250],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'response' => 'Response',
            'active' => 'Active',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

     public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
}
