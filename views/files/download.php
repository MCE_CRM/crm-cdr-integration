<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <input type="hidden" name="file_id[]" value="{%=file.id%}">
    <tr class="template-download fade">
        <td class="text-center">
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img style="width:50px;height:50px;" src="{%=file.thumbnailUrl%}"></a>
                {% } %}
            </span>
        </td>
        <td class="text-center">
            <p class="name">
                {% if (file.url) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                {% } else { %}
                    <span>{%=file.name%}</span>
                {% } %}
            </p>
            {% if (file.error) { %}
                <div><span class="label label-danger"><?= Yii::t('fileupload', 'Error') ?></span> {%=file.error%}</div>
            {% } %}
        </td>
        <td class="text-center">
            <span class="name">{%=file.custom_name%}</span>
        </td>
        <td class="text-center">
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td class="text-center">
            {% if (file.deleteUrl) { %}
                <button class="btn btn-danger delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="fa fa-trash"></i>
                    <span><?= Yii::t('fileupload', 'Delete') ?></span>
                </button>

            {% } else { %}
                <button class="btn btn-warning cancel">
                    <i class="fa fa-ban"></i>
                    <span><?= Yii::t('fileupload', 'Cancel') ?></span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}

</script>
