<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\VoiceMailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Voice Mails';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voice-mail-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Voice Mail', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            'number_press',
            'file',
            'depart_id',
            //'time_to',
            //'time_from',
            //'description',
            //'status',
            //'created_on',
            //'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
