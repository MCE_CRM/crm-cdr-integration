<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_template".
 *
 * @property int $id
 * @property string $name
 * @property string $message
 * @property int $active
 * @property int $type
 * @property int $sms_status
 * @property int $projectname
 * @property int $developer
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class SmsTemplate extends \yii\db\ActiveRecord
{
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'message', 'created_on', 'created_by','type'], 'required'],
            [['active', 'developer', 'created_by','updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['name','projectname','sms_status'], 'string', 'max' => 25],
            [['message'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'message' => 'Message',
            'active' => 'Active',
            'developer' => 'Developer',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }
}
