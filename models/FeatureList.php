<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feature_list".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property int $feature_form_id
 * @property string $value
 * @property string $required
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class FeatureList extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feature_list';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'type', 'required','feature_form_id', 'created_on', 'created_by'], 'required'],
            [['created_on', 'updated_on'], 'safe'],
            [['created_by','feature_form_id', 'updated_by'], 'integer'],
            [['name', 'value'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50],
            [['required'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'feature_form_id' => 'Feature Form',
            'value' => 'Value',
            'required' => 'Required',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }

    public function getForm()
    {
        return $this->hasOne(FeatureForm::className(), ['id' => 'feature_form_id']);

    }
}
