<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "email_template".
 *
 * @property int $id
 * @property string $template_name
 * @property string $template_subject
 * @property string $template_body
 * @property int $active
 * @property int $developer
 * @property string $created_on
 * @property int $created_by
 * @property int $updated_by
 * @property string $updated_on
 */
class EmailTemplate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['template_name', 'template_subject', 'template_body', 'created_on', 'created_by'], 'required'],
            [['template_body'], 'string'],
            [['active', 'developer', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['template_name', 'template_subject'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'template_name' => 'Template Name',
            'template_subject' => 'Template Subject',
            'template_body' => 'Template Body',
            'active' => 'Active',
            'developer' => 'Developer',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_by' => 'Updated By',
            'updated_on' => 'Updated On',
        ];
    }
}
