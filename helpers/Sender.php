<?php

namespace app\helpers;

use app\models\Activities;
use app\models\EmailTemplate;
use app\models\Settings;
use app\models\User;
use DateTime;
use Yii;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: Waqar
 * Date: 10/28/2017
 * Time: 2:35 PM
 */


class Sender
{
    public static function sendAccountEmail($receiverEmail) {

        $email = EmailTemplate::find()->where(['template_name'=>'Registration Account'])->one();
        $send = Yii::$app->mailer->compose()
            ->setFrom('waqar.sendEmail@gmail.com')
            ->setTo($receiverEmail)
            ->setSubject($email->template_subject)
            ->setTextBody($email->template_body)
            ->send();

        echo "<pre>";
        print_r($email);
        echo "</pre>";


    }

}