<?php

use app\models\User;
use kartik\date\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\export\ExportMenu;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ExtraPropertyChargesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Extra Property Charges';
$this->params['breadcrumbs'][] = $this->title;


$create = '';
$ordering = '';
if(\Yii::$app->user->can('extra-property-charges/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Charges'), [
        'create'
    ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}


$gridColumn = [
    ['class' => 'yii\grid\SerialColumn'],

    //'id',
    'name',
    //'charges',
    [
        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
        'contentOptions' => ['class' => 'text-center'],
        'attribute' =>  'created_on',
        'value' => function($model){
            return date("d/m/Y", strtotime($model->created_on));
        },
        'filterType'=>GridView::FILTER_DATE,
        'filterWidgetOptions'=> [
            'type' => DatePicker::TYPE_INPUT,
            'pluginOptions' => [
                'autoclose'=>true,
                'format' => 'dd/mm/yyyy',
            ],
        ],
        //'filter'=>false

    ],
    [
        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
        'attribute' => 'created_by',
        'contentOptions' => ['class' => 'text-center'],
        'value'=>'user.username',
        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
        'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
        'filterWidgetOptions' => [
            'theme' => Select2::THEME_BOOTSTRAP,
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ],
        'filterInputOptions' => ['placeholder' => 'All...'],
        'format'=>'raw',

    ],
    [

        //'attribute' =>  'created_on',
        'header'=>'Last Update',
        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
        'contentOptions' => ['class' => 'text-center'],
        'format'=>'raw',
        'visible'=> \Yii::$app->user->can('extra-property-charges/last-update'),
        'value' => function($model){
            if($model->updated_by)
            {
                return date("d/m/Y h:i A", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

            }else
            {
                return '';
            }
        },
        //'filter'=>false

    ],
    //'updated_on',
    //'updated_by',

    [

        'class' => '\kartik\grid\ActionColumn',

        'template' => '{update} {delete}',
        'contentOptions' => ['style' => 'width:50px;'],
        'buttons' => [
            'delete' => function ($url, $model, $key) {
                if (\Yii::$app->user->can('extra-property-charges/delete')) {
                    return Html::a('<span class="fas fa-trash"></span>', $url,
                        [
                            'title' => Yii::t('app', 'Delete'),
                            'data-pjax' => '1',
                            'data' => [
                                'method' => 'post',
                                'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                'pjax' => 1,],
                        ]
                    );
                }
            },

            'update' => function ($url, $model)

            {
                if(\Yii::$app->user->can('extra-property-charges/update')){
                    return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'extra-property-charges\',\'Update Extra Property Charges\',event)"></span></a>';

                }else
                {
                    return '';
                }
            } ,
        ]
    ]
];


?>
<div class="extra-property-charges-index">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php //Html::a('Add New Charges', ['create'], ['class' => 'btn btn-success add-new']) ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',

            'before'=>$create.' '.$ordering.' '.Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',
            
            'showFooter' => false,

        ],

        'columns' => $gridColumn,
    ]); ?>
    <?php Pjax::end(); ?>
</div>
