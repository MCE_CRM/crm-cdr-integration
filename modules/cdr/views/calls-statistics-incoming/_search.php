<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\CdrCallsStatisticsSearch */
/* @var $form yii\widgets\ActiveForm */
?>



<div class="panel panel-info">
   
    <div class="agent-session-search">
        <div class="card-body" >
            <?php 
            $form = ActiveForm::begin([
                'action' => ['index'],
                'method' => 'get',
            ]); 
            ?>

            <div class="col-md-4">
            </div>
                <div class="col-md-4">
                    <div class="form-group field-cdrsearch-channel">
                        <?php
                        $date = new DateTime('now');
                        $date = $date->format('Y-m-d');
                        $monthAgo = new DateTime('-29 days');
                        $monthAgo = $monthAgo->format('Y-m-d');

                        // If a date range was searched, we want to keep it in the DateRangePicker
                        if (isset($_GET['CdrCallsStatisticsSearch']['calldate'])) {
                            $prevDate = $_GET['CdrCallsStatisticsSearch']['calldate'];
                        }

                        $addon = <<< HTML
                        <span class="input-group-addon">
                            <i class="glyphicon glyphicon-calendar"></i>
                        </span>
HTML;

                        echo '<div class="drp-container">';
                        echo DateRangePicker::widget([
                            'name' => 'CdrCallsStatisticsSearch[calldate]',
                            'value' => isset($prevDate) ? ($prevDate) : ($monthAgo.' - '.$date),
                            'attribute' => 'calldate',
                            'convertFormat' => true,
                            'pluginOptions' => [
                                'opens' => 'left',
                                'ranges' => [
                                    "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                                    "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],
                                    "Last 30 Days" => ["moment().startOf('day').subtract(29, 'days')", "moment()"],
                                ],
                                'timePicker' => false,
                                'locale' => ['format' => 'Y-m-d']

                                //'timePicker' => true,
                                //'timePickerIncrement' => 05,
                                //'locale' => ['format' => 'Y-m-d']
                            ],
                            'presetDropdown' => false,
                            'hideInput' => true
                        ]);
                        echo '</div>';
                        
                
                        ?>
                    </div>
                </div>

                <div class="form-group" style="padding-left: 15px;">
                    <br>
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                    <!-- <button id="btnSearch" type="button" class="btn btn-primary">Search</button> -->
                    <button type="reset" class="btn btn-default">Reset</button>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<script>
    // $('#btnSearch').click(function() {
    //     var range = $('#dateRange').val();
    //     let from = range.slice(0, 10);
    //     let to = range.slice(14, 24);

    //     //console.log(from + "\n" + to);

    //     let link = "index?CdrCallsStatisticsSearch%5Bfrom%5D="+from+"&CdrCallsStatisticsSearch%5Bto%5D="+to;

    //     //console.log(range);

    //     location.href = link;
    // });
</script>


<!-- <div class="cdr-calls-statistics-search">

    <?php
    //$form = ActiveForm::begin([
    //    'action' => ['index'],
    //    'method' => 'get',
    //]); 
    ?>

    <?php //echo $form->field($model, 'id') 
    ?>

    <?php //echo $form->field($model, 'calldate') 
    ?>

    <?php //echo $form->field($model, 'total_calls') 
    ?>

    <?php //echo $form->field($model, 'ans_calls') 
    ?>

    <?php //echo $form->field($model, 'no_ans_calls') 
    ?>

    <?php // echo $form->field($model, 'entry_date') 
    ?>

    <?php // echo $form->field($model, 'call_type_id') 
    ?>

    <div class="form-group">
        <?php //echo Html::submitButton('Search', ['class' => 'btn btn-primary']) 
        ?>
        <?php //echo Html::resetButton('Reset', ['class' => 'btn btn-default']) 
        ?>
    </div>

    <?php //ActiveForm::end(); 
    ?>

</div> -->