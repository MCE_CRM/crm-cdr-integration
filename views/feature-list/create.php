<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FeatureList */

$this->title = 'Create Feature List';
$this->params['breadcrumbs'][] = ['label' => 'Feature Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="feature-list-create">

<!--    <h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
