<?php

namespace app\models;

use DateTime;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Projects;

/**
 * ProjectsSearch represents the model behind the search form of `app\models\Projects`.
 */
class ProjectsSearch extends Projects
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id','active', 'created_by', 'updated_by'], 'integer'],
            [['name', 'state', 'city', 'address', 'description', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Projects::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
		
            
			//$model2 = Projects::find()->Where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,project_show)'))->addParams([':cat_to_find' => Yii::$app->user->id])->all();
			$userroles = $this->getUserRoles(Yii::$app->user->id);
			//print_r($model2);
			if($userroles!='Admin' && $userroles!='SuperAdmin')
			{
				$query->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,project_show)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orWhere(['=','project_show', ''])->all();
			}
		if($this->created_on)
        {
            $mysql_date = $this->created_on;
            // date in Y-m-d format as MySQL stores it
            $date_obj = date_create_from_format('d/m/Y',$mysql_date);
            $created_on = date_format($date_obj, 'Y-m-d');

            $query->andFilterWhere(['like', 'created_on', $created_on]);
		}

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description]);


        return $dataProvider;
    }

    public function searchReport($params)
    {
        $query = Projects::find();



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);



        if ( ! is_null($params['date_range_1']) && strpos($params['date_range_1'], ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$params['date_range_1']);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:iA', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);



        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }




        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'active' => $this->active,
            //'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            //'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'state', $this->state])
            ->andFilterWhere(['like', 'city', $this->city])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
	function getUserRoles($user_id){
		$connection = \Yii::$app->db;
		$sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
		$command=$connection->createCommand($sql);
		$dataReader=$command->queryAll();
		$roles ='';
		if(count($dataReader) > 0){
			foreach($dataReader as $role){
				
				if($role['name']=='Admin' || $role['name']=='SuperAdmin')
				{
					return $role['name'];
					break;
				}
				
			}
		}
	}
}
