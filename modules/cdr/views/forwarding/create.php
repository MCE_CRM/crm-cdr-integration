<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Forwarding */

$this->title = 'Create Forwarding';
$this->params['breadcrumbs'][] = ['label' => 'Forwardings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="forwarding-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
