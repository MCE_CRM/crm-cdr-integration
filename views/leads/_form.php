<?php
exit;

use app\models\DefaultValueModule;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\leads */
/* @var $form yii\widgets\ActiveForm */
?>
<style>

    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }
    .form-control{
        padding:0.275rem 0.75rem;
    }

    .row{
        margin-bottom : -12px;
    }

    .coll{
        margin-bottom : -12px;
        margin-left: 0px;
        padding-right:0px;
    padding-left: 0px;
    }
    }
</style>

<?php

//Default Values

if($default_lead_status = DefaultValueModule::getDefaultValueId('lead-status'))
{
    $lead_status = \app\models\LeadStatus::findOne($default_lead_status);
    $model->lead_status = $lead_status->status;
}

if($default_lead_source = DefaultValueModule::getDefaultValueId('lead-source'))
{
    $lead_source = \app\models\LeadSource::findOne($default_lead_source);
    $model->lead_source = $lead_source->name;
}


if($default_lead_type = DefaultValueModule::getDefaultValueId('lead-type'))
{

    $lead_type = \app\models\LeadType::findOne($default_lead_type);
    $model->lead_type = $lead_type->type_name;
}

if(\Yii::$app->user->can('Agent'))
{
    $model->lead_assign  = Yii::$app->user->id;
}


$country = '';
if($model->isNewRecord)
{
    if($default_country = DefaultValueModule::getDefaultValueId('country'))
    {
        $c= \app\models\Country::findOne($default_country);
        $country = $c->country;
        $model->contact_country = $default_country;
    }
}
else
{
    $c= \app\models\Country::findOne($model->country);
    $country = $c->country;

    $s = \app\models\State::findOne($model->state);
    $state = $s->state;

    $ci = \app\models\City::findOne($model->city);
    $city = $ci->city;

}



?>




<section class="card">

    <div class="card-body">

        <?php

        if($_GET['id'])
        {
            $form = \yii\widgets\ActiveForm::begin([
                'id' => 'form',
                'enableAjaxValidation' => true,
                'validationUrl' => Yii::$app->homeUrl.'leads/validate?id='.$model->id.'',

            ]);
        }
        else
        {
            $form = \yii\widgets\ActiveForm::begin([
                'id' => 'form',
                'enableAjaxValidation' => true,
                'validationUrl' => Yii::$app->homeUrl.'leads/validate',
                'fieldConfig' => [
                    'template' => "{label}{input}<label class=\"error\">{error}</label>",
                    //'options' => ['class' => 'form-group'],
                    'labelOptions' => ['class' => 'control-label'],
                ],
                'options' => [
                    'class' => 'form-horizontal'
                ],
                'errorCssClass' => 'has-danger',
            ]);
        }



        ?>
        <section class="card card-primary" id="card-1" data-portlet-item>
            <header class="card-header portlet-handler">
                <h4 class="card-title">Lead Details</h4>
            </header>
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-8 ">
                        <?= $form->field($model, 'lead_title')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-4 ">
                        <?php

                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'lead_status')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\LeadStatus::find()->where(['active'=>1])->orderBy([
                                'sort_order' => SORT_ASC,
                            ])->all(),'status','status'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Select a Status ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);




                        ?>


                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-8 ">
                        <?= $form->field($model, 'lead_description')->textarea(['rows' => '8']) ?>
                    </div>
                    <div class="col-lg-4 ">

                        <div class="coll col-lg-12">
                            <?php

                            // Normal select with ActiveForm & model
                            echo $form->field($model, 'lead_type')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\LeadType::find()->where(['active'=>1])->orderBy([
                                    'sort_order' => SORT_ASC,
                                ])->all(),'type_name','type_name'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Select a Type ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);




                            ?>

                        </div>

                        <div class="coll col-lg-12">
                            <?php

                            // Normal select with ActiveForm & model
                            echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\User::find()->all(),'id','username'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Select a Source ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);




                            ?>

                        </div>
                        <div class="coll col-lg-12">
                            <?php

                            // Normal select with ActiveForm & model
                            echo $form->field($model, 'lead_source')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(\app\models\LeadSource::find()->where(['active'=>1])->orderBy([
                                    'sort_order' => SORT_ASC,
                                ])->all(),'name','name'),
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'options' => ['placeholder' => 'Select a Source ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                            ]);




                            ?>
                        </div>

                    </div>
                </div>

            </div>
        </section>

        <section class="card card-primary" id="card-1" data-portlet-item>
            <header class="card-header portlet-handler">


                <h4 class="card-title">Lead Property</h4>
            </header>
            <div class="card-body">

                <div class="row">
                    <div class="col-lg-4 ">
                        <?php


                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'project_id')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Projects::find()->where(['active'=>1])->all(),'id','name'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Select a Project ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);





                        ?>

                    </div>
                    <div class="col-lg-4 ">
                        <?= $form->field($model, 'property_id')->textInput() ?>

                    </div>
                    <div class="col-lg-4 ">
                        <?php

                        // usage without model
                        echo '<label class="control-label" for="leads-follow_up">Follow Up</label>';
                        echo DatePicker::widget([
                            'name' => 'check_issue_date',
                            //'value' => date('d-M-Y', strtotime('+2 days')),
                            'options' => ['placeholder' => 'Select issue date ...'],
                            'pluginOptions' => [
                                'format' => 'dd-M-yyyy',
                                'todayHighlight' => true
                            ]
                        ]);


                        ?>

                    </div>
                </div>

            </div>
        </section>

        <section class="card card-primary" id="card-1" data-portlet-item>
            <header class="card-header portlet-handler">


                <h4 class="card-title">Lead Contact Details</h4>
            </header>
            <div class="card-body">



                <div class="row">
                    <div class="col-lg-4 ">
                        <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>

                    </div>
                    <div class="col-lg-4 ">
                        <?= $form->field($model, 'contact_mobile')->textInput(['maxlength' => true]) ?>


                    </div>
                    <div class="col-lg-4 ">
                        <?= $form->field($model, 'contact_phone')->textInput(['maxlength' => true]) ?>


                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 ">
                        <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-lg-4 ">
                        <?=

                        $form->field($model, 'contact_country')->widget(Select2::classname(), [
                            'initValueText' => $country, // set the initial display text
                            'options' => ['placeholder' => 'Search for a Country ...'],
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                                'minimumInputLength' => 1,
                                'language' => [
                                    'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                ],
                                'ajax' => [
                                    'url' => \yii\helpers\Url::to(['ajax/country-list']),
                                    'dataType' => 'json',
                                    'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                ],
                                'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                            ],
                        ]);



                        ?>



                    </div>
                    <div class="col-lg-4 ">
                        <?php

                        echo $form->field($model, 'contact_state')->widget(DepDrop::classname(), [
                            'options'=>['id'=>'subcat-id'],
                            'type'=>DepDrop::TYPE_SELECT2,
                            'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],

                            'pluginOptions'=>[
                                'depends'=>['leads-contact_country'],
                                'initDepends' => ['leads-contact_country'],
                                'initialize'=>true,
                                'placeholder'=>'Select...',
                                'url'=>Url::to(['/ajax/get-country-state'])
                            ]
                        ]);

                        ?>


                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 ">

                        <?php

                        echo $form->field($model, 'contact_city')->widget(DepDrop::classname(), [
                            'options'=>['id'=>'subcat2-id'],
                            'type'=>DepDrop::TYPE_SELECT2,
                            'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],


                            'pluginOptions'=>[
                                'depends'=>['subcat-id'],

                                'placeholder'=>'Select...',
                                'url'=>Url::to(['/ajax/get-state-city'])
                            ]
                        ]);

                        ?>




                    </div>
                    <div class="col-lg-4 ">
                        <?= $form->field($model, 'contact_address')->textarea(['rows' => '3']) ?>

                    </div>
                    <div class="col-lg-4 ">
                        <?= $form->field($model, 'contact_address2')->textarea(['rows' => '3']) ?>


                    </div>
                </div>

            </div>
        </section>


    </div>
    <footer class="card-header text-center">



        <a href="<?= Yii::$app->homeUrl?>leads" class="mb-1 mt-1 mr-1 btn btn-danger">Cancel</a>

        <?= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary']) ?>

    </footer>

    <div class="property-form">


        <?php ActiveForm::end(); ?>


    </div>

</section>
