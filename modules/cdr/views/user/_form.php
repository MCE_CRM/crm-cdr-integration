<?php

use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="user-form">

    <?php

    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'user/validate':Yii::$app->homeUrl.'user/validate?id='.$model->id.'',
        
    ]);
    ?>


    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
        </div>
        <?php
        if ($model->isNewRecord) { ?>
            <div class="col-md-6">
                <?= $form->field($model, 'password')->textInput(['maxlength' => true]) ?>
            </div>

        <?php } else { ?>
            <div class="col-md-6">

                <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>
            </div>

        <?php } ?>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'InActive', ]) ?>
        </div>
    </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">

                <?php

                echo $form->field($model, 'role')->widget(Select2::classname(), [
                    'data' =>  ArrayHelper::map(\app\modules\rbac\models\AuthItem::find()->where(['type'=>1])->andWhere(['NOT IN','name',['Software Developer']])->all(), 'name', 'name'),
                    'options' => ['placeholder' => 'Select a Role ...','id' => 'user-role'],
                    'size' => Select2::SMALL,
                    'hideSearch' => true,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        // 'multiple' => true,
                    ],
                ]);




                ?>
            </div>
        </div>
        <div class="row" id="CSR">
            <div class="col-md-6">
                <?= $form->field($model, 'agent_id')->dropDownList(['2001'=>'2001','2002'=>'2002','2003'=>'2003','2004'=>'2004','2005'=>'2005'  ],['prompt'=>'Select Agent ID']) ?>
            </div>

        </div>


    <div class="row">
        <div class="col-md-12">
            <?= $form->field($model, 'about')->textarea(['maxlength' => true]) ?>
        </div>

    </div>





    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<script>

    $( document ).ready(function() {
        <?php if($model->role=="CSR"  )

        {?>
        $('#CSR').show();
        <?php }else { ?>

        $('#CSR').hide();

        <?php }?>
    });

    $('#user-role').change(function(){
        var role = $(this).val();
        if(role != "CSR" ){$('#CSR').hide();}else {$('#CSR').show();}
    })




</script>