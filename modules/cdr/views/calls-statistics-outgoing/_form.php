<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CdrCallsStatistics */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cdr-calls-statistics-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'calldate')->textInput() ?>

    <?= $form->field($model, 'total_calls')->textInput() ?>

    <?= $form->field($model, 'ans_calls')->textInput() ?>

    <?= $form->field($model, 'no_ans_calls')->textInput() ?>

    <?= $form->field($model, 'entry_date')->textInput() ?>

    <?= $form->field($model, 'call_type_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
