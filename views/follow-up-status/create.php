<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\FollowUpStatus */

$this->title = 'Create Follow Up Status';
$this->params['breadcrumbs'][] = ['label' => 'FollowUpStatus', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leadstatus-create">

 <!--   <h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
