<?php

namespace app\modules\cdr\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cdr\models\VoiceMail;

/**
 * VoiceMailSearch represents the model behind the search form of `app\models\VoiceMail`.
 */
class VoiceMailSearch extends VoiceMail
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'number_press', 'depart_id', 'status', 'created_by'], 'integer'],
            [['name', 'file', 'time_to', 'time_from', 'description', 'created_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = VoiceMail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'number_press' => $this->number_press,
            'depart_id' => $this->depart_id,
            'status' => $this->status,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'file', $this->file])
            ->andFilterWhere(['like', 'time_to', $this->time_to])
            ->andFilterWhere(['like', 'time_from', $this->time_from])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
