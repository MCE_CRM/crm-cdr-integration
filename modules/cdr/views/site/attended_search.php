<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use app\modules\cdr\models\Cdr;

/* @var $this yii\web\View */
/* @var $model app\models\CdrSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .col-md-1-5 {
        width: 12.49995%;
    }

</style>


<div class="card-body">

    <?php $form = ActiveForm::begin([
        'action' => ['attended-report'],
        'method' => 'get',
    ]); ?>


    <div class="row">
        <div class="col-md-2 col-md-1-5">
          <?= $form->field($model, 'uniqueid')->label('Caller ID',['class'=>'label-class']); ?>
        </div>

        <div class="col-md-2 col-md-1-5">
            <?= $form->field($model, 'channel')->label('Source',['class'=>'label-class']); ?>
        </div>

        <div class="col-md-2 col-md-1-5">
            <?= $form->field($model, 'dst')->label('Distination',['class'=>'label-class']); ?>
        </div>


        <div class="col-md-2 col-md-1-5">
            <?= $form->field($model, 'iorecording')
                ->dropDownList(
                    ['Outgoing' => 'outgoing', 'Incoming' => 'Incoming'],['prompt'=>'']
                )->label('Destination',['class'=>'label-class']); ?>
        </div>




        <div class="col-md-2">

              <?php
           $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;


           echo '<label class="control-label">Date Range</label>';
           echo '<div class="drp-container">';
           echo DateRangePicker::widget([
               'model'=>$model,
               'attribute'=>'calldate',
               'convertFormat'=>true,
               'pluginOptions'=>[
                   'opens'=>'left',
                   'ranges' => [

                       "Today" => ["moment().startOf('day')", "moment()"],
                       "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                       "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                   ],

                   'timePicker'=>true,
                   'timePickerIncrement'=>05,
                   'locale'=>['format'=>'d/m/Y h:i A']
               ],
               'presetDropdown'=>false,
               'hideInput'=>true
           ]);
           echo '</div>'; ?>
            
        </div>
        <div class="col-md-2 ">
           <br>

            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>

        </div>
       

        </div>


    </div>

  

    <!-- <?= $form->field($model, 'clid') ?> -->



<?php ActiveForm::end(); ?>


</div>





    



