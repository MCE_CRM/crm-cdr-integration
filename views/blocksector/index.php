<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;
use kartik\select2\Select2;
use app\models\User;


/* @var $this yii\web\View */
/* @var $searchModel app\models\BlocksectorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Blocksectors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="blocksector-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php /* Html::a('Create Blocksector', ['create'], ['class' => 'btn btn-success'])*/ ?>
    </p>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
           'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Block/sector'), [
                'create'
            ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                'class' => 'btn btn-default btn-sm add-new'
            ]),
            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            /*'id',*/
            'blocksector',
            [
                'attribute'=>'project_id',
                'value'=>'project.name',
                'format'=>'raw',
                'filter'=> Select2::widget([

                    'name' => 'BlocksectorSearch[project_id]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->project->name,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['project-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
            [
                'attribute'=>'city_id',
                'value'=>'city.city',
                'format'=>'raw',
                'filter'=> Select2::widget([

                    'name' => 'BlocksectorSearch[city_id]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->city->city,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['city-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
              [
                'attribute'=>'state_id',
                'value'=>'state.state',
                'format'=>'raw',
                'filter'=> Select2::widget([

                    'name' => 'BlocksectorSearch[state_id]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                    'value' => $searchModel->state->state,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['state-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
            [
                'attribute'=>'country_id',
                'value'=>'county.country',
                'format'=>'raw',
                'filter'=> Select2::widget([

                    'name' => 'BlocksectorSearch[country_id]',
                    'options' => ['placeholder' => 'All ...'],
                    'model' => $searchModel,
                   'value' => $searchModel->county->country,
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                        'minimumInputLength' => 1,
                        'language' => [
                            'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                        ],
                        'ajax' => [
                            'url' => \yii\helpers\Url::to(['country-list']),
                            'dataType' => 'json',
                            'data' => new JsExpression('function(params) { return {q:params.term}; }')
                        ],
                        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                        'templateResult' => new JsExpression('function(city) { return city.text; }'),
                        'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                    ],
                ]),
            ],
            [
                'attribute'=>'active',
                'width' => '125px',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => [
                    0 => 'Inactive',
                    1 => 'Active',
                ],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',
            ],
             [
                            'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                            'contentOptions' => ['class' => 'text-center'],
                            'filter'=>false,
                            'attribute' =>  'created_on',
                            'value' => function($model){
                                return date("d/m/Y g:i:s A", strtotime($model->created_on));
                            },
                            

            ],
             [
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'attribute' => 'created_by',
                'contentOptions' => ['class' => 'text-center'],
                'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update}{delete}',
                'buttons' => [
                    'update' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('blocksector/update')){
                            return '<a href="'.$url.'"><span class="fa fa-pencil-alt" ></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,
                    'delete' => function ($url, $model, $key) {
                        if (\Yii::$app->user->can('blocksector/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                    },
                   
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
 
</div>
