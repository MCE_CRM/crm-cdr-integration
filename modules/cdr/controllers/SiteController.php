<?php

namespace app\modules\cdr\controllers;

use app\modules\cdr\models\Durations;
use app\modules\cdr\models\Feedback;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\modules\cdr\models\LoginForm;
use app\modules\cdr\models\ContactForm;
use app\modules\cdr\models\UploadForm;
use yii\web\UploadedFile;
use app\modules\cdr\models\CdrSearch;
use app\modules\cdr\models\Cdr;
use app\modules\cdr\models\ServerStatus;
use DateTime;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['get'],
                ],
            ],
        ];
    }
 

    public function actionCdr()
    {

        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->renderPartial('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAttendedReport()
    {

        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->attendedreportsearch(Yii::$app->request->queryParams);
        return $this->renderPartial('attended', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
	
	 public function actionAllAgentReport()
    {

        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->allagentreportsearch(Yii::$app->request->queryParams);
        return $this->renderPartial('all_agent', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDisconnectedReport()
    {

        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->disconnectedreportsearch(Yii::$app->request->queryParams);
        return $this->renderPartial('disconnected', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionTransferReport()
    {

        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->transferreportsearch(Yii::$app->request->queryParams);
        return $this->renderPartial('transferred', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionIncoming()
    {

        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->incomingsearch(Yii::$app->request->queryParams);
        return $this->renderPartial('incoming', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);

    }


    public function actionOutgoing()
    {

        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->outgoingsearch(Yii::$app->request->queryParams);
        return $this->renderPartial('outgoing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionAgentStatus()
    {

        return $this->render('agent-status');

    }

    /**
     * {@inheritdoc}
     */

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }


   public function actionDownload()
  {
        $file=Yii::$app->request->get('file');
        $path=Yii::$app->request->get('path');
        $root=Yii::getAlias('@webroot').$path.$file;

        if (file_exists($root)) {

            return Yii::$app->response->sendFile($root);

        } else {

            throw new \yii\web\NotFoundHttpException("{$file} is not found!");
        }
  }


/*public function actionUpload()
    {
        $model = new UploadForm();

        if (Yii::$app->request->isPost) {
            $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            if ($model->upload()) {
                // file is uploaded successfully
                return;
            }
        }

        return $this->render('upload', ['model' => $model]);
    }*/
    /**
     * Displays homepage.
     *
   /*  */
   /* public function actionIndex()
    {
        $searchModel = new CdrSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }*/

    /**
     * Login action.
     *
     * 
     */

    public function actionLogin()
    {
        $this->layout='login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */

    public function actionSignup()
    {
        $this->layout='login';
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);

    }


    /**
     * Logout action.
     *
     * @return Response
     */

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,

        ]);
    }


    /**
     * Displays about page.
     *
     * @return string
     */

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionFeedback(){
        $model = new Feedback();
        $model->feedback_id = $_REQUEST['feedback_id'];
        $model->agent = $_REQUEST['agent'];
        $model->phone = $_REQUEST['phone'];
        $model->caller_id = $_REQUEST['caller_id'];
        $model->land_time = $_REQUEST['land_time'];
        $model->created_at = date('Y-m-d H:i:s');

        if($model->save()){
            return 1;
        }

    }


    public function actionDuration(){
        $model = new Durations();
        $model->land_time = $_REQUEST['land_time'];
        $model->attend_time = $_REQUEST['attend_time'];
        $model->time_out = $_REQUEST['time_out'];
        $model->phone = $_REQUEST['phone'];
        $model->caller_id = $_REQUEST['caller_id'];
        $model->status = $_REQUEST['status'];
        $model->created_on = date('Y-m-d H:i:s');
        if($model->save()){
            return 1;
        }
    }


    public function actionIndex()
    {
        $this->layout = 'dashboard.php';
		if(isset($_GET['Cdr']['calldate']) && !empty($_GET['Cdr']['calldate']))
		{
			list($start_date, $end_date) = explode(' - ',$_GET['Cdr']['calldate']);
			$start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
			$start_date = $start_date->format('Y-m-d H:i:s');
		
			$end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
			$end_date = $end_date->format('Y-m-d H:i:s');
		}else
		{
			$start_date = date('Y-m-d H:i:s');//date('Y-m-d', strtotime("-1 months", strtotime("NOW")));
			$end_date = date('Y-m-d H:i:s');
		}
		$model = new cdr;
       
            $totalcalls = Cdr::find()->select('uniqueid')->andWhere(['between', 'calldate', $start_date, $end_date])->distinct()->count();
			
            $totalcallsmonthly = Cdr::find()->select('uniqueid')->andWhere(['between', 'calldate', $start_date, $end_date])->distinct()->count();

            $totalincomming=Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Incoming'],
               ['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count();


            $totalincommingmonthly = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Incoming'],
                ['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count();


            $totaloutgoing = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Outgoing'],
                ['between', 'calldate', $start_date, $end_date]

            ])->distinct()->count();

            $totaloutgoingmonthly = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Outgoing'],
                ['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count();

            $totalnoanser = Cdr::find()->select('uniqueid')->where([
                'and',
				['between', 'calldate', $start_date, $end_date],
                ['like','disposition','NO ANSWER'],
				['like', 'iorecording','Incoming']
            ])->distinct()->count();
			
			$totalnoanserbycust = Cdr::find()->select('uniqueid')->where([
                'and',
				['between', 'calldate', $start_date, $end_date],
                ['like','disposition','NO ANSWER'],
				['like', 'iorecording','Outgoing']
            ])->distinct()->count();

            $totalnoansermonthly = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','disposition','NO ANSWER'],
                ['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count();

            $totaltransfer = Cdr::find()->select('internalcall')->where([
                'and',
                ['like','internalcall','Tranfered-call-from'],
                ['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count();

            $totaltransfermonthly = Cdr::find()->select('internalcall')->where([
                'and',
                ['like','internalcall','Tranfered-call-from'],
                ['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count();

            $voicemails = Cdr::find()->select('vmfilename')->where([
                'and',
                ['NOT LIKE','vmfilename','NULL'],
                ['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count();

            $forwaded = Cdr::find()->select('dcontext,uniqueid')->where([
                'and',
                ['NOT LIKE','dcontext','from-pstn'],
				['NOT LIKE','duration', '3'],
               	['between', 'calldate', $start_date, $end_date]
            ])->distinct()->count('DISTINCT(uniqueid)');
			
			$total_login = Cdr::find()->where([
                'and',
                ['LIKE','dst','*77'],
                ['between', 'calldate', $start_date, $end_date]
            ])->count('id');
			
			$total_logout = Cdr::find()->where([
                'and',
                ['LIKE','dst','*88'],
                ['between', 'calldate', $start_date, $end_date]
            ])->count('id');

            $sipstatus = ServerStatus::find()->select('status,ip')->orderBy(['id' => SORT_DESC])->one();

            return $this->render('adminDashboard',
                [
                    'totalcalls'=> $totalcalls,
                    'totalcallsmonthly'=> $totalcallsmonthly,
                    'totalincomming'=> $totalincomming,
                    'totalincommingmonthly'=> $totalincommingmonthly,
                    'totaloutgoing'=> $totaloutgoing,
                    'totaloutgoingmonthly'=>$totaloutgoingmonthly,
                    'totalbusy'=>$totalnoanser,
                    'totalbusymonthly'=>$totalnoansermonthly,
                    'totaltransfer'=>$totaltransfer,
                    'totaltransfermonthly'=>$totaltransfermonthly,
                    'voicemails'=>$voicemails,
                    'forwaded'=>$forwaded,
                    'sipstatus'=>$sipstatus->status,
                    'sipip'=>$sipstatus->ip,
					'totalnoanserbycust'=>$totalnoanserbycust,
					'totallogin'=>$total_login,
					'totallogout'=>$total_logout,
					'model'=>$model,

                ]);
        
    }
	
	public function actionUserDeshboard()
    {
        $this->layout = 'dashboard.php';
		
        if(\Yii::$app->user->can('site/adminDashboard')){
			 $searchModel = new CdrSearch();
			 $user_id ='';
			 $start_date = date('Y-m-d');
			 $end_date = date('Y-m-d');
			 if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']))
			 {
			 	$user_id = $_GET['CdrSearch']['channelsearch'];
			 }
			 
			 if(isset($_GET['CdrSearch']['calldate']) && !empty($_GET['CdrSearch']['calldate']))
			 {
				 	list($start_date, $end_date) = explode(' - ',$_GET['CdrSearch']['calldate']);
					$start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
					$start_date = $start_date->format('Y-m-d H:i:s');
		
					$end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
					$end_date = $end_date->format('Y-m-d H:i:s');
			 }else
			 {
				$start_date = date('Y-m-d', strtotime("-1 months", strtotime("NOW")));
				$end_date = date('Y-m-d H:i:s');
			}
			if($user_id !='all')
			{
			   $query1 = Cdr::find()->select('*')->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['NOT LIKE','dst', 'for'])->andWhere(['!=','channel', ''])->andWhere(['!=','dstchannel', ''])->andWhere(['IS NOT','iorecording', NULL])->andWhere(['between', 'calldate', $start_date, $end_date]) ->andWhere(['or',['like', 'channel', $user_id],['like', 'dstchannel', $user_id]])->orderBy(['id' => SORT_DESC])->all();
			}else
			{
				$query1 = Cdr::find()->select('*')->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['NOT LIKE','dst', 'for'])->andWhere(['!=','channel', ''])->andWhere(['!=','dstchannel', ''])->andWhere(['IS NOT','iorecording', NULL])->andWhere(['between', 'calldate', $start_date, $end_date])->orderBy(['id' => SORT_DESC])->all();
			}
	$totalcalls =0;
	$totaloutgoing =0;
	$totalincomming =0;
	$agent_Answer_incoming =0;
	$agent_drop_incoming =0;
	$agent_Answer_outgoing =0;
	$agent_drop_outgoing =0;
	  foreach($query1 as $res)
	  {
		  $calldate = date('d-m-Y',strtotime($res['calldate']));
		  $status = explode('-',$res['iorecording']);
		 // $model1 = new User();	
		  if ($status[0]=='Incoming') {
			  $myArray2 = explode('-', $res['dstchannel']);
			  $myArray3 = explode('/', $myArray2[0]);
			  
			  if($myArray3[1]!='SIPT')
			  {
				  $totalincomming=$totalincomming+1; 
				  $totalcalls = $totalcalls+1;
				  if($res['disposition']=='ANSWERED')
				  {
					  $agent_Answer_incoming = $agent_Answer_incoming+1;
				  }
				  if($res['disposition']=='BUSY' || $res['disposition']==='NO ANSWER' || $res['disposition']==='FAILED')
				  {
					  $agent_drop_incoming = $agent_drop_incoming+1;
				  }
			  }
		  }
		  else if ($status[0]=='Outgoing')
		  {
			  $myArray = explode('-', $res['channel']);
			  $myArray1 = explode('/', $myArray[0]);
			  if($myArray1[1]!='SIPT')
			  {
						  $totaloutgoing=$totaloutgoing+1;
						  $totalcalls = $totalcalls+1;
						  if($res['disposition']=='ANSWERED')
						  {
							  $agent_Answer_outgoing = $agent_Answer_outgoing+1;
						  }
						  if($res['disposition']=='BUSY' || $res['disposition']==='NO ANSWER' || $res['disposition']==='FAILED')
						  {
							  $agent_drop_outgoing = $agent_drop_outgoing+1;
						  }
					  }
		  }
			  
	  }
	  	
		if($user_id!='all')
		{
       		$totaltransfer = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','internalcall','Tranfered-call-from'],
                ['between', 'calldate', $start_date, $end_date],
                ['dst'=>$user_id]
            ])->distinct()->count();
		}else
		{
			$totaltransfer = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','internalcall','Tranfered-call-from'],
                ['between', 'calldate', $start_date, $end_date],
            ])->distinct()->count();
		}
            
            $sipstatus = ServerStatus::find()->select('status,ip')->orderBy(['id' => SORT_DESC])->one();

            return $this->render('userwisedeshboard',
                [
					'searchModel' => $searchModel,
                    'totalcalls'=>$totalcalls,
                    'totalincomming'=> $totalincomming,
                    'totaloutgoing'=> $totaloutgoing,
                    'agent_Answer_incoming'=> $agent_Answer_incoming,
                    'agent_drop_incoming'=> $agent_drop_incoming,
                    'agent_Answer_outgoing'=> $agent_Answer_outgoing,
                    'totaltransfer'=> $totaltransfer,
                    'agent_drop_outgoing'=> $agent_drop_outgoing,
                    'sipstatus'=> $sipstatus->status,
                    'sipip'=> $sipstatus->ip
                ]
            );

        }
        elseif(\Yii::$app->user->can('site/userDashboard')){

            $totalcalls=Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','calldate',date('Y-m-d')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();

            $totalcallsmonthly = Cdr::find()->select('uniqueid')->where([
                'and',
                ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();

            $totalincomming = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Incoming'],
                ['like','calldate',date('Y-m-d')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();

            $totalincommingmonthly = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Incoming'],
                ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();


            $totaloutgoing = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Outgoing'],
                ['like','calldate',date('Y-m-d')],
                ['src'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();


            $totaloutgoingmonthly = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Outgoing'],
                ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                ['src'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();


            $totalnoanser = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','disposition','NO ANSWER'],
                ['like','calldate',date('Y-m-d')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();

            $totalnoansermonthly = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','disposition','NO ANSWER'],
                ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();

            $totaltransfer = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','internalcall','Tranfered-call-from'],
                ['like','calldate',date('Y-m-d')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();


            $totaltransfermonthly = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','internalcall','Tranfered-call-from'],
                ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                ['dst'=>Yii::$app->user->identity->agent_id]
            ])->distinct()->count();

            $sipstatus = ServerStatus::find()->select('status,ip')->orderBy(['id' => SORT_DESC])->one();

            return $this->render('userDashboard',
                [
                    'totalcalls'=>$totalcalls,
                    'totalcallsmonthly'=>$totalcallsmonthly,
                    'totalincomming'=> $totalincomming,
                    'totalincommingmonthly'=> $totalincommingmonthly,
                    'totaloutgoing'=> $totaloutgoing,
                    'totaloutgoingmonthly'=> $totaloutgoingmonthly,
                    'totalbusy'=> $totalnoanser,
                    'totalbusymonthly'=> $totalnoansermonthly,
                    'totaltransfer'=> $totaltransfer,
                    'totaltransfermonthly'=> $totaltransfermonthly,
                    'sipstatus'=> $sipstatus->status,
                    'sipip'=> $sipstatus->ip
                ]
            );

        }else
        {
            $searchModel = new CdrSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            return $this->renderPartial('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }
    }

    public function actionUserProformance()
    {
        echo 'in';
    }


    public function actionClockin()
    {
        echo 'in';

    }

    public function actionClockout()
    {
        echo 'out';

    }

    public function actionProficiancy(){

        $data[] =  Array();
        for($i=0;$i<=date('d');$i++){

            $val = str_pad($i, 2, "0", STR_PAD_LEFT);

            $totaltransfer = Cdr::find()->select('uniqueid')->where([
                'and',
                ['like','internalcall','Tranfered-call-from'],

                ['like','calldate',date('Y-m-'.$val)]

            ])->distinct()->count();

            //$date1 = date('Y-m-'.$val.' 00:00:00');
            //$data[$i]['date'] = date('D M d Y 00:00:00',strtotime($date1)).'GMT+0500 (Pakistan Standard Time)';

            $data[$i] = (double)$totaltransfer;

           // array_push($data, 'date'=> date('Y-m-'.$val.' H:i:s'), 'value' => $totaltransfer );
        }

        //echo print_r($data);
        return  json_encode($data);
    }

}
