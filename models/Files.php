<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "files".
 *
 * @property int $id
 * @property string $file_name
 * @property int $project_id
 * @property int $property_id
 * @property int $lead_id
 * @property int $task_id
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 * @property string $custom_name
 * @property string $type
 * @property string $size
 */
class Files extends \yii\db\ActiveRecord
{

    public $file;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'files';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file_name', 'created_on', 'created_by'], 'required'],
            [['project_id','lead_id','property_id','task_id', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['file_name'], 'string', 'max' => 100],
            [['custom_name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 50],
            [['size'], 'string', 'max' => 20],
            [['file'], 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'project_id' => 'Project ID',
            'property_id' => 'Project ID',
            'lead_id' => 'Lead ID',
            'task_id'=>'Task ID',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'custom_name' => 'Custom Name',
            'type' => 'Type',
            'size' => 'Size',
            'file'=>'File',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
}
