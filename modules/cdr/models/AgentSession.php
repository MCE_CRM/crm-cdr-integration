<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "agent_session".
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $created_on
 * @property int $created_by
 */
class AgentSession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agent_session';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'created_on', 'created_by'], 'required'],
            [['created_on'], 'safe'],
            [['created_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['type'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'type' => 'Type',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
        ];
    }
}
