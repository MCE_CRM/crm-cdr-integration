<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use kartik\editable\Editable;
use kartik\grid\GridView;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Property */

$this->title = $model->property_title;
$this->params['breadcrumbs'][] = ['label' => 'Properties', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php  Pjax::begin(['timeout' => '30000']); ?>
<div class="property-view">

    <div class="row">
        <div class="col-lg-12">
            <div class="tabs">
                <ul class="nav nav-tabs">
                    <li class="nav-item active">
                        <a class="nav-link" href="#information" data-toggle="tab">Information</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#extra_charges" data-toggle="tab">Extra Charges</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#feature" data-toggle="tab">Features</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#files" data-toggle="tab">Files</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div id="information" class="tab-pane active">

                        <div class="row">
                            <div class="col-md-6">

                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        //'id',
                                        'property_title',
										'member_ship_no',
										'property_location_type',
                                        'project.name',
                                        'block.blocksector',
                                        'purpose',
                                        'property_type',
                                        'description',
                                        [
                                            'attribute'=>'created_on',
                                            'label'=>'Added By',
                                            'value'=>function($model)
                                            {
                                                $user = \app\models\User::find()->where(['id'=>$model->created_by])->one();
                                                return $user->username;
                                            }
                                        ],


                                        [
                                            'attribute'=>'created_by',
                                            'label'=>'Added On',
                                            'format'=>'raw',
                                            'value'=>function($model)
                                            {
                                                return app\helpers\Helper::DateTime($model->created_on);
                                            }
                                        ],

                                    ],
                                ]) ?>

                            </div>
                            <div class="col-md-6">

                                <?= DetailView::widget([
                                    'model' => $model,
                                    'attributes' => [
                                        //'id',
                                        'land_area',
                                        'land_area_unit',
                                        'price_per',
                                        'price',
                                        'extra_charges',
                                        'total_price',
                                        'status',
                                    ],
                                ]) ?>


                            </div>
                        </div>


                    </div>
                    <div id="extra_charges" class="tab-pane">

                        <table id="w0" class="table table-striped table-bordered detail-view">
                            <tbody>


                        <?php

                        $extra_charges = \app\models\ExtraCharges::find()->where(['type'=>2])->andWhere(['property_id'=>$model->id])->all();
                        foreach ($extra_charges as $charge)
                        {?>
                            <tr><th><?= $charge->name?></th><td><?= $charge->charges?><?php if($charge->value)echo '('.$charge->value.')'?></td></tr>
                        <?php } ?>

                            </tbody>

                        </table>


                    </div>

                    <div id="feature" class="tab-pane">

                        <div class="row">

                        <?php

                        $foo = explode(',', $model->additional_form);
                        if($foo){
                            foreach ($foo as $key){


                                ?>

                                <div class="col-lg-6">
                                    <section class="card">
                                        <header class="card-header">

                                            <h2 class="card-title" style="color:black"><?= \app\models\FeatureForm::findOne($key)->name ?></h2>
                                        </header>
                                        <div class="card-body">

                                            <table id="w0" class="table table-striped table-bordered detail-view">
                                                <tbody>


                                                <?php

                                                $extra_charges = \app\models\ExtraCharges::find()->where(['type'=>1])->andWhere(['property_id'=>$model->id])->andWhere(['feature_form_id'=>$key])->all();
                                                foreach ($extra_charges as $charge)
                                                {?>
                                                    <tr><th><?= $charge->name?></th><td><?= $charge->value?></td></tr>
                                                <?php } ?>

                                                </tbody>

                                            </table>

                                        </div>
                                    </section>
                                </div>

                            <?php }
                        }




                        ?>


                        </div>


                    </div>

                    <div id="files" class="tab-pane">

                        <?php


                        $filesModel = \app\models\Files::find()->where(['property_id'=>$model->id])->all();

                        ?>
                        <style>
                            .table > thead > tr > td.info, .table > tbody > tr > td.info, .table > tfoot > tr > td.info, .table > thead > tr > th.info, .table > tbody > tr > th.info, .table > tfoot > tr > th.info, .table > thead > tr.info > td, .table > tbody > tr.info > td, .table > tfoot > tr.info > td, .table > thead > tr.info > th, .table > tbody > tr.info > th, .table > tfoot > tr.info > th{
                                color:#000 !important;
                                background-color: white !important;
                            }
                        </style>

                        <div class="row">
                            <div class="col-md-1">

                            </div>
                            <div class="col-md-10">
                            <?php
                                $form = ActiveForm::begin([
                                    'method' => 'get',
                                    'action' => Url::to(['files/delete-all']),
                                 ]); 
                            ?>
                                <table class="table table-responsive-lg table-bordered table-striped table-sm mb-0">
                                    <thead>
                                    <tr>
                                        <th>Sno</th>
                                        <th>FileName</th>
                                        <th>Title</th>
                                        <th>Type</th>
                                        <th>Size</th>
                                        <th>Added On</th>
                                        <th>Added By</th>
                                        <th class="text-right">Action</th>
                                        <th class="text-center">Delete All</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $i = 0;
                                    foreach ($filesModel as $files)
                                    {
                                        $i++;

                                        ?>
                                        <tr>

                                            <td><?= $i?></td>
                                            <td><?= $files->file_name?></td>
                                            <td><?= $files->custom_name?></td>
                                            <td><?= $files->type?></td>
                                            <td><?= \app\helpers\Helper::getSize($files->size) ?></td>
                                            <td><?= \app\helpers\Helper::DateTime($files->created_on) ?></td>
                                            <td><?= \app\helpers\Helper::getCreated($files->created_by)?></td>
                                            <td class="actions">
                                            
                                            <a href="<?= \app\helpers\Helper::getBaseUrl()?>files/property<?= $model->id?>/<?= $files->file_name?>" class="html5lightbox"><i class="fas fa-play"></i></a>

                                            <a href="<?= \app\helpers\Helper::getBaseUrl()?>files/property<?= $model->id?>/<?= $files->file_name?>" download><i class="fas fa-download"></i></a>
                                                    
                                            <a href="javascript:void(0)" onclick="edit(<?= $files->id ?>,'<?= $files->custom_name ?>')" ><i class="fas fa-pencil-alt"></i></a>
                                                <a href="javascript:void(0)" onclick="del(<?= $files->id ?>)"><i class="far fa-trash-alt"></i></a>
                                            </td>
                                            <td class="text-center"><input type="checkbox" id="checkItem" name="check[]" value="<?php echo  $files->id; ?>"></td>
	
                                        </tr>

                                    <?php } ?>

                                    </tbody>
                                </table>
                                <input type='submit' name='delete' value='Delete' style=" margin-left: 95%;">
                                <?php ActiveForm::end(); ?>
                            </div>
                            <div class="col-md-1">

                            </div>
                        </div>

                        <script>

                            function edit(id,custom ) {
                                bootbox.prompt("Update", function(result){

                                    if(result !== custom)
                                    {
                                        $.ajax({
                                            url: "<?= Yii::$app->homeUrl?>files/update",
                                            type: "POST",
                                            data: { name: result,id:id},
                                            success: function(result){

                                                if(result==true)
                                                {
                                                    bootbox.hideAll();
                                                    window.location.reload();
                                                   // $.pjax.reload({container:'#p0'});
                                                }

                                            }});

                                    }

                                });

                                $('.bootbox-input').val(custom);


                            }

                            function del(id) {

                                bootbox.confirm({
                                    message: "Are You Sure Delete This?",
                                    buttons: {
                                        confirm: {
                                            label: 'OK',
                                            className: 'btn btn-primary'
                                        },
                                        cancel: {
                                            label: 'Cancel',
                                            className: 'btn btn-default'
                                        }
                                    },
                                    callback: function (result) {
                                        if(result==true)
                                        {
                                            $.ajax({
                                                url: "<?= Yii::$app->homeUrl?>files/delete",
                                                type: "POST",
                                                data: {id:id},
                                                success: function(result){

                                                    if(result==true)
                                                    {
                                                        bootbox.hideAll();
                                                        window.location.reload();
                                                        //$.pjax.reload({container:'.property-view',timeout: false});
                                                    }

                                                }});


                                        }
                                    }
                                });


                            }




                        </script>


                    </div>


                </div>
            </div>
        </div>

    </div>

</div>