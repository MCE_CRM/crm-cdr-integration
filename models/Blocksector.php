<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blocksector".
 *
 * @property string $id
 * @property string $block-sector
 * @property string $project_id
 * @property string $city_id
 * @property string $state_id
 * @property string $country_id
 * @property int $active
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property string $updated_by
 */
class Blocksector extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'blocksector';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['blocksector'/*, 'city_id', 'state_id'*/,'project_id','created_on', 'created_by'], 'required'],
            [['city_id', 'state_id', 'country_id', 'active', 'created_by'], 'integer'],
            [['created_on', 'updated_on', 'updated_by'], 'safe'],
            [['blocksector'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'blocksector' => 'Block/Sector',
            'project_id'=>'Project',
            'city_id' => 'City',
            'state_id' => 'State',
            'country_id' => 'Country',
            'active' => 'Active',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getCounty()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }
     public function getState()
    {
        return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
     public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }
     public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
     public function getProject()
    {
        return $this->hasOne(Projects::className(), ['id' => 'project_id']);
    }
}
