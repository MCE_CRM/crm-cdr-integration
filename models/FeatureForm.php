<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "feature_form".
 *
 * @property int $id
 * @property string $name
 * @property string $show_on
 * @property int $active
 * @property int $sort_order
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class FeatureForm extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feature_form';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name','show_on', 'active', 'sort_order', 'created_on', 'created_by'], 'required'],
            [['show_on'], 'string'],
            [['name'], 'unique'],
            [['active', 'sort_order', 'created_by', 'updated_by'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'active' => 'Active',
            'show_on' => 'Show On',
            'sort_order' => 'Sort Order',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }
    public function getFormlist()
    {
        return $this->hasMany(FeatureList::className(), ['feature_form_id' => 'id']);

    }
}
