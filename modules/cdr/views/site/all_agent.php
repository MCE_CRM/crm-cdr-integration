  <?php
  
  /* @var $this \yii\web\View */
  /* @var $content string */
  
  use app\widgets\Alert;
  use yii\helpers\Html;
  use yii\bootstrap\Nav;
  use yii\bootstrap\NavBar;
  use yii\widgets\Breadcrumbs;
  use app\modules\cdr\assets\AppAsset;
  use kartik\grid\GridView;
  use yii\widgets\Pjax;
  use kartik\editable\Editable; 
  use kartik\select2\Select2;
  use yii\helpers\ArrayHelper;
  use app\modules\cdr\models\User;
  use app\modules\cdr\models\Projects;
  use yii\web\JsExpression;
  use app\modules\cdr\models\Blocksector;
  use app\modules\cdr\models\CDR;
  
  AppAsset::register($this);
  $this->title = 'All Agent-Wise CDR Periodic Statistics (Incoming, Outgoing & Dropped Call) Report';
  $this->params['breadcrumbs'][] = $this->title;
  ?>
  <?php $this->beginPage() ?>
  
  <style>
	  .panel-heading-search {
		  background-color: #1ab394;
		  border-color: #1ab394;
		  text-align: center;
		  margin: 0 auto;
		  color: #fff;
		  padding: 10px 15px;
		  border-bottom: 1px solid transparent;
		  border-top-left-radius: 3px;
		  border-top-right-radius: 3px;
	  }
  
  </style>
  <!DOCTYPE html>
  <html lang="<?= Yii::$app->language ?>">
  <head>
	  <meta charset="<?= Yii::$app->charset ?>">
	  <meta http-equiv="X-UA-Compatible" content="IE=edge">
	  <meta name="viewport" content="width=device-width, initial-scale=1">
	 
	  <title><?= Html::encode($this->title) ?></title>
	  <?php $this->head() ?>
	

  </head>
  <body class="top-navigation boxed-layout skin-1">
  
  <?php $this->beginBody() ?>
  
  <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	   aria-hidden="true">
	  <div class="modal-dialog" role="document">
		  <div class="modal-content">
			  <div class="modal-header text-center">
				  <h4 class="modal-title w-100 font-weight-bold">Comments</h4>
				  <button type="button" class="close" data-dismiss="modal" id="close" aria-label="Close">
					  <span aria-hidden="true">&times;</span>
				  </button>
			  </div>
			  <div class="modal-body mx-3" style="overflow-y: hidden !important;height: auto !important;">
				  <h4 id="appartementtitle"></h4>
				  <div class="md-form mb-5">
					  <i class="fa fa-envelope prefix grey-text"></i>
					  <textarea cols="60" rows="5"id="desc" style="padding-left:4px;width: 100%;"></textarea>
					  <label data-error="wrong" data-success="right" for="defaultForm-email">Your Comments</label>
				  </div>
  
			  </div>
			  <div class="modal-footer d-flex justify-content-center">
				  <button class="btn btn-default" id="savedesc">Save</button>
				  <button class="btn btn-default updatedescription"  style="display: none;">Update</button>
			  </div>
		  </div>
	  </div>
  </div>
  <div class="inner-wrapper" >
  
 
  <div class="card-body">

    <div class="follow-up-search">

         <header class="page-header" style="margin-top: 0.000001px;" >
                <h2 style="padding-left: 3%;">All Agent-Wise CDR Periodic Statistics </h2>


               
            </header>

            <?php echo \Yii::$app->view->renderFile(Yii::getAlias('@app') . '/views/layouts/header_menu.php'); ?>
     
       
                    <div class="content-boody">
                        <div class="row">
                            <div class="col-lg-11 col-xl-11" style="margin-left:4%">
                      
                                  <?php //if(Yii::$app->user->can('dashboard/search')){?>
                                                    
                                                      
                                                      <div class="panel-body" > <?php   echo $this->render('all_agent_search', ['model' => $searchModel]); ?>
                                                        </div>
                                                             <?php
                                           // }
                                    ?>

                                    <div class="col-md-11" style="margin-left:5%">
						   <?php
		  //}
		  $end_date='';
		  $start_date ='';
        if( ! is_null($_GET['CdrSearch']['calldate']) && strpos($_GET['CdrSearch']['calldate'], ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ',$_GET['CdrSearch']['calldate']);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');
            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            
        }else
		{
			$start_date = date('Y-m-d', strtotime("-1 months", strtotime("NOW")));
            $end_date = date('Y-m-d H:i:s');
		}
		$agent_id = '';
		if(isset($_GET['CdrSearch']['agent_id']) && !empty($_GET['CdrSearch']['agent_id']) )
			{
				$agent_id = $_GET['CdrSearch']['agent_id'];
				//$query->andWhere(['LIKE','channel', $agent_id])->orWhere(['LIKE','dstchannel', $agent_id])->all();
				
			}
		  $query1 = Cdr::find()->select('*')->where(['NOT LIKE','dst', '*77'])->andWhere(['NOT LIKE','dst', '*88'])->andWhere(['NOT LIKE','dst', 'for'])->andWhere(['!=','channel', ''])->andWhere(['!=','dstchannel', ''])->andWhere(['IS NOT','iorecording', NULL])->andWhere(['between', 'calldate', $start_date, $end_date]) ->andWhere(
        ['or',
            ['like', 'channel', $agent_id],
            ['like', 'dstchannel', $agent_id]

        ]
        )->orderBy(['id' => SORT_DESC])->all();
	
		 
	  $all_agent_list = array();
	  
	  $agent_all_calls = array();
	  $agent_incoming =  array();
	  $agent_Answer_incoming =  array();
	  
	  $agent_drop_incoming =  array();
	  $agent_outgoing =  array();
	  $agent_Answer_outgoing =  array();
	  $agent_drop_outgoing =  array();
	  
	  /*echo '<pre>';
	  print_r($query1);
	  echo '</pre>';*/
	  foreach($query1 as $res)
	  {
		  $calldate = date('d-m-Y',strtotime($res['calldate']));
		  $status = explode('-',$res['iorecording']);
		  $model1 = new User();	
		  if ($status[0]=='Incoming') {
			  $myArray2 = explode('-', $res['dstchannel']);
			  $myArray3 = explode('/', $myArray2[0]);
			  if($myArray3[1]!='SIPT')
			  {
				  $all_agent_list[$calldate][$myArray3[1]] = $calldate;
				  $agent_incoming[$calldate][$myArray3[1]]=$agent_incoming[$calldate][$myArray3[1]]+1; 
				  $agent_all_calls[$calldate][$myArray3[1]] = $agent_all_calls[$calldate][$myArray3[1]]+1;
				  if($res['disposition']=='ANSWERED')
				  {
					  $agent_Answer_incoming[$calldate][$myArray3[1]] = $agent_Answer_incoming[$calldate][$myArray3[1]]+1;
				  }
				  if($res['disposition']=='BUSY' || $res['disposition']==='NO ANSWER' || $res['disposition']==='FAILED')
				  {
					  $agent_drop_incoming[$calldate][$myArray3[1]] = $agent_drop_incoming[$calldate][$myArray3[1]]+1;
				  }
			  }
		  }
		  else if ($status[0]=='Outgoing')
		  {
			  $myArray = explode('-', $res['channel']);
			  $myArray1 = explode('/', $myArray[0]);
			  if($myArray1[1]!='SIPT')
			  {
						  $all_agent_list[$calldate][$myArray1[1]] = $calldate;
						  $agent_outgoing[$calldate][$myArray1[1]]=$agent_outgoing[$calldate][$myArray1[1]]+1;
						  $agent_all_calls[$calldate][$myArray1[1]] = $agent_all_calls[$calldate][$myArray1[1]]+1;
						  if($res['disposition']=='ANSWERED')
						  {
							  $agent_Answer_outgoing[$calldate][$myArray1[1]] = $agent_Answer_outgoing[$calldate][$myArray1[1]]+1;
						  }
						  if($res['disposition']=='BUSY' || $res['disposition']==='NO ANSWER' || $res['disposition']==='FAILED')
						  {
							  $agent_drop_outgoing[$calldate][$myArray1[1]] = $agent_drop_outgoing[$calldate][$myArray1[1]]+1;
						  }
					  }
		  }
			  
	  }
	  /*echo '<pre>';
	  print_r($all_agent_list);
	  echo '</pre>';
	  exit;*/
  ?>
  
  <div id="w1-container" class="table-responsive kv-grid-container">
	
		  <table class="kv-grid-table table table-hover table-bordered table-striped" id="item">
			  <thead>
				  <tr>
					  <th>Call Date</th>
					  <th>Agent Id</th>
					  <th>Agent</th>
					  <th>Total Incoming Calls</th>
					  <th>Answered Calls (Incoming)</th>
					  <th>NotAnswered/Dropped Calls (Incoming)</th>
					  <th>Total OutGoing (Dailled) Calls</th>
					  <th>Answered Call By Customer (OutGoing)</th>
					  <th>Not Answered Call By Customer (OutGoing)</th>
					  <th>All Calls Total (Incoming+Outgoing)</th>
				   </tr>
			   </thead>
			  <tbody>
				  <?php
				  $total_agent_incoming =0;
				  $total_agent_Answer_incoming =0;
				  $total_agent_drop_incoming =0;
				  $total_agent_outgoing =0;
				  $total_agent_Answer_outgoing =0;
				  $total_agent_drop_outgoing =0;
				  $total_agent_all_calls=0;
				  foreach($all_agent_list as $key=>$res)
				  {
					  foreach($res as $key1=>$res1)
					  {
						  $agent = $model1->agentname($key1)->first_name;
						  if(!empty($agent))
						  {
				  ?>
					  <tr>
						  <td><?php echo $key;?></td>		
						  <td><?php echo $key1?></td>
						  <td><?php echo $model1->agentname($key1)->first_name;?></td>
						  <td><?php if(empty($agent_incoming[$key][$key1])){ echo '0';}
						  			else{ echo $agent_incoming[$key][$key1]; $total_agent_incoming +=$$agent_incoming[$key][$key1];}?> 
                          </td>
                                    
						  <td><?php if(empty($agent_Answer_incoming[$key][$key1])){ echo '0';}
						  			else{echo $agent_Answer_incoming[$key][$key1]; $total_agent_Answer_incoming +=$agent_Answer_incoming[$key][$key1];}?> 
                          </td>
                          
						  <td><?php if(empty($agent_drop_incoming[$key][$key1])){ echo '0';}
						 			else{ echo $agent_drop_incoming[$key][$key1]; $total_agent_drop_incoming +=$agent_drop_incoming[$key][$key1];}?> 
                          </td>
						  <td><?php if(empty($agent_outgoing[$key][$key1])){ echo '0';}
						  			else{ echo $agent_outgoing[$key][$key1]; $total_agent_outgoing +=$agent_outgoing[$key][$key1];}?> 
                          </td>
						  <td><?php if(empty($agent_Answer_outgoing[$key][$key1])){echo '0';}
						  			else{echo $agent_Answer_outgoing[$key][$key1]; $total_agent_Answer_outgoing +=$agent_Answer_outgoing[$key][$key1];}?> 
                          </td>
						  <td><?php if(empty($agent_drop_outgoing[$key][$key1])){ echo '0';}
						  			else{echo $agent_drop_outgoing[$key][$key1]; $total_agent_drop_outgoing +=$agent_drop_outgoing[$key][$key1];}?> 
                          </td>
						  <td><?php echo $agent_all_calls[$key][$key1]; $total_agent_all_calls +=$agent_all_calls[$key][$key1];?> </td>
					  </tr> 
				<?php
						  }
					  }
				}
				?>
                		<tr>
						  <td><?php echo 'Total';?></td>		
						  <td><?php echo '-'?></td>
						  <td><?php echo '-';?></td>
						  <td><?php echo $total_agent_incoming;?> </td>
						  <td><?php echo $total_agent_Answer_incoming;?> </td>
						  <td><?php echo $total_agent_drop_incoming;?> </td>
						  <td><?php echo $total_agent_outgoing;?> </td>
						  <td><?php echo $total_agent_Answer_outgoing;?> </td>
						  <td><?php echo $total_agent_drop_outgoing;?> </td>
						  <td><?php echo $total_agent_all_calls;?> </td>
					  </tr>
				</tbody>
			</table>
  
  </div>
  
								  </div>
							  </div>
						  </div>
  
					  </div>
				  </div>
			  </div>
  
	 
		  </div>
	  </div>
  <style>
	  .table-responsive > .table-bordered {
  
		  font-size: 12px;
	  }
  </style>
  
  
  
  
  
  
  <style>
  .highlight{
	background-color: #1ab394;
  
  }
  table th
  {
	  font-size:12px;
	  border:1px solid;
	  text-align:center;
  }
  </style>
  <style type="text/css" media="screen">
	body {
	  overflow-scrolling:auto;
    }
	.dataTables_filter
	{
	  text-align:right !important;
	}
	table.dataTable
	{
		width:98% !important;
	}
  </style>
  
  
  
		  <?php $this->endBody() ?>
  </body>
  </html>
  <?php $this->endPage() ?>
  
  <script>
  
	  $('.panel-heading').hide();
  
  </script>
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
<script type="text/javascript" src= "https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
 
<script type="text/javascript">
    $(document).ready(function() {
    $('#item').DataTable( {
		 "ordering": false,
		"pageLength": 25,
        dom: 'Bfrtip',
        buttons: [
            {extend: 'copy',text: 'Copy',},
            {extend: 'csv',text: 'Export CSV'},
            {extend: 'excel',text: 'Export Excel'},
            {extend: 'pdf',text: 'Export PDF',exportOptions: {columns: ':visible'}, pageSize: 'A2'},        
            {extend: 'print',text: 'Print'}
        ],
    } );
} );
    </script>