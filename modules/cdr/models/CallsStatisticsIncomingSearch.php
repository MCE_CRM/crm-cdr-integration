<?php

namespace app\modules\cdr\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cdr\models\CallsStatisticsIncoming;

/**
 * CdrCallsStatisticsSearch represents the model behind the search form of `app\models\CallsStatisticsIncoming`.
 */
class CallsStatisticsIncomingSearch extends CallsStatisticsIncoming
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'total_calls', 'ans_calls', 'no_ans_calls'], 'integer'],
            [['calldate', 'entry_date'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        // Used in $from $to search
        if (!empty($params['CdrCallsStatisticsSearch']['calldate'])) {
            // Cutting up the string.
            $from = substr($params['CdrCallsStatisticsSearch']['calldate'], 0, 10);
            $to = substr($params['CdrCallsStatisticsSearch']['calldate'], 13);

            $query = CallsStatisticsIncoming::find()
            ->where(['between', 'calldate', $from, $to ])
            ->orderBy('calldate DESC');
        } else {
            $query = CallsStatisticsIncoming::find()
            ->orderBy('calldate DESC');

            $this->load($params);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'calldate' => $this->calldate,
            'total_calls' => $this->total_calls,
            'ans_calls' => $this->ans_calls,
            'no_ans_calls' => $this->no_ans_calls,
            'entry_date' => $this->entry_date,
            'last_updated' => $this->last_updated,
        ]);

        return $dataProvider;
    }
}
