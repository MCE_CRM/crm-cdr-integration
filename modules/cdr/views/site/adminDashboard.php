

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">

</script>
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/dataviz.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/kelly.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.js"></script> 
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.init.js"></script> 




<?php

/* @var $this yii\web\View */
use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use app\modules\cdr\models\Cdr;
use app\modules\cdr\models\User;


\app\modules\cdr\assets\DashboardAsset::register($this);
$this->title = 'Dashboard';
$total_csr = 0;
$total_agents = 0;
$user = \app\modules\cdr\models\User::find()->all();
foreach ($user as $data)
{
    $role = getUserRoles($data->id);
    if($role ==='CSR')
    {
        $total_csr++;
    }
    if($role =="Agent")
    {
        $total_agents++;
    }
}

function getUserRoles($user_id){
    $connection = \Yii::$app->db;
    $sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
    $command=$connection->createCommand($sql);
    $dataReader=$command->queryAll();
    $roles ='';
    if(count($dataReader) > 0){
        foreach($dataReader as $role){
            $roles= $role['name'];
        }
    }
    return $roles;
}


?>




<style>
.datepicker
{
    margin-top: -33px;
}

.card-header
{
    text-align:center;
}
.ibox {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}
.ibox-title {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 2px 0 0;
    color: inherit;
    margin-bottom: 0;
    padding: 15px 15px 7px;
    min-height: 48px;
}


.ibox-title .label {
    float: left;
    margin-left: 4px;
}
.label, .ibox .label {
    font-size: 10px;
    border-radius: 10px;
}
.label-success, .badge-success {
    background-color: #1c84c6;
    color: #FFFFFF;
}
.label {
    background-color:#2baab1;
    color:white;
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: 600;
    padding: 0px 8px;
    text-shadow: none;
}

.ibox-title h5 {
    display: inline-block;
    font-size: 14px;
    margin: 0 0 7px;
    padding: 0;
    text-overflow: ellipsis;
    float: left;
    font-weight: 600;
}
.ibox-content {
    clear: both;
}

.ibox-content {
    background-color: #ffffff;
    color: inherit;
    padding: 15px 20px 20px 20px;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 0;
}
small, .small {
    font-size: 85%;
}
.no-margins {
    margin: 0 !important;
}
.stat-percent {
    float: right;
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
}
#myChart
{
    height:100% !important;
    width:100% !important;

}
.amcharts-chart-div a {display:none !important;}
a.dlink:hover{
    text-decoration: none;
}
</style>

 

         <?php
   $query = \app\modules\cdr\models\Setting::find()->orderBy('id DESC')->limit(1)->offset(0)->one();
  
  ?>

<?php
if(isset($_GET['Cdr']['calldate']) && !empty($_GET['Cdr']['calldate']))
			 {
				 	list($start_date, $end_date) = explode(' - ',$_GET['Cdr']['calldate']);
					$start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
					$start_date = $start_date->format('Y-m-d H:i:s');
		
					$end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
					$end_date = $end_date->format('Y-m-d H:i:s');
			 }else
			 {
				$start_date = date('Y-m-d H:i:s');//date('Y-m-d', strtotime("-1 months", strtotime("NOW")));
				$end_date = date('Y-m-d H:i:s');
			}
?>
<div class="cdr-search" style="width: 100%;">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
	<div class="row">
        <div class="col-md-2 col-md-3">

              <?php
          

           // echo DateRangePicker::widget([
           //     'model'=>$model,
           //     'attribute'=>'calldate',
           //     'convertFormat'=>true,
           //     'pluginOptions'=>[
           //         'opens'=>'left',
           //         'ranges' => [

           //             "Today" => ["moment().startOf('day')", "moment()"],
           //             "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
           //             "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

           //         ],

           //         'timePicker'=>true,
           //         'timePickerIncrement'=>05,
           //         'locale'=>['format'=>'d/m/Y h:i A']
           //     ],
           //     'presetDropdown'=>false,
           //     'hideInput'=>true
           // ]);
           ?>
            
        </div>
        <div class="col-md-2 col-md-3" style="margin-top:24px;">
            
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>

        </div>
   	</div>

	
<?php ActiveForm::end(); ?>
</div>
<h3 style="align-content: center; margin: 0 auto;" >Daily Statistics (<?=$start_date.'  - '.$end_date?>)</h3>

<br><br>
<div class="row">
    <div class="col-lg-6">
        <div class="row mb-3">
            <div class="col-xl-6">
              <div class="card card-featured-left card-featured-primary mb-3 ">
                    <a class="dlink" href="">

                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-random"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Total Calls</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalincomming+$totaloutgoing;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </a>
               </div>
                
            </div>

            <div class="col-xl-6">
               
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <a class="dlink" href="">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-secondary">
                                        <i class="fa fa-reply"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Incoming Calls</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo $totalincomming;?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>


        </div>

        <div class="row">
            <div class="col-xl-6">
             
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-quaternary">
                                    <i class="fa fa-tty"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">No Answer/dropped Calls by Agent</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalbusy;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

            <div class="col-xl-6">
              
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-user-tie"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">No Answer Calls by Customers</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalnoanserbycust;?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>

              <div class="col-xl-6">
               
                  <section class="card card-featured-left card-featured-tertiary mb-3">
                      <a class="dlink" href="" >
                          <div class="card-body">
                              <div class="widget-summary">
                                  <div class="widget-summary-col widget-summary-col-icon">
                                      <div class="summary-icon bg-primary">
                                          <i class="fa fa-circle-o-notch"></i>
                                      </div>
                                  </div>
                                  <div class="widget-summary-col">
                                      <div class="summary">
                                          <h4 class="title"> Internal Transfer</h4>
                                          <div class="info">
                                              <strong class="amount"><?php echo $totaltransfer;?></strong>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </a>
                  </section>
            </div>

            <div class="col-xl-6">
          
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fas fa-microphone"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"> Total Voice Mails</h4>
                                        <div class="info">
                                            <strong class="amount"><?=$voicemails?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
        </div>
    </div>
    <div class="col-lg-6">

        <div class="row mb-3">

            <div class="col-xl-6">
       
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fa fa-mail-forward"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> Total Outgoing</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totaloutgoing;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>

            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-circle-o-notch"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"> Total Forwaded</h4>
                                        <div class="info">
                                            <strong class="amount"><?php
                                                echo $forwaded;
                                                ?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>

        </div>
        <div class="row">
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-tertiary">
                                        <i class="fas fa-sign-in-alt"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title">Total Login/Active CSRs</h4>
                                        <div class="info">
                                            <strong class="amount">
                                                <?php
                                                date_default_timezone_set("Asia/Karachi");

                                                exec('sudo /usr/sbin/asterisk -r -x "queue show" 2>&1',$online);
                                                /*echo $online;*/
                                                echo $totallogin-$totallogout;

                                                ?>
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>

            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-user-tie"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Total CSR</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $total_csr;?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-laptop"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                       
                                        <div class="info">
                                            <p class=""><?php 
														$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
														$REMOTE_ADDR=$hostname;
														$ip= $REMOTE_ADDR;
														$ip = GetHostByName($ip);
											echo 'IP: '.$ip;?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <a class="dlink" href="">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-quaternary">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                         <h4 class="title">SIP Status:</h4>
                                        <div class="info">
                                            <strong class="amount" style="font-size: 12px !important;"><?php echo '<h3><b>'.$sipstatus.'</b></h3>'."";echo "ip:".$sipip;?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>




        </div>
    </div>

</div>


<br><br><br>
<h3 style="align-content: center; margin: 0 auto;" >Monthly Statistics (<?=date('M-Y')?>)</h3>
<br><br><br>
<div class="row" style="width: 100%;">

    <div class="col-lg-12">
        <div class="row mb-3">



            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly incoming</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">

                    <div class="circular-bar">
                        <div id="incomingchartContainer" style="height: 200px; width: 90%;"></div>
                    </div>
                </section>
            </div>

            <div class="col-xl-4">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly outgoing Calls</div>
                <section class="card card-featured-left card-featured-secondary mb-3">

                    <div class="circular-bar">
                        <div id="outgoingchartContainer" style="height: 200px; width: 90%;"></div>

                    </div>

                </section>
            
            </div>

            <div class="col-xl-4">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Top 5 Agents of Month</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <table class="tab1">

                        <tr class="tr1" id="caltbl tbody">
                            <th class="th1"> # </th>
                            <th class="th1"> Name </th>
                            <th class="th1"> Agent ID</th>
                            <th class="th1"> Out. Calls </th>
                            <th class="th1"> In. Calls </th>
                            <th class="th1"> Total</th>
                        </tr>

                        <?php $user1 = \app\models\User::find()->where(['LIKE','about','CSR'])->limit(5)->all();
                        $i =1;
                        foreach ($user1 as $value){

                            $agent = 'SIP/'.$value->agent_id;
                            $agentoutgoing = \app\modules\cdr\models\Cdr::find()->where([
                                'and',
                                ['LIKE', 'iorecording','Outgoing'],
                                // ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                                ['LIKE','channel',$agent]
                            ])->distinct()->count();

                            // echo "<pre>";
                            // print_r($agentoutgoing);
                            // exit;

                            $agentincoming = \app\modules\cdr\models\Cdr::find()->select('iorecording')->where([
                                'and',
                                ['LIKE', 'iorecording','Incoming'],
                                // ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                                ['LIKE','dstchannel',$agent]
                            ])->distinct()->count();

                            ?>

                            <tr class="tr1">

                                <td class="td1"> <?=$i++?></td>
                                <td class="td1"> <?=$value->username?></td>
                                <td class="td1"> <?=$value->agent_id?></td>
                                <td class="td1"> <?=$agentoutgoing?> </td>
                                <td class="td1"><?=$agentincoming?></td>
                                <td class="td1"><?=$agentincoming + $agentoutgoing?></td>
                            </tr>

                        <?php } ?>
                    </table>

                </section>
            
            </div>
            
        </div>
        <div class="row">

            <div class="col-xl-4">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Transferred Calls</div>
                <section class="card card-featured-left card-featured-quaternary mb-3">

                    <!--<div id="transferContainer" style="height: 200px; width: 90%;"></div>-->
                    <canvas id="transferContainer" style="height: 200px; width: 90%;"></canvas>

                </section>
            </div>
            <div class="col-xl-4">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Dropped Calls/Not Answered</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div id="noanswerContainer" style="height: 200px; width: 90%;"></div>
                </section>
            </div>

              <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly Inquires</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">

                    <div id="inquirieschartdiv" style="height: 200px; width: 90%;">

                    </div>

                </section>
            </div>

        </div>
    </div>

  </div>

<div class="row" style="width: 100%;">

    <div class="col-lg-12">

        <br><br>
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">CSR/Agent-wise Efficiency Graph</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div class="circular-bar">
                        <div id="efficiencychartContainer" style="height: 350px; width: 100%;"></div>
                    </div>
                </section>


            <!--<div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly outgoing Calls</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <div class="circular-bar">
                        <div id="outgoingchartContainer" style="height: 200px; width: 90%;"></div>
                    </div>
                </section>
            </div>


            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Top 5 Agents of Month</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    jhjkhj

                </section>
            </div>-->
    </div>
</div>

    <br><br><br>
    <h3 style="align-content: center; margin: 0 auto;" >Live Status of Agents/CSRs</h3>
    <br><br><br>

    <table class="tab2">
        <tr class="tr1">
            <th class="th1"> # </th>
            <th class="th1"> Name </th>
            <th class="th1"> Agent ID </th>
            <th class="th1">IP Address</th>
            <th class="th1">Active Status</th>
        </tr>

        <?php
        $i =0;
        $udata[] =  Array(); 

        foreach ($user1 as $value){

            $agent = 'SIP/'.$value->agent_id;
            $agentoutgoinging = app\modules\cdr\models\Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Outgoing'],
                ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                ['LIKE','channel',$agent]
            ])->distinct()->count();

            $agentincoming = app\modules\cdr\models\Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Incoming'],
                ['between','calldate',date('Y-m-01'),date('Y-m-t')],
                ['LIKE','dstchannel',$agent]
            ])->distinct()->count();

            ?>

            <tr class="tr1">
                <td class="td1"> <?=$i?></td>
                <td class="td1"> <?=$value->username?> </td>
                <td class="td1"> <?=$value->agent_id?> </td>
                <td class="td1"> <?='192.168.5.'.$i?> </td>
                <td class="td1"> <?='Live'?> </td>
            </tr>

        <?php
            $udata[$i]['year'] = $value->agent_id;
            $udata[$i]['in'] = $agentincoming;
            $udata[$i]['out'] = $agentoutgoinging;


        $i++;
        }
        $udata=json_encode($udata);
        ?>

    </table>

<?php
$trans =  Array();


for($i=0;$i<= date('d');$i++){

    $val = str_pad($i, 2, "0", STR_PAD_LEFT);

    $totaltransfer = app\modules\cdr\models\Cdr::find()->select('uniqueid')->where([
        'and',
        ['like','internalcall','Tranfered-call-from'],

        ['like','calldate',date('Y-m-'.$val)]

    ])->distinct()->count();

    $trans[$i] = (double)$totaltransfer;



}


//echo print_r($data);
//return  json_encode($data);

?>

<script src="https://cdnjs.com/libraries/Chart.js"></script>
<script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script>
function myFunction() {

  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}




am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
    var chartdiv = am4core.create("chartdiv1", am4charts.XYChart);

// Create axes
    var categoryAxis = chartdiv.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    categoryAxis.renderer.grid.template.location = 0;
    var valueAxis = chartdiv.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.inside = true;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.min = 0;

// Create series
    function createSeries(field, name) {
        // Set up series
        var series = chartdiv.series.push(new am4charts.ColumnSeries());
        series.name = name;
        series.dataFields.valueY = field;

        // Make it stacked
        series.stacked = true;

        // Configure columns
        series.columns.template.width = am4core.percent(60);
        series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

        // Add label
        var labelBullet = series.bullets.push(new am4charts.LabelBullet());
        labelBullet.label.text = "{valueY}";
        labelBullet.locationY = 0.5;
        return series;

    }



// Legend
    chartdiv.legend = new am4charts.Legend();

}); // end am4core.ready()


        //Incoming Calls Graph
am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_dataviz);
    am4core.useTheme(am4themes_animated);
// Themes end



// Create chart instance

    var chart = am4core.create("incomingchartContainer", am4charts.PieChart);

// Add data
    chart.data = [

        { "sector": "M", "size": <?= $totalincommingmonthly ?>},
        { "sector": "D", "size": <?= $totalincomming ?> },

    ];

// Add label
    chart.innerRadius = 100;
    var label = chart.seriesContainer.createChild(am4core.Label);
    label.text = <?php echo $totalincommingmonthly  ?>;
    label.horizontalCenter = "middle";
    label.verticalCenter = "middle";
    label.fontSize = 50;

// Add and configure Series

    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "size";
    pieSeries.dataFields.category = "sector";

// Animate chart data




}); // end am4core.ready()

am4core.ready(function() {

// Themes begin

    am4core.useTheme(am4themes_animated);
// Themes end



// Create chart instance

    var chart = am4core.create("outgoingchartContainer", am4charts.PieChart);

// Add data
    chart.data = [

        { "sector": "M", "size": "<?= $totaloutgoingmonthly ?>"},
        { "sector": "D", "size": "<?= $totaloutgoing?>" },

    ];

// Add label
    chart.innerRadius = 100;
    var label = chart.seriesContainer.createChild(am4core.Label);
    label.text = <?php echo $totaloutgoingmonthly + $totaloutgoing ?>;
    label.horizontalCenter = "middle";
    label.verticalCenter = "middle";
    label.fontSize = 50;

// Add and configure Series

    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "size";
    pieSeries.dataFields.category = "sector";

// Animate chart data




}); // end am4core.ready()

//transfer graph

// Themes end




       // var chart = am4core.create("transferContainer", am4charts.XYChart);
        var today = new Date();

        var datalb = [];
        var value = 50;
        for(var i = 1; i <= today.getDate(); i++){
            var date = new Date();
            date.setHours(0,0,0,0);
            date.setDate(i);
            //value -= Math.round((Math.random() < 0.5 ? 1 : -1) * Math.random() * 10);

           /// date.getDate();
            datalb.push(date.getDate());
        }

       // var array = JSON.parse(result);





        var ctx = document.getElementById('transferContainer');

        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: datalb,
                datasets: [{
                    type: 'line',
                    label:'',
                    data: <?php echo json_encode($trans)?>,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });











//chart.scrollbarY = new am4core.Scrollbar();
    //chart.scrollbarX = new am4core.Scrollbar();

 // end am4core.ready()




//agent efficiencychartContainer

am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_kelly);
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance

    $.ajax({
        url:"<?=Yii::$app->homeUrl?>site/proficiancy",
        success: function(result){




        }});
    var chart = am4core.create("efficiencychartContainer", am4charts.XYChart);

// Add data
    chart.data = <?=$udata?>
    /*chart.data = [ {
        "year": "2001",
        "in": 2,
        "out": 2.5,

    }, {
        "year": "2002",
        "in": 3,
        "out": 2,

    },{
        "year": "2003",
        "in": 4,
        "out": 2,

    },{
        "year": "2004",
        "in": 3,
        "out": 2,

    } ];*/

// Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    //categoryAxis.title.text = "Local country offices";
    categoryAxis.renderer.grid.template.location = 0;
    categoryAxis.renderer.minGridDistance = 20;
    categoryAxis.renderer.cellStartLocation = 0.1;
    categoryAxis.renderer.cellEndLocation = 0.9;

    var  valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.min = 0;
   // valueAxis.title.text = "Expenditure (M)";

// Create series
    function createSeries(field, name, stacked) {
        var series = chart.series.push(new am4charts.ColumnSeries());
        series.dataFields.valueY = field;
        series.dataFields.categoryX = "year";
        series.name = name;
        series.columns.template.tooltipText = "{name}: [bold]{valueY}[/]";
        series.stacked = stacked;
        series.columns.template.width = am4core.percent(95);
    }

    createSeries("in", "In", true);
    createSeries("out", "Out", true);



// Add legend
    chart.legend = new am4charts.Legend();

}); // end am4core.ready()

//dropped/no answers calls

am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
    var chart = am4core.create("noanswerContainer", am4charts.XYChart);
    //chart.scrollbarX = new am4core.Scrollbar();

// Add data
    chart.data = [{
        "country": "<?=date('F')?>",
        "visits": "<?=$totalbusymonthly?>",
    },
    ];

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

    categoryAxis.dataFields.category = "country";

    //categoryAxis.renderer.labels.template.rotation = 270;
    categoryAxis.tooltip.disabled = true;
    //categoryAxis.renderer.minHeight = 110;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    // valueAxis.renderer.minWidth = 50;

    // Create series
    var series = chart.series.push(new am4charts.ColumnSeries());

    series.dataFields.valueY = "visits";
    series.dataFields.categoryX = "country";
    series.columns.template.strokeWidth = 0;

    series.calculatePercent = true;

    series.label = 1;

    series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: [bold]{valueY.percent}%[/] ({valueY})";

    series.legendSettings.itemValueText = "{valueY.percent}%";


    labelBullet.label.text = "{valueY.percent}%";


    // on hover, make corner radiuses bigger


    series.columns.template.adapter.add("fill", function(fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
    });

    // Cursor


}); // end am4core.ready()


//end dropped/no answer calls

am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
    var chartdiv = am4core.create("chartdiv1", am4charts.XYChart);


// Add data
    chartdiv.data = [{
        "year": "2016",
        "europe": 2.5,
        "namerica": 2.5,
        "asia": 2.1,
        "lamerica": 0.3,
        "meast": 0.2,
        "africa": 0.1
    }];

// Create axes
    var categoryAxis = chartdiv.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = "year";
    categoryAxis.renderer.grid.template.location = 0;
    var valueAxis = chartdiv.yAxes.push(new am4charts.ValueAxis());
    valueAxis.renderer.inside = true;
    valueAxis.renderer.labels.template.disabled = true;
    valueAxis.min = 0;


// Create series
    function createSeries(field, name) {
        // Set up series
        var series = chartdiv.series.push(new am4charts.ColumnSeries());
        series.name = name;
        series.dataFields.valueY = field;

        // Make it stacked
        series.stacked = true;

        // Configure columns
        series.columns.template.width = am4core.percent(60);
        series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

        // Add label
        var labelBullet = series.bullets.push(new am4charts.LabelBullet());
        labelBullet.label.text = "{valueY}";
        labelBullet.locationY = 0.5;
        return series;

    }


// Legend
    chartdiv.legend = new am4charts.Legend();

}); // end am4core.ready()

am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_animated);

// Themes end

    /**
     * Define data for each year
     */



// Create chart instance



    var chart = am4core.create("inquirieschartdiv", am4charts.PieChart);

// Add data
    chart.data = [

        { "sector": "Open", "size": 6},
        { "sector": "Closed", "size": 20 },

    ];

// Add label
    chart.innerRadius = 100;
    var label = chart.seriesContainer.createChild(am4core.Label);
    label.text = "10";
    label.horizontalCenter = "middle";
    label.verticalCenter = "middle";
    label.fontSize = 50;

// Add and configure Series

    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "size";
    pieSeries.dataFields.category = "sector";

// Animate chart data




}); // end am4core.ready()



var $tbody = $('#caltbl tbody');
$tbody.find('tr').sort(function (a, b) {
    var tda = $(a).find('td:eq(0)').text();
    var tdb = $(b).find('td:eq(0)').text();
    // if a < b return 1
    return tda > tdb ? 1
        // else if a > b return -1
        : tda < tdb ? -1
            // else they are equal - return 0
            : 0;
}).appendTo($tbody);


$(document.body).on("click",".btn-default",function(){
		window.location.href  = "<?php echo Yii::$app->homeUrl?>site/index";
		
	});
</script>

<!-- HTML -->









