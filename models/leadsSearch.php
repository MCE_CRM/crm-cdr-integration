<?php

namespace app\models;

use DateTime;
use Yii; 
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Leads;
 
/**
 * leadsSearch represents the model behind the search form of `app\models\leads`.
 */
class leadsSearch extends Leads
{

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id','property_id', 'created_by','active', 'updated_by'], 'integer'],
            [['lead_title', 'lead_description', 'lead_type','lead_response','lead_source', 'lead_status', 'follow_up', 'contact_name', 'contact_phone', 'contact_mobile', 'contact_email', 'contact_country', 'contact_state', 'contact_city', 'contact_address', 'contact_address2', 'created_on', 'updated_on','lead_assign','project_id'], 'safe'],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */


    public function searchMyAssigned($params)
    {




        //$query = Leads::find()->where(['active'=>1])->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,lead_assign)'))->addParams([':cat_to_find' => Yii::$app->user->id]);

         if(isset($_GET['country']) && !empty($_GET['country']))
            {
                if($_GET['country']==771)
                {
                  $query = Leads::find()->where([
                    'and',
                    ['!=','contact_country',177],
                    ['active'=>1]
                ])->andWhere(['lead_assign' => Yii::$app->user->id])->orderBy('id DESC');
                }
                else
                {
                   $query = Leads::find()->where([
                    'and',
                    ['contact_country'=>177],
                    ['active'=>1]
                ])->andWhere(['lead_assign' => Yii::$app->user->id])->orderBy('id DESC');
                }
            }
            else
            {
                 $query = Leads::find()->where(['active'=>1])->andWhere(['lead_assign' => Yii::$app->user->id])->orderBy('created_on DESC');
            }

        /*$query = Leads::find()->where(['active'=>1])->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,lead_assign)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orderBy('id DESC');*/

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

       /*  if($this->created_on)
        {
            $mysql_date = $this->created_on;
            // date in Y-m-d format as MySQL stores it
            $date_obj = date_create_from_format('d/m/Y',$mysql_date);
            $created_on = date_format($date_obj, 'Y-m-d');
		}*/

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

         if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);



        }
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_assign' => $this->lead_assign,
            'active'=>$this->active,
            'follow_up' => $this->follow_up,

            'property_id' => $this->property_id,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
            'contact_city'=> $this->contact_city,
            'contact_state'=> $this->contact_state,
        ]);

        $query->andFilterWhere(['like', 'lead_title', $this->lead_title])
            ->andFilterWhere(['like', 'lead_description', $this->lead_description])
            ->andFilterWhere(['like', 'lead_assign', $this->lead_assign])
            ->andFilterWhere(['like', 'lead_type', $this->lead_type])
            ->andFilterWhere(['like', 'lead_source', $this->lead_source])
            ->andFilterWhere(['like', 'project_id', $this->project_id])
            ->andFilterWhere(['like', 'lead_status', $this->lead_status])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_mobile', $this->contact_mobile])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_country', $this->contact_country])
             ->andFilterWhere(['like', 'lead_response', $this->lead_response])
            ->andFilterWhere(['like', 'contact_state', $this->contact_state])
            //->andFilterWhere(['like', 'contact_city', $this->contact_city])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_address2', $this->contact_address2]);
             // ->andFilterwhere(['between', 'created_on', $this->from_date, $this->to_date]);

        return $dataProvider;
    }

    public function searchTeamLead($params)
    {

         if(isset($_GET['country']) && !empty($_GET['country']))
            {
                if($_GET['country']==771)
                {
                    $team_lead=Yii::$app->user->identity->lead_assign;
                    $split = preg_split('/(?:"[^"]*"|)\K\s*(,\s*|$)/', $team_lead);
                    $result = array_filter($split);

                  

                    $query = Leads::find()->where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,lead_assign)'))->addParams([':cat_to_find' => $result])->orderBy('id DESC');
                   
                }
                else
                {
                    $team_lead=Yii::$app->user->identity->lead_assign;
                    $split = preg_split('/(?:"[^"]*"|)\K\s*(,\s*|$)/', $team_lead);
                    $result = array_filter($split);

                
                   $query = Leads::find()->where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,lead_assign)'))->addParams([':cat_to_find' => $result])->orderBy('id DESC');
                  
               
                }
            }
            else
            {       
                     $user=Yii::$app->user->identity->id;
                    $team_lead=Yii::$app->user->identity->lead_assign;
                    $result =  array_map('intval', explode(',', $team_lead));

                    // $query = \app\models\Leads::find()->WHERE(['in', 'lead_assign', $result])->andwhere(['active'=>1])->orderBy('created_on DESC');

                    $query = \app\models\Leads::find()->WHERE(['in', 'lead_assign', $result])->andwhere(['active'=>1])->orderBy('created_on DESC');

                    // $query = \app\models\Leads::find()->WHERE(['in', 'lead_assign', $result])->andwhere(['active'=>1])->andwhere(['created_by'=> $user])->orderBy('created_on DESC');
                   
            }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            // this $params['pagesize'] is an id of dropdown list that we set in view file
            'pagesize' => (isset($params['pagesize']) ? $params['pagesize'] :  '20')]

        ]);
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

         if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);



        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_assign' => $this->lead_assign,
            'active'=>$this->active,
            'follow_up' => $this->follow_up,
            'project_id' => $this->project_id,
            'property_id' => $this->property_id,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
            'contact_city'=> $this->contact_city,
            'contact_state'=> $this->contact_state,
        ]);

        $query->andFilterWhere(['like', 'lead_title', $this->lead_title])
            ->andFilterWhere(['like', 'lead_description', $this->lead_description])
            ->andFilterWhere(['like', 'lead_assign', $this->lead_assign])
            ->andFilterWhere(['like', 'lead_type', $this->lead_type])
            ->andFilterWhere(['like', 'lead_source', $this->lead_source])
            ->andFilterWhere(['like', 'lead_status', $this->lead_status])
             ->andFilterWhere(['like', 'lead_response', $this->lead_response])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_mobile', $this->contact_mobile])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_country', $this->contact_country])
            ->andFilterWhere(['like', 'contact_state', $this->contact_state])
            //->andFilterWhere(['like', 'contact_city', $this->contact_city])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_address2', $this->contact_address2]);
             // ->andFilterwhere(['between', 'created_on', $this->start_date, $this->end_date]);

        return $dataProvider;
    }

    public function search($params)
    {

        
		if(\Yii::$app->user->can('AllLeadsView'))
        {
            if(isset($_GET['country']) && !empty($_GET['country']))
            {
                if($_GET['country']==771)
                {
                  $query = Leads::find()->where([
                    'and',
                    ['!=','contact_country',177],
                    ['active'=>1]
                ])->orderBy('id DESC');
                }
                else{
               $query = Leads::find()->where(['active'=>1])->andWhere(['contact_country'=>$_GET['country']])->orderBy('id DESC'); 
               }  
            }
            else
            {
            $query = Leads::find()->where(['active'=>1])->orderBy('id DESC');
            }
        }else
        {

            $query = Leads::find()->where(['active'=>1])->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,lead_assign)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orderBy('created_on DESC')->all();
        }
		

        // add conditions that should always apply here
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            // this $params['pagesize'] is an id of dropdown list that we set in view file
            'pagesize' => (isset($params['pagesize']) ? $params['pagesize'] :  '20')]

        ]);

        $this->load($params);
        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


         if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);



        }
       
        // grid filtering conditions
          
       
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_assign' => $this->lead_assign,
            'active'=>$this->active,
            'follow_up' => $this->follow_up,
            
            'property_id' => $this->property_id,
            'contact_city'=> $this->contact_city,
             'contact_state'=> $this->contact_state,
            /*'created_on' => $this->created_on,*/
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

       // $query->LEFTJOIN('projects','projects.id=leads.project_id');

        $query->andFilterWhere(['like', 'lead_title', $this->lead_title])
            ->andFilterWhere(['like', 'lead_description', $this->lead_description])
            ->andFilterWhere(['like', 'lead_type', $this->lead_type])
            ->andFilterWhere(['like', 'lead_source', $this->lead_source])

            ->andFilterWhere(['like', 'project_id', $this->project_id])
             ->andFilterWhere(['like', 'lead_assign', $this->lead_assign])
            ->andFilterWhere(['like', 'lead_status', $this->lead_status])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_mobile', $this->contact_mobile])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_country', $this->contact_country])
            ->andFilterWhere(['like', 'contact_state', $this->contact_state])
			//->andFilterWhere(['like', 'contact_city', $this->contact_city])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_address2', $this->contact_address2])
            ->andFilterWhere(['like', 'lead_response', $this->lead_response]);
            // ->andFilterwhere(['between', 'created_on', $this->from_date, $this->to_date]);
            

        return $dataProvider;
    }


    //myregister leads

       public function searchRegister($params)
    {

          if(isset($_GET['country']) && !empty($_GET['country']))
            {
                if($_GET['country']==771)
                {
                  $query = Leads::find()->where([
                        'and',
                        ['!=','contact_country',177],
                        ['active'=>1],
                        ['created_by'=>Yii::$app->user->id]
                    ])->orderBy('id DESC');
                }
                else
                {
                    $query = Leads::find()->where([
                        'and',
                        ['contact_country'=>177],
                        ['active'=>1],
                        ['created_by'=>Yii::$app->user->id]
                    ])->orderBy('id DESC');
                }
            }
            else
            {
                 $query = Leads::find()->where([
                        'and',
                        ['active'=>1],
                        ['created_by'=>Yii::$app->user->id]
                    ])->orderBy('id DESC');
            }
			$lead_assignment = '';
			if(!empty($_GET['leadsSearch']['lead_assign']))
			{
				$this->lead_assign =$_GET['leadsSearch']['lead_assign']; 
				$lead_assign = User::find()->select('id')->where(['like','first_name', $this->lead_assign])->one();
				$lead_assignment = $lead_assign['id'];
				 $query->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,lead_assign)'))->addParams([':cat_to_find' => $lead_assignment]);
			}
			// add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);






        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_assign' => $this->lead_assign,
            'active'=>$this->active,
            'follow_up' => $this->follow_up,
            'project_id' => $this->project_id,
            'property_id' => $this->property_id,
            /*'created_on' => $this->created_on,*/
            /*'created_by' => $this->created_by,*/
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'lead_title', $this->lead_title])
            ->andFilterWhere(['like', 'lead_description', $this->lead_description])
            ->andFilterWhere(['like', 'lead_assign', $this->lead_assign])
            ->andFilterWhere(['like', 'lead_type', $this->lead_type])
            ->andFilterWhere(['like', 'lead_source', $this->lead_source])
            ->andFilterWhere(['like', 'project_id', $this->project_id])
            ->andFilterWhere(['like', 'lead_status', $this->lead_status])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_mobile', $this->contact_mobile])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_country', $this->contact_country])
            ->andFilterWhere(['like', 'contact_state', $this->contact_state])
            ->andFilterWhere(['like', 'contact_city', $this->contact_city])
			->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_address2', $this->contact_address2])
            ->andFilterWhere(['like', 'created_on', $this->created_on])
            ->andFilterWhere(['like', 'created_by', $this->created_by]);
            

        return $dataProvider;
    }

    //

    public function searchDelete($params)
    {

        $query = Leads::find()->where(['active'=>0]);


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);






        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_assign' => $this->lead_assign,
            'active'=>$this->active,
            'follow_up' => $this->follow_up,
           
            'property_id' => $this->property_id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'lead_title', $this->lead_title])
            ->andFilterWhere(['like', 'lead_assign', $this->lead_assign])
            ->andFilterWhere(['like', 'lead_description', $this->lead_description])
            ->andFilterWhere(['like', 'lead_type', $this->lead_type])
            ->andFilterWhere(['like', 'lead_source', $this->lead_source])
            ->andFilterWhere(['like', 'project_id', $this->project_id])
            ->andFilterWhere(['like', 'lead_status', $this->lead_status])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_mobile', $this->contact_mobile])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_country', $this->contact_country])
            ->andFilterWhere(['like', 'contact_state', $this->contact_state])
            ->andFilterWhere(['like', 'contact_city', $this->contact_city])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_address2', $this->contact_address2]);

        return $dataProvider;
    }

    public function searchStart($params)
    {
        if ( ! is_null($params['date_range_1']) && strpos($params['date_range_1'], ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$params['date_range_1']);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:iA', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');




        }
        return $start_date;
    }

    public function searchEnd($params)
    {
        if ( ! is_null($params['date_range_1']) && strpos($params['date_range_1'], ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$params['date_range_1']);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:iA', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');




        }
        return $end_date;
    }


    public function searchLeadsReport($params)
    {
        $query = Leads::find()->where(['not', ['property_id' => null]])->groupBy(['property_id']);


          
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);



        if ( ! is_null($params['date_range_1']) && strpos($params['date_range_1'], ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$params['date_range_1']);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:iA', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);



        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }




        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lead_assign' => $this->lead_assign,
            'follow_up' => $this->follow_up,
            
            'property_id' => $this->property_id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'lead_title', $this->lead_title])
            ->andFilterWhere(['like', 'lead_assign', $this->lead_assign])
            ->andFilterWhere(['like', 'lead_description', $this->lead_description])
            ->andFilterWhere(['like', 'lead_type', $this->lead_type])
            ->andFilterWhere(['like', 'lead_source', $this->lead_source])
            ->andFilterWhere(['like', 'project_id', $this->project_id])
            ->andFilterWhere(['like', 'lead_status', $this->lead_status])
            ->andFilterWhere(['like', 'contact_name', $this->contact_name])
            ->andFilterWhere(['like', 'contact_phone', $this->contact_phone])
            ->andFilterWhere(['like', 'contact_mobile', $this->contact_mobile])
            ->andFilterWhere(['like', 'contact_email', $this->contact_email])
            ->andFilterWhere(['like', 'contact_country', $this->contact_country])
            ->andFilterWhere(['like', 'contact_state', $this->contact_state])
            ->andFilterWhere(['like', 'contact_city', $this->contact_city])
            ->andFilterWhere(['like', 'contact_address', $this->contact_address])
            ->andFilterWhere(['like', 'contact_address2', $this->contact_address2]);

        return $dataProvider;

    }




}
