<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\FeatureList */

$this->title = 'Update Feature List: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Feature Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="feature-list-update">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
