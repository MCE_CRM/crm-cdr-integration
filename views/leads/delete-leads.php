<?php

use kartik\editable\Editable;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\date\DatePicker;
use app\models\User;
use kartik\select2\Select2;
use app\models\Notes;
/* @var $this yii\web\View */
/* @var $searchModel app\models\leadsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Delete Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>

</style>


<div class="leads-index">
    <div class="row">
        <div class="col-md-2">
            <div class="list-group" id="list-tab" role="tablist">

                <?php




                if(\Yii::$app->user->can('DeleteLeadsView'))
                { ?>

                    <a href="#leads-list" data-toggle="list" onclick="leadTab('')" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action active" id="my_lead" role="tab" aria-controls="delete_lead">Delete Leads
                        <?php
                        $my_lead = \app\models\Leads::find()->where(['active'=>0])->count();

                        ?>

                        <span class="badge badge-success badge-pill"><?= $my_lead?></span>
                    </a>

                <?php } ?>

                <br>

                <?php
                $i=0;
                foreach ($leadStaus as $status)
                {
                    $stu = \app\models\Leads::find()->where(['like', 'lead_status', $status->status])->andWhere(['active'=>0])->count();


                    if($i==0){


                        ?>
                        <a href="#leads-list" onclick="leadTab('<?= $status->status?>')" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="<?= $status->status?> " role="tab" aria-controls="<?= $status->status?>"><?= $status->status?>
                            <span class="badge badge-success badge-pill"><?= $stu?></span>
                        </a>
                    <?php }else { ?>

                        <a href="#leads-list" onclick="leadTab('<?= $status->status?>')" data-toggle="list" class="list-group-item d-flex justify-content-between align-items-center list-group-item-action" id="<?= $status->status?> " role="tab" aria-controls="<?= $status->status?>"><?= $status->status?>
                            <span class="badge badge-success badge-pill"><?= $stu?></span>
                        </a>

                    <?php  } $i++; }

                ?>


            </div>

        </div>
        <div class="col-md-10" style="background-color: white;padding: 0px">

            <?php Pjax::begin(['id' => 'leadGridview']) ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'responsiveWrap' => false,
                'tableOptions' => [
                    'class' => 'table table-condensed table-sm',
                ],
                'toolbar' =>  [
                    //'{export}',
                    '{toggleData}',
                ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i><span id="card-title">'.Yii::t ( 'app', ' List' ).'</span> </h5>',
                    'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                            'index'
                        ], [
                            'class' => 'btn btn-primary btn-sm'
                        ]),


                    'showFooter' => false,

                ],

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'contact_name',
                    'contact_mobile',
                    [
                        'attribute'=>'contact_city',
                        'value'=> function($model)
                        {
                            $city=  \app\models\City::findOne($model->contact_city);
                            return $city->city;
                        }
                    ],
                    /*[
                        'attribute'=>'lead_title',
                        'label'=>'Name',
                    ],*/
                    [
                        'attribute'=>'lead_type',
                        'label'=>'Purpose',
                    ],
                    [
                        'attribute'=>'lead_source',
                        'label'=>'Source',
                    ],
                    //'lead_description',
                    [
                        'attribute'=>'lead_assign',
                        'format'=>'raw',
                        'value'=>function($model)
                        {
                            if(empty($model->lead_assign))
                            {

                                return '<button type="button" onclick="assign(event,'.$model->id.')"; class="kv-editable-value kv-editable-link assign">Not Set</button>';

                            }else {


                                $model->lead_assign = explode(',', $model->lead_assign);
                                $e = '';
                                foreach ($model->lead_assign as $key=>$data)
                                {
                                    $user = \app\models\User::findOne($data);

                                    $e .=$user->username.',<br>';
                                }

                                $e = substr_replace($e, "", -1);
                                return '<button type="button" onclick="assign(event,'.$model->id.')"; class="kv-editable-value kv-editable-link assign">'.$e.'</button>';

                            }
                        }
                    ],

                    //'follow_up',
                    //'project_id',
                    //'property_id',

                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'lead_status',
                        'label'=>'Status',
                        'filter'=>false,
                        'options' => ['style' => 'width: 15%'],

                        'editableOptions'=> function ($model, $key, $index, $widget) {
                            $appttypes = ArrayHelper::map(\app\models\LeadStatus::find()->orderBy('sort_order')->asArray()->all(), 'status', 'status');
                            return [
                                'header' => 'Status',
                                'attribute' => 'status',
                                'asPopover' => false,
                                'inlineSettings' => [
                                    'closeButton' => '<button class="btn btn-sm btn-danger kv-editable-close kv-editable-submit" title="Cancel Edit"><i class="fa fa-times-circle"></i></button>'
                                ],

                                'type' => 'primary',
                                //'size'=> 'lg',
                                'size' => 'sm',
                                'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                'displayValueConfig' => $appttypes,
                                'data' => $appttypes,
                                'formOptions'=> ['action' => ['/ajax/update-lead-status']] // point to new action
                            ];
                        },

                    ],
                     [
                        'attribute'=>'Property status',
                        'value'=>function($model)
                        {
                            if( !empty($model->property_id))
                            {
                                $status= \app\models\Property::find()->select('status')->where(['id'=>$model->property_id])->one();
                                return $status->status;
                            }
                            else
                            {
                                return"";
                            }
                        }
                    ],

                     [
                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                        'contentOptions' => ['class' => 'text-center'],
                        'filter'=>false,
                        'attribute' =>  'created_on',
                        'value' => function($model){
                            return date("d/m/Y", strtotime($model->created_on));
                        },
                        'filterType'=>GridView::FILTER_DATE,
                        'filterWidgetOptions'=> [
                            'type' => DatePicker::TYPE_INPUT,
                            'pluginOptions' => [
                                'autoclose'=>true,
                                'format' => 'dd/mm/yyyy',
                            ],
                        ],
                        //'filter'=>false

                    ],
                    [
                        'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                        'attribute' => 'created_by',
                        'contentOptions' => ['class' => 'text-center'],
                        'value'=>'user.username',
                        'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                        'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                        'filterWidgetOptions' => [
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ],
                        'filterInputOptions' => ['placeholder' => 'All...'],
                        'format'=>'raw',

                    ],
                    [
                        'label'=>'Call Status',
                        'value'=>'sms_status'
                    ],
                    //'contact_email:email',
                    //'contact_country',
                    //'contact_state',
                    //'contact_city',
                    //'contact_address',
                    //'contact_address2',
                    // 'created_on',
                    //'created_by',
                    //'lead_status',

                    //'updated_on',
                    //'updated_by',
                     [
                        'label'=>'Last Note',
                        'value'=>function($model)
                        {
                           $lastnote=Notes::find()->where(['lead_id'=>$model->id])->orderBy('id DESC')->one();
                           $lastnotcommint= $lastnote->comment;
                           if(!empty($lastnotcommint))
                           {
                            return $lastnotcommint;
                           }
                           else
                           {
                            return"";
                           }
                        }
                    ],

                    [
                        'class' => '\kartik\grid\ActionColumn',
                        'header'=>'Actions',
                        'template'=>'{view} {update} {delete}',
                        'buttons' => [
                            'delete' => function ($url, $model, $key) {
                                if (\Yii::$app->user->can('leads/delete')) {
                                    return Html::a('<span class="fas fa-trash"></span>', $url,
                                        [
                                            'title' => Yii::t('app', 'Delete'),
                                            'data-pjax' => '1',
                                            'data' => [
                                                'method' => 'post',
                                                'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                                'pjax' => 1,],
                                        ]
                                    );
                                }
                            },
                            'update' => function ($url, $model)

                            {
                                if(\Yii::$app->user->can('leads/update')){
                                    return '<a href="'.$url.'"><span class="fa fa-pencil-alt"></span></a>';

                                }else
                                {
                                    return '';
                                }
                            } ,

                        ],

                    ]
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>







</div>

<script>

    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";

    var lastUrl = baseUrl+'leads/index';

    var tit = 'List';




    function leadTab(title) {

        var base = baseUrl+'leads?type=delete&leadsSearch[lead_status]='+title;

        base = encodeURI(base);

        lastUrl = base;


        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#leadGridview', url: base});
        if(title=='')
        {
            tit = "List";
        }else {

            tit = title;
        }






    }

    $(document).on('pjax:success', function() {
        window.history.pushState('page2', 'Title', baseUrl+'leads/index?type=delete');
        $('#card-title').text(" "+tit);
    });



    function assign(event,id) {

        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-lead?id='+id+'';

        var dialog = bootbox.dialog({
            title: 'Assign Lead',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#leadGridview', url: lastUrl});
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };







</script>