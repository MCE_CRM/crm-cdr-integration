<?php

namespace app\modules\cdr\controllers;

use Yii;
use app\modules\cdr\models\AgentSession;
use app\modules\cdr\models\AgentSessionSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * AgentSessionController implements the CRUD actions for AgentSession model.
 */
class AgentSessionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AgentSession models.
     * @return mixed
     */
    public function actionIndex()
    {

        $searchModel = new AgentSessionSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            $today_date = date('Y-m-d');
            $cond = '';
             $agent = $_GET['name'];

           //echo "<pre"; print_r($_GET);
            if (!empty($_GET['from'])) {//&& $_GET['id']<=2014
                $from1 = $_GET['from'];
                $from=  date("Y-m-d", strtotime($from1));
                $from=$from.' 00:00:00';

                //$cond = " and Date(agent_session.created_on) >='$from'";

            }
            else{
                $from = date('Y-m-d');
                $from = $from.' 00:00:00';
                //echo $from;
            }

            if (!empty($_GET['to'])) {//&& $_GET['id']<=2014
                $to1=$_GET['to'];
                $to = date("Y-m-d", strtotime($to1));
                $to = $to . ' 23:59:59';


            }
            else{
                $to = date('Y-m-d');
                $to = $to . ' 23:59:59';
               // echo $to;exit;
            }


        /*$connection= Yii::$app->getDb();
        // echo "SELECT ps.*,rg.*,rg.id as rid FROM payments ps join registration rg on ps.member_id = rg.id WHERE rg.status = 1 $cond";
        $command= $connection->CreateCommand("SELECT agent_session.*,agent_session.id as pid, agent_session.name as name,agent_session.created_on as date FROM agent_session WHERE agent_session.name = $agent $cond");
        $session=$command->queryAll();*/
        /*echo $from;
        echo 'r'.$to;exit;*/

        $session = \app\modules\cdr\models\AgentSession::find()->andWhere(['=','name',$agent])->andWhere(['between', 'created_on', $from, $to ])->all();

        /*echo '<pre>';
        echo print_r($session);
        echo '</pre>';exit;*/


        /*echo '<pre>';
        echo print_r($session);exit;*/
        //$session = \app\models\AgentSession::find()->where()

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'session' => $session,
        ]);
    }

    /**
     * Displays a single AgentSession model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AgentSession model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new AgentSession();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AgentSession model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AgentSession model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AgentSession model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgentSession the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AgentSession::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
