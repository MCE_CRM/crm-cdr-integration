<?php

namespace app\modules\cdr\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cdr\models\Forwarding;

/**
 * ForwardingSearch represents the model behind the search form of `app\models\Forwarding`.
 */
class ForwardingSearch extends Forwarding
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'updated_by'], 'integer'],
            [['time_to', 'time_from', 'phone_no', 'created_on', 'updated_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Forwarding::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'time_to', $this->time_to])
            ->andFilterWhere(['like', 'time_from', $this->time_from])
            ->andFilterWhere(['like', 'phone_no', $this->phone_no]);

        return $dataProvider;
    }
}
