<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PropertyPurpose */

$this->title = 'Create Property Purpose';
$this->params['breadcrumbs'][] = ['label' => 'Property Purposes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-purpose-create">
    
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
