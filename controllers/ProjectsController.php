<?php

namespace app\controllers;

use app\models\ExtraCharges;
use app\models\FeatureList;
use app\models\Files;
use app\models\PropertySearch;
use Yii;
use app\models\Projects;
use app\models\ProjectsSearch;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * ProjectsController implements the CRUD actions for Projects model.
 */
class ProjectsController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if($route=='projects/validate')
                        {
                            return true;
                        }
                        else if (\Yii::$app->user->can($route)) {
                            return true;
                        }


                    }
                ],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Projects models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Projects model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchModel = new PropertySearch();
        $dataProvider = $searchModel->searchByProject(Yii::$app->request->queryParams,$id);

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
       
    }

    /**
     * Creates a new Projects model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {
        $model = new Projects();
        $model->created_by = Yii::$app->user->id;
        $model->created_on = date("Y-m-d H:i:s");
        $model->active = 1;


        if ($model->load(Yii::$app->request->post())) {

            $dyn = implode(', ', $_POST['dynamic_form']);

            $model->additional_form = $dyn;


            if($model->save())
            {

                foreach ($_POST['feature'] as $key=>$data)
                {
                    if(!empty($data))
                    {

                        $feature = FeatureList::find()->where(['name'=>$key])->one();
                        if(in_array($feature->feature_form_id, $_POST['dynamic_form']))
                        {
                            $extra = new ExtraCharges();
                            $extra->name = $key;

                            $extra->value = $data;
                            $extra->type = 1;
                            $extra->project_id = $model->id;
                            $extra->feature_form_id = $feature->feature_form_id;

                            $feature = FeatureList::find()->where(['name'=>$key])->one();

                            $extra->feature_type =$feature->type;

                            $extra->save();

                        }

                    }

                }



                /*$old_directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . Yii::$app->user->id . DIRECTORY_SEPARATOR;

                $directory = Yii::getAlias('@app/files') . DIRECTORY_SEPARATOR . $model->name . DIRECTORY_SEPARATOR;
                if (!is_dir($directory)) {
                    FileHelper::createDirectory($directory);
                }

                $files = $_POST['file_id'];
                foreach ($files as $file)
                {
                    $fil = Files::findOne($file);
                    if($fil)
                    {
                        $fil->project_id = $model->id;
                        rename($old_directory.$fil->file_name,$directory.$fil->file_name);
                        $fil->save();
                    }
                }*/

                return $this->redirect('index');

            }else{

                echo "<pre>";
                print_r($model);
                echo "</pre>";

            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionValidate()
    {
        if($_GET['id'])
        {
            $model = $this->findModel($_GET['id']);
            // $model->scenario = 'update';
        }
        else
        {
            $model = new Projects();
            // $model->scenario = 'create';
        }
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
    }

    /**
     * Updates an existing Projects model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            $dyn = implode(', ', $_POST['dynamic_form']);

            $model->additional_form = $dyn;

            if($model->save()){

                foreach ($_POST['feature'] as $key=>$data)
                {
                    $find = \app\models\ExtraCharges::find()->where(['project_id'=>$model->id])->andWhere(['type'=>'1'])->andWhere(['name'=>$key])->one();

                    if($find)
                        {
                            if(in_array($find->feature_form_id, $_POST['dynamic_form'])) {

                                if (empty($data)) {
                                    $find->delete();
                                } else {
                                    $find->name = $key;

                                    $find->value = $data;
                                    $find->type = 1;

                                    $feature = FeatureList::find()->where(['name' => $key])->one();

                                    $find->feature_type = $feature->type;
                                    $find->save();
                                }
                            }
                        }else
                        {
                            if(!empty($data))
                            {
                                $feature = FeatureList::find()->where(['name'=>$key])->one();
                                if(in_array($feature->feature_form_id, $_POST['dynamic_form']))
                                {
                                    $extra = new ExtraCharges();
                                    $extra->name = $key;

                                    $extra->value = $data;
                                    $extra->type = 1;
                                    $extra->project_id = $model->id;
                                    $extra->feature_form_id = $feature->feature_form_id;

                                    $feature = FeatureList::find()->where(['name'=>$key])->one();

                                    $extra->feature_type =$feature->type;

                                    $extra->save();

                                }

                            }
                        }



                }

            }



            return $this->redirect('index');
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Projects model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Projects model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Projects the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Projects::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
