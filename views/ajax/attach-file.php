<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 9/12/2018
 * Time: 2:46 PM
 */

use dosamigos\fileupload\FileUploadUI;

$id = $_GET['id'];



if($_GET['type']=="task") {
    echo FileUploadUI::widget([
        //'model' => $model,
        'name' => 'Files[file]',
        'url' => ['site/file-upload-task?id=' . $id . ''],
        'formView'=>'@app/views/files/form',
        //'uploadTemplateId'=>'template-upload-custom',
        'downloadTemplateView' => '@app/views/files/download',
        'uploadTemplateView' => '@app/views/files/upload',
        'gallery' => false,
        'fieldOptions' => [
            //'accept' => 'image/*'
        ],
        'clientOptions' => [
            //'maxFileSize' => 2000000
        ],
        // ...
        'clientEvents' => [
            'fileuploadadd' => 'function(e, data) {
                                $(".start").removeClass("hidden");
                                $(".cancel").removeClass("hidden");
                                $(".thead").removeClass("hidden");
                                console.log("Waqar");
                                console.log(e);
                                console.log(data);
                            }',
            'fileuploaddrop' => 'function(e, data) {
                                console.log("Ahmed");
                              
                            }',
            'fileuploadsubmit' => 'function(e, data) {
                                var inputs = data.context.find(\':input\');
                                    if (inputs.filter(function () {
                                            return !this.value && $(this).prop(\'required\');
                                        }).first().focus().length) {
                                        data.context.find(\'button\').prop(\'disabled\', false);
                                        return false;
                                    }
                                    data.formData = inputs.serializeArray();

                                }',
        ],
    ]);


}else if($_GET['type']==="project") {
    

    echo FileUploadUI::widget([
        //'model' => $model,
        'name' => 'Files[file]',
        'url' => ['site/file-upload-project?id='.$id.''],
        'formView'=>'@app/views/files/form',
        //'uploadTemplateId'=>'template-upload-custom',
        'downloadTemplateView' => '@app/views/files/download',
        'uploadTemplateView' => '@app/views/files/upload',
        'gallery' => false,
        'fieldOptions' => [
            //'accept' => 'image/*'
        ],
        'clientOptions' => [
            //'maxFileSize' => 2000000
        ],
        // ...
        'clientEvents' => [
            'fileuploadadd' => 'function(e, data) {
                                $(".start").removeClass("hidden");
                                $(".cancel").removeClass("hidden");
                                $(".thead").removeClass("hidden");
                                console.log("Waqar");
                                console.log(e);
                                console.log(data);
                            }',
            'fileuploaddrop' => 'function(e, data) {
                                console.log("Ahmed");
                              
                            }',
            'fileuploadsubmit' => 'function(e, data) {
                                var inputs = data.context.find(\':input\');
                                    if (inputs.filter(function () {
                                            return !this.value && $(this).prop(\'required\');
                                        }).first().focus().length) {
                                        data.context.find(\'button\').prop(\'disabled\', false);
                                        return false;
                                    }
                                    data.formData = inputs.serializeArray();

                                }',
        ],
    ]);


} else if($_GET['type']==="property") {


    echo FileUploadUI::widget([
        //'model' => $model,
        'name' => 'Files[file]',
        'url' => ['site/file-upload-property?id='.$id.''],
        'formView'=>'@app/views/files/form',
        //'uploadTemplateId'=>'template-upload-custom',
        'downloadTemplateView' => '@app/views/files/download',
        'uploadTemplateView' => '@app/views/files/upload',
        'gallery' => false,
        'fieldOptions' => [
            //'accept' => 'image/*'
        ],
        'clientOptions' => [
            //'maxFileSize' => 2000000
        ],
        // ...
        'clientEvents' => [
            'fileuploadadd' => 'function(e, data) {
                                $(".start").removeClass("hidden");
                                $(".cancel").removeClass("hidden");
                                $(".thead").removeClass("hidden");
                                console.log("Waqar");
                                console.log(e);
                                console.log(data);
                            }',
            'fileuploaddrop' => 'function(e, data) {
                                console.log("Ahmed");
                              
                            }',
            'fileuploadsubmit' => 'function(e, data) {
                                var inputs = data.context.find(\':input\');
                                    if (inputs.filter(function () {
                                            return !this.value && $(this).prop(\'required\');
                                        }).first().focus().length) {
                                        data.context.find(\'button\').prop(\'disabled\', false);
                                        return false;
                                    }
                                    data.formData = inputs.serializeArray();

                                }',
        ],
    ]);


} else{
echo FileUploadUI::widget([
    //'model' => $model,
    'name' => 'Files[file]',
    'url' => ['site/file-upload-lead?id='.$id.''],
    'formView'=>'@app/views/files/form',
    //'uploadTemplateId'=>'template-upload-custom',
    'downloadTemplateView' => '@app/views/files/download',
    'uploadTemplateView' => '@app/views/files/upload',
    'gallery' => false,
    'fieldOptions' => [
        //'accept' => 'image/*'
    ],
    'clientOptions' => [
        //'maxFileSize' => 2000000
    ],
    // ...
    'clientEvents' => [
        'fileuploadadd' => 'function(e, data) {
                                $(".start").removeClass("hidden");
                                $(".cancel").removeClass("hidden");
                                $(".thead").removeClass("hidden");
                                console.log("Waqar");
                                console.log(e);
                                console.log(data);
                            }',
        'fileuploaddrop' => 'function(e, data) {
                                console.log("Ahmed");
                              
                            }',
        'fileuploadsubmit' => 'function(e, data) {
                                var inputs = data.context.find(\':input\');
                                    if (inputs.filter(function () {
                                            return !this.value && $(this).prop(\'required\');
                                        }).first().focus().length) {
                                        data.context.find(\'button\').prop(\'disabled\', false);
                                        return false;
                                    }
                                    data.formData = inputs.serializeArray();

                                }',
    ],
]);

}
?>

