<?php

namespace app\modules\cdr\models;

use Yii;
use yii\web\UploadedFile;


/**
 * This is the model class for table "setting".
 *
 * @property int $id
 * @property string $logo
 */
class Setting extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public $file;
    public static function tableName()
    {
        return 'setting';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['logo'], 'required'],
            [['logo'], 'string'],
            [['file'],'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logo' => 'Logo',
        ];
    }
}
