<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "voice_mail".
 *
 * @property int $id
 * @property string $name
 * @property int $number_press
 * @property string $file
 * @property int $depart_id
 * @property string $time_to
 * @property string $time_from
 * @property string $description
 * @property int $status
 * @property string $created_on
 * @property int $created_by
 */
class VoiceMail extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'voice_mail';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number_press', 'depart_id', 'status', 'created_by'], 'integer'],
            [['name','time_to','time_from','number_press','created_on', 'created_by','file'], 'required'],
            [['created_on'], 'safe'],
            [['name', 'file', 'description'], 'string', 'max' => 255],
            [['time_to', 'time_from'], 'string', 'max' => 256],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'number_press' => 'Number Press',
            'file' => 'File',
            'depart_id' => 'Depart ID',
            'time_to' => 'Time To',
            'time_from' => 'Time From',
            'description' => 'Description',
            'status' => 'Status',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
        ];
    }
}
