<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
 use kartik\form\ActiveForm;


$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="panel card-sign" >
    <div class="card-title-sign mt-3 text-right">
        <h2 class="title text-uppercase font-weight-bold " style="margin-right: 40%"></i> Sign In</h2>
    </div>
    <div class="card-body">
        <div class="site-login">



            <?php $form = ActiveForm::begin([
                'id' => 'login-form-horizontal',
                //'type' => ActiveForm::TYPE_HORIZONTAL,
                'formConfig' => ['labelSpan' => 3, 'deviceSize' => ActiveForm::SIZE_SMALL]
            ]);
            ?>
            <?= $form->field($model, 'username')->textInput()->input('username', ['placeholder' => "Enter User Name"])->label(false);  ?>


            <?= $form->field($model, 'password')->passwordInput()->input('password', ['placeholder' => "Enter Your Password"])->label(false);  ?>





            <?= Html::submitButton('Login', ['class' => 'btn btn-primary block full-width m-b']) ?>


            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
