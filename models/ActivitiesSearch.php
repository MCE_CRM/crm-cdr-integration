<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Activities;
use DateTime;

/**
 * ActivitiesSearch represents the model behind the search form of `app\models\Activities`.
 */
class ActivitiesSearch extends Activities
{
    public $lead_assign;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_by', 'lead_id','task_id','lead_assign'], 'integer'],
            [['activity', 'created_on'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
       $currentdate=date("Y-m-d");
       $lastsevendate = date("Y-m-d", strtotime("-7 day"));
        if(!empty($_GET['ActivitiesSearch']['created_on']))
        {
           $query = Activities::find()->where(['is not', 'lead_id', new \yii\db\Expression('null')])->orderBy(['created_on'=>SORT_DESC])->groupby('lead_id');


        }
        else
        {
            $query = Activities::find()->where([
                'and',
                ['>=','activities.created_on',$lastsevendate],
                ['is not', 'activities.lead_id', new \yii\db\Expression('null')]
            ])->orderBy(['created_on'=>SORT_DESC])->groupby('lead_id');


        }
        

         
       

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
            // this $params['pagesize'] is an id of dropdown list that we set in view file
            'pagesize' => (isset($params['pagesize']) ? $params['pagesize'] :  '20'),
        ],
        ]);

        $this->load($params);
       $query->joinWith('lead');
         $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);


       
         if($roles['Admin']->name=='Admin' || $roles['SuperAdmin']->name=='SuperAdmin' || $roles['TeamLeader']->name=='TeamLeader')
        {


        }
        else
        {
        $query->andWhere(['lead_assign'=>Yii::$app->user->id]);
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        //this work for created on search filter
         if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'activities.created_on', $start_date, $end_date]);



        }


      
         $query->andFilterWhere(['leads.lead_assign' => $this->lead_assign]);
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            /*'created_on' => $this->created_on,*/
            'created_by' => $this->created_by, 
            'lead_id' => $this->lead_id,
        ]);

        $query->andFilterWhere(['like', 'activity', $this->activity]);
        

        return $dataProvider;

    }
	
	public function searchuser($params)
    {
       $currentdate=date("Y-m-d");
       $lastsevendate = date("Y-m-d", strtotime("-7 day"));
        if(!empty($_GET['ActivitiesSearch']['created_on']))
        {
           $query = Activities::find()->andWhere(['created_by' => $model->created_by])->where(['is not', 'lead_id', new \yii\db\Expression('null')])->orderBy(['created_on'=>SORT_DESC])->groupby('lead_id');
          
        }
        else
        {
            $query = Activities::find()->andWhere(['created_by' => $model->created_by])->where([
                'and',
                ['>=','activities.created_on',$lastsevendate],
                ['is not', 'activities.lead_id', new \yii\db\Expression('null')]
            ])->orderBy(['created_on'=>SORT_DESC])->groupby('lead_id');

         
        }
        

         
       

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
             'pagination' => [
            // this $params['pagesize'] is an id of dropdown list that we set in view file
            'pagesize' => (isset($params['pagesize']) ? $params['pagesize'] :  '20'),
        ],
        ]);

        $this->load($params);
       $query->joinWith('lead');
         $roles = Yii::$app->authManager->getRolesByUser(Yii::$app->user->id);
       
         if($roles['Admin']->name=='Admin' || $roles['SuperAdmin']->name=='SuperAdmin' || $roles['TeamLeader']->name=='TeamLeader')
        {
			if(!empty($_GET['ActivitiesSearch']['created_by']))
			{
				$query->andWhere(['activities.created_by'=>$_GET['ActivitiesSearch']['created_by']]);	
               
        	  // echo "<pre>";
           // print_r($query);exit;
			}

        }
        else
        {
        $query->andWhere(['activities.created_by'=>Yii::$app->user->id]);
       
        
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        //this work for created on search filter
         if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'activities.created_on', $start_date, $end_date]);



        }


      
         $query->andFilterWhere(['leads.lead_assign' => $this->lead_assign]);
        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            /*'created_on' => $this->created_on,*/
           // 'created_by' => $this->created_by,
            'lead_id' => $this->lead_id,
        ]);

        $query->andFilterWhere(['like', 'activity', $this->activity]);
    // echo "<pre>";
    // print_r($query);exit;
        //$query->andFilterWhere(['like', 'created_by', $this->created_by]);

        return $dataProvider;
    }
    public function searchByLead($params,$id='')
    {
        $query = Activities::find()->where(['lead_id'=>$id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            /*'created_on' => $this->created_on,*/
            'created_by' => $this->created_by,
            'lead_id' => $this->lead_id,
        ]);

        $query->andFilterWhere(['like', 'activity', $this->activity]);

        return $dataProvider;
    }
    public function searchByTask($params,$id='')
    {
        $query = Activities::find()->where(['task_id'=>$id]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'lead_id' => $this->lead_id,
            'task_id'=>$this->task_id,
        ]);

        $query->andFilterWhere(['like', 'activity', $this->activity]);

        return $dataProvider;
    }

}
