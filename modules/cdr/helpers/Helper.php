<?php

namespace app\modules\cdr\helpers;

use app\modules\cdr\models\Activities;
use app\modules\cdr\models\PropertyCategory;
use app\modules\cdr\models\PropertyPurpose;
use app\modules\cdr\models\Settings;
use app\modules\cdr\models\User;
use DateTime;
use Yii;
use yii\base\Exception;

/**
 * Created by PhpStorm.
 * User: Waqar
 * Date: 10/28/2017
 * Time: 2:35 PM
 */


class Helper
{


    public static function getPropertyPurpose() {

        $options = [];
        $model = PropertyPurpose::find()->where(['active'=>1])->orderBy('sort_order')->all();
        foreach ($model as $mod)
        {
            $options[$mod->name] = $mod->name;
        }

        return $options;
    }

    public  static function getPropertyUnit()
    {
        $options = [];

        $model = Settings::findByName('PROPERTY_LAND_AREA_UNIT');

        $json_decode  = json_decode($model->config_item_value, true);
        $array = $json_decode['property_land_area_unit'];
        $res = array();
        foreach ($array as $each) {

            $options[$each['unit']] = $each['name'];
            
        }
        
        return $options;

    }

    public  static function getPropertyType()
    {


        $options = [];
        $model = PropertyCategory::find()->where(['active'=>1])->orderBy('sort_order')->all();
        foreach ($model as $mod)
        {
            $options[$mod->name] = $mod->name;

        }

        return $options;


        /*$options = [];

        $model = Settings::findByName('PROPERTY_CATEGORY');

        $json_decode  = json_decode($model->config_item_value, true);
        $array = $json_decode['property_category'];
        $res = array();
        foreach ($array as $each) {

            if (isset($res[$each['code']]))
                array_push($res[$each['code']], $each['name']);
            else
                $res[$each['code']] = array($each['name']);
        }


        foreach ($res as $code => $name){
            $child_options = [];
            foreach ($name as $task)
                $child_options[$task] = $task;
            $options[$code] = $child_options;
        }


        return $options;*/

    }

    public  static function getSize($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
    }



    public static function DateTime($dateTime)
    {
        $phpdate = strtotime( $dateTime );
        $date = date( 'd/m/Y', $phpdate );
        $time = date('h:i A',$phpdate);

        return $date.'<br>'.$time;
    }

    public static function getCreated($id)
    {
        $user = User::findOne($id);
        
        return  $user->username;
    }


 

    public static function getBaseUrl()
    {
        $url = Yii::$app->homeUrl;
        $str  = str_replace("/web","",$url);

        return $str;
    }


    public static function DatTim($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);


        return $date;
    }

    public static function getMySql($dateTime)
    {
        $phpdate = strtotime($dateTime);
        $date = date('d/m/Y h:i A', $phpdate);


        return $date;
    }

    public static function getUser($id)
    {
       $user = User::findOne($id);
        return $user->first_name;
    }
    
    
    
    //Activity Saved 
    
    
    public static function setCommit($commit='',$task='',$lead='')
    {
        $activity = new Activities();
        $activity->created_on = date("Y-m-d H:i:s");
        $activity->created_by = Yii::$app->user->id;
        $activity->activity = $commit;
        $activity->task_id = $task;
        $activity->lead_id = $lead;
        $activity->save();
        
        
    }
    

}
?>