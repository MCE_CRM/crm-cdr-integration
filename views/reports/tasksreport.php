<?php
    use app\models\TaskStatus;
	use yii\helpers\ArrayHelper;
	use yii\helpers\Html;
	use kartik\grid\GridView;
	use yii\widgets\Pjax;
   $this->title="Tasks Report";



?>


<div class="row">
	<div class="col-md-12">
		 <?php Pjax::begin(['id' => 'taskGridview']) ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                /*'filterModel' => $searchModel,*/
                'responsiveWrap' => false,
                'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                'fontAwesome' => true
	            ],
	            'exportConfig' => [
	                GridView::CSV => ['label' => 'Save as CSV', 'icon' => 'file-excel'],
	                GridView::EXCEL => ['label' => 'Save as EXCEL'],
	                GridView::TEXT => ['label' => 'Save as TEXT'],
	                GridView::PDF => ['label' => 'Save as PDF'],

	            ],
                'panel' => [
                    'type' => GridView::TYPE_PRIMARY,
                    'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i><span id="card-title">'.Yii::t ( 'app', ' List' ).'</span> </h5>',
                    'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                            'index'
                        ], [
                            'class' => 'btn btn-primary btn-sm'
                        ]),


                    'showFooter' => false,

                ],

                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'task_id',
                        //'width' => '350px' ,
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $widget)
                        {
                            return '<a href="tasks/update?id='.$model->id.'">'.$model->task_id.'</a>';
                        }
                    ],
                    [
                        'attribute' => 'task_name',
                        'width' => '250px' ,
                        'format' => 'raw',

                    ],
                   
                    [
                        'attribute' => 'task_status',
                        'filterType' => GridView::FILTER_SELECT2,
                        'format' => 'raw',
                        //'width' => '100px',
                        'filter' => ArrayHelper::map ( TaskStatus::find ()->where("active=1")->orderBy ('sort_order' )->asArray ()->all (), 'id', 'status' ),
                        'filterWidgetOptions' => [
                            'options' => [
                                'placeholder' => Yii::t('app', 'All...')
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ],
                    ],
                    [
                        'attribute' => 'task_priority',
                        'filterType' => GridView::FILTER_SELECT2,
                        'format' => 'raw',
                        //'width' => '100px',
                        'filter' => ArrayHelper::map ( TaskStatus::find ()->where("active=1")->orderBy ('sort_order' )->asArray ()->all (), 'id', 'status' ),
                        'filterWidgetOptions' => [
                            'options' => [
                                'placeholder' => Yii::t('app', 'All...')
                            ],
                            'pluginOptions' => [
                                'allowClear' => true
                            ]
                        ],
                    ],
                    [
                    	'attribute' => 'Task Assign',
                    	'value'=>function($model)
                    	{
                    		 $assignname=\app\models\User::find()->select('first_name')->where(['=','id',$model->user_assigned_id])->one();
                    		 return  $assignname['first_name'];
                    	},
                    	'format' => 'raw'
                    ],
                
                    [
                        'attribute' => 'task_progress',
                        //'width'=>'80px',
                        'label' => Yii::t('app','Progress'),
                        'format' => 'raw',
                        'value' => function ($model, $key, $index, $widget)
                        {
                            $per = $model->task_progress==''?0:$model->task_progress;
                            return '<small>Progress: '.$per.'%</small>
<div class="progress progress-mini">
<div class="progress-bar" style="width:'.$model->task_progress.'%;"></div>';
    
                        }
                    ],
                   

                   
                ],
            ]); ?>
            <?php Pjax::end(); ?>
	</div>
</div>