<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activities".
 *
 * @property int $id
 * @property string $activity
 * @property string $created_on
 * @property int $created_by
 * @property int $lead_id
 */
class Activities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'activities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['activity', 'created_on', 'created_by'], 'required'],
            [['created_on'], 'safe'],
            [['created_by', 'lead_id','task_id'], 'integer'],
            [['activity'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'activity' => 'Activity',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'lead_id' => 'Lead ID',
            'task_id'=>'Task ID',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }

      public function getLead()
    {
        return $this->hasOne(Leads::className(), ['id' => 'lead_id']);
    }
}
