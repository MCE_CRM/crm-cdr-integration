<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Clients;

/**
 * ClientsSearch represents the model behind the search form of `app\models\Clients`.
 */
class ClientsSearch extends Clients
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'country', 'state', 'city', 'created_by', 'updated_by','property_id'], 'integer'],
            [['name', 'email','phone', 'mobile', 'address', 'created_on', 'updated_on','LeadType'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        if (\Yii::$app->user->can('clients/view-all')) {
            $query = Clients::find()->orderBy('id DESC');
        }
        else
        {
            $team_lead=Yii::$app->user->identity->lead_assign;
            $user=Yii::$app->user->identity->id;
            $result =  array_map('intval', explode(',', $team_lead));
            $query = Clients::find()->Where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,show_to)'))->addParams([':cat_to_find' => $result])->andWhere(['created_by'=>$result])->orderBy('id DESC');
            // echo "<pre>";
            // print_r($query);
            // exit();

            
        }

        
    


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
            // this $params['pagesize'] is an id of dropdown list that we set in view file
            'pagesize' => (isset($params['pagesize']) ? $params['pagesize'] :  '20')]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'country' => $this->country,
            'property_id' => $this->property_id,
            'state' => $this->state,
            'city' => $this->city,
            'created_on' => $this->created_on,
            'created_by' => $this->created_by,
            'updated_on' => $this->updated_on,
            'updated_by' => $this->updated_by,
        ]);

       if($_GET['ClientsSearch']['LeadType']=="NULL")
       {
           
            $query->andFilterWhere(['is', 'LeadType',new \yii\db\Expression('null')]);
       }
       else
       {
      
        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'mobile', $this->mobile])
            ->andFilterWhere(['like', 'LeadType', $this->LeadType])
            ->andFilterWhere(['like', 'address', $this->address]);
        
       }

        return $dataProvider;
    }
}
