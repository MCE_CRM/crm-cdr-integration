<?php

namespace app\modules\cdr\controllers;


use Yii;
use app\modules\cdr\models\Setting;
use app\modules\cdr\models\SettingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * SettingController implements the CRUD actions for Setting model.
 */
class SettingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Setting models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SettingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Setting model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Setting model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

 
 
        $model = new Setting();
        if ($model->load(Yii::$app->request->post())) {

           // if($_POST['pictureval']) {echo 'sdsdsdsdsd';}
            //echo '<pre>';print_r($_POST);echo '</pre>';exit;
            //echo Yii::$app->user->id;exit;
            $tempSave = UploadedFile::getInstance($model, 'logo');
            
            if (!empty($tempSave->name)){
                $filename = date('Ymdhis');
                $model->file = UploadedFile::getInstance($model, 'logo');
                $file = date('Ymdhis') . '.png';
                //$success = file_put_contents(\Yii::getAlias('@webroot') . '/regimages/' . $file, $data);
                $model->file->saveAs(\Yii::getAlias('@webroot') .'/logoimages/'.$filename.'.'.$model->file->extension);
                $model->logo = $filename.'.'.$model->file->extension;

            }
           
            
            if($model->save()){

                return $this->redirect(['index']);

            }
            else{
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }




        /*if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);*/
    }

    /**
     * Updates an existing Setting model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
     public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

           // if($_POST['pictureval']) {echo 'sdsdsdsdsd';}
            //echo '<pre>';print_r($_POST);echo '</pre>';exit;
            //echo Yii::$app->user->id;exit;
            $tempSave = UploadedFile::getInstance($model, 'logo');
            
            if (!empty($tempSave->name)){
                $filename = date('Ymdhis');
                $model->file = UploadedFile::getInstance($model, 'logo');
                $file = date('Ymdhis') . '.png';
                //$success = file_put_contents(\Yii::getAlias('@webroot') . '/regimages/' . $file, $data);
                $model->file->saveAs(\Yii::getAlias('@webroot') .'/logoimages/'.$filename.'.'.$model->file->extension);
                $model->logo = $filename.'.'.$model->file->extension;

            }
           
            
            if($model->save()){

             
             

                return $this->redirect(['index']);

            }
            else{
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }


    }
    /**
     * Deletes an existing Setting model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Setting model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Setting the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Setting::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
