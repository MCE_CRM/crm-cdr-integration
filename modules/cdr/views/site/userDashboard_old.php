
<?php

/* @var $this yii\web\View */

\app\assets\DashboardAsset::register($this);
$this->title = 'Dashboard';
$total_csr = 0;
$total_agents = 0;
$user = \app\models\User::find()->all();
foreach ($user as $data)
{
    $role = getUserRoles($data->id);
    if($role ==='CSR')
    {
        $total_csr++;
    }
    if($role =="Agent")
    {
        $total_agents++;
    }
}

function getUserRoles($user_id){
    $connection = \Yii::$app->db;
    $sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
    $command=$connection->createCommand($sql);
    $dataReader=$command->queryAll();
    $roles ='';
    if(count($dataReader) > 0){
        foreach($dataReader as $role){
            $roles= $role['name'];
        }
    }
    return $roles;
}


?>




<style>
.datepicker
{
    margin-top: -33px;
}

.card-header
{
    text-align:center;
}
.ibox {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}
.ibox-title {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 2px 0 0;
    color: inherit;
    margin-bottom: 0;
    padding: 15px 15px 7px;
    min-height: 48px;
}


.ibox-title .label {
    float: left;
    margin-left: 4px;
}
.label, .ibox .label {
    font-size: 10px;
    border-radius: 10px;
}
.label-success, .badge-success {
    background-color: #1c84c6;
    color: #FFFFFF;
}
.label {
    background-color:#2baab1;
    color:white;
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: 600;
    padding: 0px 8px;
    text-shadow: none;
}

.ibox-title h5 {
    display: inline-block;
    font-size: 14px;
    margin: 0 0 7px;
    padding: 0;
    text-overflow: ellipsis;
    float: left;
    font-weight: 600;
}
.ibox-content {
    clear: both;
}

.ibox-content {
    background-color: #ffffff;
    color: inherit;
    padding: 15px 20px 20px 20px;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 0;
}
small, .small {
    font-size: 85%;
}
.no-margins {
    margin: 0 !important;
}
.stat-percent {
    float: right;
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
}
#myChart
{
    height:100% !important;
    width:100% !important;

}
.amcharts-chart-div a {display:none !important;}
a.dlink:hover{
    text-decoration: none;
}



#notdashboard{
    display: none;
}

.topnav {
  overflow: hidden;
 /* background-color: white;*/
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;

  display: inline-block;
    border-radius: 4px;
    font-size: 12px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    padding: 10px;
    text-transform: uppercase;
    white-space: initial;
}

.active {
  background-color: white;
  color: black;
}

.topnav .icon {
  display: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 17px;    
  border: none;
  outline: none;
  color: black;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;

  display: inline-block;
    border-radius: 4px;
    font-size: 12px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    padding: 10px;
    text-transform: uppercase;
    white-space: initial;
}

.dropdown-content {
  display: none;
  position: fixed;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 9999;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.topnav a:hover, .dropdown:hover .dropbtn {
  background-color: #555;
  color: white;
}

.dropdown-content a:hover {
  background-color: #ddd;
  color: black;
}

.dropdown:hover .dropdown-content {
  display: block;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child), .dropdown .dropbtn {
    display: none;
  }
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .topnav.responsive .dropdown {float: none;}
  .topnav.responsive .dropdown-content {position: relative;}
  .topnav.responsive .dropdown .dropbtn {
    display: block;
    width: 100%;
    text-align: left;
  }
}

.top-navigation .wrapper.wrapper-content {
    padding: 0px;
}
</style>

         <?php
   $query = \app\models\Setting::find()->orderBy('id DESC')->limit(1)->offset(0)->one();
  
  ?>
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.js"></script> 
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.init.js"></script>
<div class="row" style=" width: 95%;">
    <div class="col-lg-12">
        <div class="logo" style="float: left;">
        <a href="#" class=""><img src="<?= Yii::$app->homeUrl ?>logoimages/<?=$query->logo?>" style = "width: 80px; height: 45px;"></a>
    </div>
<div class="topnav" id="myTopnav">
   
<a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/dashboard">Dashboard</a>
 <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/cdr">CDR</a>
            
  <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/agent-status">Agent Status</a>

  <div class="dropdown">
    <button class="dropbtn">Reports 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
        <?php if(Yii::$app->user->can('cdr/logo')){?>
            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>agent-session">Agent Attendance Report</a>
        <?php } ?>
    </div>
  </div> 

  <div class="dropdown">
    <button class="dropbtn">Setting 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
       <?php if(Yii::$app->user->can('cdr/logo')){?>
                    <a href="<?= Yii::$app->homeUrl?>setting/index">logo upload</a>
                    <?php } ?>

                    <?php if(Yii::$app->user->can('user/management')){?>
                        
                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>user/index">Users Managnment</a>
                        
                    <?php } ?>
                    <?php if(Yii::$app->user->can('user/management')){?>
                        
                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>rbac">Role Based Access Control</a>
                        
                    <?php } ?>
    </div>
  </div> 

 
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>
</div>
</div>
</div>
<br><br><br><br><br>
<div class="row">

    <div class="col-lg-6">
        <div class="row mb-3">
            <div class="col-xl-6">
                
                <section class="card card-featured-left card-featured-tertiary mb-3">
                   <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fas fa-phone"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title">Total Login/Active CSRs</h4>
                                    <div class="info">
                                        <strong class="amount">
                                            <?php
                                            date_default_timezone_set("Asia/Karachi");

                                              exec('sudo /usr/sbin/asterisk -r -x "queue show" 2>&1',$online);
                                              /*echo $online;*/
                                              echo"2";

                                            ?>
                                            

                                        </strong>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                     </a>
                </section>
           
            </div>

            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fas fa-mobile-alt"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> Total Calls</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalcalls;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </a>
                </section>
            
            </div>
            
        </div>
        <div class="row">

            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-quaternary">
                                    <i class="fas fa-tasks"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">No Answer/dropped Calls</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalbusy;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-crown"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> Total Transfer</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totaltransfer;?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>

  

              <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-crown"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Total Voicemails:</h4>
                                    <div class="info">
                                        <strong class="amount"></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="row mb-3">
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-secondary">
                                    <i class="fas fa-project-diagram"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Total Incoming</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalincomming;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-home"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> Total Outgoing</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totaloutgoing;?></strong>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>

        </div>
        <div class="row">

            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-quaternary">
                                    <i class="fas fa-users"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> SIP STATUS</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $sipstatus;echo"<br>";echo "ip:".$sipip;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>
        <!--     <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-microphone"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">Total CSR</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $total_csr;?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div> -->

                        <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-crown"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title">User System/Computer IP:</h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo 'User IP - '.$_SERVER['REMOTE_ADDR'];?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>

        </div>
    </div>
</div>





<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}
</script>

<!-- HTML -->









