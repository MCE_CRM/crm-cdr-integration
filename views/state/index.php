<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadStatusSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */




$this->title = 'States';
$this->params['breadcrumbs'][] = $this->title;



?>
<div class="state-index">
    <?php Pjax::begin(); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New State'), [
                'create'
            ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                'class' => 'btn btn-default btn-sm add-new'
            ]),
            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'state',
            //'country_id',
            [
                'attribute'=>'country_id',
                'value'=>'county.country',
                'format'=>'raw',
            ],
           // 'state_code',

            [
                'attribute'=>'active',
                'value'=>function($model)
                {
                    return statusLabel($model->active);
                },
                'format'=>'raw',
            ],
            //'created_on',
            //'created_by',
            //'updated_on',
            //'updated_by',
            //'country_id',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update} {defaultValue}',
                'buttons' => [
                    'update' => function ($url, $model)

                    {
                        return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'state\',\'Update State\',event)"></span></a>';
                    } ,
                    'defaultValue' => function ($url, $model) {
                        if(\app\models\DefaultValueModule::checkDefaultValue('state',$model->id)){
                            return Html::a('<span class="fa fa-eraser"></span>', Yii::$app->urlManager->createUrl(['state/index','del_id' => $model->id]), [
                                'title' => Yii::t('app', 'Delete Default'),
                            ]);
                        }else{
                            return Html::a('<span class="fa fa-tag"></span>', Yii::$app->urlManager->createUrl(['state/index','id' => $model->id]), [
                                'title' => Yii::t('app', 'Make Default'),
                            ]);
                        }
                    }

                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>


</div>

