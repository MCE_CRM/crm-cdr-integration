<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use app\modules\cdr\models\Cdr;
use app\modules\cdr\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\CdrSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<script>
    /*if(typeof window.history.pushState == 'function') {
        window.history.pushState({}, "Hide", '');
    }*/
</script>

<?php //echo $_SERVER['PHP_SELF'];?>

        <span class="input-group-text"><i class="fa fa-calendar-alt"></i></span>
		<div class="cdr-search">

    <?php $form = ActiveForm::begin([
        'action' => ['user-deshboard'],
        'method' => 'get',
    ]); ?>


    <div class="row">
        

        <div class="col-md-2 col-md-3">
            <?php 
			if(isset($_GET['CdrSearch']['channelsearch']) && !empty($_GET['CdrSearch']['channelsearch']))
			{
				$model->channelsearch = $_GET['CdrSearch']['channelsearch'];
			}
			$all = array();
			$users =  User::find()->select('first_name,agent_id')->where(['NOT LIKE','agent_id','NULL'])->asArray()->all();
			$open_user = array('first_name'=>'All','agent_id'=>'all');
			
			array_push($users,$open_user);
			if(!empty($_GET['CdrSearch']['channelsearch']))
			{
				$id = $_GET['CdrSearch']['channelsearch'];
			}else
			{
				$id = 'all';
			}
			?>
			<?= $form->field($model, 'channelsearch')
                ->dropDownList(
                    ArrayHelper::map($users, 'agent_id', 'first_name'),
					['options' =>
						[                        
							  $id => ['selected' => true]
						]

          			])->label('Agent',['class'=>'label-class']); ?>
        </div>
        
        

        <div class="col-md-2 col-md-3">

              <?php
           $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;


           echo '<label class="control-label">Date Range</label>';
           echo '<div class="drp-container">';
           echo DateRangePicker::widget([
               'model'=>$model,
               'attribute'=>'calldate',
               'convertFormat'=>true,
               'pluginOptions'=>[
                   'opens'=>'left',
                   'ranges' => [

                       "Today" => ["moment().startOf('day')", "moment()"],
                       "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                       "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                   ],

                   'timePicker'=>true,
                   'timePickerIncrement'=>05,
                   'locale'=>['format'=>'d/m/Y h:i A']
               ],
               'presetDropdown'=>false,
               'hideInput'=>true
           ]);
           echo '</div>'; ?>
            
        </div>
        <div class="col-md-2 col-md-3" style="margin-top:24px;">
            
            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>

        </div>
       
\\
        </div>


    </div>

  

    <!-- <?= $form->field($model, 'clid') ?> -->



<?php ActiveForm::end(); ?>


</div>





    



