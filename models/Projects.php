<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "projects".
 *
 * @property int $id
 * @property string $name
 * @property string $category
 * @property string $state
 * @property string $city
 * @property string $blocksector
 * @property string $address
 * @property string $description
 * @property string $additional_form
 * @property int $active
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Projects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'projects';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'state','country', 'city','category','active','address', 'description', 'created_on', 'created_by'], 'required'],
            [['description'], 'string'],
            [['created_on', 'updated_on'], 'safe'],
            [['active','country','state','city','created_by', 'updated_by','blocksector'], 'integer'],
            [['category'], 'string', 'max' => 50],
            [['name', 'address','additional_form'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Project Name',
            'country'=>'Country',
            'blocksector'=>'Block/Sector',
            'state' => 'State',
            'city' => 'City',
            'category'=>'Category',
            'address' => 'Address',
            'active' => 'Active',
            'description' => 'Description',
            'additional_form'=>'additional_form',
            'created_on' => 'Added On',
            'created_by' => 'Added By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
			'project_assign'=>'Property Add',
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);

    }

    public function getStat()
    {
        return $this->hasOne(State::className(), ['id' => 'state']);
    }

    public function getCit()
    {
        return $this->hasOne(City::className(), ['id' => 'city']);
    }


    public function getCountr()
    {
        return $this->hasOne(Country::className(), ['id' => 'country']);
    }


    public function getProperty()
    {
        return $this->hasMany(Property::className(), ['project_id' => 'id']);

    }
     public function getBlocksector()
    {
        return $this->hasMany(Blocksector::className(), ['id' => 'blocksector']);

    }


}
