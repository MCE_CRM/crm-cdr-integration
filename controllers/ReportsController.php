<?php

namespace app\controllers;


use app\models\Files;
use app\models\FollowUpSearch;
use app\models\ActivitiesSearch;
use app\models\Settings;
use app\models\TaskStatus;
use app\models\User;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\FileHelper;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Projects;
use app\models\Tasks;
use app\models\Leads; 
use app\models\Property;
use app\models\LeadStatus;
use yii\data\ActiveDataProvider;
use app\models\ProjectsSearch;
use app\models\leadsSearch;
use app\models\Blocksector;
use DateTime;

class ReportsController extends Controller
{
    /**
     * {@inheritdoc}
     */

    public function behaviors()
    {

        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'actions' => ['login','lock-screen'],
                    'allow' => true,
                ],
                [

                    'allow' => true,
                    'roles' => ['@'],
                    'matchCallback' => function ($rule, $action) {

                        // $module                 = Yii::$app->controller->module->id;
                        $action                 = Yii::$app->controller->action->id;
                        $controller         = Yii::$app->controller->id;
                        $route                     = "$controller/$action";
                        $post = Yii::$app->request->post();


                        if (\Yii::$app->user->can($route)) {
                            return true;
                        }else{
                            return false;
                        }

                    }
                ],
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'logout' => ['GET'],
            ]
        ];


        return $behaviors;
    }


    /**
     * {@inheritdoc}
     */

    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }


    public function actions()
    {

        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        $searchModel = new ProjectsSearch();
        $dataProvider = $searchModel->searchReport(Yii::$app->request->queryParams);
        //$dataProvider->pagination= false;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionProperty()
    {
        //searching property using project and block sector============================
        if(isset($_GET['projectid']) && empty($_GET['blocksectorid']))
        {
            $projectid=$_GET['projectid'];
            if($projectid=='NULL')
            {
                
              $result=Property::find()->where(['project_id'=>NULL])->all();
             $projects=Projects::find()->all();
             $blocksector=Blocksector::find()->all();

               $title="Open Property";
                                  $Available=\app\models\Property::find()->where([
                                        'and',
                                        ['=','status','Available'],
                                        ['project_id'=>NULL]
                                    ])->count();
                                    $Sold=\app\models\Property::find()->where([ 'and',
                                        ['=','status','Sold'],
                                        ['project_id'=>NULL]
                                    ])->count();
                                    $Occupy=\app\models\Property::find()->where([ 'and',
                                        ['=','status','Occupied'],
                                        ['project_id'=>NULL]
                                    ])->count();
            return $this->render('property',[
                'result'=>$result,
                'projects'=>$projects,
                'blocksector'=>$blocksector,
                'Available'=>$Available,
                'Sold'=>$Sold,
                'Occupy'=>$Occupy,
                'title'=>$title,

            ]);
            }
           
       
        elseif($projectid=='All')
        {
            
            $result=Property::find()->all();
            $projects=Projects::find()->Where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,project_show)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orWhere(['=','project_show', ''])->all();
            $blocksector=Blocksector::find()->all();

             $Available=\app\models\Property::find()->where(
                                    ['status'=>'Available'])->count();
                                $Sold=\app\models\Property::find()->where(
                                    ['=','status','Sold'])->count();
                                $Occupy=\app\models\Property::find()->where(
                                    ['=','status','Occupied'])->count();
                    $title="All Property";
            return $this->render('property',[
                'result'=>$result,
                'projects'=>$projects,
                'blocksector'=>$blocksector,
                'Available'=>$Available,
                'Sold'=>$Sold,
                'Occupy'=>$Occupy,
                'title'=>$title,
            ]);
        }
         else
            {
                
            $result=Property::find()->where(['project_id'=>$projectid])->all();
            $projects=Projects::find()->Where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,project_show)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orWhere(['=','project_show', ''])->all();
           $blocksector=Blocksector::find()->all();
             $projectresult=Projects::find()->select('name')->where(['id'=>$projectid])->one();
                $title= $projectresult->name;
                $Available=\app\models\Property::find()->where([
                    'and',
                    ['=','status','Available'],
                    ['project_id'=>$_GET['projectid']]
                ])->count();
                $Sold=\app\models\Property::find()->where([ 'and',
                    ['=','status','Sold'],
                    ['project_id'=>$_GET['projectid']]
                ])->count();
                $Occupy=\app\models\Property::find()->where([ 'and',
                    ['=','status','Occupied'],
                    ['project_id'=>$_GET['projectid']]
                ])->count();
            return $this->render('property',[
                'result'=>$result,
                'projects'=>$projects,
                'blocksector'=>$blocksector,
                'Available'=>$Available,
                'Sold'=>$Sold,
                'Occupy'=>$Occupy,
                'title'=>$title,
            ]);
           }
       }
       elseif(isset($_GET['projectid']) && isset($_GET['blocksectorid']))
       {
           $blocksector=Blocksector::find()->all();
           $blocksectorresult=Blocksector::find()->select('blocksector')->where(['id'=>$_GET['blocksectorid']])->one();
           $projects=Projects::find()->Where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,project_show)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orWhere(['=','project_show', ''])->all();
           $projectresult=Projects::find()->select('name')->where(['id'=>$_GET['projectid']])->one();
                if($_GET['projectid']=="All")
                {
                    $title="All";
                    $blocsectortitle=$blocksectorresult->blocksector;
                }
                elseif($_GET['projectid']=="NULL")
                {
                    $title="Open Property";
                    $blocsectortitle=$blocksectorresult->blocksector;
                }
                else
                {
                $title= $projectresult->name;
                $blocsectortitle=$blocksectorresult->blocksector;
                }
          $result=Property::find()->where([
            'and',
            ['blocksector'=>$_GET['blocksectorid']],
            ['project_id'=>$_GET['projectid']]
        ])->all();
           $Available=\app\models\Property::find()->where([
                    'and',
                    ['=','status','Available'],
                    ['blocksector'=>$_GET['blocksectorid']],
                    ['project_id'=>$_GET['projectid']]
                ])->count();
                $Sold=\app\models\Property::find()->where([ 'and',
                    ['=','status','Sold'],
                    ['blocksector'=>$_GET['blocksectorid']],
                    ['project_id'=>$_GET['projectid']]
                ])->count();
                $Occupy=\app\models\Property::find()->where([ 'and',
                    ['=','status','Occupied'],
                    ['blocksector'=>$_GET['blocksectorid']],
                    ['project_id'=>$_GET['projectid']]
                ])->count();

                 return $this->render('property',[
                'result'=>$result,
                'projects'=>$projects,
                'blocksector'=>$blocksector,
                'Available'=>$Available,
                'Sold'=>$Sold,
                'Occupy'=>$Occupy,
                'title'=>$title,
                'blocsectortitle'=>$blocsectortitle,
            ]);
       }
      //end of condition


      //getting all property===================================================
         $result=Property::find()->all();
            $projects=Projects::find()->Where(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,project_show)'))->addParams([':cat_to_find' => Yii::$app->user->id])->orWhere(['=','project_show', ''])->all();
            $blocksector=Blocksector::find()->all();

             $Available=\app\models\Property::find()->where(
                                    ['status'=>'Available'])->count();
                                $Sold=\app\models\Property::find()->where(
                                    ['=','status','Sold'])->count();
                                $Occupy=\app\models\Property::find()->where(
                                    ['=','status','Occupied'])->count();
                    $title="Property";
            return $this->render('property',[
                'result'=>$result,
                'projects'=>$projects,
                'blocksector'=>$blocksector,
                'Available'=>$Available,
                'Sold'=>$Sold,
                'Occupy'=>$Occupy,
                'title'=>$title,
            ]);

            //=====================================================================

    }

    public function actionLeads()
    {
        $searchModel = new leadsSearch();
        $dataProvider = $searchModel->searchLeadsReport(Yii::$app->request->queryParams);
        $start_date=$searchModel->searchStart(Yii::$app->request->queryParams);
        $end_date=$searchModel->searchEnd(Yii::$app->request->queryParams);
        $lead_status = LeadStatus::find()->where(['active'=>1])->orderBy([
            'sort_order' => SORT_ASC,
        ])->all();



        return $this->render('leadsreport', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'leadStaus'=>$lead_status,
            'start_date'=>$start_date,
            'end_date'=>$end_date


        ]);
    }


    public function actionFollowUp()
    {
        $model = new FollowUpSearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('follow-up', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);

    }
	//////////////////////////////////////////////////
	public function actionFollowUserWise()
    {
        $model = new FollowUpSearch();
		$dataProvider = $model->searchuserwise(Yii::$app->request->queryParams);
		
$daterange = '';
		if (!empty($_GET['FollowUpSearch']['from'])) {
			$fromdate = $_GET['FollowUpSearch']['from'];
			$from = DateTime::createFromFormat('d/m/Y',$fromdate)->format('Y-m-d');
			$daterange = date('d-m-Y',strtotime($from));
		}else
		{
			$from = date('Y-m-d',strtotime('-15 days'));
			$daterange = date('d-m-Y',strtotime($from));
		}
		if (!empty($_GET['FollowUpSearch']['to'])) {
			$todate = $_GET['FollowUpSearch']['to'];
			$to = DateTime::createFromFormat('d/m/Y',$todate)->format('Y-m-d');
			$daterange .= ' '.date('d-m-Y',strtotime($to));
		} else
		{
			$to = date('Y-m-d');
			$daterange .= ' '.date('d-m-Y',strtotime($to));
		}
		if (empty($_GET['FollowUpSearch']['from']) && empty($_GET['FollowUpSearch']['to'])) {
			$from = date('Y-m-d',strtotime('-15 days')).' 00:00:00';
			$to = date('Y-m-d').' 23:59:59';
			$daterange = date('d-m-Y',strtotime($from)).' '.date('d-m-Y',strtotime($to));
		}
        return $this->render('follow-userwise', [
            'model' => $model,
            'dataProvider' => $dataProvider,
			'modeldaterange'=>$daterange,
        ]);

    }
	

     public function actionActivityUp()
    {
        $model = new ActivitiesSearch();
        $dataProvider = $model->search(Yii::$app->request->queryParams);

        return $this->render('acitivity-up', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);

    }

	public function actionActivityUserWise()
    {
        $model = new ActivitiesSearch();
        $dataProvider = $model->searchuser(Yii::$app->request->queryParams);

        return $this->render('acitivity-userwise', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);

    }



    public function actionUserDashboard()
    {
        $id = $_GET['id'];
		$start_date ='';
		$end_date ='';
		if(isset($_GET['leadsSearch']['created_on']))
		{
			$datetime = explode(' - ',$_GET['leadsSearch']['created_on']);
			$start_date = date('Y-m-d', strtotime($datetime[0]));
			$end_date = date('Y-m-d', strtotime($datetime[1]));
		}
		
       	 
		$model = new leadsSearch();
        $this->layout = 'dashboard';
		if(!empty($start_date))
		{
			$totalLeads= Leads::find()->where([
				'and',
				['active'=>1],
				['lead_assign'=>$id]
			])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
		}else
		{
			$totalLeads= Leads::find()->where([
				'and',
				['active'=>1],
				['lead_assign'=>$id]
			])->count();			
		}
        /*count all pending tasks*/
		if(!empty($start_date))
		{
			$pendingtask= Tasks::find()->where([
				'and',
				['!=','task_status','completed'],
				['user_assigned_id'=>$id]
			])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
		}else
		{
			$pendingtask= Tasks::find()->where([
				'and',
				['!=','task_status','completed'],
				['user_assigned_id'=>$id]
			])->count();
		}
        /*Total tasks */
		if(!empty($start_date))
		{
        	$totaltasks=Tasks::find()->where(['user_assigned_id'=>$id])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
		}else
		{
			 $totaltasks=Tasks::find()->where(['user_assigned_id'=>$id])->count();
		}

        /*Today Leads*/
		if(!empty($start_date))
		{
			$todayleads= Leads::find()->where([
				'and',
				['LIKE','created_on',date("Y-m-d")],
				['lead_assign'=>$id]
			])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
		}else
		{
			 $todayleads= Leads::find()->where([
            'and',
            ['LIKE','created_on',date("Y-m-d")],
            ['lead_assign'=>$id]
        	])->count();
		}
        $lead_status = LeadStatus::find()->where(['active'=>1])->orderBy([
            'sort_order' => SORT_ASC,
        ])->all();

        $task_status=TaskStatus::find()->where(['active'=>1])->orderBy(['sort_order'=>SORT_ASC,
        ])->all();




        return $this->render('user-dashboard',
            [
				'model' => $model,
                'todayleads'=>$todayleads,
                'totalLeads'=>$totalLeads,
                'pending_tasks'=>$pendingtask,
                'totaltasks'=>$totaltasks,
                'leadStaus' =>$lead_status,
                'taskstatus'=>$task_status

        ]);
    }

}
