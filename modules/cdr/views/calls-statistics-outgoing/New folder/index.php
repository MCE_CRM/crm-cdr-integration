<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CallsStatisticsOutgoingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calls Statistics Outgoings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calls-statistics-outgoing-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Calls Statistics Outgoing', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'calldate',
            'total_calls',
            'ans_calls',
            'no_ans_calls',
            //'entry_date',
            //'last_updated',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
