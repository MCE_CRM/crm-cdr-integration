<?php

use app\models\DefaultValueModule;
use app\models\TaskStatus;
use kartik\slider\Slider;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\widgets\Pjax;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\Tasks */
/* @var $form yii\widgets\ActiveForm */
?>

<style>
    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }

    .tooltip{
        opacity: 1;
    }
</style>

<div class="tasks-form">


    <?php $form = ActiveForm::begin ( [
        'type' => ActiveForm::TYPE_VERTICAL,
        'errorCssClass' => 'has-danger',

    ] ); ?>


    <section class="card " id="card-1" data-portlet-item>
        <header class="card-header portlet-handler">


            <h4 class="card-title">View Task</h4>
        </header>
        <div class="card-body">
            <?php // $form->field($model, 'task_id')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'task_name')->textInput(['maxlength' => true]) ?>

            <div class="row">
                <div class="col-lg-3">
                    <?php

                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'user_assigned_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\User::find()->all(),'id','username'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a User ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);




                    ?>
                </div>
                <div class="col-lg-3">
                    <?php

                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'task_status')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\TaskStatus::find()->where(['active'=>1])->orderBy([
                            'sort_order' => SORT_ASC,
                        ])->all(),'status','status'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Status ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);




                    ?>
                </div>
                <div class="col-lg-3">
                    <?php

                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'task_priority')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\TaskPriority::find()->where(['active'=>1])->orderBy([
                            'sort_order' => SORT_ASC,
                        ])->all(),'priority','priority'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Priority ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);




                    ?>

                </div>

                <div class="col-lg-3">
                    <?php

                    if(!isset($_REQUEST['id'])){
                        $model->task_progress=0;
                    }

                    echo $form->field($model, 'task_progress')->widget(Slider::classname(), [
                        'value'=>0,

                        'sliderColor' => Slider::TYPE_SUCCESS,
                        'handleColor' => Slider::TYPE_SUCCESS,
                        'pluginOptions' => [
                            'handle' => 'square',
                            'min' => 0,
                            'max' => 100,
                            'step' => 10,
                            'tooltip'=>'always',
                        ]
                    ]);



                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3">
                    <?php

                    // usage without model
                    echo '<label class="control-label" for="expected_start_datetime">Expected Start Date</label>';
                    echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'expected_start_datetime',
                        'options' => ['placeholder' => 'Select date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ],
                        'disabled' => $dFlag,
                    ]);

                    ?>

                </div>
                <div class="col-lg-3">
                    <?php

                    // usage without model
                    echo '<label class="control-label" for="expected_end_datetime">Expected End Date</label>';
                    echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'expected_end_datetime',
                        'options' => ['placeholder' => 'Select date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ],
                        'disabled' => $dFlag,
                    ]);

                    ?>

                </div>
                <div class="col-lg-3">
                    <?php

                    // usage without model
                    echo '<label class="control-label" for="actual_start_datetime">Actual Start Date</label>';
                    echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'actual_start_datetime',
                        'options' => ['placeholder' => 'Select date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ],
                        'disabled' => $dFlag,
                    ]);

                    ?>

                </div>

                <div class="col-lg-3">
                    <?php

                    // usage without model
                    echo '<label class="control-label" for="actual_end_datetime">Actual End Date</label>';
                    echo DatePicker::widget([
                        'model' => $model,
                        'attribute' => 'actual_end_datetime',
                        'options' => ['placeholder' => 'Select date ...'],
                        'pluginOptions' => [
                            'format' => 'yyyy-mm-dd',
                            'todayHighlight' => true,
                        ],
                        'disabled' => $dFlag,
                    ]);

                    ?>

                </div>
            </div>





            <?php // $form->field($model, 'created_on')->textInput() ?>

            <?php // $form->field($model, 'created_by')->textInput() ?>

            <?php // $form->field($model, 'updated_on')->textInput() ?>

            <?php // $form->field($model, 'updated_by')->textInput() ?>

<br>

            <div class="tabs">
                <ul class="nav nav-tabs">
                    <li class="nav-item active">
                        <a class="nav-link" href="#description" data-toggle="tab">Task Description</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#attachment" data-toggle="tab">Attachments</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#notes" data-toggle="tab">Notes</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#activities" data-toggle="tab">Activities</a>
                    </li>
                </ul>

                <div class="tab-content">
                    <div id="description" class="tab-pane active show">
                        <?= $form->field($model, 'task_description')->textarea(['rows' => 6]) ?>

                    </div>
                    <div id="attachment" class="tab-pane">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="files-index">
                                    <?php Pjax::begin(['id' => 'filesPjax']); ?>

                                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                                    <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        //'filterModel' => $searchModel,
                                        'responsiveWrap' => false,
                                        //'filter'=>false,
                                        'toolbar' =>  [
                                            //'{export}',
                                            //'{toggleData}',
                                        ],

                                        'panel' => [
                                            'type' => GridView::TYPE_PRIMARY,
                                            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                                            'showFooter' => false,

                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],

                                            // 'id',
                                            'file_name',
                                            'custom_name',
                                            'type',
                                            //'size',
                                            [
                                                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                                'contentOptions' => ['class' => 'text-center'],
                                                'attribute' =>  'size',
                                                'value' => function($model){
                                                    return \app\helpers\Helper::getSize($model->size);
                                                },
                                                //'filter'=>false

                                            ],

                                            //'project_id',
                                            //'inventory_id',
                                            [
                                                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                                'contentOptions' => ['class' => 'text-center'],
                                                'attribute' =>  'created_on',
                                                'value' => function($model){
                                                    return date("d/m/Y", strtotime($model->created_on));
                                                },
                                                //'filter'=>false

                                            ],
                                            [
                                                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                                'attribute' => 'created_by',
                                                'contentOptions' => ['class' => 'text-center'],
                                                'value'=>'user.username',
                                                'format'=>'raw',

                                            ],
                                            //'updated_on',
                                            //'updated_by',
                                            //'custom_name',
                                            //'type',
                                            //'size',

                                            [
                                                'class' => '\kartik\grid\ActionColumn',
                                                'template'=>'{play}{update}{download}{delete}',
                                                'buttons' => [
                                                    'play' => function ($url, $model)

                                                    {
                                                        return '<a href="'.\app\helpers\Helper::getBaseUrl().'files/task'.$model->task_id.'/'.$model->file_name.'" class="html5lightbox"><span class="fas fa-play"></span></a>'.'&nbsp;';
                                                    } ,
                                                    'update' => function ($url, $model)

                                                    {
                                                        return '<a href="javascript:void(0)"><span class="fa fa-pencil-alt"  onclick="editLeadFile('.$model->id.',\''.$model->custom_name.'\')"></span></a>'.'&nbsp;';
                                                    } ,
                                                    'download' => function ($url, $model)

                                                    {
                                                        return '<a href="'.\app\helpers\Helper::getBaseUrl().'files/task'.$model->task_id.'/'.$model->file_name.'"  data-pjax =0 download><span class="fas fa-download" ></span></a>'.'&nbsp;';
                                                    } ,
                                                    'delete' => function ($url, $model) {
                                                        return '<a href="javascript:void(0)"><span class="far fa-trash-alt"  onclick="delLeadFile('.$model->id.')"></span></a>'.'&nbsp;';

                                                    }

                                                ],
                                            ],
                                        ],
                                    ]); ?>
                                    <?php Pjax::end(); ?>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div id="notes" class="tab-pane">
                        <div class="timeline timeline-simple mt-3 mb-3">
                            <div class="tm-body">
                                <div class="tm-title">
                                    <h5 class="m-0 pt-2 pb-2 text-uppercase">Notes</h5>
                                </div>
                                <ol class="tm-items">

                                </ol>
                            </div>
                        </div>


                    </div>
                    <div id="activities" class="tab-pane">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="files-index">

                                    <?php Pjax::begin(['id' => 'activitiesPjax']);
                                    ?>
                                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


                                    <?= GridView::widget([
                                        'dataProvider' => $dataProviderActivity,
                                        //'filterModel' => $searchModel,
                                        'responsiveWrap' => false,
                                        //'filter'=>false,
                                        'toolbar' =>  [
                                            //'{export}',
                                            //'{toggleData}',
                                        ],

                                        'panel' => [
                                            'type' => GridView::TYPE_PRIMARY,
                                            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                                            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                                                    'index'
                                                ], [
                                                    'class' => 'btn btn-primary btn-sm'
                                                ]),


                                            'showFooter' => false,

                                        ],
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],

                                            // 'id',
                                            'activity',
                                            [
                                                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                                'contentOptions' => ['class' => 'text-center'],
                                                'attribute' =>  'created_on',
                                                'value' => function($model){
                                                    return date("d/m/Y", strtotime($model->created_on));
                                                },
                                                //'filter'=>false

                                            ],
                                            [
                                                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                                                'attribute' => 'created_by',
                                                'contentOptions' => ['class' => 'text-center'],
                                                'value'=>'user.username',
                                                'format'=>'raw',

                                            ],
                                            //'updated_on',
                                            //'updated_by',
                                            //'custom_name',
                                            //'type',
                                            //'size',

                                        ],
                                    ]); ?>
                                    <?php Pjax::end(); ?>
                                </div>
                            </div>


                        </div>


                    </div>
                </div>


                <?= Html::submitButton('Update', ['class' => 'btn btn-primary btn-sm ']) ?>

                <a href="javascript:void(0)" class="btn btn-primary btn-sm notes" ><i class="fas fa-comment"></i> New Note</a>
                <a href="javascript:void(0)" class="btn btn-primary btn-sm attach-files"><i class="fas fa-save"></i> New Attachment</a>

                <?php ActiveForm::end(); ?>

            </div>

        </div>



    </section>




</div>


<script>



    $( ".attach-files" ).on( "click", function() {

        event.preventDefault();
        var url = '<?= Yii::$app->homeUrl?>ajax/attach-files?type="task"&id=<?= $model->id?>';
        var dialog = bootbox.dialog({
            title: 'Attach Files',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
            size: 'large',
            onEscape: function() {
                $.pjax.reload({container: '#filesPjax', async: false});
            }

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });



        });


    });

    $( ".notes" ).on( "click", function() {


        var html  = '<section class="simple-compose-box mb-3">'+
            '<form method="get" id="note_form" action="<?= Yii::$app->homeUrl?>ajax/submit-note?type="task"&id=<?= $model->id?>">'+
            '<textarea name="message-text" data-plugin-textarea-autosize placeholder="Whats on your mind?" rows="3"></textarea>'+
            '<div class="compose-box-footer">'+
            '<ul class="compose-btn">'+
            '<li>'+
            '<button type="submit" class="btn btn-primary btn-xs">Post</button>'+
            '</li>'+
            '</ul>'+
            '</div>'+
            '</form>'+
            '</section>';



        var dialog = bootbox.dialog({
            title: 'Add New Notes',
            message: html,
        });
        dialog.init(function(){

            $(document).on("submit", "#note_form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $form = $(this); //wrap this in jQuery

                var url = '<?= Yii::$app->homeUrl?>ajax/submit-note?type="task"&id=<?= $model->id?>';

                $.ajax({
                    type: "GET",
                    url: url,
                    data: $("#note_form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {

                        if(data==true)
                        {
                            reloadComment();
                            bootbox.hideAll();
                        }
                        else
                        {
                            //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                        }


                    }
                });
                // avoid to execute the actual submit of the form.
            });

        });



    });


    function updateComment(id,event) {


        var text = $('#comment'+id+'').text();

        event.preventDefault();
        var html  = '<section class="simple-compose-box mb-3">'+
            '<form method="get" id="update-note_form" action="<?= Yii::$app->homeUrl?>ajax/update-note?type="task"&id='+id+'">'+
            '<textarea name="message-text" data-plugin-textarea-autosize  placeholder="Whats on your mind?" rows="3">'+text+'</textarea>'+
            '<div class="compose-box-footer">'+
            '<ul class="compose-btn">'+
            '<li>'+
            '<button type="submit" class="btn btn-primary btn-xs">Post</button>'+
            '</li>'+
            '</ul>'+
            '</div>'+
            '</form>'+
            '</section>';



        var dialog = bootbox.dialog({
            title: 'Update Note',
            message: html,
        });
        dialog.init(function(){

            $(document).on("submit", "#update-note_form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $form = $(this); //wrap this in jQuery

                var url = '<?= Yii::$app->homeUrl?>ajax/update-note?type="task"&id='+id+'';

                $.ajax({
                    type: "GET",
                    url: url,
                    data: $("#update-note_form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {

                        if(data==true)
                        {
                            reloadComment();
                            bootbox.hideAll();
                        }
                        else
                        {
                            //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                        }


                    }
                });
                // avoid to execute the actual submit of the form.
            });

        });



    }

    function reloadComment() {

        var $el = $('#LoadingOverlayApi');
        $el.trigger('loading-overlay:show');

        var url = '<?= Yii::$app->homeUrl?>ajax/get-note?type="task"&id=<?= $model->id?>';

        $.ajax({
            url: url,
            // serializes the form's elements.
            success: function(data)
            {
                $('.tm-items').html('');
                $('.tm-items').html(data);
                //$el.trigger('loading-overlay:hide');
            }
        });



        $.pjax.reload({container: '#activitiesPjax', async: false});

    }

    function deleteComment(id,event) {

        var url = '<?= Yii::$app->homeUrl?>ajax/delete-note?type="task"&id='+id+'';


        bootbox.confirm({
            message: "Are You Sure Delete This?",
            buttons: {
                confirm: {
                    label: 'OK',
                    className: 'btn btn-primary'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if(result==true)
                {
                    $.ajax({
                        url: url,
                        success: function(result){

                            if(result==true)
                            {
                                reloadComment();
                            }

                        }});


                }
            }
        });





    }


    function editLeadFile(id,custom) {

        bootbox.prompt("Update", function(result){

            console.log(result);

            if(result === null)
            {
                console.log(result);

            }else {

                $.ajax({
                    url: baseUrl+"files/update",
                    type: "POST",
                    data: { name: result,id:id},
                    success: function(result){

                        if(result==true)
                        {
                            bootbox.hideAll();
                            $.pjax.reload({container: '#filesPjax', async: false});
                        }

                    }});
            }

        });

        $('.bootbox-input').val(custom);


    }

    function delLeadFile(id) {

        bootbox.confirm({
            message: "Are You Sure Delete This?",
            buttons: {
                confirm: {
                    label: 'OK',
                    className: 'btn btn-primary'
                },
                cancel: {
                    label: 'Cancel',
                    className: 'btn btn-default'
                }
            },
            callback: function (result) {
                if(result==true)
                {
                    $.ajax({
                        url: "<?= Yii::$app->homeUrl?>files/delete",
                        type: "POST",
                        data: {id:id},
                        success: function(result){

                            if(result==true)
                            {
                                bootbox.hideAll();
                                $.pjax.reload({container: '#filesPjax', async: false});
                            }

                        }});


                }
            }
        });


    }




    $( document ).ready(function() {
        reloadComment();
    });


</script>