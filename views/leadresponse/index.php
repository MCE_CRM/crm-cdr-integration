<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use kartik\widgets\Select2;


/* @var $this yii\web\View */
/* @var $searchModel app\models\LeadresponseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lead Response';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$create = '';
$ordering = '';

     if (\Yii::$app->user->can('leadresponse/create')){
    $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add Response'), [
        'create'
    ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
        'class' => 'btn btn-default btn-sm add-new'
    ]);
}


     if (\Yii::$app->user->can('leadresponse/ordering')){
     $ordering = '<a href="'.Yii::$app->homeUrl.'leadresponse/ordering" class="btn btn-success btn-sm"><i class="fa fa-sort"> Ordering</i></a>';

 }

?>
<style type="text/css">

</style>
<div class="leadresponse-index">

  
       
       <?php


    Pjax::begin(['timeout' => '30000']);


    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
         'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before'=>$create.' '.$ordering.' '.Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',

            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'response',
            [
                'attribute'=>'active',
                'label'=>'Active',
                'value'=>function($model)
                {
                    if($model->active==1)
                    {
                        return "Active";
                    }
                    else
                    {
                        return "Inactive";
                    }
                },
                 'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' =>array(1=>"Active",2=>"Inactive"),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',
            ],
            'created_on',
            [
              'attribute'=>'created_by',
              'label'=>"Create By",
              'value'=>'user.username'
            ],
            /*'created_by',*/
          'updated_on',
            /*'updated_by',*/
            [
                'attribute'=>'updated_by',
                'label'=>'Updated By',
                'value'=>'user.username'
            ],

              [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{update} {defaultValue} {delete}',
                'buttons' => [
                    'delete' => function ($url, $model, $key) {
                           if (\Yii::$app->user->can('leads/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                       
                    },
                    'update' => function ($url, $model)

                    {
                            if (\Yii::$app->user->can('leadresponse/update')) {
                            return '<a href="#"><span class="fa fa-pencil-alt"  onclick="updateRecord('.$model->id.',\'leadresponse\',\'Update Response\',event)"></span></a>';
                        }

                      
                    } ,

                      'defaultValue' => function ($url, $model) {

                        

                            if (\app\models\DefaultValueModule::checkDefaultValue('lead-response', $model->id)) {
                                return Html::a('<span class="fa fa-eraser"></span>', Yii::$app->urlManager->createUrl(['leadresponse/index', 'del_id' => $model->id]), [
                                    'title' => Yii::t('app', 'Delete Default'),
                                ]);
                            } else {
                                return Html::a('<span class="fa fa-tag"></span>', Yii::$app->urlManager->createUrl(['leadresponse/index', 'id' => $model->id]), [
                                    'title' => Yii::t('app', 'Make Default'),
                                ]);
                            }
                        
                    }
                 

                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>
</div>
