<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Setting */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-view" style="width: 350px;">





    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                    'label' => 'Image',
                'format' => ['image',['width'=>'150','height'=>'100']],


                'value'=>function($model){

                    return('@web/logoimages/'.$model->logo);

                },


            ],
        ],


    ]) ?>
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <style type="text/css">
        .setting-view{
            width: 45%;
            margin: 0 auto;
        }
    </style>
</div>
