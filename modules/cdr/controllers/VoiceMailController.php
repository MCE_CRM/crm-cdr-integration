<?php

namespace app\modules\cdr\controllers;

use Yii;
use app\modules\cdr\models\VoiceMail;
use app\modules\cdr\models\VoiceMailSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\web\UploadedFile;

/**
 * VoiceMailController implements the CRUD actions for VoiceMail model.
 */
class VoiceMailController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all VoiceMail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new VoiceMailSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single VoiceMail model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new VoiceMail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new VoiceMail();

        if ($model->load(Yii::$app->request->post())) {

            // if($_POST['pictureval']) {echo 'sdsdsdsdsd';}
            //echo '<pre>';print_r($_POST);echo '</pre>';exit;
            //echo Yii::$app->user->id;exit;
            $tempSave = UploadedFile::getInstance($model, 'file');

            if (!empty($tempSave->name)){
                $filename = date('Ymdhis').'_'.$model->name;
                $model->file = UploadedFile::getInstance($model, 'file');
                $file = date('Ymdhis') . '.mp3';
                //$success = file_put_contents(\Yii::getAlias('@webroot') . '/regimages/' . $file, $data);
                $model->file->saveAs(\Yii::getAlias('@webroot') .'/voicemails/'.$filename.'.'.$model->file->extension);
                $model->file = $filename.'.'.$model->file->extension;
                $model->created_on = date('Y-m-d H:i:s');
                $model->created_by = Yii::$app->user->identity->id;

            }

            if($model->save()){

                return $this->redirect(['index']);
            }
            else{

               /* echo '<pre>';
                echo print_r($model);exit;*/
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing VoiceMail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing VoiceMail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the VoiceMail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return VoiceMail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = VoiceMail::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
