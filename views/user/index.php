<?php

use app\models\User;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\grid\GridView; 
use yii\widgets\Pjax;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;



function getUserRoles($user_id){
    $connection = \Yii::$app->db;
    $sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
    $command=$connection->createCommand($sql);
    $dataReader=$command->queryAll();
    $roles ='';
    if(count($dataReader) > 0){
        foreach($dataReader as $role){
            $roles.="<span class=\"label label-primary\">".$role['name']."</span></br>";
        }
    }
    return $roles;
}



?>

<style>

    .project-people, .project-actions{
        text-align: right;
        vertical-align: middle;
    }

    .project-people img{
        width: 32px;
        height: 32px;
    }

    .label-primary, .badge-primary{
        background-color: #1ab394;
        color: #FFFFFF;
        padding: 3px 6px;
    }


</style>
<div class="user-index">
     <?php Pjax::begin([ 'id' => 'leadGridview','timeout' => '30000']) ?>

  

<?php $isVisible = !Yii::$app->user->isGuest;?> 
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
            //'{export}',
            '{toggleData}',
        ],

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            'before' => Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New User'), [
                'create'
            ], ['data-pjax' => 0,'onclick'=>'addnew(event,this)',
                'class' => 'btn btn-default btn-sm add-new'
            ]

        ),
            'after' => '</form>'.Html::a('<i class="fa fa-sync"></i> ' . Yii::t('app', 'Reset List'), [
                    'index'
                ], [
                    'class' => 'btn btn-primary btn-sm'
                ]),


            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            [

                'attribute' => 'image',

                'label' => Yii::t('app', 'Image'),

                'format' => 'raw',

                'width' => '50px',

                'value' => function ($model, $key, $index, $widget)

                {
                    $url = \app\helpers\Helper::getBaseUrl().'files/profile_image/'.$model->image;
                    $image = '<div class="project-people"><img src ="'.$url.'"></div>';

                    return $image;

                }
            ],
            'first_name',
            'last_name',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'tr-b'],
                'attribute' => 'id',
                'label'=>Yii::t('app', 'Roles'),

                'format' => 'raw',

                'value' => function ($model, $key, $index, $widget)

                {
                    return getUserRoles($model->id);

                },
                'filter'=>false,

            ], 

            [
                'attribute'=>'status',
                'headerOptions' => ['style' => 'width:8%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],

                'value'=>function($model)
                {
                    return statusLabel($model->status);
                },
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => [
                    0 => 'Inactive',
                    1 => 'Active',
                ],
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',
            ],
            [

                'attribute' =>  'created_on',
                'value' => function($model){
                    return date("d/m/Y", strtotime($model->created_on));
                },
                'filterType'=>GridView::FILTER_DATE,
                'filterWidgetOptions'=> [
                    'type' => DatePicker::TYPE_INPUT,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy',
                    ],
                ],
                //'filter'=>false

            ],
            [

                'attribute' => 'created_by',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'value'=>function($model)
                {
                    return \app\helpers\Helper::getUser($model->created_by);
                },
                //'value'=>'user.username',
                'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
                'filterWidgetOptions' => [
                    'theme' => Select2::THEME_BOOTSTRAP,
                    'pluginOptions' => [
                        'allowClear' => true,
                    ],
                ],
                'filterInputOptions' => ['placeholder' => 'All...'],
                'format'=>'raw',

            ],
            [

                //'attribute' =>  'created_on',
                'header'=>'Last Update',
                'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'format'=>'raw',
                'value' => function($model){
                    if($model->updated_by)
                    {
                        return date("d/m/Y", strtotime($model->updated_on)).'<br>'.\app\helpers\Helper::getUser($model->updated_by);

                    }else
                    {
                        return '';
                    }
                },
                //'filter'=>false

            ],


              
             [  
               
                    'header'=>'Assign User',
                    'attribute'=>'lead_assign',
                    'format'=>'raw',
                    'value'=>function($model) {
                         
                        if (Yii::$app->user->can("leads/assign-other")) {
    
    
                            if ($model->lead_assign) {
                                $lead_assign = $model->lead_assign;
                                $model->lead_assign = explode(',', $model->lead_assign);
                                if(!empty($model->lead_assign[0]))
                                {
                                    $model->lead_assign=array_unique($model->lead_assign);
                                    $e = '';
        
                                    foreach ($model->lead_assign as $key => $data) {
                                        $user = \app\models\User::findOne($data);
        
                                        $e .= $user->first_name . ',<br>';
                                    }
    
                                    $e = substr_replace($e, "", -1);
                                    return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</a>';
                                }else
                                {
                                    foreach ($lead_assign as $data) {
                                        $user = \app\models\User::findOne($data);
        
                                        $e .= $user->first_name . ',<br>';
                                    }
                                    return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</a>';
                                }
    
                            } else {
                                return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</a>';
                            }
                        }else {
                            if ($model->lead_assign) {
                                    
                                    $model->lead_assign = explode(',', $model->lead_assign);
                                    $model->lead_assign=array_unique($model->lead_assign);
                                    $e = '';
        
                                    foreach ($model->lead_assign as $key => $data) {
                                        $user = \app\models\User::findOne($data);
        
                                        $e .= $user->first_name . ',<br>';
                                    }
        
                                    //$e = substr_replace($e, "", -1);
                                    
                                    return '<a href="JavaScript:Void(0);"  onclick="assignsingle(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</a>';
        
                                } else {
                                    return '<a href="JavaScript:Void(0);"  onclick="assignsingle(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</a>';
                                }
                        }
    
                    }
               
                ],
            

            [
                'class' => 'kartik\grid\ActionColumn',
                'dropdown' => true,
                'dropdownButton'=>['class'=>'mb-1 mt-1 mr-1 btn btn-xs btn-primary dropdown-toggle'],
                'dropdownOptions' => ['class' => 'pull-right'],
                'template' => '{profile}{login}{send_email}{send-sms}{edit}{change_password}{assign_role}',
                'buttons' => [
                    'profile' => function ($url, $model) {
                        $title = Yii::t('app', 'Profile');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = Yii::$app->homeUrl.'user/view?id='.$model->id;
                        $options['class'] = 'dropdown-item text-1';
                       // $options['onclick'] = 'externalWindow('.$model->id.',event)';
                        if (\Yii::$app->user->can('user/actionprofile')) {
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                    }
                    },
                    'login' => function ($url, $model) {
                        $title = Yii::t('app', 'Auto Login');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = Yii::$app->homeUrl.'site/switch-identity?id='.$model->id;
                        $options['class'] = 'dropdown-item text-1';
                        //$options['onclick'] = 'externalWindow('.$model->id.',event)';
                        if (\Yii::$app->user->can('user/login')) {
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                        }
                    },
                    'send_email' => function ($url, $model) {
                        $title = Yii::t('app', 'Send Email');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = '';
                        $options['class'] = 'dropdown-item text-1';
                        $options['onclick'] = 'sendEmail('.$model->id.',event)';
                        if (\Yii::$app->user->can('user/sendemail')) {
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                    }
                    },
                    'send-sms' => function ($url, $model) {
                        $title = Yii::t('app', 'Send SMS');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = '';
                        $options['class'] = 'dropdown-item text-1';
                        $options['onclick'] = 'updateRecord('.$model->id.',\'user\',\'Update User\',event)';
                        if (\Yii::$app->user->can('user/sendsms')) {
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                    }
                    },
                    'edit' => function ($url, $model) {
                        $title = Yii::t('app', 'Update User');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = '';
                        $options['class'] = 'dropdown-item text-1';
                        $options['onclick'] = 'updateRecord('.$model->id.',\'user\',\'Update User\',event)';
                        if (\Yii::$app->user->can('user/updateuser')) {
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                    }
                    },
                    'change_password' => function ($url, $model) {
                        $title = Yii::t('app', 'Change Password');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = '';
                        $options['onclick'] = 'changePassword('.$model->id.',\'user\',\'Update Password\',event)';

                        $options['class'] = 'dropdown-item text-1';
                        if (\Yii::$app->user->can('user/changepassword')) {
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                    }
                    },
                    'session_detail' => function ($url, $model) {
                        $title = Yii::t('app', 'View Session Detail');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = Yii::$app->homeUrl.'patient/view?id='.$model->id;
                        $options['class'] = 'dropdown-item text-1';
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                    },
                    'assign_role' => function ($url, $model) {
                        $title = Yii::t('app', 'Assign Role & Operation');
                        $options = []; // you forgot to initialize this
                        $icon = '<span class=""></span>';
                        $label = $icon . ' ' . $title;
                        $url = Yii::$app->homeUrl.'rbac/default/index?assign_user_id='.$model->id;
                        $options['class'] = 'dropdown-item text-1';
                        if (\Yii::$app->user->can('user/assignrole')) {
                        return '<li>' . Html::a($label, $url, $options) . '</li>' . PHP_EOL;
                    }
                    },

                ],
                'headerOptions' => ['class' => 'kartik-sheet-style'],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>


<script>

    function changePassword(id,controller,title,event) {

        event.preventDefault();
        var dialog = bootbox.dialog({
            title: title,
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',


        });


        dialog.init(function(){
            var request = $.ajax({
                url: baseUrl+controller+"/update?id="+id+"&change-password=true",
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);


            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });


                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            //toastr.success('', 'Update Successfully', {timeOut: 2000});
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#leadGridview', url: base});
                        }
                        else
                        {
                            //toastr.success('', 'Some Error Occur', {timeOut: 2000});
                        }


                    }
                });
                // avoid to execute the actual submit of the form.
            });

        });

    }



    function externalWindow(id,event) {
        var scr_w, scr_w1;
        var scr_h, scr_h1;
        scr_w1 = 1015;
        scr_h1 = 740;

        if (scr_w == '800'){
            scr_w1 = 785;
            scr_h1 = 500;
        }

        var url = "<?= Yii::$app->homeUrl?>site/switch-identity?id="+id;
        
        location.href = url;
        
        /*var printWindow =

            window.open (url, 'Print' ,
                "dependant=no,directories=no,location=no,menubar=no"
                +   ",resizable=no,scrollbars=yes,titlebar=no,toolbar=no,"
                + "0, 0, top=0,left=0,status=1,width=" +scr_w1+",height="+scr_h1);*/



        //window.open(url, 'Print', 'left=100, top=100, width=' + width + ', height=' + height + ', toolbar=0, resizable=0');


    }


</script>

<script>
      var array = new Array();
$(document).ready(function() {   

 $("input:checkbox").change(function() {
    if ($(this).is(':checked')) {
      var checked = ($(this).val());
      array.push(checked);

      console.log(array);
    
    }
     else {
           var index=array.indexOf($(this).val());
           if (index > -1) {
               array.splice(index, 1);
               console.log(array);
            }
        }


  });


    }); 

    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/";
       
    var lastUrl = baseUrl+'leads/index';

    var tit = 'List'; 
    
    var array=new Array();
     function myFunction(id)
     {
  
   /* $("input[type='checkbox']")click(function(e){*/
        if ($("#"+id).is(':checked')){
           array.push(id);
          

        } else {
           var index=array.indexOf(id);
           if (index > -1) {
               array.splice(index, 1);
            }

        }
    }

    var getUrl = window.location;
    var baseUrl = getUrl .protocol + "//" + getUrl.host + "/" + getUrl.pathname.split('/')[1]+"/web/user";
       
    var lastUrl = baseUrl+'leads/index';

    var tit = 'List';




    function leadTab(title) {

        var base = baseUrl+'leads?type=teamlead&leadsSearch[lead_status]='+title;
  
        base = encodeURI(base);
            
        lastUrl = base;
         

        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#leadGridview', url: base});
        if(title=='')
        {
            tit = "List";
        }else {

            tit = title;
        }






    }

    function todayLeads(date) {

        var base = baseUrl+'leads?type=teamlead&leadsSearch[created_on]='+date;

        base = encodeURI(base);

        lastUrl = base;


        $.pjax.defaults.timeout = 5000;
        $.pjax.reload({container:'#leadGridview', url: base});
        if(title=='')
        {
            tit = "List";
        }else {

            tit = title;
        }






    }

    $(document).on('pjax:success', function() {
        window.history.pushState('page2', 'Title', '');
        $('#card-title').text(" "+tit);
    });



    function assign(event,id) { 

        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-user?id='+id+'&array='+array+'';
       

       

        var dialog = bootbox.dialog({
            title: 'Assign User',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({ 
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),

                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#leadGridview'});
                            array=[];
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };
    
    function assignsingle(event,id) {
        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-user-single?id='+id+'&array='+array+'';
        var dialog = bootbox.dialog({
            title: 'Assign Lead',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),

                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#leadGridview'});
                            array=[];
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };

</script>
<script type="text/javascript">
    function gridShow(event) {
        $('#gridview-display').show();
        $('#summary-display').hide();
        $('#calender-display').hide();
    }
    function calenderShow(event) {
        $('#gridview-display').hide();
        $('#summary-display').hide();
        $('#calender-display').show();
        showcalendar();
    }
    $(function(){

        <?php foreach ($meter as $met) { ?>

        $('#meterDark<?=$met?>').liquidMeter({

            shape: 'circle',
            color: $('#meterDark<?=$met?>').attr("color"),
            background: '#272A31',
            stroke: '#33363F',
            fontSize: '24px',
            fontWeight: '600',
            textColor: '#FFFFFF',
            liquidOpacity: 0.9,
            liquidPalette: ['#0088CC'],
            speed: 3000,
            animate: !$.browser.mobile
        });



        <?php } ?>
    });

    /*
     Name:          Pages / Calendar - Examples
     Written by:    Okler Themes - (http://www.okler.net)
     Theme Version:     2.1.1
     */
    function showcalendar(){
    (function($) {

        'use strict';

        var initCalendarDragNDrop = function() {
            $('#external-events div.external-event').each(function() {

                // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
                // it doesn't need to have a start or end
                var eventObject = {
                    title: $.trim($(this).text()) // use the element's text as the event title
                };

                // store the Event Object in the DOM element so we can get to it later
                $(this).data('eventObject', eventObject);

                // make the event draggable using jQuery UI
                $(this).draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });

            });
        };

        var initCalendar = function() {
            var $calendar = $('#calendar');
            var date = new Date();
            var d = date.getDate();
            var m = date.getMonth();
            var y = date.getFullYear();

            $calendar.fullCalendar({
                header: {
                    left: 'title',
                    right: 'prev,today,next,basicDay,basicWeek,month'
                },
                timeFormat: 'h:mm',

                themeButtonIcons: {
                    prev: 'fas fa-caret-left',
                    next: 'fas fa-caret-right',
                },

                editable: true,
                droppable: true, // this allows things to be dropped onto the calendar !!!
                drop: function(date, allDay) { // this function is called when something is dropped
                    var $externalEvent = $(this);
                    // retrieve the dropped element's stored Event Object
                    var originalEventObject = $externalEvent.data('eventObject');

                    // we need to copy it, so that multiple events don't have a reference to the same object
                    var copiedEventObject = $.extend({}, originalEventObject);

                    // assign it the date that was reported
                    copiedEventObject.start = date;
                    copiedEventObject.allDay = allDay;
                    copiedEventObject.className = $externalEvent.attr('data-event-class');

                    // render the event on the calendar
                    // the last `true` argument determines if the event "sticks" (http://arshaw.com/fullcalendar/docs/event_rendering/renderEvent/)
                    $('#calendar').fullCalendar('renderEvent', copiedEventObject, true);

                    // is the "remove after drop" checkbox checked?
                    if ($('#RemoveAfterDrop').is(':checked')) {
                        // if so, remove the element from the "Draggable Events" list
                        $(this).remove();
                    }

                },
                events:'<?=Yii::$app->homeUrl?>ajax/allevents',
                eventRender: function(event, element) {
                    $(element).tooltip({title: event.description});
                }


            }); 

            // FIX INPUTS TO BOOTSTRAP VERSIONS
            var $calendarButtons = $calendar.find('.fc-header-right > span');
            $calendarButtons
                .filter('.fc-button-prev, .fc-button-today, .fc-button-next')
                .wrapAll('<div class="btn-group mt-sm mr-md mb-sm ml-sm"></div>')
                .parent()
                .after('<br class="hidden"/>');

            $calendarButtons
                .not('.fc-button-prev, .fc-button-today, .fc-button-next')
                .wrapAll('<div class="btn-group mb-sm mt-sm"></div>');

            $calendarButtons
                .attr({ 'class': 'btn btn-sm btn-default' });
        };

        $(function() {
            initCalendar();
            initCalendarDragNDrop();
        });

    }).apply(this, [jQuery]);
}
</script>
<style>
.select2-selection
{
    overflow:auto;
}
</style> 