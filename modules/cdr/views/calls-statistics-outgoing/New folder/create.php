<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CallsStatisticsOutgoing */

$this->title = 'Create Calls Statistics Outgoing';
$this->params['breadcrumbs'][] = ['label' => 'Calls Statistics Outgoings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="calls-statistics-outgoing-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
