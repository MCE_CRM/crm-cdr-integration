<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AgentSession */

$this->title = 'Update Agent Session: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Agent Sessions', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="agent-session-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
