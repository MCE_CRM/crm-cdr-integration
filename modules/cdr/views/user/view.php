<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 9/23/2018
 * Time: 6:03 PM
 */

use kartik\file\FileInput;
use kartik\password\PasswordInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = "Profile";


//$role = \Yii::$app->authManager->getRolesByUser(Yii::$app->user->identity->id);




?>


<div class="row">
    <div class="col-lg-4 col-xl-3 mb-4 mb-xl-0">

      <!--   <section class="card">
            <div class="card-body">
                <div class="thumb-info mb-3">
                    <img src="<?= \app\helpers\Helper::getBaseUrl()?>files/profile_image/<?=Yii::$app->user->identity->image?>" style="width:255px;height: 255px;" class="rounded img-fluid" alt="John Doe">
                    <div class="thumb-info-title">
                        <span class="thumb-info-inner"><?= Yii::$app->user->identity->first_name." ".Yii::$app->user->identity->last_name?></span>
                        <span class="thumb-info-type"><?php
                            $i=0;
                            foreach ($role as $key=>$r)
                            {

                                if($i>0)
                                {
                                    echo $key.",";
                                }
                                $i++;
                            }


                            ?></span>
                    </div>
                </div>


                <hr class="dotted short">

                <h5 class="mb-2 mt-3">About</h5>
                <p class="text-2"><?= Yii::$app->user->identity->about?></p>

            </div>
        </section>
 -->
    </div>
    <div class="col-lg-8 col-xl-6">

        <div class="tabs">
            <ul class="nav nav-tabs tabs-primary">
                <li class="nav-item active">
                    <a class="nav-link" href="#overview" data-toggle="tab">Overview</a>
                </li>

            </ul>
            <div class="tab-content">
                <div id="overview" class="tab-pane active">

                    <div class="p-3">
                        <h4 class="mb-3 pt-4">Activity</h4>

                        <div class="timeline timeline-simple mt-3 mb-3">
                            <div class="tm-body">
                                <div class="tm-title">
                                    <h5 class="m-0 pt-2 pb-2 text-uppercase">Activity</h5>
                                </div>
                                <ol class="tm-items">
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-0">7 months ago.</p>
                                            <p>
                                                It's awesome when we find a good solution for our projects, Porto Admin is <span class="text-primary">#awesome</span>
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-0">7 months ago.</p>
                                            <p>
                                                What is your biggest developer pain point?
                                            </p>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="tm-box">
                                            <p class="text-muted mb-0">7 months ago.</p>
                                            <p>
                                                Checkout! How cool is that!
                                            </p>
                                            <div class="thumbnail-gallery">
                                                <a class="img-thumbnail lightbox" href="img/projects/project-4.jpg" data-plugin-options='{ "type":"image" }'>
                                                    <img class="img-fluid" width="215" src="img/projects/project-4.jpg">
																		<span class="zoom">
																			<i class="fas fa-search"></i>
																		</span>
                                                </a>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-3">
        <br>
        <ul class="simple-card-list mb-3">
            <li class="primary">
                <h3>Email</h3>
                <p class="text-light"><?= $model->email?></p>
            </li>
            <li class="primary">
                <h3>Phone</h3>
                <p class="text-light"><?= $model->phone_no ?></p>
            </li>
            <li class="primary">
                <h3>Status</h3>
                <p class="text-light">
                    <?= statusLabel($model->status) ?>
                </p>
            </li>
        </ul>

    </div>

</div>

<script>

    $(document).ready(function(e) {


        $('#myTable').on('click', '.checkbox-click', function(){



            var $this = $(this);

            var child = $(this).parent().parent().siblings(":first").text();
            console.log(child);
            var parent = $(this).val();
            if ($(this).is(':checked')) {



                // alert("checked");


                var url = '?child='+child+'&parent='+parent+'';
                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (data) {
                        //alert(data);
                        if(!data=="Revoke")
                        {
                            $this.removeAttr("checked");
                        }


                    },
                    error: function(xhr, ajaxOptions, thrownError){

                    },
                    //timeout : 15000//timeout of the ajax call
                });




            }
            else
            {
                var url = '?child='+child+'&parent='+parent+'&remove_child=true';

                $.ajax({
                    url: url,
                    type: "GET",
                    success: function (data) {

                        //alert(data);

                        if(!data=="ASSIGN")
                        {
                            $this.prop('checked');
                        }




                    },
                    error: function(xhr, ajaxOptions, thrownError){

                    },
                    // timeout : 15000//timeout of the ajax call
                });

            }
        });



        $('a[data-toggle="tab"]').on('shown.bs.tab', function () {

            //save the latest tab; use cookies if you like 'em better:

            localStorage.setItem('lastTab_leadview', $(this).attr('href'));

        });
        $('[data-toggle="hover"]').popover({ trigger: "hover" });


        //go to the latest tab, if it exists:

        var lastTab_leadview = localStorage.getItem('lastTab_leadview');

        if ($('a[href="' + lastTab_leadview + '"]').length > 0) {

            $('a[href="' + lastTab_leadview + '"]').tab('show');
        }
        else
        {
            // Set the first tab if cookie do not exist

            $('a[data-toggle="tab"]:first').tab('show');
        }

    });


</script>
