<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ServerStatus */

$this->title = 'Create Server Status';
$this->params['breadcrumbs'][] = ['label' => 'Server Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="server-status-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
