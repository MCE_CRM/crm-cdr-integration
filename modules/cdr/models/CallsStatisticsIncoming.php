<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "calls_statistics_incoming".
 *
 * @property int $id
 * @property string $calldate
 * @property int $total_calls
 * @property int $ans_calls
 * @property int $no_ans_calls
 * @property string $entry_date
 */
class CallsStatisticsIncoming extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'calls_statistics_incoming';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['calldate'], 'required'],
            [['calldate', 'entry_date'], 'safe'],
            [['total_calls', 'ans_calls', 'no_ans_calls'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'calldate' => 'Calldate',
            'total_calls' => 'Total Incoming Calls',
            'ans_calls' => 'Answered Calls',
            'no_ans_calls' => 'No Answer / Dropped Calls',
            'entry_date' => 'Entry Date',
            'last_updated' => 'Last Updated',
        ];
    }

    /**
     * Generates recent record. 
     * 
     * @return void
     */
    public function generateRecord() {
        // In the last 7 days, check for earliest record and that is callDate.
        $callDate = Yii::$app->db->createCommand(
            "SELECT calldate FROM cdr 
            WHERE calldate != '0000-00-00 00:00:00' 
            AND calldate >= (DATE(NOW()) - INTERVAL 7 DAY)
            ORDER BY calldate ASC LIMIT 1"
        )->queryScalar();

        // If there are calls in the last 7 days, generate rows.
        if (!empty($callDate)) {
            // Getting rid of the time.
            $callDate = substr($callDate, 0, 10);

            // Generating any missing rows in last month.
            CallsStatisticsIncoming::generateRows($callDate);
        }
    }

    /**
     * Generates rows in (calls_statistics_incoming) based on data in (cdr)
     * 
     * @param date $callDate The date from where to start generating rows.
     * @return int The number of generated/updated rows.
     */
    public function generateRows($callDate) {
        // Today's Date
        $todayDate = date('Y-m-d');

        $inserts = 0;
        $ins = 0;

        while ($callDate <= $todayDate) {
            // Check if the calldate exists in (calls_statistics_incoming)
            $model = CallsStatisticsIncoming::find()
            ->where(['calldate' => $callDate])
            ->one();

            if (!empty($model->entry_date)) {
                // $ins++;
                // print_r("AYY".$ins." calldate: ".$callDate);exit();
                // The calldate exists. Check if the entry date is bigger than it.
                // ! If the entry date or last update is bigger than call date, 
                // !   we want to do nothing.
                if ($model->entry_date <= $callDate.' 23:59:59' && 
                    $model->last_updated <= $callDate.' 23:59:59') {
                    // Re-counting the records, since they weren't counted for 
                    //   the whole day
                    CallsStatisticsIncoming::populateCallsStatisticsIncomingTable($model, 'update');

                    $inserts++; // Counting number of re-counts / updates.
                }
            } else {
                $newModel = new CallsStatisticsIncoming();
                $newModel->calldate = $callDate;

                // The calldate doesn't exist. Create an entry.
                CallsStatisticsIncoming::populateCallsStatisticsIncomingTable($newModel, 'insert');
                $inserts++;     // Counting number of inserts.
            }

            // Adding a day to $callDate
            $callDate = date('Y-m-d', strtotime($callDate . ' +1 day'));
        }

        return $inserts;
    }

    
    /**
     * Checks the (cdr) table and populates the (calls_statistics_incoming) table
     *  by counting number of fiels for each day
     * 
     * @param CallsStatisticsIncoming $model The model object of the row to be inserted or updated.
     * @param string $operation Either 'insert' or 'update'. Insert by default.
     * @return void
     */
	
	
    public function populateCallsStatisticsIncomingTable($model, $operation = 'insert') {
        // The lastUpdated will always be current date/time
        $lastUpdated = date('Y-m-d H:i:s');

        // On Inserting, Entry Date is current date
        $entryDate = $lastUpdated;

        $totalcalls = 0;
        $totalcallsanswer = 0;
        $totalcallsdropped = 0;

        //print_r($model->calldate);exit();

        // Fetching the numbers 
        /* $numbers = Cdr::find()
        ->where(['between','calldate',$model->calldate.' 00:00:00',$model->calldate.' 23:59:59'])
		->andWhere(['!=', 'src', '2218687'])
		->andWhere(['LIKE', 'iorecording', 'Incoming'])
        ->andWhere(['!=', 'dst', '*77'])
        ->andWhere(['!=', 'dst', '*88'])
        ->all(); */

        // echo '<pre>';
        // print_r($numbers);
        // echo '</pre>';
        // exit();
		$totalcallsanswer = 0;
        $totalcallsdropped = 0;
		
		$totalcalls = Cdr::find()->select('iorecording')->where([
                'and',
                ['like', 'iorecording','Incoming'],
                ['like','calldate',$model->calldate]
            ])->distinct()->count();
			
			$totalcallsdropped = Cdr::find()->select('uniqueid')->where([
                'and',
				['like','calldate',$model->calldate],
                ['like','disposition','NO ANSWER'],
				['like', 'iorecording','Incoming']
            ])->distinct()->count();

			$totalcallsanswer = $totalcalls - $totalcallsdropped;

        /* foreach($numbers as $number) {
            if ($number['disposition'] == 'ANSWERED') {
                $totalcallsanswer++;
            } else {
                $totalcallsdropped++;
            }
        } */

        // Total Answered Calls
        // $totalcallsanswerquery = 'SELECT COUNT(*) FROM cdr 
        //         WHERE calldate LIKE \'' . $model->calldate . '%\' 
        //         AND dst != \'*77\' 
        //         AND dst != \'*88\' 
        //         AND clid LIKE \'"Agent%\'
        //         AND channel NOT LIKE \'SIP/SIPT%\'
        //         AND disposition = \'ANSWERED\'';
        // $totalcallsanswer = Yii::$app->db->createCommand($totalcallsanswerquery)
        //     ->queryScalar();
        // $totalcallsanswer = Cdr::find()
        // ->where(['like', 'calldate', $model->calldate])
        // ->andWhere(['!=', 'dst', '*77'])
        // ->andWhere(['!=', 'dst', '*88'])
        // ->andWhere(['like', 'clid', 'Agent'])
        // ->andWhere(['not like', 'channel', 'SIP/SIPT'])
        // ->andWhere(['disposition' => 'ANSWERED'])
        // ->count();

        // $totalcallsanswer = $numbers->where(['disposition' => 'ANSWERED'])->count();

        //print_r($totalcallsanswer);exit();

        // var_dump($totalcallsanswer
        //     ->prepare(Yii::$app->db->queryBuilder)->createCommand()->rawSql);
        // exit();

        
        // Total Dropped / Unanswered Calls
        // $totalcallsdroppedquery = 'SELECT COUNT(*) FROM cdr 
        //         WHERE calldate LIKE \'' . $model->calldate . '%\' 
        //         AND dst != \'*77\' 
        //         AND dst != \'*88\' 
        //         AND clid LIKE \'"Agent%\'
        //         AND channel NOT LIKE \'SIP/SIPT%\'
        //         AND disposition NOT LIKE  \'ANSWERED\'';
        // $totalcallsdropped = Yii::$app->db->createCommand($totalcallsdroppedquery)
        //     ->queryScalar();
        // $totalcallsdropped = Cdr::find()
        // ->where(['like', 'calldate', $model->calldate])
        // ->andWhere(['!=', 'dst', '*77'])
        // ->andWhere(['!=', 'dst', '*88'])
        // ->andWhere(['like', 'clid', 'Agent'])
        // ->andWhere(['!=', 'channel', 'SIP/SIPT'])
        // ->andWhere(['not like', 'disposition', 'ANSWERED'])
        // ->count();
        
        //$totalcalls = $totalcallsanswer + $totalcallsdropped;
        

        // The Final Query.
        if ($operation == 'insert') {
            $newModel = new CallsStatisticsIncoming();

            $newModel->calldate = $model->calldate;
            $newModel->total_calls = $totalcalls;
            $newModel->ans_calls = $totalcallsanswer;
            $newModel->no_ans_calls = $totalcallsdropped;
            $newModel->entry_date = $entryDate;

            $newModel->save();

            // $query = "INSERT INTO calls_statistics_incoming (
            //     calldate, total_calls, ans_calls, no_ans_calls, 
            //     entry_date
            // ) VALUES (
            //     '$callDate', $totalcalls, $totalcallsanswer, 
            //     $totalcallsdropped, '$entryDate'
            // )";     // 1 = Incoming type call (2 = Outgoing)
        } else { // Update operation.
            $model->total_calls = $totalcalls;
            $model->ans_calls = $totalcallsanswer;
            $model->no_ans_calls = $totalcallsdropped;
            $model->last_updated = $lastUpdated;

            $model->save();

            // $query = "UPDATE calls_statistics_incoming 
            //     SET total_calls = $totalcalls, 
            //     ans_calls = $totalcallsanswer, 
            //     no_ans_calls = $totalcallsdropped, 
            //     last_updated = '$lastUpdated'
            //     WHERE calldate = '$callDate'";
        }
        
        // Eggxecuting the query, inserting new data into calls_statistics_incoming
        //Yii::$app->db->createCommand($query)->execute();
    }

}
