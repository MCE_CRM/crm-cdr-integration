<?php

namespace app\modules\cdr\controllers;
use Yii;
use app\modules\cdr\models\CallsStatisticsIncoming;
use app\modules\cdr\models\CallsStatisticsIncomingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CallsStatisticsIncomingController implements the CRUD actions for CallsStatisticsIncoming model.
 */
class CallsStatisticsIncomingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Generates the rows in cdr_calls_statistics table.
     * 
     * @return void
     */
    public function actionGenerate() {
        // Earliest date from (cdr) table
        $callDate = Yii::$app->db->createCommand(
            "SELECT calldate FROM cdr 
            WHERE calldate != '0000-00-00 00:00:00' ORDER BY calldate ASC LIMIT 1"
        )->queryScalar();

        // Getting rid of the time.
        $callDate = substr($callDate, 0, 10);

        // Saving previous execution time.
        $executionTime = ini_get('max_execution_time'); 

        // to get unlimited php script execution time
        ini_set('max_execution_time', 0); 

        // !!! This will take a long time to execute if there are a lot of records.
        $generatedRows = CallsStatisticsIncoming::generateRows($callDate);

        // back to old php script execution time
        ini_set('max_execution_time', $executionTime); 

        echo $generatedRows;
    }

    /**
     * Lists all CallsStatisticsIncoming models.
     *   
     * @return mixed
     */
    public function actionIndex() {
        // Generate records in (cdr_calls_statistics)
		
		//CallsStatisticsIncoming::deleteAll();
			//exit;
			
		
        CallsStatisticsIncoming::generateRecord();

        $searchModel = new CallsStatisticsIncomingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all CallsStatisticsIncoming models.
     *   
     * @return mixed
     */
    public function actionOutgoing() {
        // Generate records in (cdr_calls_statistics)
        CallsStatisticsIncoming::generateRecord();

        $searchModel = new CallsStatisticsIncomingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'outgoing');

        return $this->render('outgoing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CallsStatisticsIncoming model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Finds the CallsStatisticsIncoming model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CallsStatisticsIncoming the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CallsStatisticsIncoming::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
