<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use kartik\spinner\Spinner;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CdrCallsStatisticsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Daily Calls Report (Outgoing)';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="cdr-calls-statistics-index">
    <h2 style="margin-top: -20px; margin-bottom: 20px;">(Outgoing Calls) Call Status based Statistics Report (Date wise)</h2>

    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php 
    if (\Yii::$app->user->can('site/adminDashboard')) {
        Modal::begin([
            'toggleButton' => [
                'label' => 'Generate', 
                'class' => 'btn btn-primary',
                'style' => 'margin-bottom: 10px;'
            ],
        ]);

        echo '<h3>Are you sure?</h3>
        <p>Executing the complete script
        may take a long time depending on number of entries being generated.
        <i>(An Entry for each day may take up to 1.5-2 seconds)</i></p>';

        echo Html::button(
            'Generate Records',
            [ 
                'class' => 'btn btn-primary',
                'style' => 'margin-bottom: 10px;',
                'id' => 'btnGenerate'
            ]
        );

        echo '<div id="contentDiv"></div>';

        echo Spinner::widget([
            'preset' => Spinner::LARGE,
            'color' => 'black',
            'align' => 'center',
            'id' => 'spinner',
            'hidden' => 'true'
        ]);
        
        Modal::end();
    }
    ?>

    <?php Pjax::begin();?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'options' => [
            'bootstrap' => true,
            'hover' => true,
        ],

        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => $this->title,
        ],
        'toolbar' =>  [
            [
                'content' => Html::a(
                    '<i class="glyphicon glyphicon-repeat"></i>', 
                    ['index'], 
                    [
                        'data-pjax' => 0, 
                        'class' => 'btn btn-default', 
                        'title' => 'Reset'
                    ]
                )
            ],
            '{export}',
            '{toggleData}',
        ],
        // set export properties
        'export' => [
            'fontAwesome' => true
        ],

        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'calldate',
            'total_calls',
            'ans_calls',
            'no_ans_calls',
            'entry_date',
            'last_updated',

            //['class' => 'yii\grid\ActionColumn'],
            [
                'label' => 'Actions',
                'value' => function ($model) {
                    return Html::a(
                        '<span class="glyphicon glyphicon-eye-open"></span>', 
                        ['view', 'id' => $model->id]
                    );
                },
                'format'=>'raw',
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

<script>
    $('#btnGenerate').click(function() {
        $.ajax({
            type: "POST",
            url: "<?php echo Yii::$app->homeUrl?>cdr-calls-statistics/generate?type=outgoing",
            cache: false,
            beforeSend: function() {
                $('#spinner').show();
            },
            success: function(data) {
                $('#spinner').hide();
                $('#contentDiv').html(data + " rows added/updated.");
                location.href = "<?php echo Yii::$app->homeUrl?>cdr-calls-statistics/outgoing";
            }
        });
    });    
</script>