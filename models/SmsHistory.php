<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sms_history".
 *
 * @property int $id
 * @property string $number
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 * @property string $message
 * @property int $lead_id
 */
class SmsHistory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sms_history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'created_on', 'created_by'], 'required'],
            [[ 'created_by', 'updated_by', 'lead_id'], 'integer'],
            [['created_on', 'updated_on'], 'safe'],
            [['number'], 'string', 'max' => 20],

            [['message'], 'string', 'max' => 5000],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Number',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
            'message' => 'Message',
            'lead_id' => 'Lead ID',
        ];
    }
}
