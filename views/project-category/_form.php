<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ProjectCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-category-form">

    <?php
    $form = ActiveForm::begin([
        'id' => 'form',
        'enableAjaxValidation' => true,
        'validationUrl' => ($model->isNewRecord) ?Yii::$app->homeUrl.'project-category/validate':Yii::$app->homeUrl.'project-category/validate?id='.$model->id.'',
        'errorCssClass' => 'has-danger',
    ]);
    ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php // $form->field($model, 'sort_order')->textInput() ?>

    <?= $form->field($model, 'active')->dropDownList(['1'=>'Active','0'=>'InActive', ]) ?>
    <?php // $form->field($model, 'created_on')->textInput() ?>

    <?php // $form->field($model, 'created_by')->textInput() ?>

    <?php //$form->field($model, 'updated_on')->textInput() ?>

    <?php // $form->field($model, 'updated_by')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
