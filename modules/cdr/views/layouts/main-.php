<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\cdr\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>
<body class="top-navigation boxed-layout skin-1">

<?php $this->beginBody() ?>

<div id="wrapper">
    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom white-bg">
            <?=$this->render('header')?>
        </div>

        <div class="wrapper wrapper-content">
            <div class="container">
                <div class="row">
                    <?= $content ?>
                </div>
            </div>
        </div>

        <?=$this->render('footer')?>


    </div>
</div>

    
   
<?=$this->render('footer')?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

