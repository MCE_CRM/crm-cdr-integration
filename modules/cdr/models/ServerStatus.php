<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "server_status".
 *
 * @property int $id
 * @property string $username
 * @property string $ip
 * @property string $status
 * @property int $type 1 for agent 2 for sip
 * @property string $total_calls
 * @property string $last_call_taken
 * @property string $created_on
 * @property string $latency
 */
class ServerStatus extends \yii\db\ActiveRecord
{
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'server_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            
            [['type'], 'integer'],
            [['created_on'], 'safe'],
            [['username'], 'string', 'max' => 25],
            [['ip'], 'string', 'max' => 35],
            [['status'], 'string', 'max' => 20],
            [['total_calls', 'last_call_taken'], 'string', 'max' => 5],
            [['latency'], 'string', 'max' => 10],
            
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'ip' => 'IP Address',
            'status' => 'Status',
            'type' => 'Type',
            'total_calls' => 'Total Calls',
            'last_call_taken' => 'Last Call Taken',
            'created_on' => 'Created On',
            'latency' => 'Latency',
        ];
    }
}
