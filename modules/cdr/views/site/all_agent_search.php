<?php

use kartik\daterange\DateRangePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\checkbox\CheckboxX;
use kartik\icons\Icon;
use yii\helpers\ArrayHelper;
use app\modules\cdr\models\Cdr;
use app\modules\cdr\models\User;
/* @var $this yii\web\View */
/* @var $model app\models\CdrSearch */
/* @var $form yii\widgets\ActiveForm */
?>
<style>
    .col-md-1-5 {
        width: 12.49995%;
    }
	.right
	{
		margin-left:30px !important;
	}
	.daterangepicker
	{
		min-width:52% !important;
	}

</style>

<script>
    /*if(typeof window.history.pushState == 'function') {
        window.history.pushState({}, "Hide", '');
    }*/
</script>

<?php //echo $_SERVER['PHP_SELF'];?>

     
<div class="cdr-body">

    <?php $form = ActiveForm::begin([
        'action' => ['all-agent-report'],
        'method' => 'get',
    ]); ?>


    <div class="row">




        <div class="col-md-2">

              <?php
           $addon = <<< HTML
<span class="input-group-addon">
    <i class="glyphicon glyphicon-calendar"></i>
</span>
HTML;


           echo '<label class="control-label">Date Range</label>';
           echo '<div class="drp-container">';
           echo DateRangePicker::widget([
               'model'=>$model,
               'attribute'=>'calldate',
               'convertFormat'=>true,
               'pluginOptions'=>[
                   'opens'=>'left',
                   'ranges' => [

                       "Today" => ["moment().startOf('day')", "moment()"],
                       "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                       "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                   ],

                   'timePicker'=>true,
                   'timePickerIncrement'=>05,
                   'locale'=>['format'=>'d/m/Y h:i A']
               ],
               'presetDropdown'=>false,
               'hideInput'=>true
           ]);
           echo '</div>'; ?>
            
        </div>
        
        
         <div class="col-md-2 col-md-1-5">
            <?= $form->field($model, 'agent_id')
              ->dropDownList(
               ArrayHelper::map(User::find()->select('User.*')->leftJoin('auth_assignment', 'auth_assignment.user_id = User.id')->where(['NOT LIKE','agent_id','NULL'])->andwhere(['LIKE','auth_assignment.item_name','CSR'])->asArray()->all(), 'agent_id', 'first_name'),['prompt'=>'']
            )->label('Agent',['class'=>'label-class']); ?>

        </div>

        <div class="col-md-2 ">
           <br>

            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>

        </div>
       

        </div>


    </div>

  

    <!-- <?= $form->field($model, 'clid') ?> -->



<?php ActiveForm::end(); ?>


</div>





    



