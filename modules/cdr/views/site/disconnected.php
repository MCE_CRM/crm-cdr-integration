<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\modules\cdr\assets\AppAsset;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use kartik\editable\Editable;

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\modules\cdr\models\User;
use app\modules\cdr\models\Projects;
use yii\web\JsExpression;
use app\modules\cdr\models\Blocksector;

AppAsset::register($this);
$this->title = 'Disconnected Call Detail Record';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .panel-heading-search {
        background-color: #1ab394;
        border-color: #1ab394;
        text-align: center;
        margin: 0 auto;
        color: #fff;
        padding: 10px 15px;
        border-bottom: 1px solid transparent;
        border-top-left-radius: 3px;
        border-top-right-radius: 3px;
    }

</style>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="top-navigation boxed-layout skin-1">

<?php $this->beginBody() ?>

<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Comments</h4>
                <button type="button" class="close" data-dismiss="modal" id="close" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body mx-3" style="overflow-y: hidden !important;height: auto !important;">
                <h4 id="appartementtitle"></h4>
                <div class="md-form mb-5">
                    <i class="fa fa-envelope prefix grey-text"></i>
                    <textarea cols="60" rows="5"id="desc" style="padding-left:4px;width: 100%;"></textarea>
                    <label data-error="wrong" data-success="right" for="defaultForm-email">Your Comments</label>
                </div>

            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-default" id="savedesc">Save</button>
                <button class="btn btn-default updatedescription"  style="display: none;">Update</button>
            </div>
        </div>
    </div>
</div>
<div class="inner-wrapper" >
  
 
  <div class="card-body">

    <div class="follow-up-search">

         <header class="page-header" style="margin-top: 0.000001px;" >
                <h2 style="padding-left: 3%;">Disconnected Call Detail Record</h2>


               
            </header>

            <?php echo \Yii::$app->view->renderFile(Yii::getAlias('@app') . '/views/layouts/header_menu.php'); ?>
     
       
                    <div class="content-boody">
                        <div class="row">
                            <div class="col-lg-11 col-xl-11" style="margin-left:4%">
                      
                                  <?php //if(Yii::$app->user->can('dashboard/search')){?>
                                                    
                                                      
                                                      <div class="panel-body" > <?php   echo $this->render('disconnected_search', ['model' => $searchModel]); ?>
                                                        </div>
                                                             <?php
                                           // }
                                    ?>

                                    <div class="col-md-11" style="margin-left:5%">
                                        <?php Pjax::begin();?>
                                        <?= GridView::widget([
                                            'dataProvider' => $dataProvider,
                                             'layout'=>"{items}\n{pager}\n{summary}",

                                              /*'floatHeader'=>true,
                                              'perfectScrollbar' => true,
                                               */

                                                /*
                                                        'pager' => [
                                                        'options'=>['class'=>'pagination'],   // set clas name used in ui list of pagination
                                                        'prevPageLabel' => 'Previous',   // Set the label for the "previous" page button
                                                        'nextPageLabel' => 'Next',   // Set the label for the "next" page button
                                                        'firstPageLabel'=>'First',   // Set the label for the "first" page button
                                                        'lastPageLabel'=>'Last',    // Set the label for the "last" page button
                                                        'nextPageCssClass'=>'next',    // Set CSS class for the "next" page button
                                                        'prevPageCssClass'=>'prev',    // Set CSS class for the "previous" page button
                                                        'firstPageCssClass'=>'first',    // Set CSS class for the "first" page button
                                                        'lastPageCssClass'=>'last',    // Set CSS class for the "last" page button
                                                        'maxButtonCount'=>20,    // Set maximum number of page buttons that can be displayed
                                                        ],
                                                                               */             //'filterModel' => $searchModel,

                                            'responsive'=>true,
                                            'hover'=>true,
                                            'condensed' => false,

                                            'responsiveWrap'=>false,
                                            'rowOptions'   => function ($model) {
                                                return ['id' => $model->id];
                                            },
                                             


                                            'pjax'=>true,
                                                'panel' => [
                                                //'type' => GridView::TYPE_PRIMARY,
                                                //'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
                                                //'before'=>$create.' '.$ordering.' ',
                                                'showFooter' => false,

                                            ],
                                            'toolbar' =>  [
                                                ['content' =>

                                                    Html::a('<i class="glyphicon glyphicon-repeat"></i>', ['index'], ['data-pjax' => 0, 'class' => 'btn btn-default', 'title' => 'Reset'])
                                                ],
                                                '{export}',
                                                '{toggleData}',
                                            ],
                                            // set export properties
                                            'export' => [
                                                'fontAwesome' => true
                                            ],

                                            'columns' => [

                                                ['class' => 'yii\grid\SerialColumn'],

                                                //'id',
                                                [
                                                    'attribute'=>'uniqueid',
                                                    'label'=>'Caller ID',

                                                    'group'=>'true',

                                                ],
                                               
                                                [
                                                    'attribute'=>'calldate',
                                                   'value' => function($model){
                                                return date("d/m/Y G:i a", strtotime($model->calldate));
                                          },
                                   'filter'=>false




                                                ],

                                                /*[
                                                    'attribute'=>'calldate',
                                                    'filter'=>false,

                                                ],*/
                                                [
                                                    'label'=>'Source',
                                                    'value'=>function($model)
                                                    {

                                                    if ($model->iorecording) {
                                                        if (strpos($model->iorecording, 'Outgoing') !== false) {

                                                            $myArray = explode('-', $model->channel);
                                                            return  $myArray[0];

                                                          

                                                        }else {return $model->src;}
                                                    }else {

                                                        return $model->src;
                                                    }
                                                    }

                                                ],
                                                [

                                                    'label'=>'Destination',

                                                    'value'=>function($model)
                                                    {
                                                        if($model->iorecording)
                                                        {
                                                            if (strpos($model->iorecording, 'Incoming') !== false) {
                                                                $myArray = explode('-', $model->dstchannel);
                                                                return  $myArray[0];
                                                            }else
                                                            {
                                                                return $model->dst;
                                                            }

                                                        }else{
                                                            if($model->dst=='o'){
                                                                return '';
                                                            }else { return $model->dst; }

                                                        }

                                                    }

                                                ],

                                                // 'clid',
                                                //'dst',
                                                //'channel',
                                                //'dstchannel',

                                                [

                                                    //'attribute'=>'lastapp',
                                                    'label'=>'Call Status',
                                                    'value'=>function($model)
                                                    {

                                                        if($model->lastapp=='Queue')
                                                        {
                                                            return 'Incomming';
                                                        }else if($model->lastapp=="Dial") {
                                                            if ($model->iorecording) {
                                                                if (strpos($model->iorecording, 'Incoming') !== false) {

                                                                    return 'Transfer';

                                                                }else {



                                                                    return 'OutGoining';
                                                                }
                                                            }else {
                                                                return 'Internal Call';
                                                            }
                                                        }

                                                    }
                                                ],


                                                [
                                                    //'attribute'=>'disposition',
                                                    'label'=>'Call Response',
                                                    'value'=>function($model)
                                                    {
                                                        return $model->disposition;
                                                    }
                                                ],

                                                /*  [
                                                      'label'=>'Call Type',
                                                      'value'=>function($model)
                                                      {
                                                          if(!empty($model->internalcall))
                                                          {
                                                              return 'Internal Call';
                                                          }else if(!empty($model->iorecording))
                                                          {
                                                                  if (strpos($model->iorecording, 'Incomming') !== false) {
                                                                      echo 'Incomming Call';
                                                                  }else
                                                                  {
                                                                      return 'OutGoining Call';
                                                                  }
                                                          }else if($model->vmfilename)
                                                          {
                                                              return 'Voice File';
                                                          }
                                                      }

                                                  ],*/
                                                //'internalcall',
                                                //'iorecording',
                                                //'vmfilename',

                                                /*[
                                                 'attribute'=>'lastapp',
                                                 'value'=>function($model){
                                                  if($model->lastapp==0){
                                                    return 'Queue';
                    
                                                }
                                                 else{
                                                  return $model->lastapp;
                                                 }
                                    }
                                                ],*/


                                                /* [

                                                     'attribute'=>'channel',
                                                     'value'=>function($model)
                                                     {

                                                         $channel = $model->channel;
                                                         $channelbreak = explode("-",$channel);
                                                         $dahdi = explode("/",$channelbreak[0]);


                                                         if($dahdi[0] == "DAHDI")
                                                         {
                                                             return "PTCL/".$dahdi[1];

                                                         }
                                                         else
                                                         {
                                                             return $dahdi[0]."/".$dahdi[1];

                                                         }

                                                     }


                                                 ],*/


                                                /* [

                                                     'attribute'=>'dstchannel',
                                                     'value'=>function($model)
                                                     {

                                                         $Dstchannel = $model->dstchannel;
                                                         $destination_line = explode("-",$Dstchannel);
                                                         $dst_dahdi = explode("/",$destination_line[0]);

                                                         if(!empty($Dstchannel))
                                                         {
                                                             if($dst_dahdi[0] == "DAHDI")
                                                             {

                                                                 return "PTCL/".$dst_dahdi[1];
                                                             }
                                                             else
                                                             {

                                                                 return $dst_dahdi[0]."/".$dst_dahdi[1];
                                                             }

                                                         }

                                                     }
                                                 ],*/
                                                /*
												[
                                                    'attribute' => 'duration',
                                                    'label'=>'Call duration',

                                                    'value' => function ($model) {
                                                        $calltimeio = array();
                                                        $hoursio = array();
                                                        $miuntsio = array();
                                                        $secondio = array();

                                                        $calltimeintercall=array();
                                                        $hoursintercall=array();
                                                        $minutintercall=array();
                                                        $secondintercall=array();




                                                            ////////////call incoming/////
                                                            $calltimeio = explode('__',$model->iorecording);

                                                            $hoursio = explode('H',$calltimeio[1]);
                                                            $miuntsio = explode('M',$hoursio[1]);
                                                            $secondio = explode('S',$miuntsio[1]);
                                                             $callintime =  $hoursio[0].':'. $miuntsio[0].':'.$secondio[0]."-------";


                                                            /////////////////////////////////////////
                                                            /// ///////////////call recieve/////////
                                                             $calltimeintercall = explode('__',$model->internalcall);
                                                            $hoursintercall = explode('H',$calltimeintercall[1]);
                                                            $minutintercall = explode('M',$hoursintercall[1]);
                                                            $secondintercall= explode('S',$minutintercall[1]);
                                                            $callintimeinternalcall =  $hoursintercall[0].':'. $minutintercall[0].':'.$secondintercall[0];
                                                            /// ////////////////////////////////////

                                                            //////////////////////////time difference////////////////////////////////
                                                            if(($secondintercall[0]-$secondio[0])<0)
                                                            {

                                                                $econdtime = -1*($secondintercall[0]-$secondio[0]);
                                                            }else
                                                            {
                                                                $econdtime = ($secondintercall[0]-$secondio[0]);
                                                            }
                                                            if(($minutintercall[0]-$miuntsio[0])<0)
                                                            {
                                                                $minutstime = -1*($minutintercall[0]-$miuntsio[0]);
                                                            }else
                                                            {
                                                                $minutstime = ($minutintercall[0]-$miuntsio[0]);
                                                            }

                                                        if($model->lastapp=='Queue')
                                                        {
                                                            return $dteDiff  = ($minutstime).'m '.$econdtime.'s';
                                                        }else if($model->lastapp=="Dial") {
                                                            if ($model->iorecording) {
                                                                if (strpos($model->iorecording, 'Incoming') !== false) {
                                                                    if($model->duration >=60)
                                                                    {

                                                                        //echo dat =time('Y-m-d', strtotime($model->duration));
                                                                        $min = $model->duration/60;
                                                                        $mints = explode(".",$min);
                                                                        $second = $model->duration%60;

                                                                        return ($minutstime+$mints[0].'m ').($econdtime+$second.'s');
                                                                    } else
                                                                    {
                                                                         return $dteDiff  = ($minutstime).'m '.($econdtime+$model->duration).'s';

                                                                    }


                                                                }else {

                                                                    return $dteDiff  = ($minutstime).'m '.$econdtime.'s';
                                                                }
                                                            }else {
                                                                return $dteDiff  = ($minutstime).'m '.$econdtime.'s';
                                                            }
                                                        }
                                                            ////////////////////////////////////////////////////////////////////////

                                                    },
                                                ],
*/
                                                /*[
                                                    'attribute' => 'clip duration',
                                                    'value' => function ($model) {

                                                        if($model->duration >=60)
                                                        {
                                                            //echo dat =time('Y-m-d', strtotime($model->duration));
                                                            $min = $model->duration/60;
                                                            $mints = explode(".",$min);
                                                            $second = $model->duration%60;
                                                            return $mints[0].m." ".$second.s;
                                                        } else
                                                        {
                                                            return $model->duration.s;

                                                        }
                                                    },
                                                ],*/
                                                /*[
                                                    'label'=>'Recording',
                                                    'format'=>'raw',
                                                    'visible'=> (int)Yii::$app->user->can('audio/play'),

                                                    'value'=>function($model)
                                                    {


                                                       if($model->internalcall){

                                                            $play = '<a href="#" class="bg-cl" id='.$model->id.' onClick="playInternal(\''.$model->internalcall.'\'); Display1(\''.$model->id.'\');" >Play</span></a>';
                                                            return $play;


                                                        } else if($model->iorecording)
                                                        {
                                                            if (strpos($model->iorecording, 'Incoming') !== false) {
                                                                $play = '<a href="#" class="bg-cl" id='.$model->id.' onClick="playIncomming(\''.$model->iorecording.'\'); Display1(\''.$model->id.'\');" >Play</a>';
                                                                return $play;
                                                            }else
                                                            {

                                                                $play = '<a href="#" class="bg-cl" id='.$model->id.' onClick="playOutgoining(\''.$model->iorecording.'\'); Display1(\''.$model->id.'\');" >Play</span></a>';
                                                                return $play;
                                                            }
                                                        }



                                                      }


                                                ],*/


                                                /*[
                                                    //'attribute'=>'disposition',
                                                    'label'=>'Download',
                                                    'format'=>'raw',
                                                    'visible'=> (int)Yii::$app->user->can('audio/download'),

                                                    'value'=>function($model)
                                                    {
                                                        if($model->internalcall){

                                                        return '<a href="http://125.209.89.206:84/cdr/InternalCalls/'.$model->internalcall.'" target="_blank" data-pjax =0  download >Download</a>';


                                                        } else if($model->iorecording)
                                                        {
                                                            if (strpos($model->iorecording, 'Incoming') !== false) {
                                                              return '<a href="http://125.209.89.206:84/cdr/Incoming_Calls/'.$model->iorecording.'"  target="_blank"  data-pjax =0  download>Download</a>';

                                                            }else
                                                            {

                                                             return '<a href="http://125.209.89.206:84/cdr/Outgoing_Calls/'.$model->iorecording.'" target="_blank"  data-pjax =0  download >Download</a>';

                                                            }

                                                        }

                                                    }

                                                ],*/
                                                /*[
                                                    'attribute'=>'desc',
                                                    'label'=>'Comment',
                                                    'visible'=> (int)Yii::$app->user->can('comment'),
                                                    'filter'=>true,


                                                ],
                                                [
                                                    'class' => '\kartik\grid\ActionColumn',
                                                    'template'=>' {desc}',
                                                    'visible'=> (int)Yii::$app->user->can('comment'),
                                                    'buttons' => [
                                                        'desc'=>function($url,$model)
                                                        {

                                                                return '<a href="javascript:void(0)"  onclick="adddescription('.$model->id.');" title="Description"><span class="fa fa-comment" ></span></a>';

                                                        },

                                                    ],
                                                ],*/
                                                /* ['attribute'=>'Download',
                                                  'format'=>'raw',
                                                'value' => function($model)
                                                  {
                                                  return
                                                 Html::a('Download', ['/site/download','id' => $model->id],['class' => 'btn btn-primary']);

                                                  }
                                               ],*/

                                            ],
                                        ]); ?>

                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

<div class="footer fixed">
        <audio id="player" preload="metadata"  controls"><source  src="http://182.180.95.236:82/CDR_OLD/sample/sample.wav" type="audio/wav"></audio>


    </div>
        </div>
    </div>
<style>
    .table-responsive > .table-bordered {

        font-size: 12px;
    }
</style>
 <script type="text/javascript">


     function Display1(id) {

         $("#"+id).on("click",function(){
            // var id=$(this).attr('id');

             $('tr td').css({ 'background-color' : 'white'});
              $('td', this).css({ 'background-color' : '#1ab394' });
             /*$('html,body').animate({
                     scrollTop: $("#"+id).offset().top},
                 'slow');*/
             return false;
         });

     }



</script>

<script>
    function adddescription(id)
    {
        $("#modalLoginForm").modal('show');
        //alert(id);die();
        $.ajax({
            url:"<?php echo Yii::$app->homeUrl?>ajax/getdesc",
            method:"post",
            data:{id:id},
            success:function(res)
            {

                var result = $.parseJSON(res);
                $("#appartementtitle").text(result[1]);
                $("#desc").text(result[0]);
                $("#desc").attr('value',result[0]);
                if($("#desc").text()=="")
                {

                    $("#desc").text(" ");
                    $("#desc").attr('value',"");
                    $("#savedesc").val(id);
                }
                else
                {

                    $("#savedesc").val(id);
                }

            }
        });




    }

    $("#savedesc").on('click',function(){
        var desc=$("#desc").val();

        var id=$(this).val();

        if(desc==" ")
        {
            /*("#desc").addClass('description');
            return false;*/
        }
        else
        {
           /* ("#desc").addClass('descpvalid');*/

            $.ajax({
                url:"<?php echo Yii::$app->homeUrl?>ajax/adddesc",
                method:"post",
                data:{id:id,desc:desc},
                success:function(res)
                {

                    $("#desc").text("");
                    $("#modalLoginForm").modal('hide');
                    location.reload();


                }
            });


        }
    });
</script>





<style>
.highlight{
  background-color: #1ab394;

}
</style>

    


    <script>

        $( function() { $( 'audio' ).audioPlayer(); } );

        function playIncomming(fileName)
        {

            var vid = document.getElementById("player");
           // var mdid=document.getElementById(id='.$model->id.');

            var data = 'http://125.209.89.206:84/cdr/Incoming_Calls/'+fileName;

            var src = encodeURI(data);
            vid.src = src;

            vid.load();
            vid.play();





        }

        function playOutgoining(fileName)
        {

            var vid = document.getElementById("player");

            var data = 'http://125.209.89.206:84/cdr/Outgoing_Calls/'+fileName;

            var src=encodeURI(data);
            vid.src = src;

            vid.load();
            vid.play();




        }

        function playInternal(fileName)
        {

            var vid = document.getElementById("player");

            var data = 'http://125.209.89.206:84/cdr/InternalCalls/'+fileName;

            var src=encodeURI(data);
            vid.src = src;

            vid.load();
            vid.play();


        }

     /*   $("i.fa.fa-play").click(function(){
          if($(this).hasClass("fa fa-play"))
          {
            $(this).removeClass("fa fa-play").addClass("fa fa-pause");
            



          }
          else
          {
            $(this).removeClass("fa fa-pause").addClass("fa fa-play");
           
            vid.play();


          }*/

       // });

    </script>
        <style type="text/css" media="screen">
            body {
                overflow-scrolling:auto;

            }
        </style>



        <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<script>

    $('.panel-heading').hide();

</script>