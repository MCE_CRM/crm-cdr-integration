<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "forwarding".
 *
 * @property int $id
 * @property string $time_to
 * @property string $time_from
 * @property string $phone_no
 * @property string $created_on
 * @property int $created_by
 * @property string $updated_on
 * @property int $updated_by
 */
class Forwarding extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forwarding';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_on', 'updated_on'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['updated_on', 'updated_by'], 'required'],
            [['time_to', 'time_from', 'phone_no'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time_to' => 'Time To',
            'time_from' => 'Time From',
            'phone_no' => 'Phone No',
            'created_on' => 'Created On',
            'created_by' => 'Created By',
            'updated_on' => 'Updated On',
            'updated_by' => 'Updated By',
        ];
    }
}
