<?php

namespace app\modules\cdr\models;

use DateTime;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\cdr\models\ServerStatus;

/**
 * ServerStatusSearch represents the model behind the search form of `app\models\ServerStatus`.
 */
class ServerStatusSearch extends ServerStatus
{
    /**
     * @inheritdoc
     */




    public function rules()
    {
        return [
            [['id', 'type'], 'integer'],
            [['username', 'ip', 'status', 'total_calls', 'last_call_taken', 'created_on', 'latency'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ServerStatus::find()->orderBy(['id'=>SORT_DESC]);



        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'type' => $this->type,
            //'created_on' => $this->created_on,
        ]);

        if ( ! is_null($this->created_on) && strpos($this->created_on, ' - ') !== false ) {
            list($start_date, $end_date) = explode(' - ', $this->created_on);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            $query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);

           

        }

        $query->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'ip', $this->ip])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'total_calls', $this->total_calls])
            ->andFilterWhere(['like', 'last_call_taken', $this->last_call_taken])
            ->andFilterWhere(['like', 'latency', $this->latency]);






        return $dataProvider;
    }
}
