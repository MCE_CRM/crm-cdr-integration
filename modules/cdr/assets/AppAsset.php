<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\modules\cdr\assets;

use yii\web\AssetBundle; 

/** 
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800|Shadows+Into+Light',
        'porto/vendor/bootstrap/css/bootstrap.css',
        'porto/vendor/datatables/media/css/dataTables.bootstrap4.css',
        'porto/vendor/animate/animate.css',
        'porto/vendor/font-awesome/css/fontawesome-all.min.css',
        'porto/vendor/magnific-popup/magnific-popup.css',
        'porto/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.css',
        'porto/vendor/jquery-ui/jquery-ui.css',
        'porto/vendor/jquery-ui/jquery-ui.theme.css',
        'porto/vendor/bootstrap-multiselect/bootstrap-multiselect.css',
        'porto/vendor/morris/morris.css',
        'porto/css/theme.css',
        'porto/css/skins/default.css',
        'porto/css/custom.css',
        'porto/vendor/fullcalendar/fullcalendar.css',
        'css/custom.css',
        'amcharts/export.css',
        'AudioPlayer/css/demo.css',
        'AudioPlayer/css/audioplayer.css',

    ];
    public $js = [

        //'porto/vendor/jquery/jquery.js',
        'porto/vendor/jquery-browser-mobile/jquery.browser.mobile.js',
        'js/html5lightbox/html5lightbox.js',
        'porto/vendor/popper/umd/popper.min.js',
        'porto/vendor/bootstrap/js/bootstrap.js',
        'porto/vendor/nprogress/nprogress.js',
        'porto/vendor/datatables/media/js/jquery.dataTables.min.js',
        'porto/vendor/datatables/media/js/dataTables.bootstrap4.min.js',
        'porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/dataTables.buttons.min.js',
        'porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.bootstrap4.min.js',
        'porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.html5.min.js',
        'porto/vendor/datatables/extras/TableTools/Buttons-1.4.2/js/buttons.print.min.js',
        'porto/vendor/datatables/extras/TableTools/JSZip-2.5.0/jszip.min.js',
        'porto/vendor/datatables/extras/TableTools/pdfmake-0.1.32/pdfmake.min.js',
        'porto/vendor/datatables/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js',
        'porto/vendor/snap.svg/snap.svg.js',
        'porto/vendor/liquid-meter/liquid.meter.js',
        'porto/vendor/moment/moment.js',
		'porto/vendor/modernizr/modernizr.js',
        'porto/vendor/fullcalendar/fullcalendar.js',

        //'porto/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js',
        'porto/vendor/common/common.js',
        'porto/vendor/nanoscroller/nanoscroller.js',
        //'porto/vendor/magnific-popup/jquery.magnific-popup.js',
        //'porto/vendor/jquery-placeholder/jquery-placeholder.js',
        //'porto/vendor/jquery-ui/jquery-ui.js',
        //'porto/vendor/jqueryui-touch-punch/jqueryui-touch-punch.js',
        'porto/vendor/jquery-appear/jquery-appear.js',
        'porto/vendor/bootstrap-multiselect/bootstrap-multiselect.js',
        //'porto/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js',
        //'porto/vendor/jquery-sparkline/jquery-sparkline.js',
        //'porto/vendor/raphael/raphael.js',
        //'porto/vendor/morris/morris.js',
        //'porto/vendor/gauge/gauge.js',
        //'porto/vendor/snap.svg/snap.svg.js',
        //'porto/vendor/liquid-meter/liquid.meter.js',
        //'porto/vendor/jqvmap/jquery.vmap.js',
        //'porto/vendor/jqvmap/data/jquery.vmap.sampledata.js',
        //'porto/vendor/jqvmap/maps/jquery.vmap.world.js',
        //'porto/js/theme.js',
        //'porto/js/custom.js',
        'porto/js/theme.init.js',
        'porto/js/examples/examples.header.menu.js',
        //'porto/js/examples/examples.dashboard.js',


        //'src/assets/global/scripts/app.min.js',
        //'src/assets/layouts/layout3/scripts/layout.min.js',
        //'src/assets/layouts/layout3/scripts/demo.min.js',
        //'src/assets/layouts/global/scripts/quick-sidebar.min.js',
        //'src/assets/layouts/global/scripts/quick-nav.min.js',
        //'porto/js/examples/extras/TableTools/pdfmake-0.1.32/vfs_fonts.js',
        //'porto/examples.loading.progress.js',
        //'porto/examples.loading.overlay.js',
        'js/bootbox.min.js',
        'js/custom.js',
        'amcharts/amchart.js',
        'amcharts/serial.js',
        'amcharts/plugins/export/export.min.js',
        'amcharts/themes/light.js',
        'amcharts/plugins/dataloader/dataloader.min.js',
        'AudioPlayer/js/audioplayer.js',



    ];
    public $depends = [
      


    ];
}
