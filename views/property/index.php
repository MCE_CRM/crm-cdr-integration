<?php
use kartik\editable\Editable;
use yii\helpers\Html;
use yii\widgets\Pjax;
use kartik\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\User;
use app\models\Projects;
use yii\web\JsExpression;
use app\models\Blocksector;
/* @var $this yii\web\View */
/* @var $searchModel app\models\PropertySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Properties';
$this->params['breadcrumbs'][] = $this->title;
?>
<style type="text/css">
    .description{
        border:1px solid red;
    }
    .descpvalid{
        border:1px solid black;
    }
    .modal-open .modal {
    overflow-x: hidden;
     overflow-y:hidden !important;
    }
   
   .modal-body {
    max-height:400px !important;
    overflow-y:scroll;
   }
</style>
<div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Sold Description</h4>
        <button type="button" class="close" data-dismiss="modal" id="close" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
       <div class="modal-body mx-3" style="overflow-y: hidden !important;height: auto !important;">
        <h4 id="appartementtitle"></h4>
        <div class="md-form mb-5">
          <i class="fa fa-envelope prefix grey-text"></i>
          <textarea cols="60" rows="5"id="desc" style="padding-left:4px;width: 100%;"></textarea>
          <label data-error="wrong" data-success="right" for="defaultForm-email">Your Description</label>
        </div>

      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default" id="savedesc">Save</button>
        <button class="btn btn-default updatedescription"  style="display: none;">Update</button>
      </div>
    </div>
  </div>
</div>
<div class="property-index">

    <?php

    $create = '';
    if(\Yii::$app->user->can('property/create')){
        $create = Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', 'Add New Property'), [
            'create'
        ], ['data-pjax' => 0,
            'class' => 'btn btn-default btn-sm add-new'
        ]);
    }




    ?>

    <?php  Pjax::begin(['timeout' => '30000']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php
        if(\Yii::$app->user->can('propertyDownload')){
            $export = '{export}';
        }

    ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'responsiveWrap' => false,
        'toolbar' =>  [
             $export,
            '{toggleData}',
        ],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',

            'before'=>$create.' ',

            'after'=>Html::a('<i class="fa fa-sync"></i> '.Yii::t ( 'app', 'Reset List' ), ['index'], ['class' => 'btn btn-info btn-sm']).' ',

            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            /*[
                'attribute'=>'project_id',
                'value'=>function($model)
                {
                    $projectname=\app\models\Projects::find()->select('name')->where(['id'=>$model->project_id])->one();
                    if(!empty($projectname))
                    {
                        return $projectname->name;
                    }
                    else
                    {
                        return 'open';
                    }
                },

                 'format'=>'raw',
                    'width'=>'200px',
                    'filter'=> Select2::widget([

                        'name' => 'PropertySearch[project_id]',
                        'options' => ['placeholder' => 'All ...'],
                        'model' => $searchModel,
                        'value' => [''.$searchModel->projects->id.''=>$searchModel->projects->name],
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/project-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]),
            ],*/


            [
                    'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                    'attribute' => 'project_id',
                    'contentOptions' => ['class' => 'text-center'],
                    'value'=>function($model)
                    {
                      $blocksector=\app\models\Projects::find()->where(['id'=>$model->project_id])->one();
                  
                        return $blocksector->name;
                    },
                    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(Projects::find()->all(), 'id', 'name'),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',

                ],

            [
                    'headerOptions' => ['style' => 'width:10%','class' => 'text-center'],
                    'attribute' => 'blocksector',
                    'contentOptions' => ['class' => 'text-center'],
                    'value'=>function($model)
                    {
                      $blocksector=\app\models\Blocksector::find()->where(['id'=>$model->blocksector])->one();
                  
                        return $blocksector->blocksector;
                    },
                    'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(Blocksector::find()->all(), 'id', 'blocksector'),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',

                ],

            /* [
                'attribute'=>'blocksector',
                'value'=>function($model)
                {
                    $blocksector=\app\models\Blocksector::find()->where(['id'=>$model->blocksector])->one();
                  
                        return $blocksector->blocksector;
                  
                },

                 'format'=>'raw',
                    'width'=>'200px',
                    'filter'=> Select2::widget([

                        'name' => 'PropertySearch[blocksector]',
                        'options' => ['placeholder' => 'All ...'],
                        'model' => $searchModel,
                        'value' => $searchModel->blocksector->blocksector,
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/blocksector-list']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]),
            ],*/
          

            [
                'attribute'=>'property_title',
                'label'=>'Title',
            ],
             [
                'attribute'=>'dimension',
                 'label'=>'Dimension',
            ],

            'purpose',
            'property_type',
            'property_location_type',
			'member_ship_no',
			[
					'attribute'=>'property_assign',
					'format'=>'raw',
					'value'=>function($model) {
						if (Yii::$app->user->can("property/assign-other")) {
	
	
							if ($model->property_assign) {
								
								$model->property_assign = explode(',', $model->property_assign);
								$model->property_assign=array_unique($model->property_assign);
								$e = '';
	
								foreach ($model->property_assign as $key => $data) {
									$user = \app\models\User::findOne($data);
	
									$e .= $user->first_name . ',<br>';
								}
	
								$e = substr_replace($e, "", -1);
	
								return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">' . $e . '</button>';
	
							} else {
								return '<a href="JavaScript:Void(0);"  onclick="assign(event,' . $model->id . ')"; class="kv-editable-value kv-editable-link assign">Not Set</button>';
							}
						}else {
							if ($model->property_assign) {
							   
								$model->property_assign = explode(',', $model->property_assign);
								$model->property_assign=array_unique($model->property_assign);
								$e = '';
								foreach ($model->property_assign as $key => $data) {
									$user = \app\models\User::findOne($data);
									$e .= $user->first_name . ',<br>';
								}
	
								$e = substr_replace($e, "", -1);
	
								return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">' . $e . '</button>';
	
							} else {
								return '<a href="JavaScript:Void(0);"   class="kv-editable-value kv-editable-link assign">Not Set</button>';
							}
						}
	
					}
            	],
            //'description',
            //'price',
            //'price_range',
            [
                'attribute'=>'land_area',
                'label'=>'Land Area',
                'format'=>'raw',
                'value'=>function($model)
                {
                    return $model->land_area .' '.$model->land_area_unit;
                }
            ],
            [
                'attribute'=>'price_per',
                'label'=>'Unit Price',
                'value'=>function($model)
                {
                    return number_format($model->price_per);
                }
            ],
            [
                'attribute'=>'price',
                'value'=>function($model)
                {
                    return number_format($model->price);
                }
            ],
			[
                'attribute'=>'demand_price',
                'value'=>function($model)
                {
                    return number_format($model->demand_price);
                }
            ],
            [
                'attribute'=>'extra_charges',
                'value'=>function($model)
                {
                    return number_format($model->extra_charges);
                }
            ],
            [
                'attribute'=>'total_price',
                'value'=>function($model)
                {
                    return number_format($model->total_price);
                }
            ],
			
			
			
			[
                
                'attribute'=>'Lead Details',
                'format' => 'raw',
                'value'=>function($model)
                {
                    /*$assignid=\app\models\Leads::find()->select('lead_assign')->where(['property_id'=>$model->id])->one();*/
                    $userdata=User::find()->select('first_name,last_name')->where(['id'=>$model->created_by])->one();
                   /* return $assignid->lead_assign;*/
                   /* $assignname=\app\models\User::find()->select('first_name')->where(['id'=>$assignid])->one();*/
                    
                      return Html::a('<i class="'.$assignid->lead_assign.'"  onClick="getallleadsonproperty('.$model->id.','.$model->created_by.',event)" data-toggle="tooltip" style="color:blue;cursor:pointer;">'.$userdata->first_name.' '.$userdata->last_name.'</i>' );
                    
                    
                }
            ],
            [
                'class' => 'kartik\grid\EditableColumn',
                'attribute' => 'status',
                'label'=>'Status',
                 'filterType' => \kartik\grid\GridView::FILTER_SELECT2,
                    'filter' => ArrayHelper::map(\app\models\PropertyStatus::find()->all(), 'status', 'status'),
                    'filterWidgetOptions' => [
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ],
                    'filterInputOptions' => ['placeholder' => 'All...'],
                    'format'=>'raw',
                //'options' => ['style' => 'width: 8%'],

                'editableOptions'=> function ($model, $key, $index, $widget) {
                    if(Yii::$app->user->can('property/status')){
                        $appttypes = ArrayHelper::map(\app\models\PropertyStatus::find()->all(), 'status', 'status');
                        return [
                        'header' => 'Status',
                        'attribute' => 'status',
                        'asPopover' => false,
                        'inlineSettings' => [
                            'closeButton' => '<button class="btn btn-sm btn-danger kv-editable-close kv-editable-submit" title="Cancel Edit"><i class="fa fa-times-circle"></i></button>'
                        ],

                        'type' => 'primary',
                        //'size'=> 'lg',
                        'size' => 'md',
                        'options' => ['class'=>'form-control', 'placeholder'=>'Enter person name...'],
                        'inputType' => Editable::INPUT_DROPDOWN_LIST,
                        'displayValueConfig' => $appttypes,
                        'data' => $appttypes,
                        'formOptions'=> ['action' => ['/ajax/update-property-status']] // point to new action
                    ];
                    }
                },

            ],
            
            [
            
              'attribute'=>'statusdesc',
              'label'=>'Sold/Hold',
              'filter'=>true,
              'visible' => Yii::$app->user->can('property/solddesc'),

            ],

            //'land_area_unit',
            //'land_area_unit_name',
            //'created_on',
            //'created_by',
            //'updated_on',
            //'updated_by',

            [
                'class' => '\kartik\grid\ActionColumn',
                'template'=>'{view} {update} {attach} {delete} {desc} {activity}',
                'buttons' => [
                    'update' => function ($url, $model)

                    {
                      
                        $user_id=Yii::$app->user->id; 
                      
                   
                       
                        // if($user_id==$model->created_by) {

                            if(\Yii::$app->user->can('property/update')){
                            return '<a href="'.$url.'"><span class="fa fa-pencil-alt" ></span></a>';
                        }

                        else if(\Yii::$app->user->can('property/update/myproperty')AND ($user_id==$model->created_by) ) {
                        
                            return '<a href="'.$url.'"><span class="fa fa-pencil-alt" ></span></a>';
                        }
                    },
                    
                
                    'delete' => function ($url, $model, $key) {
                        if (\Yii::$app->user->can('property/delete')) {
                            return Html::a('<span class="fas fa-trash"></span>', $url,
                                [
                                    'title' => Yii::t('app', 'Delete'),
                                    'data-pjax' => '1',
                                    'data' => [
                                        'method' => 'post',
                                        'confirm' => Yii::t('app', 'Are You Sure To Delete ?'),
                                        'pjax' => 1,],
                                ]
                            );
                        }
                    },
                    'attach' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('property/attach-files')){
                            return '<a href="javascript:void(0)" onclick="attachFiles('.$model->id.',event);" title="Attachment"><span class="fas fa-paperclip mr-2 "></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,
                    'view' => function ($url, $model)

                    {
                        if(\Yii::$app->user->can('property/view')){
                            return '&nbsp;<a href="'.$url.'"><span class="fa fa-eye" title="view"></span></a>';

                        }else
                        {
                            return '';
                        }
                    } ,
                    'desc'=>function($url,$model)
                    {
                        if(\Yii::$app->user->can('property/desc')){
                        return '<a href="javascript:void(0)"  onclick="adddescription('.$model->id.',event);" title="Description"><span class="fa fa-comment" ></span></a>';
                         }
                    },

                     'activity'=>function($url,$model)
                    {
                        if(\Yii::$app->user->can('property/activity')){
                         return '<a href="javascript:void(0)"  onclick="showactivity('.$model->id.',event);" title="Activity"><span class="fa fa-history"></span></a>';
                        }
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
<script>
	
	
    function attachFiles(id,event) {
        event.preventDefault();
        var url = '<?= Yii::$app->homeUrl?>ajax/attach-files?type=property&id='+id;
        var dialog = bootbox.dialog({
            title: 'Attach Files',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
            size: 'large',
            onEscape: function() {
                $.pjax.reload({container: '#p0', async: false});
            }

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });



        });

    }
</script>
<script type="text/javascript">
    $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip(); 
  $('.kv-editable-popover').hide();
//   $('[data-toggle="popover-x"]').hide();


 

  

    });

   function assign(event,id) {

        event.preventDefault();
        var url = '<?=Yii::$app->homeUrl?>ajax/assign-property?id='+id+'';

        var dialog = bootbox.dialog({
            title: 'Show Other',
            message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',

        });

        dialog.init(function(){
            var request = $.ajax({
                url: url,
                method: "GET",
            });

            request.done(function( msg ) {
                dialog.find('.bootbox-body').html(msg);
            });

            $(document).on("submit", "#form", function (event) {
                event.preventDefault();
                event.stopImmediatePropagation();

                $(this).submit(function() {
                    return false;
                });



                $form = $(this); //wrap this in jQuery

                var url = $form.attr('action');

                $.ajax({
                    type: "POST",
                    url: url,
                    data: $("#form").serialize(),
                    // serializes the form's elements.
                    success: function(data)
                    {
                        if(data==true)
                        {
                            $.pjax.defaults.timeout = 5000;
                            $.pjax.reload({container:'#p0'});
                            bootbox.hideAll();
                        }


                    }
                });
                // avoid to execute the actual submit of the form.

            });

        });

    };
   

  


   function getallleadsonproperty(id,createdby,event) {

    event.preventDefault();

    var dialog = bootbox.dialog({
        title:'<h4 style="color:#85C0E7"><b>All leads Against Property</b></h4>',
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
        onEscape: function() {
            // you can do anything here you want when the user dismisses dialog
            $(".sp-palette-buttons-disabled").hide();
        }

    });

    dialog.init(function(){
        var request = $.ajax({
            url:'<?php echo Yii::$app->homeUrl;?>ajax/getpropertyleads',
            method: "GET",
            data:{id:id,createdby:createdby}
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);
        });


    });

};



 function adddescription(id)
 {
    $("#modalLoginForm").modal('show');
     $.ajax({
     url:"<?php echo Yii::$app->homeUrl?>ajax/getdesc",
     method:"post",
     data:{id:id},
     success:function(res)
     {  
        var result = $.parseJSON(res);
        $("#appartementtitle").text(result[1]);
        $("#desc").text(result[0]);
        $("#desc").attr('value',result[0]);
        if($("#desc").text()=="")
        {
          
          $("#desc").text(" ");
          $("#desc").attr('value',"");
          $("#savedesc").val(id);
        }
        else
        {   
            
            $("#savedesc").val(id);
        }

     }
    });
    
   

    
 }
   
    $("#savedesc").on('click',function(){
       var desc=$("#desc").val();
       var id=$(this).val();
       if(desc==" ")
       {
        $("#desc").addClass('description');
        return false;
       }
       else
       {
       $("#desc").addClass('descpvalid');
       
       $.ajax({
        url:"<?php echo Yii::$app->homeUrl?>ajax/adddesc",
        method:"post",
        data:{id:id,desc:desc},
        success:function(res)
        {  
            
            $("#desc").text("");
           $("#modalLoginForm").modal('hide');
           location.reload();


        }
       });
   

   }
    });

   function showactivity(id,event) {
    event.preventDefault();

    var dialog = bootbox.dialog({
        title:'<h4 style="color:#85C0E7"><b>Property Activities</b></h4>',
        message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
        onEscape: function() {
            // you can do anything here you want when the user dismisses dialog
            $(".sp-palette-buttons-disabled").hide();
        }

    });

    dialog.init(function(){
        var request = $.ajax({
            url:'<?php echo Yii::$app->homeUrl;?>ajax/getpropertyactivities',
            method: "GET",
            data:{id:id}
        });

        request.done(function( msg ) {
            dialog.find('.bootbox-body').html(msg);
        });

        
    });
};


$("#close").on('click',function(){
    $("#desc").attr('value',"");
    $("#desc").text("");
});

    $("#select2-property-project_id-container").change(function(){
        alert();
    });



</script>

<style>
.select2-selection
{
	overflow:auto;
	
}
</style>