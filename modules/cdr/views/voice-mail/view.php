<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\VoiceMail */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Voice Mails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voice-mail-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'number_press',
            'file',
            'depart_id',
            'time_to',
            'time_from',
            'description',
            'status',
            'created_on',
            'created_by',
        ],
    ]) ?>

</div>
