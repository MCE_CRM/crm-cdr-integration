<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AgentSessionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agent Sessions';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="server-status-index">

    <div class="card-body">
      
        <div class="agent-session-search">
            
                <form action="" method="get">
                   <div class="col-lg-3">
                    <div class="form-group field-cdrsearch-channel rounded" >
                        <label class="label-class" for="cdrsearch-channel">Agent</label>
                        <select name="name" class="form-control rounded">

                            <?php for($j = 2001; $j<=2004; $j++){?>
                                <option value="<?=$j?>" <?php echo $_GET['name']== $j ? 'selected':''?>><?=$j?>(<?php $user = \app\modules\cdr\models\User::find()->where(['=','agent_id',$j])->one(); echo  $user->first_name.' '.$user->last_name; ?>)</option>
                            <?php } ?>

                        </select>
                        <!--<input type="text" id="cdrsearch-channel" class="form-control rounded" name="name">-->

                    
                        <label >From</label>
                        <input type="text" id="cdrsearch-channel" class="form-control from" name="from" value="<?php echo empty($_GET['from']) ? date('m/d/Y'): $_GET['from']  ?>" autocomplete="off">

                
                        <label >To</label>
                        <input type="text" id="cdrsearch-channel" class="form-control to" name="to" value="<?php echo empty($_GET['to']) ? date('m/d/Y') : $_GET['to']?>" autocomplete="off">
                        <div class="help-block"></div>
                    </div>
                </div>
            </div>



            <div class="form-group" style="padding-left: 15px;">
                <button type="submit" class="btn btn-primary">Search</button>
                <button type="reset" class="btn btn-default">Reset</button>
            </div>

            </form>

        </div>

    </div>

<div class="card-body">
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox ">
                    <div class="ibox-content">
                        <div class="">
                            <table class="table table-striped table-bordered table-hover dataTables-example" id="agent-session">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Agent</th>
                                    <th>Date</th>
                                    <th>Login</th>
                                    <th>Logout</th>
                                    <th>Hours</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                $i =0;
                                $total  = 0;
                                /*echo '<pre>';
                                echo print_r($session);exit;*/
                                foreach($session as $k => $myModel){

                                    $prevs = \app\modules\cdr\models\AgentSession::find()->where(['=','name',$myModel['name']])->andWhere(['<', 'id', $myModel['id']])->one();

                                    if($myModel['type'] == 'login'){
                                        $next = \app\modules\cdr\models\AgentSession::find()->where(['=','name',$myModel['name']])->andWhere(['>', 'id', $myModel['id']])->one();
                                        /*echo '<pre>';
                                        echo print_r($next);exit;*/
                                    }
                                    if($next->type == 'logout'){

                                        //echo $next->created_on;
                                        $n = array_search($next, $session);
                                        $n=(int)$n;
                                        if($k==$n )
                                        {
                                            continue;
                                        }
                                    }

                                    ?>
                                        <tr class="gradeX">
                                            <td><?php $user = \app\modules\cdr\models\User::find()->where(['=','agent_id',$myModel['name']])->one();
                                            echo $user->first_name.' '.$user->last_name;
                                            ?></td>
                                            <td><?=$myModel['name']?></td>
                                            <td class="center"><?= date('d-m-Y',strtotime($myModel['created_on']))?></td>
                                            <td><?php
                                                if($myModel['type'] == 'login') {
                                                      $login = $myModel['created_on'];
                                                    echo date('d-m-Y g:i:A',strtotime($myModel['created_on']));
                                                }
                                                else{
                                                     $login = 'missing';
                                                     echo '<p style="color:red;">missing</p>';

                                                }
                                                ?>
                                            </td>
                                            <td><?php

                                                if($next->type == 'logout'){
                                                    if($myModel['type'] == 'logout') {
                                                         $logout = $myModel['created_on'];
                                                        echo date('d-m-Y g:i:A',strtotime($myModel['created_on']));
                                                    }else{
                                                        echo date('d-m-Y g:i:A',strtotime($next->created_on));
                                                         $logout = $next->created_on;
                                                    }

                                                    $n = array_search($next, $session);

                                                }else{
                                                     $logout = 'missing';
                                                    echo '<p style="color:red;" >missing</p>';
                                                }
                                                    ?></td>

                                            <td class="center">
                                                <?php

                                                    if($logout != 'missing' && $login != 'missing'){
                                                        $time_diff = strtotime($logout) - strtotime($login);
                                                         $sec = round($time_diff,0);

                                                        echo gmdate("H:i:s", $sec);

                                                        $total += $sec;
                                                    }
                                                ?>
                                            </td>
                                </tr>

                                <?php /*}*/
                                    $n = (int)$n;
                                    /* unset($session[$k]);*/
                                      $i++;

                                } ?>

                                </tbody>
                                <tfoot>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th>Total Time</th>
                                <th><?php echo floor($total / 3600) . gmdate(":i:s", $total % 3600);?></th>
                                </tfoot>

                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

    <?php

        /*echo '<pre>';
        echo print_r($dataProvider->models);exit;*/
    ?>

    <?php GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

           // 'id',
            'name',
            [

                    'attribute' => 'created_on',
                    'value' => function($model){
                        return date('Y-m-d H:i:s',strtotime($model->created_on));

                    }
            ],


            /*[
                'format' =>'raw',
                'attribute' => 'type',
                'value' => function($model){
                    if($model->type == 'login'){
                        return  '<span class="label label-primary">'.Yii::t('app','Login').'</span>';

                    }
                    else{
                        return  '<span class="label label-danger">'.Yii::t('app','Logout').'</span>';

                    }

                },
            ],*/

            [
                'format' =>'raw',
                'header' => 'Logout',
                'value' => function($model){

                    $logout = \app\modules\cdr\models\AgentSession::find()->where(['=','name',$model->name])->andWhere(['>', 'id', $model->id])->one();

                    if($logout->type == 'logout'){
                        return $logout->created_on;
                    }else{
                        return 'missing';
                    }

                },
            ],

            //'created_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>


<script>

    $(document).ready(function(){

        $('#agent-session').dataTable({

            pageLength: 25,

            //responsive: true,
            //"bPaginate": true,
            "searching": false, "paging": true, "info": false

                /*dom: '<"html5buttons"B>lTfgitp',
                    buttons: [
                    { extend: 'copy'},
                    {extend: 'csv'},
                    {extend: 'excel', title: 'ExampleFile'},
                    {extend: 'pdf', title: 'ExampleFile'},

                    {extend: 'print',
                        customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                        }
                    }
                ]*/
        })
    })



    $(document).ready(function(){

        //$('input[name="daterange"]').daterangepicker();
        $('[name="to"]').datepicker();
        $('[name="from"]').datepicker();
        /*$('.from').datepicker({
            format: "dd-mm-yyyy",
            weekStart: 0,
            autoclose: true,
            todayHighlight: true
        });
        $('.to').datepicker({
            format: "dd-mm-yyyy",
            weekStart: 0,
            autoclose: true,
            todayHighlight: true
        });*/
    });

</script>