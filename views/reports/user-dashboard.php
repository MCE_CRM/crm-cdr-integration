<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\daterange\DateRangePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use yii\widgets\Pjax;

use app\models\Clients;
use app\models\Tasks;

$this->title = 'User Dashboard Report';

function getUserRoles($user_id){

    $connection = \Yii::$app->db;
    $sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
    $command=$connection->createCommand($sql);
    $dataReader=$command->queryAll();
    $roles ='';
    if(count($dataReader) > 0){
        foreach($dataReader as $role){
            $roles.="<span class=\"label label-primary\">".$role['name']."</span></br>";
        }
    }
    return $roles;
}
$start_date ='';
$end_date ='';

if(isset($_GET['leadsSearch']['created_on']))
{
	$datetime = explode(' - ',$_GET['leadsSearch']['created_on']);
	$start_date = date('Y-m-d', strtotime($datetime[0]));
	$end_date = date('Y-m-d', strtotime($datetime[1]));
}
?>




<style>
    .datepicker
    {
        margin-top: -33px;
    }

    .card-header
    {
        text-align:center;
    }
    .ibox {
        clear: both;
        margin-bottom: 25px;
        margin-top: 0;
        padding: 0;
    }
    .ibox-title {
        -moz-border-bottom-colors: none;
        -moz-border-left-colors: none;
        -moz-border-right-colors: none;
        -moz-border-top-colors: none;
        background-color: #ffffff;
        border-color: #e7eaec;
        border-image: none;
        border-style: solid solid none;
        border-width: 2px 0 0;
        color: inherit;
        margin-bottom: 0;
        padding: 15px 15px 7px;
        min-height: 48px;
    }


    .ibox-title .label {
        float: left;
        margin-left: 4px;
    }
    .label, .ibox .label {
        font-size: 10px;
        border-radius: 10px;
    }
    .label-success, .badge-success {
        background-color: #1c84c6;
        color: #FFFFFF;
    }
    .label {
        background-color:#2baab1;
        color:white;
        font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-weight: 600;
        padding: 0px 8px;
        text-shadow: none;
    }

    .ibox-title h5 {
        display: inline-block;
        font-size: 14px;
        margin: 0 0 7px;
        padding: 0;
        text-overflow: ellipsis;
        float: left;
        font-weight: 600;
    }
    .ibox-content {
        clear: both;
    }

    .ibox-content {
        background-color: #ffffff;
        color: inherit;
        padding: 15px 20px 20px 20px;
        border-color: #e7eaec;
        border-image: none;
        border-style: solid solid none;
        border-width: 1px 0;
    }
    small, .small {
        font-size: 85%;
    }
    .no-margins {
        margin: 0 !important;
    }
    .stat-percent {
        float: right;
    }

    .fa {
        display: inline-block;
        font: normal normal normal 14px/1;
        font-size: inherit;
        text-rendering: auto;
        -webkit-font-smoothing: antialiased;
    }
    #myChart
    {
        height:100% !important;
        width:100% !important;

    }
    @media (min-width: 381px) and (max-width: 767px) {

        #legenddiv{
            position: absolute !important;
            top:494px !important;
        }

    }
    .amcharts-chart-div a {display:none !important;}
    a.dlink:hover{
        text-decoration: none;
    }
	form
	{
		width:100%;
	}
</style>
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.js"></script>
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.init.js"></script>



<div class="row" style="width:100%">

    <div class="col-md-2"></div>
    
    
    
    	
        <?php 
		
		$form = ActiveForm::begin([
            'action' => ['user-dashboard'],
            'method' => 'get',
        ]); 
		?>
        		<input type="hidden" name="id" value="<?php if(isset($_GET['id'])){ echo $_GET['id'];}?>" />
        		<div class="col-md-4" style="float: left;">

                    <select class="form-control" id="user">
                    <option value="0">Select User</option>
                        <?php

                        if(\Yii::$app->user->can('teamlead')){

                        $team_lead=Yii::$app->user->identity->lead_assign;
                        $user=Yii::$app->user->identity->id;
                        $result =  array_map('intval', explode(',', $team_lead));  
            
                        $user = \app\models\User::find()->where(['id'=>$result])->all();
                        foreach ($user as $value)
                        {
                            $role = getUserRoles($value->id);
            
                      //  if (strpos($role, 'Agent') !== false  || strpos($role, 'CSR') !== false) { ?>
                            <option value="<?= $value->id?>"> <?= $value->first_name.' '.$value->last_name .' ['.$role.'] '?></option>
                        <?php } }
                        //}

                        else{
                             $user = \app\models\User::find()->all();
                        foreach ($user as $value)
                        {
                            $role = getUserRoles($value->id);
            
                      if (strpos($role, 'Agent') !== false  || strpos($role, 'CSR') !== false) { ?>
                            <option value="<?= $value->id?>"> <?= $value->first_name.' '.$value->last_name .' ['.$role.'] '?></option>
                        <?php } }
                        }
                        
            
                        ?>
                    <!-- <option value="All" >All</option> -->
            
            
                    </select>
            
            
                </div>
    			<div class="col-md-3" style="float: left;">
                <?php
				if(isset($_GET['leadsSearch']['created_on']))
				{
					$model->created_on =$_GET['leadsSearch']['created_on'];
				}
                echo DateRangePicker::widget([
						'model'=>$model,
						'attribute'=>'created_on',
						'convertFormat'=>true,
						'pluginOptions'=>[
							'timePicker'=>true,
							'timePickerIncrement'=>30,
							'locale'=>['format'=>'Y-m-d']
						],
						 'presetDropdown'=>false,
                    	'hideInput'=>true
					]);
				 ?>
				</div>
        
        	<?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default obaid']) ?>
        
        <?php ActiveForm::end(); ?>

</div>


<?php
if(isset($_GET['id'])) {
?>

<div class="row"> 

    <div class="col-lg-6">
        <div class="row mb-3">
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="<?php echo Yii::$app->homeurl;?>leads?leadsSearch[created_on]=<?php echo Date('Y-m-d');?>">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-tertiary">
                                        <i class="fas fa-phone"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><!-- <a href="<?php echo Yii::$app->homeurl;?>leads?leadsSearch[created_on]=<?php echo Date('Y-m-d');?>">Today's Leads</a> -->Today's Leads</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo $todayleads;?></strong>
                                        </div>
                                    </div>
                                    <!--<div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="#"></a>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-secondary">
                    <a class="dlink" href="<?php echo Yii::$app->homeUrl;?>leads/index">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-tertiary">
                                        <i class="fas fa-mobile-alt"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><!-- <a href="<?php echo Yii::$app->homeUrl;?>leads/index">Total Leads</a> --> Total Leads</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo $totalLeads;?></strong>
                                        </div>
                                    </div>
                                    <!-- <div class="summary-footer">
    <a class="text-muted text-uppercase" href="<?php /*echo yii::$app->homeurl;*/?>projects/index">view all</a>
    </div>-->
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
        </div>

    </div>
    <div class="col-lg-6">
        <div class="row mb-3">
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="<?php echo Yii::$app->homeurl;?>clients/index">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fas fa-crown"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><!-- <a href="<?php echo Yii::$app->homeurl;?>clients/index">Total Clients</a> -->Total Clients</h4>
                                        <div class="info">
                                            <strong class="amount"><?php 
											if(!empty($start_date))
											{
											echo Clients::find()->where(['created_by'=>$_GET['id']])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
											}else
											{
											echo Clients::find()->where(['created_by'=>$_GET['id']])->count();			
											}
											?></strong>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
            <div class="col-xl-6">
                <section class="card card-featured-left card-featured-quaternary">
                    <a class="dlink" href="<?php echo Yii::$app->homeurl;?>tasks">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-quaternary">
                                        <i class="fas fa-tasks"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><!-- <a href="<?php echo Yii::$app->homeurl;?>tasks">Pending Tasks</a> --> Pending Tasks</h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo $pending_tasks;?></strong>
                                        </div>
                                    </div>
                                    <!--<div class="summary-footer">
    <a class="text-muted text-uppercase" href="<?php /*echo Yii::$app->homeUrl;*/?>tasks/index">View All</a>
    </div>-->
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>

        </div>

    </div>
</div>


<div class="row">
    <div class="col-lg-12"><h3 style="text-align:center;color:#000;margin-top:-15px">Leads</h3></div>
    <div class="col-lg-6 mb-3 row-eq-height" style="background-color: #f9f9f9;">
        <section class="card" style="margin-top:74px;">
            <div class="card-body">
                <canvas id="myChart"></canvas>
                <h4 style="text-align: center;">status</h4>
            </div>
        </section>
    </div>

    <div class="col-lg-6 mb-3 row-eq-height" style="
    background: #f5f5f7;
    padding-top: 20px;
">
        <div class="row">
            <?php
            foreach ($leadStaus as $status):
				if(!empty($start_date))
				{
                	$stu = \app\models\Leads::find()->where([
                    'and',
                    ['like', 'lead_status', $status->status],
                    ['lead_assign'=>$_GET['id']]
                ])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
				}else
				{
					$stu = \app\models\Leads::find()->where([
                    'and',
                    ['like', 'lead_status', $status->status],
                    ['lead_assign'=>$_GET['id']]
                ])->count();
				}
				$color=$status->color;

                ?>
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-danger pull-right">All Time</span>
                            <h5 style="color:<?php echo $color;?>"><?php echo $status->status;?></h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"><?php echo $stu;?></h1>
                            <div class="stat-percent font-bold text-danger"> <i class="fa fa-level-down"></i></div>
                            <small><?php echo $status->status;?></small>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>



        </div>


    </div>


</div>



<div class="row ">
    <div class="col-lg-12"><h3 style="text-align:center;color:#000;margin-top:-15px">Tasks</h3></div>
    <div class="col-lg-6 mb-3 row-eq-height">
        <section class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-xl-12 text-center">

                        <canvas id="myChart2"></canvas>
                        <h4 style="text-align: center;">status</h4>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="col-lg-6 mb-3 row-eq-height">
        <div class="row">
            <?php
            foreach ($taskstatus as $status):
			if(!empty($start_date))
			{
                $stu = \app\models\Tasks::find()->where([
                    'and',
                    ['like', 'task_status', $status->status],
                    ['user_assigned_id'=>$_GET['id']]
                ])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
			}else
			{
				$stu = \app\models\Tasks::find()->where([
                    'and',
                    ['like', 'task_status', $status->status],
                    ['user_assigned_id'=>$_GET['id']]
                ])->count();
			}
			    // $color=$status->color;

                ?>
                <div class="col-lg-6">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <span class="label label-danger pull-right">All Time</span>
                            <h5 style="color:<?php echo $color;?>"><?php echo $status->status;?></h5>
                        </div>
                        <div class="ibox-content">
                            <h1 class="no-margins"><?php echo $stu;?></h1>
                            <div class="stat-percent font-bold text-danger"> <i class="fa fa-level-down"></i></div>

                        </div>
                    </div>
                </div>
            <?php endforeach;?>

        </div>


    </div>

</div>

<div class="col-lg-12" style="display:none">
    <section class="card">
        <header class="card-header" style="text-align: left;">

            <h3 class="">Project/Property Graph</h3>
        </header>
        <div class="card-body">
            <div class="col-lg-12 mb-12">
                <section class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12 text-center">
                                <div id="chartdiv">


                                </div>
                                <h4><i>Projects</i></h4>
                                <div id="legenddiv"></div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
    </section>
</div>


<?php } ?>


<?php
$lead_status= \app\models\LeadStatus::find()->select('status')->where(['active'=>1])->orderBy(['sort_order' => SORT_ASC,])->all();
$data=[];
$stu=[];
foreach($lead_status as $value)
{
    $data[]=$value->status;
	if(!empty($start_date)){
		(int)$stu[] = \app\models\Leads::find()->where([
			'and',
			['=', 'lead_status', $value->status],
			['lead_assign'=>$_GET['id']]
		])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
	}else
	{
		(int)$stu[] = \app\models\Leads::find()->where([
			'and',
			['=', 'lead_status', $value->status],
			['lead_assign'=>$_GET['id']]
		])->count();
	}

}

$task_status=\app\models\TaskStatus::find()->select('status')->where(['active'=>1])->orderBy([
    'sort_order' => SORT_ASC,
])->all();

$data1=[];
$stu1=[];
foreach($task_status as $value)
{
    $data1[]=$value->status;
	if(!empty($start_date))
	{
		(int)$stu1[] = \app\models\Tasks::find()->where([
			'and',
			['=', 'task_status', $value->status],
			['user_assigned_id'=>$_GET['id']]
		])->andwhere(['and', "created_on>='$start_date'", "created_on<='$end_date'"])->count();
	}else
	{
		(int)$stu1[] = \app\models\Tasks::find()->where([
			'and',
			['=', 'task_status', $value->status],
			['user_assigned_id'=>$_GET['id']]
		])->count();
	}

}

?>



<script type="text/javascript">
    $(function(){


        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels:<?php echo json_encode($data);?>,
                datasets: [
                    {
                        label: 'Leads',
                        data:<?php echo json_encode($stu);?>,

                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }

                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value) {if (value % 1 === 0) {return value;}}
                        }
                    }]
                }
            }
        });

    });


</script>



<!-- Styles -->
<style>
    #chartdiv
    {
        width: 100%;
        height: 500px;
    }


</style>


<!-- Chart code -->
<script>

    $(function(){



        var chart = AmCharts.makeChart("chartdiv", {
            "type": "serial",
            "theme": "black",
            "depth3D": 20,
            "angle": 10,
            "legend": {
                "horizontalGap": 20,
                "maxColumns": 1,
                "divId": "legenddiv",
                "position": "right",
                "useGraphSettings": true,
                "markerSize": 10
            },
            "dataLoader": {
                "url": "<?=Yii::$app->homeUrl?>ajax/getpropertyforuserdashboard"
            },

            "valueAxes": [{
                "stackType": "regular",
                "axisAlpha": 0.3,
                "gridAlpha": 0,
                "dashLength": 0,
                "autoGridCount": false,
                "gridCount": 20,
                'integersOnly':true
            }],
            "graphs": [{
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Sold",
                "type": "column",
                "color": "#000000",
                "valueField": "Sold",
                "fixedColumnWidth": 65
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Avaliable",
                "type": "column",
                "color": "#000000",
                "valueField": "Available",
                "fixedColumnWidth": 65
            }, {
                "balloonText": "<b>[[title]]</b><br><span style='font-size:14px'>[[category]]: <b>[[value]]</b></span>",
                "fillAlphas": 0.8,
                "labelText": "[[value]]",
                "lineAlpha": 0.3,
                "title": "Occupied",
                "type": "column",
                "color": "#000000",
                "valueField": "Occupied",
                "fixedColumnWidth": 65
            }],
            "categoryField": "name",
            "categoryAxis": {
                "gridPosition": "start",
                "axisAlpha": 0,
                "gridAlpha": 0,
                "position": "left",
                "labelRotation": 75

            },
            "export": {
                "enabled": true
            }

        });
        ;


    });

    $(function(){
        var ctx = document.getElementById("myChart2");
        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                labels:<?php echo json_encode($data1);?>,
                datasets: [
                    {
                        label: 'Tasks',
                        data:<?php echo json_encode($stu1);?>,

                        backgroundColor: [
                            'rgba(255, 99, 132, 0.2)',
                            'rgba(54, 162, 235, 0.2)',
                            'rgba(255, 206, 86, 0.2)',
                            'rgba(75, 192, 192, 0.2)',
                            'rgba(153, 102, 255, 0.2)',
                            'rgba(255, 159, 64, 0.2)'
                        ],
                        borderColor: [
                            'rgba(255,99,132,1)',
                            'rgba(54, 162, 235, 1)',
                            'rgba(255, 206, 86, 1)',
                            'rgba(75, 192, 192, 1)',
                            'rgba(153, 102, 255, 1)',
                            'rgba(255, 159, 64, 1)'
                        ],
                        borderWidth: 1
                    }

                ]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            callback: function(value) {if (value % 1 === 0) {return value;}}
                        }
                    }]
                }
            }
        });

    });



    $(document.body).on("change","#user",function(){

        if(this.value=='0')
        {
            window.location.href  = "<?php echo Yii::$app->homeUrl?>reports/user-dashboard";

        }else {

            window.location.href  = "<?php echo Yii::$app->homeUrl?>reports/user-dashboard?id="+this.value;
        }


    });
	
	$(document.body).on("click",".obaid",function(){
		window.location.href  = "<?php echo Yii::$app->homeUrl?>reports/user-dashboard";
		//document.getElementById('w0').reset();
	 	//$('.range-value').val('');
	});
	

    <?php

            if(isset($_GET['id'])){ ?>
    document.getElementById('user').value = "<?= $_GET['id'];?>";

           <?php }else { ?>

        document.getElementById('user').value = "0";
    <?php }



    ?>





</script>

<!-- HTML -->









