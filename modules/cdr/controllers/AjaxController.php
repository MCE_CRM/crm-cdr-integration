<?php

namespace app\modules\cdr\controllers;

use app\modules\cdr\helpers\Helper;
use app\modules\cdr\models\AgentSession;
use app\modules\cdr\models\ServerStatus;
use DateTime;
use yii;

use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\modules\cdr\models\Cdr;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class AjaxController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }


    public function actionLogin()
    {
	date_default_timezone_set("Asia/Karachi");
        $model = new AgentSession();
        $model->name = $_GET['agent'];
        $model->created_on = date("Y-m-d H:i:s");
        $model->created_by = 1;
        $model->type = 'login';
        $model->save();
    }


    public function actionLogout()
    {
	date_default_timezone_set("Asia/Karachi");

        $model = new AgentSession();
        $model->name = $_GET['agent'];
        $model->created_on = date("Y-m-d H:i:s");
        $model->created_by = 1;
        $model->type = 'logout';
        $model->save();
    }

    public function actionGetAgentStatus()
    {
	date_default_timezone_set("Asia/Karachi");

        exec('sudo /usr/sbin/asterisk -r -x "queue show" 2>&1',$online);
        exec('sudo /usr/sbin/asterisk -r -x "sip show peers"',$sip);




        return $this->renderAjax('ajax-agent-status', [
            'online' => $online,
            'sip' => $sip,
        ]);


    }

    public function actionCronAgentStatus()
    {
	date_default_timezone_set("Asia/Karachi");




        exec('sudo /usr/sbin/asterisk -r -x "queue show" 2>&1',$online);
        exec('sudo /usr/sbin/asterisk -r -x "sip show peers"',$sip);


        session_start();
        $countQueue = count($online);
        unset($online[0]);
        unset($online[1]);
        unset($online[$countQueue - 3]);
        unset($online[$countQueue - 2]);
        unset($online[$countQueue -1]);
        $match = preg_split('/\s*[()]/', $online[2]);



        foreach($online as $key => $data) {

            $username = '';
            $takencall = '';
            $last = '';
            $match = preg_split('/\s*[()]/', $data);
            $count = count($match);

            unset($match[1]);
            unset($match[2]);
            unset($match[3]);
            unset($match[4]);
            unset($match[5]);
            unset($match[$count - 1]);

            $username = $match[0];
            $takencall = $match[6];
            $last = $match[7];
            $online[$key] = array(
                'username' => $username,
                'takencall' => $takencall,
                'last'=> $last,
            );

            unset($match);



        }


        $count = count($sip);
//echo $count;
        unset($sip[0]);
        unset($sip[$count-1]);
        unset($sip[$count-2]);
//unset($sip[$count-3]);

        foreach($sip as $key => $line) {


            $ms = '';
            $status = '';
            $user = '';
            $ip = '';

            $status = trim(substr($line,105,150));
            if(substr($status,0,2) =='OK'){
                preg_match('#\((.*)\)#Umsi',$status,$res);
                $ms = $res[1];
                $status = 'OK';
            }

            $ip = trim(substr($line,26,40));




            $user = trim(substr($line,0,21));
            $user = substr($user,strpos($user,'/')+1);

            $usernam[] = $user;

            $sip[$key] = array(
                'ext' => $user,
                'user' => (array_key_exists($user,$names) ? $names[$user] : ''),
                'status'=> $status,
                'ip'=>$ip,
                'latency' => $ms,
            );

        }



//Insert In TO Database Now


        echo "<pre>";
        print_r($sip);
        echo "</pre>";


        echo "<pre>";
        print_r($online);
        echo "</pre>";



        foreach($online as $key => $value)
        {

            $uname  = preg_replace('/[^0-9]/', '', $value['username']);




            if (in_array($uname, $usernam)){

                $cron_username = $value['username'];
                if(empty($value['takencall']))
                {
                    $cron_taken_call = "Not in use";
                }else {

                    $takenCall = preg_replace('/[^0-9]/', '', $value['takencall']);
                    if(!$takenCall == 0)
                    {
                        $cron_taken_call = $takenCall;

                        /*$data = json_decode(file_get_contents(Yii::getAlias('@webroot/assets/newfile.txt')), true);

                        array_push($data[$cron_username],$takenCall);

                        $myfile = fopen(Yii::getAlias('@webroot/assets/newfile.txt'), "w") or die("Unable to open file!");

                        $txt = json_encode($data);
                        fwrite($myfile, $txt);
                        fclose($myfile);*/



                    }else
                    {
                        $cron_taken_call = '-';
                    }

                    //$data = json_decode(file_get_contents(Yii::getAlias('@webroot/assets/newfile.txt')), true);



                }



                if(empty($value['last']))
                {
                    $cron_last = "No Calls Yet";
                }

                else
                {
                    if (strpos($value['last'], 'Not in use') !== false) {
                        $last = 'In Call';
                    }else {

                        $last = preg_replace('/[^0-9]/', '', $value['last']);
                        $last = $last;
                    }


                    $cron_last = $last;
                }


                echo $cron_username;
                echo "<br>";

                echo $cron_taken_call;
                echo "<br>";

                echo $cron_last;
                echo "<br>";



                foreach ($sip as $key => $value) {
                    if($value['ext']==preg_replace('/[^0-9]/', '', $cron_username))
                    {
                        $cron_ip_address = $value['ip'];
                        break;
                        # code...
                    }
                }

                echo $cron_ip_address;
                echo "<br>";



                $model = new ServerStatus();
                $model->username = $cron_username ;
                $model->ip = $cron_ip_address ;
                $model->type = 1 ;
                $model->total_calls = $cron_taken_call ;
                $model->last_call_taken = $cron_last ;
                $model->created_on = date("Y-m-d H:i:s");

                $model->save();





            }


        }




        $keys = array_keys($sip);
        $last = end($keys);

        $cron_sip_username = $sip[$last]['ext'];
        $cron_sip_ip = $sip[$last]['ip'];
        $cron_sip_status = $sip[$last]['status'];
        $cron_sip_latency = $sip[$last]['latency'];


        echo $cron_sip_latency;


        $model = new ServerStatus();
        $model->username = $cron_sip_username ;
        $model->ip = $cron_sip_ip ;
        $model->type = 2 ;
        $model->status = $cron_sip_status ;
        $model->latency = $cron_sip_latency ;
        $model->created_on = date("Y-m-d H:i:s");

        $model->save();




    }



    public function  actionGetdesc(){
 $id=$_POST['id'];

    $description=Cdr::find()->select('desc')->where(['id'=>$id])->one();

    $desc=$description->desc;
    echo json_encode(array($desc));


}
    public function actionAdddesc()
    {

        $id=$_POST['id'];
        $desc=$_POST['desc'];

        $query = new Query;
        $query->createCommand()
            ->update('cdr', ['desc' => $desc], 'id= '.$id.'')
            ->execute();


    }
}

?>