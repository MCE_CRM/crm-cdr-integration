<?php

namespace app\controllers;

use app\helpers\Helper;
use app\models\Activities;
use app\models\Cdr;
use app\models\City;
use app\models\Clients; 
use app\models\EmailTemplate;
use app\models\FeatureList;
use app\models\Leads;
use app\models\LeadStatus;
use app\models\Notes;
use app\models\Projects;
use app\models\Property;
use app\models\PropertyCategory;
use app\models\SmsHistory;
use app\models\SmsTemplate;
use app\models\State;
use DateTime;
use Yii;
use app\models\Country;
use app\models\CountrySearch;
use yii\db\Query;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\User;
use app\models\FollowUp;
use app\models\Blocksector;
use app\models\Notification;
use app\models\EmailHistory;
use Pusher;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class AjaxController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }



    // public function actionStateList($q = null, $id = null) {
    //     \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    //     $out = ['results' => ['id' => '', 'text' => '']];
    //     if (!is_null($q)) {
    //         $query = new Query;
    //         $query->select('id, state AS text')
    //             ->from('state')
    //             ->where(['like', 'state', $q])
    //             ->limit(20);
    //         $command = $query->createCommand();
    //         $data = $command->queryAll();
    //         $out['results'] = array_values($data);
    //     }
    //     elseif ($id > 0) {
    //         $out['results'] = ['id' => $id, 'text' => State::find($id)->name];
    //     }
    //     return $out;
    // }

    public function actionCityList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, city AS text')
                ->from('city')
                ->where(['like', 'city', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => City::find($id)->name];
        }
        return $out;
    }

     public function actionNameList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, first_name AS text')
                ->from('user') 
                ->where(['like', 'first_name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            
            $out['results'] = array_values($data);
            // echo("<pre>");
            // print_r($out);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::find($id)->name];

        }
        return $out;
    }

    public function actionLeadList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
         $team_lead=Yii::$app->user->identity->lead_assign;
                    $result =  array_map('intval', explode(',', $team_lead));
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, first_name AS text')
                ->from('user') 
                ->where(['like', 'first_name', $q])->andWhere(['id'=>$result])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            
            $out['results'] = array_values($data);
            // echo("<pre>");
            // print_r($out);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => User::find($id)->name];

        }
        return $out;
    }


         public function actionStateList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, state AS text')
                ->from('state')
                ->where(['like', 'state', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => State::find($id)->name];
        }
        return $out;
    }


    public function actionCountryList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, country AS text')
                ->from('country')
                ->where(['like', 'country', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Country::find($id)->name];
        }
        return $out;
    }

    public function actionProjectList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, name AS text')
                ->from('projects')
                ->where(['like', 'name', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Projects::find($id)->name];
        }
        return $out;
    }

     public function actionPropertyList($q = null, $id = null) {
             \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
             $out = ['results' => ['id' => '', 'text' => '']];
             if (!is_null($q)) {
                 $query = new Query;
                 $query->select('id, property_title AS text')
                     ->from('property')
                     ->where(['like', 'property_title', $q])
                     ->limit(20);
                 $command = $query->createCommand();
                 $data = $command->queryAll();
                 $out['results'] = array_values($data);
             }
             elseif ($id > 0) {
                 $out['results'] = ['id' => $id, 'text' => State::find($id)->name];
             }
             return $out;
      }


      public function actionPropertyListAva($q = null, $id = null) {
		  	$cond ='';
			$cond1 ='';
			$block_id ='';
			$project_id ='';
		  	
                   \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                   $out = ['results' => ['id' => '', 'text' => '']];
                   if (!is_null($q)) {
                       $query = new Query;
                      $query->select('id, property_title AS text')
                           ->from('property')
                           ->where(['like', 'property_title', $q])
                          	->andWhere(['status'=>'Available']);
						   if(!empty($_REQUEST['project']))
							{
								$project_id = $_REQUEST['project'];
								$query->andWhere(['project_id'=>$project_id]);
							}
						   if(!empty($_REQUEST['block']) && $_REQUEST['block']>0)
							{
								
								$block_id = $_REQUEST['block'];
								$query->andWhere(['=','blocksector', $block_id]);
							}
							else
							{
								$query->andWhere(['is','blocksector',NULL]);
							}
						   


                       $command = $query->createCommand();
					  
                       $data = $command->queryAll();
                       $out['results'] = array_values($data);
                   }
                   elseif ($id > 0) {
                       $out['results'] = ['id' => $id, 'text' => Property::find($id)->property_title];
                   }
                   return $out;
            }

     public function actionBlocksectorList($q = null, $id = null) {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $query = new Query;
            $query->select('id, blocksector AS text')
                ->from('blocksector')
                ->where(['like', 'blocksector', $q])
                ->limit(20);
            $command = $query->createCommand();
            $data = $command->queryAll();
            $out['results'] = array_values($data);
        }
        elseif ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Blocksector::find($id)->blocksector];
        }
        return $out;
    }



    public function actionGetCountryState() {


        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cat_id = empty($ids[0]) ? null : $ids[0];
            //$subcat_id = empty($ids[1]) ? null : $ids[1];
            if ($cat_id != null) {
                $data = State::find()->where(['country_id'=>$cat_id])->all();
                $result = array();
                foreach ($data as $dat)
                {
                    $res['id'] = $dat->id;
                    $res['name'] = $dat->state;
                    $result[] = $res;


                }

                $response['out'] = $result;
                /**
                 * the getProdList function will query the database based on the
                 * cat_id and sub_cat_id and return an array like below:
                 *  [
                 *      'out'=>[
                 *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
                 *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
                 *       ],
                 *       'selected'=>'<prod-id-1>'
                 *  ]
                 */


                return Json::encode(['output'=> $response['out'], 'selected'=>'']);
                return;
            }
        }


        return Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionGetCountrySector() {


        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cat_id = empty($ids[0]) ? null : $ids[0];
            //$subcat_id = empty($ids[1]) ? null : $ids[1];
            if ($cat_id != null) {
                $data = Blocksector::find()->where(['project_id'=>$cat_id])->all();
                $result = array();
                foreach ($data as $dat)
                {
                    $res['id'] = $dat->id;
                    $res['name'] = $dat->blocksector;
                    $result[] = $res;


                }

                $response['out'] = $result;
                /**
                 * the getProdList function will query the database based on the
                 * cat_id and sub_cat_id and return an array like below:
                 *  [
                 *      'out'=>[
                 *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
                 *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
                 *       ],
                 *       'selected'=>'<prod-id-1>'
                 *  ]
                 */


                return Json::encode(['output'=> $response['out'], 'selected'=>'']);
                return;
            }
        }


        return Json::encode(['output'=>'', 'selected'=>'']);
    }

    public function actionGetStateCity() {


        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cat_id = empty($ids[0]) ? null : $ids[0];
            //$subcat_id = empty($ids[1]) ? null : $ids[1];
            if ($cat_id != null) {
                $data = City::find()->where(['state_id'=>$cat_id])->all();
                $result = array();
                foreach ($data as $dat)
                {
                    $res['id'] = $dat->id;
                    $res['name'] = $dat->city;
                    $result[] = $res;


                }

                $response['out'] = $result;
                /**
                 * the getProdList function will query the database based on the
                 * cat_id and sub_cat_id and return an array like below:
                 *  [
                 *      'out'=>[
                 *          ['id'=>'<prod-id-1>', 'name'=>'<prod-name1>'],
                 *          ['id'=>'<prod_id_2>', 'name'=>'<prod-name2>']
                 *       ],
                 *       'selected'=>'<prod-id-1>'
                 *  ]
                 */

                return Json::encode(['output'=> $response['out'], 'selected'=>'']);

            }
        }
        return Json::encode(['output'=>'', 'selected'=>'']);
    }



    public function actionSubmitNote()
    {

        $id = $_GET['id'];
        $comment = new Notes();
        if(isset($_GET['type'])=="task")
        {
            $comment->task_id = $id;
        }
        else
        {
            $comment->lead_id = $id;

        }
        $comment->comment = $_GET['message-text'];
        $comment->created_by = Yii::$app->user->id;
        $comment->created_on = date("Y-m-d H:i:s");
        $comment->save();
        if($comment->save())
        {
            $activity = new Activities();
            $activity->activity = Yii::$app->user->identity->username.' Posted New Note '.$_GET['message-text'];
            if(isset($_GET['type'])=="task")
            {
                $activity->task_id = $id;
            }
            else
            {
                $activity->lead_id = $id;

            }
            $activity->created_on = date("Y-m-d H:i:s");
            $activity->created_by = Yii::$app->user->id;
            $activity->save();
            return true;
        }else
        {
            echo "<pre>";
            print_r($comment);
            echo "</pre>";

        }

    }

    public function actionUpdateNote()
    {
        $id = $_GET['id'];
        $comment = Notes::findOne($id);
        $comment->comment = $_GET['message-text'];
        $comment->updated_by = Yii::$app->user->id;
        $comment->updated_on = date("Y-m-d H:i:s");
        $comment->save();
        if($comment->save())
        {
            $activity = new Activities();
            $activity->activity = Yii::$app->user->identity->username.' Update Note Reference: '.Helper::DatTim($comment->created_on).' '.$_GET['message-text'];
            if(isset($_GET['type'])=="task")
            {
                $activity->task_id = $comment->task_id;
            }
            else
            {
                $activity->lead_id = $comment->lead_id;

            }
            $activity->created_on = date("Y-m-d H:i:s");
            $activity->created_by = Yii::$app->user->id;
            $activity->save();
            return true;
        }else
        {
            echo "<pre>";
            print_r($comment);
            echo "</pre>";

        }

    }

    public function actionDeleteNote()
    {
        $id = $_GET['id'];

        $comment = Notes::findOne($id);
        $activity = new Activities();
        $activity->activity = Yii::$app->user->identity->username.' Delete Note (Detail): '.$comment->comment.' '.Helper::DatTim($comment->created_on);
        if(isset($_GET['type'])=="task")
        {
            $activity->task_id = $comment->task_id;
        }
        else
        {
            $activity->lead_id = $comment->lead_id;;

        }

        $activity->created_on = date("Y-m-d H:i:s");
        $activity->created_by = Yii::$app->user->id;
        $activity->save();


        $comment->delete();

        return true;


    }


    public function actionGetNote()
    {
        $id = $_GET['id'];
        if(isset($_GET['type'])=="task")
        {
            $comments = Notes::find()->where(['task_id'=>$id])->all();
        }
        else
        {
            $comments = Notes::find()->where(['lead_id'=>$id])->all();

        }


        return $this->renderAjax('show-notes', [
            'comments' => $comments,
        ]);

    }



    public function actionGetEmailTemplate()
    {
        $id = $_GET['id'];
        $template = EmailTemplate::findOne($_GET['id']);
        if($template)
        {
            $resp['id'] = $id;
            $resp['subject'] = $template->template_subject;
            $resp['body'] = $template->template_body;
            $resp['status'] = 1;

            echo json_encode($resp);

        }else
        {
            $resp['status'] = 0;
            echo json_encode($resp);
        }

    }





    public function actionAttachFiles()
    {
        return $this->renderAjax('attach-file');
    }

    public function actionUpdatePropertyStatus()
    {

        if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');
            $model = \app\models\Property::findOne($Id);
            $propertystatus=$model->status;
            if ($model->load($_POST)) {

                $indx = $_POST['editableIndex'];

                $model->status = $_POST['Property'][$indx]['status'];

                $value = $model->status;
                $model->save();

                //code for notification base alert system=============================
                 $options = array(
                        'cluster' => 'ap2',
                        'useTLS' => true
                      );
                      $pusher = new Pusher(
                        '476f5e28cd574a378338',
                        '1f62f95d523fe98a5099',
                        '660623',
                        $options
                      );
                            
                            $created_by=Yii::$app->user->id;
                            $user=User::find()->select('first_name,last_name')->where(['id'=>$created_by])->one();
                            $userfullname=$user->first_name." ".$user->last_name;
    
                            $message="Property ".$model->property_title." Status change from ".$propertystatus." to ".$model->status." by ".$userfullname."";
                            $url=''.Yii::$app->homeUrl.'property/view?id='.$model->id.'';
                            $created_on=date('Y-m-d g:i:s A');
                          
                            $myjson= json_encode(array($url,$message,$created_on));

                      //$arr['message'] = 'hello world';

                      $pusher->trigger('my-channel', 'my-event', $myjson);
                      
                      //saving notification on notification table=========
                            
                            $user=User::find()->all();
                            foreach ($user as  $value1) {
                           
                            $notification=new Notification();
                            $notification->message=$message;
                            $notification->created_on=date('Y-m-d H:i:s');
                            $notification->created_by=$created_by;
                            $notification->user_id=$value1->id;
                            $notification->property_id=$model->id;
                            $notification->save();
                            }


                            //==============================end notification============
                      //End of notification code============================================================
                    $activity = new Activities();
                    $activity->activity = Yii::$app->user->identity->username. '  Change the Status from '.$propertystatus.'  to '.$model->status.' ';
                    $activity->created_on = date("Y-m-d H:i:s");
                    $activity->created_by = Yii::$app->user->id;
                    $activity->property_id = $model->id;
                    $activity->save();

                // return JSON encoded output in the below format
                return ['output'=>$value, 'message'=>''];
            }else {
                return ['output'=>'', 'message'=>''];
            }


        }
    }

	public function actionUpdateProjectStatus()
    {
        	if (Yii::$app->request->post('hasEditable')) {
				\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
				$Id = Yii::$app->request->post('editableKey');
				$model = \app\models\Projects::findOne($Id);
				$propertystatus=$model->active;
				if ($model->load($_POST)) {
	
					$indx = $_POST['editableIndex'];
	
					$model->active = $_POST['Projects'][$indx]['active'];
	
					$value = $model->active;
					$model->save();
	
					// return JSON encoded output in the below format
					return ['output'=>$value, 'message'=>''];
				}else {
					return ['output'=>'', 'message'=>''];
				}
			}
		
    }

	public function actionUploadFile()
	{
		
		if(isset($_POST["import"]))
		{
			
		
		 $file = $_FILES["file"]["tmp_name"];
		 if(!empty($file))
		 {
			 $file_open = fopen($file,"r");
			 if($_POST['upload_type']==1)
			 {
				  while(($csv = fgetcsv($file_open, 1000, ",")) !== false)
				  {
					if($csv[2]!='Name')
					{
						
						$result=City::find()->where(['city'=>$csv[4]])->one();
						$model = new Leads();
						$model->created_by = Yii::$app->user->id;
						$model->created_on = date("Y-m-d H:i:s");
						$model->active = 1;
						$model->contact_name =$csv[2];
						$model->contact_mobile=$csv[3];
						$model->contact_city = $result->id;
						$model->contact_state = $result->state_id;
						$model->contact_country = $result->country_id;
						$model->lead_type = $csv[7];
						$model->lead_status = $csv[10];
						$model->lead_source = $csv[8];
						$model->lead_response = 'Pending';
						$model->save();
					 }
				  }
			 }
			 else if($_POST['upload_type']==2)
			 {
				  while(($csv = fgetcsv($file_open, 1000, ",")) !== false)
				  {
						if($csv[9]!='form_name')
						{
							
							$result=City::find()->where(['city'=>$csv[16]])->one();
							$data = Projects::find()->where(['name'=>$csv[9]])->one();
							$model = new Leads();
							$model->created_by = Yii::$app->user->id;
							$model->created_on = date("Y-m-d H:i:s");
							$model->active = 1;
							$model->contact_name =$csv[14];
							$model->contact_mobile=$csv[15];
							$model->contact_city = $result->id;
							$model->contact_state = $result->state_id;
							$model->contact_country = $result->country_id;
							$model->lead_type = 'Purchase';
							$model->lead_status = 'New';
							$model->lead_source = 'FaceBook';
							$model->lead_response = 'Pending';
							$model->project_id = $data->id;
							$model->contact_email = $csv[13];
							$model->lead_description = $csv[12];
							$model->save();
						}
				  }
			 }
			 else if($_POST['upload_type']==3)
			 {
				  while(($csv = fgetcsv($file_open, 1000, ",")) !== false)
				  {
						if($csv[9]!='form_name')
						{
							
							$result=City::find()->where(['city'=>$csv[15]])->one();
							$data = Projects::find()->where(['name'=>$csv[9]])->one();
							$model = new Leads();
							$model->created_by = Yii::$app->user->id;
							$model->created_on = date("Y-m-d H:i:s");
							$model->active = 1;
							$model->contact_name =$csv[13];
							$model->contact_mobile=$csv[14];
							$model->contact_city = $result->id;
							$model->contact_state = $result->state_id;
							$model->contact_country = $result->country_id;
							$model->lead_type = 'Purchase';
							$model->lead_status = 'New';
							$model->lead_source = 'FaceBook';
							$model->lead_response = 'Pending';
							$model->project_id = $data->id;
							$model->contact_email = $csv[12];
							$model->customer_follow_date_time = $csv[16];
							$model->save();
						}
				  }
			 }
			 
			
		 }else
		 {
				yii::app()->user->setFlash('warning','Select The File First');
			 	//$this->redirect(['user/index']);
				return $this->redirect(['leads/index?type=upload']);
		 }
			return $this->redirect(['leads/index?type=upload']);
		}
	}
	
    public function actionGetTemplate()
    {
        $template = SmsTemplate::findOne($_GET['id']);
        echo $template->message;
    }

    public function actionSmsSend()
    {
        if(Yii::$app->request->post())
        {
            $numbers = explode(',', $_POST['number']);

            foreach ($numbers as $mobile_number)
            {
                $sendSms = Helper::sendSMS($mobile_number,$_POST['message']);
                if($sendSms==true)
                {
                    $smsHistory = new SmsHistory();
                    $smsHistory->number = $mobile_number;
                    $smsHistory->message = $_POST['message'];
                    $smsHistory->lead_id = $_POST['lead_id'];
                    $smsHistory->created_on = date("Y-m-d H:i:s");
                    $smsHistory->created_by = Yii::$app->user->id;
                    if($smsHistory->save()){
                        echo  "Message Send Successfully to $mobile_number <br>";

                    }else {
                        echo "<pre>";
                        print_r($smsHistory);
                        echo "</pre>";
                    }
                }else{

                    echo  "Message Send Failed to $mobile_number <br>";
                }
            }

        }
    }



    public function actionAssignLead()
    {
       //lead assign with ajax but page is not refresh as discussed with ahtesham
       /* if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');
            $model = \app\models\Leads::findOne($Id);
            $leadassign=$model->lead_assign;*/
        $id = $_GET['id'];
        
        $model = Leads::findOne($id);
        $model->lead_assign = explode(',', $model->lead_assign);
        if ($model->load(Yii::$app->request->post())) {
            $lead_assign ='';
           
               $leads = $_POST['Leads']['lead_assign'];

                $assign = implode(",",$leads);



            foreach($_POST['Leads']['lead_assign'] as $assignleads)
            {
                $lead_assign .= ','.$assignleads;
                $user = \app\models\User::findOne($assignleads);
                $user3 = \app\models\User::findOne(Yii::$app->user->id);
                $multipleleads=$_POST['myarray'];
                 if($multipleleads)
                 {
                    $myArray = explode(',', $multipleleads);
                    for($int=0;$int<count($myArray);$int++)
                    {
                       Yii::$app->db->createCommand()
                     ->update('leads', ['lead_assign' =>$assign ], 'id ='.$myArray[$int].'')
                     ->execute();
                      Yii::$app->db->createCommand()
                     ->update('leads', ['assign_by' =>Yii::$app->user->id], 'id ='.$myArray[$int].'')
                     ->execute();
                        /*$mymodel=Leads::findOne($myArray[$int]);
                        $mymodel->lead_assign=$_POST['Leads']['lead_assign'];
                        $mymodel->assign_by=Yii::$app->user->id;
                        $mymodel->save();*/
                        $sendActivity = Helper::setCommit($user3->first_name." Has Assigned The Lead To ".$user->first_name,'',$myArray[$int]);
                        $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
                         $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

                                          if($mymodel->project_id)
                            {
                                $project = Projects::findOne($mymodel->project_id);
                                $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                               
                            }

                             if($mymodel->property_id)
            {
                $property = \app\models\Property::findOne($mymodel->property_id);
                $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
               
            }
                        $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
                            $sms->message = str_replace("#ID",$myArray[$int],$sms->message);
                            $sms->message = str_replace("#Assigned_User_Name",$user3->first_name,$sms->message);
                            $sms->message = str_replace("#Client Name",$mymodel->contact_name,$sms->message);
                            $sms->message = str_replace("#Cell Number",$mymodel->contact_mobile,$sms->message);
                            $sms->message = str_replace("#Purpose",$mymodel->lead_type,$sms->message);
                            $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$myArray[$int];

                             $sendSms = Helper::sendSMS($user->phone_no, $sms->message);

                            if($sendSms)
                            {
                                $smsHistory = new SmsHistory();
                                $smsHistory->number = $user->phone_no;
                                $smsHistory->lead_id = $myArray[$int];
                                $smsHistory->message =  $sms->message;
                                $smsHistory->created_by = Yii::$app->user->id;
                                $smsHistory->created_on = date("Y-m-d H:i:s");
                                $smsHistory->save();

                            }
                         

       
                     }
                    return true;
                   }
                
             
            $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
            $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

            $user2 = \app\models\User::findOne(Yii::$app->user->id);
            
            if($model->project_id)
            {
                $project = Projects::findOne($model->project_id);
                $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                $emailresult->template_body = str_replace("#Project Name",$project->name,$emailresult->template_body);
                $emailresult->template_subject=str_replace("#Project_Name",$project->name,$emailresult->template_subject);
            }else
            {
                $sms->message = str_replace("#Project Name",'',$sms->message);
                 $emailresult->template_body = str_replace("#Project Name",'',$emailresult->template_body);
                 $emailresult->template_subject=str_replace("#Project_Name",'',$emailresult->template_subject);
            }

            if($model->property_id)
            {
                $property = \app\models\Property::findOne($model->property_id);
                $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
                $emailresult->template_body = str_replace("#Property name",$property->property_title,$emailresult->template_body);
            }
            else
            {
                $sms->message = str_replace("#Property name",'',$sms->message);
                $emailresult->template_body = str_replace("#Property name",'',$emailresult->template_body);
            }

            //send email to agent at the time of assign lead======================================================================
                $emailresult->template_body = str_replace("#Agent_name",$user->first_name,$emailresult->template_body);
                $emailresult->template_body = str_replace("#ID",$model->id,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Assigned_User_Name",$user2->first_name,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Client Name",$model->contact_name,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Cell Number",$model->contact_mobile,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Email",$model->contact_email,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Purpose",$model->lead_type,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Lead_Status",$model->lead_status,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Lead_Source",$model->lead_source,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Lead_assigned_Date",Date('Y-m-d'),$emailresult->template_body);
                $email=$user->email;
                
                $emailresult->template_subject = str_replace("#Lead_ID",$model->id,$emailresult->template_subject);
                $emailresult->template_subject = str_replace("#Agent_name",$user2->first_name,$emailresult->template_subject);
                

                $emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);
                if($emailsend)
                {
                    
                    $emailHistory = new EmailHistory();
                    $emailHistory->email = $email;
                    $emailHistory->lead_id = $model->id;
                    $emailHistory->message =$emailresult->template_body;
                    $emailHistory->created_by = Yii::$app->user->id;
                    $emailHistory->created_on = date("Y-m-d H:i:s");
                    $emailHistory->save();
                    $sendActivity = Helper::setCommit($user2->first_name." Has send Email to  ".$user->first_name,'',$model->id);

                }
//===========================end of email================================================================================


            //send sms to agent at the time of lead assign==========================================================================
                $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
                $sms->message = str_replace("#ID",$model->id,$sms->message);
                $sms->message = str_replace("#Assigned_User_Name",$user2->first_name,$sms->message);
                $sms->message = str_replace("#Client Name",$model->contact_name,$sms->message);
                $sms->message = str_replace("#Cell Number",$model->contact_mobile,$sms->message);
                $sms->message = str_replace("#Purpose",$model->lead_type,$sms->message);
                $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$model->id;
                $sendSms = Helper::sendSMS($user->phone_no, $sms->message);
                if($sendSms)
                {
                    $smsHistory = new SmsHistory();
                    $smsHistory->number = $user->phone_no;
                    $smsHistory->lead_id = $model->id;
                    $smsHistory->message =  $sms->message;
                    $smsHistory->created_by = Yii::$app->user->id;
                    $smsHistory->created_on = date("Y-m-d H:i:s");
                    $smsHistory->save();
    
                }
                $sendActivity = Helper::setCommit($user2->first_name." Has Assigned The Lead To ".$user->first_name,'',$model->id);

                
            }
            //echo $lead_assign;exit;
            if (!empty($lead_assign)) {
                $lead_assign = substr($lead_assign, 1);
            }
            
            Yii::$app->db->createCommand()
             ->update('leads', ['lead_assign' => $lead_assign], 'id ='.$id.'')
             ->execute();
              Yii::$app->db->createCommand()
             ->update('leads', ['assign_by' =>Yii::$app->user->id], 'id ='.$id.'')
             ->execute();
             return true;
        }

        return $this->renderAjax('assign_lead', [
            'model' => $model,
        ]);
    }

	public function actionAssignLeadSingle()
    {
       //lead assign with ajax but page is not refresh as discussed with ahtesham
       /* if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');
            $model = \app\models\Leads::findOne($Id);
            $leadassign=$model->lead_assign;*/
        $id = $_GET['id'];
        
        $model = Leads::findOne($id);
		//echo  $model->lead_assign = $_POST['Leads']['lead_assign'];exit;
		if ($model->load(Yii::$app->request->post())) {
			//foreach($_POST['Leads']['lead_assign'] as $key=>$assignleads)
			{
				$model->lead_assign = $_POST['Leads']['lead_assign'];
				$user = \app\models\User::findOne($model->lead_assign);
                $user3 = \app\models\User::findOne(Yii::$app->user->id);
              
                 $multipleleads=$_POST['myarray'];


                   if($multipleleads)
    				{
                    $myArray = explode(',', $multipleleads);
                     for($int=0;$int<count($myArray);$int++)
                     {
                       Yii::$app->db->createCommand()
                     ->update('leads', ['lead_assign' => $_POST['Leads']['lead_assign']], 'id ='.$myArray[$int].'')
                     ->execute();
                      Yii::$app->db->createCommand()
                     ->update('leads', ['assign_by' =>Yii::$app->user->id], 'id ='.$myArray[$int].'')
                     ->execute();
                        /*$mymodel=Leads::findOne($myArray[$int]);
                        $mymodel->lead_assign=$_POST['Leads']['lead_assign'];
                        $mymodel->assign_by=Yii::$app->user->id;
                        $mymodel->save();*/
                        $sendActivity = Helper::setCommit($user3->first_name." Has Assigned The Lead To ".$user->first_name,'',$myArray[$int]);
                        $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
                         $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

                                          if($mymodel->project_id)
                            {
                                $project = Projects::findOne($mymodel->project_id);
                                $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                               
                            }

                            if($mymodel->property_id)
							{
								$property = \app\models\Property::findOne($mymodel->property_id);
								$sms->message = str_replace("#Property name",$property->property_title,$sms->message);
							   
							}
                        $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
                            $sms->message = str_replace("#ID",$myArray[$int],$sms->message);
                            $sms->message = str_replace("#Assigned_User_Name",$user3->first_name,$sms->message);
                            $sms->message = str_replace("#Client Name",$mymodel->contact_name,$sms->message);
                            $sms->message = str_replace("#Cell Number",$mymodel->contact_mobile,$sms->message);
                            $sms->message = str_replace("#Purpose",$mymodel->lead_type,$sms->message);
                            $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$myArray[$int];

                             $sendSms = Helper::sendSMS($user->phone_no, $sms->message);

                            if($sendSms)
                            {
                                $smsHistory = new SmsHistory();
                                $smsHistory->number = $user->phone_no;
                                $smsHistory->lead_id = $myArray[$int];
                                $smsHistory->message =  $sms->message;
                                $smsHistory->created_by = Yii::$app->user->id;
                                $smsHistory->created_on = date("Y-m-d H:i:s");
                                $smsHistory->save();

                            }
                         

       
                     }

                     return true;
                   }
				
             
            $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
            $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

            $user2 = \app\models\User::findOne(Yii::$app->user->id);
            
            if($model->project_id)
            {
                $project = Projects::findOne($model->project_id);
                $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                $emailresult->template_body = str_replace("#Project Name",$project->name,$emailresult->template_body);
                $emailresult->template_subject=str_replace("#Project_Name",$project->name,$emailresult->template_subject);
            }else
            {
                $sms->message = str_replace("#Project Name",'',$sms->message);
                 $emailresult->template_body = str_replace("#Project Name",'',$emailresult->template_body);
                 $emailresult->template_subject=str_replace("#Project_Name",'',$emailresult->template_subject);
            }

            if($model->property_id)
            {
                $property = \app\models\Property::findOne($model->property_id);
                $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
                $emailresult->template_body = str_replace("#Property name",$property->property_title,$emailresult->template_body);
            }
            else
            {
                $sms->message = str_replace("#Property name",'',$sms->message);
                $emailresult->template_body = str_replace("#Property name",'',$emailresult->template_body);
            }

			//send email to agent at the time of assign lead======================================================================
				$emailresult->template_body = str_replace("#Agent_name",$user->first_name,$emailresult->template_body);
				$emailresult->template_body = str_replace("#ID",$model->id,$emailresult->template_body);
				$emailresult->template_body = str_replace("#Assigned_User_Name",$user2->first_name,$emailresult->template_body);
				$emailresult->template_body = str_replace("#Client Name",$model->contact_name,$emailresult->template_body);
				$emailresult->template_body = str_replace("#Cell Number",$model->contact_mobile,$emailresult->template_body);
				$emailresult->template_body = str_replace("#Email",$model->contact_email,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Purpose",$model->lead_type,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Lead_Status",$model->lead_status,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Lead_Source",$model->lead_source,$emailresult->template_body);
				$emailresult->template_body= str_replace("#Lead_assigned_Date",Date('Y-m-d'),$emailresult->template_body);
				$email=$user->email;
				
				$emailresult->template_subject = str_replace("#Lead_ID",$model->id,$emailresult->template_subject);
				$emailresult->template_subject = str_replace("#Agent_name",$user2->first_name,$emailresult->template_subject);
                

                $emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);
                if($emailsend)
                {
                    
                    $emailHistory = new EmailHistory();
                    $emailHistory->email = $email;
                    $emailHistory->lead_id = $model->id;
                    $emailHistory->message =$emailresult->template_body;
                    $emailHistory->created_by = Yii::$app->user->id;
                    $emailHistory->created_on = date("Y-m-d H:i:s");
                    $emailHistory->save();
                    $sendActivity = Helper::setCommit($user2->first_name." Has send Email to  ".$user->first_name,'',$model->id);

                }
//===========================end of email================================================================================


			//send sms to agent at the time of lead assign==========================================================================
				$sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
				$sms->message = str_replace("#ID",$model->id,$sms->message);
				$sms->message = str_replace("#Assigned_User_Name",$user2->first_name,$sms->message);
				$sms->message = str_replace("#Client Name",$model->contact_name,$sms->message);
				$sms->message = str_replace("#Cell Number",$model->contact_mobile,$sms->message);
				$sms->message = str_replace("#Purpose",$model->lead_type,$sms->message);
				$sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$model->id;
				$sendSms = Helper::sendSMS($user->phone_no, $sms->message);
				if($sendSms)
				{
					$smsHistory = new SmsHistory();
					$smsHistory->number = $user->phone_no;
					$smsHistory->lead_id = $model->id;
					$smsHistory->message =  $sms->message;
					$smsHistory->created_by = Yii::$app->user->id;
					$smsHistory->created_on = date("Y-m-d H:i:s");
					$smsHistory->save();
	
				}
            	$sendActivity = Helper::setCommit($user2->first_name." Has Assigned The Lead To ".$user->first_name,'',$model->id);

            	
       		}
			//echo $lead_assign;exit;
			if (!empty($lead_assign)) {
            	$lead_assign = substr($lead_assign, 1);
			}
			
			Yii::$app->db->createCommand()
             ->update('leads', ['lead_assign' => $_POST['Leads']['lead_assign']], 'id ='.$id.'')
             ->execute();
              Yii::$app->db->createCommand()
             ->update('leads', ['assign_by' =>Yii::$app->user->id], 'id ='.$id.'')
             ->execute();
			 return true;
		}

        return $this->renderAjax('assign_lead_single', [
            'model' => $model,
        ]);
    }

    public function actionAssignUser()
    {
       //lead assign with ajax but page is not refresh as discussed with ahtesham
       /* if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');
            $model = \app\models\Leads::findOne($Id);
            $leadassign=$model->lead_assign;*/
        $id = $_GET['id']; 
        
        $model = User::findOne($id);
        $model->lead_assign = explode(',', $model->lead_assign);
        if ($model->load(Yii::$app->request->post())) {
            $lead_assign ='';

            foreach($_POST['User']['lead_assign'] as $key=>$assignleads)
            {
                $lead_assign .= ','.$assignleads;
                $user = \app\models\User::findOne($assignleads);
                $user3 = \app\models\User::findOne(Yii::$app->user->id);
                $multipleleads=$_POST['myarray'];
                 if($multipleleads)
                 {
                    $myArray = explode(',', $multipleleads);
                    for($int=0;$int<count($myArray);$int++)
                    {
                       Yii::$app->db->createCommand()
                     ->update('user', ['lead_assign' => $_POST['User']['lead_assign']], 'id ='.$myArray[$int].'')
                     ->execute();
                      Yii::$app->db->createCommand()
                     ->update('user', ['assign_by' =>Yii::$app->user->id], 'id ='.$myArray[$int].'')
                     ->execute();
                        /*$mymodel=Leads::findOne($myArray[$int]);
                        $mymodel->lead_assign=$_POST['Leads']['lead_assign'];
                        $mymodel->assign_by=Yii::$app->user->id;
                        $mymodel->save();*/
                        // $sendActivity = Helper::setCommit($user3->first_name." Has Assigned The Lead To ".$user->first_name,'',$myArray[$int]);
                        // $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
                        //  $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

            //                               if($mymodel->project_id)
            //                 {
            //                     $project = Projects::findOne($mymodel->project_id);
            //                     $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                               
            //                 }

            //                  if($mymodel->property_id)
            // {
            //     $property = \app\models\Property::findOne($mymodel->property_id);
            //     $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
               
            // }
            //             $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
            //                 $sms->message = str_replace("#ID",$myArray[$int],$sms->message);
            //                 $sms->message = str_replace("#Assigned_User_Name",$user3->first_name,$sms->message);
            //                 $sms->message = str_replace("#Client Name",$mymodel->contact_name,$sms->message);
            //                 $sms->message = str_replace("#Cell Number",$mymodel->contact_mobile,$sms->message);
            //                 $sms->message = str_replace("#Purpose",$mymodel->lead_type,$sms->message);
            //                 $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$myArray[$int];

            //                  $sendSms = Helper::sendSMS($user->phone_no, $sms->message);

            //                 if($sendSms)
            //                 {
            //                     $smsHistory = new SmsHistory();
            //                     $smsHistory->number = $user->phone_no;
            //                     $smsHistory->lead_id = $myArray[$int];
            //                     $smsHistory->message =  $sms->message;
            //                     $smsHistory->created_by = Yii::$app->user->id;
            //                     $smsHistory->created_on = date("Y-m-d H:i:s");
            //                     $smsHistory->save();

            //                 }
                         

       
                     }
                    return true;
                   }
                
             
//             $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
//             $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

            // $user2 = \app\models\User::findOne(Yii::$app->user->id);
            
//             if($mymodel->project_id)
//             {
//                 $project = Projects::findOne($mymodel->project_id);
//                 $sms->message = str_replace("#Project Name",$project->name,$sms->message);
//                 $emailresult->template_body = str_replace("#Project Name",$project->name,$emailresult->template_body);
//                 $emailresult->template_subject=str_replace("#Project_Name",$project->name,$emailresult->template_subject);
//             }else
//             {
//                 $sms->message = str_replace("#Project Name",'',$sms->message);
//                  $emailresult->template_body = str_replace("#Project Name",'',$emailresult->template_body);
//                  $emailresult->template_subject=str_replace("#Project_Name",'',$emailresult->template_subject);
//             }

//             if($mymodel->property_id)
//             {
//                 $property = \app\models\Property::findOne($mymodel->property_id);
//                 $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
//                 $emailresult->template_body = str_replace("#Property name",$property->property_title,$emailresult->template_body);
//             }
//             else
//             {
//                 $sms->message = str_replace("#Property name",'',$sms->message);
//                 $emailresult->template_body = str_replace("#Property name",'',$emailresult->template_body);
//             }

//             //send email to agent at the time of assign lead======================================================================
//                 $emailresult->template_body = str_replace("#Agent_name",$user->first_name,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#ID",$model->id,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Assigned_User_Name",$user2->first_name,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Client Name",$mymodel->contact_name,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Cell Number",$mymodel->contact_mobile,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Email",$mymodel->contact_email,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Purpose",$mymodel->lead_type,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Lead_Status",$mymodel->lead_status,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Lead_Source",$mymodel->lead_source,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Lead_assigned_Date",Date('Y-m-d'),$emailresult->template_body);
//                 $email=$user->email;
                
//                 $emailresult->template_subject = str_replace("#Lead_ID",$model->id,$emailresult->template_subject);
//                 $emailresult->template_subject = str_replace("#Agent_name",$user2->first_name,$emailresult->template_subject);
                

//                 $emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);
//                 if($emailsend)
//                 {
                    
//                     $emailHistory = new EmailHistory();
//                     $emailHistory->email = $email;
//                     $emailHistory->lead_id = $model->id;
//                     $emailHistory->message =$emailresult->template_body;
//                     $emailHistory->created_by = Yii::$app->user->id;
//                     $emailHistory->created_on = date("Y-m-d H:i:s");
//                     $emailHistory->save();
//                     $sendActivity = Helper::setCommit($user2->first_name." Has send Email to  ".$user->first_name,'',$model->id);

//                 }
// //===========================end of email================================================================================


//             //send sms to agent at the time of lead assign==========================================================================
//                 $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
//                 $sms->message = str_replace("#ID",$model->id,$sms->message);
//                 $sms->message = str_replace("#Assigned_User_Name",$user2->first_name,$sms->message);
//                 $sms->message = str_replace("#Client Name",$mymodel->contact_name,$sms->message);
//                 $sms->message = str_replace("#Cell Number",$mymodel->contact_mobile,$sms->message);
//                 $sms->message = str_replace("#Purpose",$mymodel->lead_type,$sms->message);
//                 $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$model->id;
//                 $sendSms = Helper::sendSMS($user->phone_no, $sms->message);
//                 if($sendSms)
//                 {
//                     $smsHistory = new SmsHistory();
//                     $smsHistory->number = $user->phone_no;
//                     $smsHistory->lead_id = $model->id;
//                     $smsHistory->message =  $sms->message;
//                     $smsHistory->created_by = Yii::$app->user->id;
//                     $smsHistory->created_on = date("Y-m-d H:i:s");
//                     $smsHistory->save();
    
//                 }
               // $sendActivity = Helper::setCommit($user2->first_name." Has Assigned The User ".$user->first_name." to Teamleader ".$model->id);


                
            } 
            //echo $lead_assign;exit;
            if (!empty($lead_assign)) {
                $lead_assign = substr($lead_assign, 1);
            }
            
            Yii::$app->db->createCommand()
             ->update('User', ['lead_assign' => $lead_assign], 'id ='.$id.'')
             ->execute();
              Yii::$app->db->createCommand()
             ->update('User', ['assign_by' =>Yii::$app->user->id], 'id ='.$id.'')
             ->execute();
             return true;
        }

        return $this->renderAjax('assign_user', [
            'model' => $model,
        ]);
    }

     public function actionViewUser()
    {
       //lead assign with ajax but page is not refresh as discussed with ahtesham
       /* if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');
            $model = \app\models\Leads::findOne($Id);
            $leadassign=$model->lead_assign;*/
       $id = $_GET['id'];
        
        $model = Leads::findOne($id);
        $model->lead_assign = explode(',', $model->lead_assign);
        if ($model->load(Yii::$app->request->post())) {
            $lead_assign ='';
            $leads = $_POST['Leads']['lead_assign'];

                $assign = implode(",",$leads);

            foreach($_POST['Leads']['lead_assign'] as $key=>$assignleads)
            {
                $lead_assign .= ','.$assignleads;
                $user = \app\models\User::findOne($assignleads);
                $user3 = \app\models\User::findOne(Yii::$app->user->id);
                $multipleleads=$_POST['myarray'];
                 if($multipleleads)
                 {
                    $myArray = explode(',', $multipleleads);
                    for($int=0;$int<count($myArray);$int++)
                    {
                       Yii::$app->db->createCommand()
                     ->update('leads', ['lead_assign' => $assign], 'id ='.$myArray[$int].'')
                     ->execute();
                      Yii::$app->db->createCommand()
                     ->update('leads', ['assign_by' =>Yii::$app->user->id], 'id ='.$myArray[$int].'')
                     ->execute();
                        /*$mymodel=Leads::findOne($myArray[$int]);
                        $mymodel->lead_assign=$_POST['Leads']['lead_assign'];
                        $mymodel->assign_by=Yii::$app->user->id;
                        $mymodel->save();*/
                        $sendActivity = Helper::setCommit($user3->first_name." Has Assigned The Lead To ".$user->first_name,'',$myArray[$int]);
                        $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
                         $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

                                          if($mymodel->project_id)
                            {
                                $project = Projects::findOne($mymodel->project_id);
                                $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                               
                            }

                             if($mymodel->property_id)
            {
                $property = \app\models\Property::findOne($mymodel->property_id);
                $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
               
            }
                        $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
                            $sms->message = str_replace("#ID",$myArray[$int],$sms->message);
                            $sms->message = str_replace("#Assigned_User_Name",$user3->first_name,$sms->message);
                            $sms->message = str_replace("#Client Name",$mymodel->contact_name,$sms->message);
                            $sms->message = str_replace("#Cell Number",$mymodel->contact_mobile,$sms->message);
                            $sms->message = str_replace("#Purpose",$mymodel->lead_type,$sms->message);
                            $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$myArray[$int];

                             $sendSms = Helper::sendSMS($user->phone_no, $sms->message);

                            if($sendSms)
                            {
                                $smsHistory = new SmsHistory();
                                $smsHistory->number = $user->phone_no;
                                $smsHistory->lead_id = $myArray[$int];
                                $smsHistory->message =  $sms->message;
                                $smsHistory->created_by = Yii::$app->user->id;
                                $smsHistory->created_on = date("Y-m-d H:i:s");
                                $smsHistory->save();

                            }
                         

       
                     }
                    return true;
                   }
                
             
            $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
            $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

            $user2 = \app\models\User::findOne(Yii::$app->user->id);
            
            if($model->project_id)
            {
                $project = Projects::findOne($model->project_id);
                $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                $emailresult->template_body = str_replace("#Project Name",$project->name,$emailresult->template_body);
                $emailresult->template_subject=str_replace("#Project_Name",$project->name,$emailresult->template_subject);
            }else
            {
                $sms->message = str_replace("#Project Name",'',$sms->message);
                 $emailresult->template_body = str_replace("#Project Name",'',$emailresult->template_body);
                 $emailresult->template_subject=str_replace("#Project_Name",'',$emailresult->template_subject);
            }

            if($model->property_id)
            {
                $property = \app\models\Property::findOne($model->property_id);
                $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
                $emailresult->template_body = str_replace("#Property name",$property->property_title,$emailresult->template_body);
            }
            else
            {
                $sms->message = str_replace("#Property name",'',$sms->message);
                $emailresult->template_body = str_replace("#Property name",'',$emailresult->template_body);
            }

            //send email to agent at the time of assign lead======================================================================
                $emailresult->template_body = str_replace("#Agent_name",$user->first_name,$emailresult->template_body);
                $emailresult->template_body = str_replace("#ID",$model->id,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Assigned_User_Name",$user2->first_name,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Client Name",$model->contact_name,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Cell Number",$model->contact_mobile,$emailresult->template_body);
                $emailresult->template_body = str_replace("#Email",$model->contact_email,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Purpose",$model->lead_type,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Lead_Status",$model->lead_status,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Lead_Source",$model->lead_source,$emailresult->template_body);
                $emailresult->template_body= str_replace("#Lead_assigned_Date",Date('Y-m-d'),$emailresult->template_body);
                $email=$user->email;
                
                $emailresult->template_subject = str_replace("#Lead_ID",$model->id,$emailresult->template_subject);
                $emailresult->template_subject = str_replace("#Agent_name",$user2->first_name,$emailresult->template_subject);
                

                $emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);
                if($emailsend)
                {
                    
                    $emailHistory = new EmailHistory();
                    $emailHistory->email = $email;
                    $emailHistory->lead_id = $model->id;
                    $emailHistory->message =$emailresult->template_body;
                    $emailHistory->created_by = Yii::$app->user->id;
                    $emailHistory->created_on = date("Y-m-d H:i:s");
                    $emailHistory->save();
                    $sendActivity = Helper::setCommit($user2->first_name." Has send Email to  ".$user->first_name,'',$model->id);

                }
//===========================end of email================================================================================


            //send sms to agent at the time of lead assign==========================================================================
                $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
                $sms->message = str_replace("#ID",$model->id,$sms->message);
                $sms->message = str_replace("#Assigned_User_Name",$user2->first_name,$sms->message);
                $sms->message = str_replace("#Client Name",$model->contact_name,$sms->message);
                $sms->message = str_replace("#Cell Number",$model->contact_mobile,$sms->message);
                $sms->message = str_replace("#Purpose",$model->lead_type,$sms->message);
                $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$model->id;
                $sendSms = Helper::sendSMS($user->phone_no, $sms->message);
                if($sendSms)
                {
                    $smsHistory = new SmsHistory();
                    $smsHistory->number = $user->phone_no;
                    $smsHistory->lead_id = $model->id;
                    $smsHistory->message =  $sms->message;
                    $smsHistory->created_by = Yii::$app->user->id;
                    $smsHistory->created_on = date("Y-m-d H:i:s");
                    $smsHistory->save();
    
                }
                $sendActivity = Helper::setCommit($user2->first_name." Has Assigned The Lead To ".$user->first_name,'',$model->id);

                
            }
            //echo $lead_assign;exit;
            if (!empty($lead_assign)) {
                $lead_assign = substr($lead_assign, 1);
            }
            
            $asad= Yii::$app->db->createCommand()
             ->update('leads', ['lead_assign' => $lead_assign], 'id ='.$id.'')
             ->execute();

              Yii::$app->db->createCommand()
             ->update('leads', ['assign_by' =>Yii::$app->user->id], 'id ='.$id.'')
             ->execute();
             return true;
        }

        return $this->renderAjax('view_user', [
            'model' => $model,
        ]);
    }
    public function actionAssignUserSingle()
    {
       //lead assign with ajax but page is not refresh as discussed with ahtesham
       /* if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');
            $model = \app\models\Leads::findOne($Id);
            $leadassign=$model->lead_assign;*/
        $id = $_GET['id'];
        
        $model = Leads::findOne($id);
        //echo  $model->lead_assign = $_POST['Leads']['lead_assign'];exit;
        if ($model->load(Yii::$app->request->post())) {
            //foreach($_POST['Leads']['lead_assign'] as $key=>$assignleads)
            {
                $model->lead_assign = $_POST['User']['lead_assign'];
                $user = \app\models\User::findOne($model->lead_assign);
                $user3 = \app\models\User::findOne(Yii::$app->user->id);
              
                 $multipleleads=$_POST['myarray'];


                   if($multipleleads)
                    {
                    $myArray = explode(',', $multipleleads);
                     for($int=0;$int<count($myArray);$int++)
                     {
                       Yii::$app->db->createCommand()
                     ->update('user', ['lead_assign' => $_POST['User']['lead_assign']], 'id ='.$myArray[$int].'')
                     ->execute();
                      Yii::$app->db->createCommand()
                     ->update('user', ['assign_by' =>Yii::$app->user->id], 'id ='.$myArray[$int].'')
                     ->execute();
                        /*$mymodel=Leads::findOne($myArray[$int]);
                        $mymodel->lead_assign=$_POST['Leads']['lead_assign'];
                        $mymodel->assign_by=Yii::$app->user->id;
                        $mymodel->save();*/
                        // $sendActivity = Helper::setCommit($user3->first_name." Has Assigned The Lead To ".$user->first_name,'',$myArray[$int]);
                        // $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
                        //  $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

                        //                   if($mymodel->project_id)
                        //     {
                        //         $project = Projects::findOne($mymodel->project_id);
                        //         $sms->message = str_replace("#Project Name",$project->name,$sms->message);
                               
                        //     }

                        //     if($mymodel->property_id)
                        //     {
                        //         $property = \app\models\Property::findOne($mymodel->property_id);
                        //         $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
                               
                        //     }
                        // $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
                        //     $sms->message = str_replace("#ID",$myArray[$int],$sms->message);
                        //     $sms->message = str_replace("#Assigned_User_Name",$user3->first_name,$sms->message);
                        //     $sms->message = str_replace("#Client Name",$mymodel->contact_name,$sms->message);
                        //     $sms->message = str_replace("#Cell Number",$mymodel->contact_mobile,$sms->message);
                        //     $sms->message = str_replace("#Purpose",$mymodel->lead_type,$sms->message);
                        //     $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$myArray[$int];

                        //      $sendSms = Helper::sendSMS($user->phone_no, $sms->message);

                        //     if($sendSms)
                        //     {
                        //         $smsHistory = new SmsHistory();
                        //         $smsHistory->number = $user->phone_no;
                        //         $smsHistory->lead_id = $myArray[$int];
                        //         $smsHistory->message =  $sms->message;
                        //         $smsHistory->created_by = Yii::$app->user->id;
                        //         $smsHistory->created_on = date("Y-m-d H:i:s");
                        //         $smsHistory->save();

                        //     }
                         

       
                     }

                     return true;
                   }
                
             
//             $sms = SmsTemplate::find()->where(['name'=>'Lead Assign SMS'])->one();
//             $emailresult=EmailTemplate::find()->where(['template_name'=>'Lead Assign Email'])->one();

        // $user2 = \app\models\User::findOne(Yii::$app->user->id);
            
//             if($mymodel->project_id)
//             {
//                 $project = Projects::findOne($mymodel->project_id);
//                 $sms->message = str_replace("#Project Name",$project->name,$sms->message);
//                 $emailresult->template_body = str_replace("#Project Name",$project->name,$emailresult->template_body);
//                 $emailresult->template_subject=str_replace("#Project_Name",$project->name,$emailresult->template_subject);
//             }else
//             {
//                 $sms->message = str_replace("#Project Name",'',$sms->message);
//                  $emailresult->template_body = str_replace("#Project Name",'',$emailresult->template_body);
//                  $emailresult->template_subject=str_replace("#Project_Name",'',$emailresult->template_subject);
//             }

//             if($mymodel->property_id)
//             {
//                 $property = \app\models\Property::findOne($mymodel->property_id);
//                 $sms->message = str_replace("#Property name",$property->property_title,$sms->message);
//                 $emailresult->template_body = str_replace("#Property name",$property->property_title,$emailresult->template_body);
//             }
//             else
//             {
//                 $sms->message = str_replace("#Property name",'',$sms->message);
//                 $emailresult->template_body = str_replace("#Property name",'',$emailresult->template_body);
//             }

//             //send email to agent at the time of assign lead======================================================================
//                 $emailresult->template_body = str_replace("#Agent_name",$user->first_name,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#ID",$model->id,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Assigned_User_Name",$user2->first_name,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Client Name",$mymodel->contact_name,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Cell Number",$mymodel->contact_mobile,$emailresult->template_body);
//                 $emailresult->template_body = str_replace("#Email",$mymodel->contact_email,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Purpose",$mymodel->lead_type,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Lead_Status",$mymodel->lead_status,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Lead_Source",$mymodel->lead_source,$emailresult->template_body);
//                 $emailresult->template_body= str_replace("#Lead_assigned_Date",Date('Y-m-d'),$emailresult->template_body);
//                 $email=$user->email;
                
//                 $emailresult->template_subject = str_replace("#Lead_ID",$model->id,$emailresult->template_subject);
//                 $emailresult->template_subject = str_replace("#Agent_name",$user2->first_name,$emailresult->template_subject);
                

//                 $emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);
//                 if($emailsend)
//                 {
                    
//                     $emailHistory = new EmailHistory();
//                     $emailHistory->email = $email;
//                     $emailHistory->lead_id = $model->id;
//                     $emailHistory->message =$emailresult->template_body;
//                     $emailHistory->created_by = Yii::$app->user->id;
//                     $emailHistory->created_on = date("Y-m-d H:i:s");
//                     $emailHistory->save();
//                     $sendActivity = Helper::setCommit($user2->first_name." Has send Email to  ".$user->first_name,'',$model->id);

//                 }
// //===========================end of email================================================================================


//             //send sms to agent at the time of lead assign==========================================================================
//                 $sms->message = str_replace("#Agent_name",$user->first_name,$sms->message);
//                 $sms->message = str_replace("#ID",$model->id,$sms->message);
//                 $sms->message = str_replace("#Assigned_User_Name",$user2->first_name,$sms->message);
//                 $sms->message = str_replace("#Client Name",$mymodel->contact_name,$sms->message);
//                 $sms->message = str_replace("#Cell Number",$mymodel->contact_mobile,$sms->message);
//                 $sms->message = str_replace("#Purpose",$mymodel->lead_type,$sms->message);
//                 $sms->message.="182.191.120.180:85/crm/web/leads/view?id=".$model->id;
//                 $sendSms = Helper::sendSMS($user->phone_no, $sms->message);
//                 if($sendSms)
//                 {
//                     $smsHistory = new SmsHistory();
//                     $smsHistory->number = $user->phone_no;
//                     $smsHistory->lead_id = $model->id;
//                     $smsHistory->message =  $sms->message;
//                     $smsHistory->created_by = Yii::$app->user->id;
//                     $smsHistory->created_on = date("Y-m-d H:i:s");
//                     $smsHistory->save();
    
//                 }
                // $sendActivity = Helper::setCommit($user2->first_name." Has Assigned The User ".$user->first_name." to Teamleader ".$model->id);

                
            }
            //echo $lead_assign;exit;
            if (!empty($lead_assign)) {
                $lead_assign = substr($lead_assign, 1);
            }
            
            Yii::$app->db->createCommand()
             ->update('user', ['lead_assign' => $_POST['Leads']['lead_assign']], 'id ='.$id.'')
             ->execute();
              Yii::$app->db->createCommand()
             ->update('user', ['assign_by' =>Yii::$app->user->id], 'id ='.$id.'')
             ->execute();
             return true;
        }

        return $this->renderAjax('assign_user_single', [
            'model' => $model,
        ]);
    }


    public function actionAssignClient()
    {
        $id = $_GET['id'];
        $model = Clients::findOne($id);

        $model->show_to = explode(',', $model->show_to);

        if ($model->load(Yii::$app->request->post())) {
            

            $model->show_to = implode(',', $_POST['Clients']['show_to']);

            if($model->save())
            {
                return true;
            }else
            {
                echo "<pre>";
                print_r($model);
                echo "</pre>";

            }

        }

        return $this->renderAjax('show-to', [
            'model' => $model,
        ]);
    }
	
	 public function actionAssignProjects()
     {
        $id = $_GET['id'];
        $model = Projects::findOne($id);

        $model->project_show = explode(',', $model->project_show);

        if ($model->load(Yii::$app->request->post())) {
            
            $model->project_show = implode(',', $_POST['Projects']['project_show']);
			$model->assign_by=Yii::$app->user->id;
            if($model->save())
            {
                return true;
            }else
            {
                echo "<pre>";
                print_r($model);
                echo "</pre>";

            }

        }

        return $this->renderAjax('show-projects', [
            'model' => $model,
        ]);
    }
	
	 public function actionAssignPropertyProjects()
     {
        $id = $_GET['id'];
        $model = Projects::findOne($id);

        $model->project_assign = explode(',', $model->project_assign);

        if ($model->load(Yii::$app->request->post())) {
            
            $model->project_assign = implode(',', $_POST['Projects']['project_assign']);
			$model->assign_by=Yii::$app->user->id;
            if($model->save())
            {
                return true;
            }else
            {
                echo "<pre>";
                print_r($model);
                echo "</pre>";

            }

        }

        return $this->renderAjax('show-property-projects', [
            'model' => $model,
        ]);
    }
	
	 public function actionAssignProperty()
     {
        $id = $_GET['id'];
        $model = Property::findOne($id);

        $model->property_assign = explode(',', $model->property_assign);

        if ($model->load(Yii::$app->request->post())) {
            $model->property_assign = implode(',', $_POST['Property']['property_assign']);
            if($model->save())
            {
                return true;
            }else
            {
                echo "<pre>";
                print_r($model);
                echo "</pre>";

            }

        }

        return $this->renderAjax('show-property', [
            'model' => $model,
        ]);
    }



    public function actionUpdateLeadStatus()
    {

        if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');

            /*$model = \app\models\Leads::findOne($Id);*/
            $model = Leads::find()
                    ->where(['id' => $Id])->one();
                    $oldstatus= $model->lead_status;
            /*if ($model->load($_POST)) {*/

                $indx = $_POST['editableIndex'];

                /*$model->*/$lead_status = $_POST['Leads'][$indx]['lead_status'];

              
                Yii::$app->db->createCommand()
             ->update('leads', ['lead_status' => $lead_status], 'id ='.$Id.'')
             ->execute();
                /*$model->save();*/
                $user=\app\models\User::find()->where(['id'=>Yii::$app->user->id])->one();
                $sendActivity = Helper::setCommit($user->first_name." Has change the lead status from ".$oldstatus." to ".$lead_status."",'',$Id);

                // return JSON encoded output in the below format
                return ['output'=>$value, 'message'=>''];
           /* }else {
                return ['output'=>'', 'message'=>''];
            }
*/

        }
    }

    public function actionUpdateLeadResponse()
    {

        if (Yii::$app->request->post('hasEditable')) {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $Id = Yii::$app->request->post('editableKey');
           
             $model = Leads::find()
                    ->where(['id' => $Id])->one();
                    $oldstatus= $model->lead_response;
            

                $indx = $_POST['editableIndex'];

               $lead_response = $_POST['Leads'][$indx]['lead_response'];

                Yii::$app->db->createCommand()
             ->update('leads', ['lead_response' => $lead_response], 'id ='.$Id.'')
             ->execute();

             
                 $user=\app\models\User::find()->where(['id'=>Yii::$app->user->id])->one();
                $sendActivity = Helper::setCommit($user->first_name." Has change the lead response from ".$oldstatus." to ".$lead_response."",'',$Id);

                // return JSON encoded output in the below format
                return ['output'=>$value, 'message'=>''];
           }


       
    }

    public function actionUpdateFollowUpStatus()
    {
            if (Yii::$app->request->post('hasEditable')) {
                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $Id = Yii::$app->request->post('editableKey');
                $model = \app\models\FollowUp::findOne($Id);
                $oldstatus=$model->status;
                if ($model->load($_POST)) {

                    $indx = $_POST['editableIndex'];

                    $model->status = $_POST['FollowUp'][$indx]['status'];
                    $model->status_updated_by = Yii::$app->user->id;
                    $model->status_updated_on = date("Y-m-d H:i:s");
                    $value = $model->status;
                    $model->save();
                    $user=\app\models\User::find()->where(['id'=>Yii::$app->user->id])->one();
                    $sendActivity = Helper::setCommit($user->first_name." Has change the Follow Up status from ".$oldstatus." to ".$value."",'',$_GET['leadid']);

                    // return JSON encoded output in the below format
                    return ['output'=>$value, 'message'=>''];
                }else {
                    return ['output'=>'', 'message'=>''];
                }


            }
        }


    public function actionGetBlockSector()
    {

                    $out = [];
                    if (isset($_POST['depdrop_parents'])) {
                        $ids = $_POST['depdrop_parents'];
                        $cat_id = empty($ids[0]) ? null : $ids[0];
                        //$subcat_id = empty($ids[1]) ? null : $ids[1];
                        if ($cat_id != null) {
                            $data = BlockSector::find()->where(['project_id'=>$cat_id])->all();
                            $result = array();
                            foreach ($data as $dat)
                            {
                                $res['id'] = $dat->id;
                                $res['name'] = $dat->blocksector;
                                $result[] = $res;


                            }

                            $response['out'] = $result;


                            return Json::encode(['output'=> $response['out'], 'selected'=>'']);

                        }
                    }
                    return Json::encode(['output'=>'', 'selected'=>'']);




        }

    public function actionGetProperties()
    {

        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $ids = $_POST['depdrop_parents'];
            $cat_id = empty($ids[0]) ? null : $ids[0];
            //$subcat_id = empty($ids[1]) ? null : $ids[1];
            if ($cat_id != null) {
                //$prop_cat = PropertyCategory::find()->where(['project_id'=>$cat_id])->all();
                $data = Property::find()->where(['project_id'=>$cat_id])->all();
                $result = array();
                foreach ($data as $dat)
                {
                    $res['id'] = $dat->id;
                    $res['name'] = $dat->blocksector;
                    $result[] = $res;

                }

                $response['out'] = $result;


                return Json::encode(['output'=> $response['out'], 'selected'=>'']);

            }
        }
        return Json::encode(['output'=>'', 'selected'=>'']);




    }





    public function actionGetDynamicForm()
    {
        $id = $_GET['id'];

        $form = FeatureList::find()->where(['feature_form_id'=>$id])->all();
        echo "<pre>";
        print_r($form);
        echo "</pre>";


        return $this->renderAjax('dynamic_form', [
            'form' => $form,
        ]);

        


    }

    public function actionGetPropertyStatus()
    {
        if ( ! is_null($_GET['date_range_1']) && strpos($_GET['date_range_1'], ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$_GET['date_range_1']);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:iA', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            /*$query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);*/

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result=Projects::find()
                ->joinWith(['property'])
                ->all();

            foreach($result as $key => $value)
            {

                $data[]=["name"=>$value->name,"Available"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Available'],
                    ['=','project_id',$value->id],
                    ['between', 'created_on', $start_date, $end_date]
                ])->count(),"Sold"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Sold'],
                    ['=','project_id',$value->id],
                    ['between', 'created_on', $start_date, $end_date]
                ])->count(),"Interested"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Occupied'],
                    ['=','project_id',$value->id],
                    ['between', 'created_on', $start_date, $end_date]
                ])->count()];



            }

        }
        else
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result=Projects::find()
                ->joinWith(['property'])
                ->all();

            foreach($result as $key => $value)
            {

                $data[]=["name"=>$value->name,"Available"=>Property::find()->where([
                    'and',
                    ['=','status','Available'],
                    ['=','project_id',$value->id]
                ])->count(),"Sold"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Sold'],
                    ['=','project_id',$value->id]
                ])->count(),"Occupied"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Occupied'],
                    ['=','project_id',$value->id]
                ])->count()];



            }


            $data[]=["name"=>'Other',"Available"=>Property::find()->where([
                'and',
                ['=','status','Available'],
                ['project_id'=>NULL]
            ])->count(),"Sold"=>(int)Property::find()->where([
                'and',
                ['=','status','Sold'],
                ['project_id'=>NULL]
            ])->count(),"Occupied"=>(int)Property::find()->where([
                'and',
                ['=','status','Occupied'],
                ['project_id'=>NULL]
            ])->count()];



        }

        return $data;
    }

   public function actionGetpropertyforuserdashboard()
   {

     if ( ! is_null($_GET['date_range_1']) && strpos($_GET['date_range_1'], ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ',$_GET['date_range_1']);
            $start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
            $start_date = $start_date->format('Y-m-d H:i:s');

            $end_date = DateTime::createFromFormat('d/m/Y h:iA', $end_date);
            $end_date = $end_date->format('Y-m-d H:i:s');
            /*$query->andFilterWhere(['between', 'created_on', $start_date, $end_date]);*/

            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result=Projects::find()
                ->joinWith(['property'])
                ->all();

            foreach($result as $key => $value)
            {

                $data[]=["name"=>$value->name,"Available"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Available'],
                    ['=','project_id',$value->id],
                    ['between', 'created_on', $start_date, $end_date]
                ])->count(),"Sold"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Sold'],
                    ['=','project_id',$value->id],
                    ['between', 'created_on', $start_date, $end_date]
                ])->count(),"Interested"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Occupied'],
                    ['=','project_id',$value->id],
                    ['between', 'created_on', $start_date, $end_date]
                ])->count()];



            }

        }
        else
        {
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            $result=Projects::find()
                ->joinWith(['property'])
                ->all();

            foreach($result as $key => $value)
            {

                $data[]=["name"=>$value->name,"Available"=>Property::find()->where([
                    'and',
                    ['=','status','Available'],
                    ['=','project_id',$value->id]
                ])->count(),"Sold"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Sold'],
                    ['=','project_id',$value->id]
                ])->count(),"Occupied"=>(int)Property::find()->where([
                    'and',
                    ['=','status','Occupied'],
                    ['=','project_id',$value->id]
                ])->count()];



            }


            $data[]=["name"=>'Other',"Available"=>Property::find()->where([
                'and',
                ['=','status','Available'],
                ['project_id'=>NULL]
            ])->count(),"Sold"=>(int)Property::find()->where([
                'and',
                ['=','status','Sold'],
                ['project_id'=>NULL]
            ])->count(),"Occupied"=>(int)Property::find()->where([
                'and',
                ['=','status','Occupied'],
                ['project_id'=>NULL]
            ])->count()];



        }

        return $data;

   }


    public function actionTest()
    {


    $server = "182.180.95.236";
    //ip address will work too i.e. 192.168.254.254 just make sure this is your public ip address not private as is the example

    //specify your username
    $username = "root";

    //select port to use for SSH
    $port = "7896";

    //command that will be run on server B
    $command = "uptime";

    //form full command with ssh and command, you will need to use links above for auto authentication help
    $cmd_string = "ssh -p ".$port." ".$username."@".$server." ".$command;

    //this will run the above command on server A (localhost of the php file)
    exec($cmd_string, $output);

    //return the output to the browser
    //This will output the uptime for server B on page on server A
    echo '<pre>';
    print_r($output);
    echo '</pre>';

    }



    public function actionLogin()
    {
        $model = new Cdr();
        $model->agent = $_GET['agent'];
        $model->created_on = date("Y-m-d H:i:s");
        $model->type = 'login';
        $model->save();
    }


    public function actionLogout()
    {
        $model = new Cdr();
        $model->agent = $_GET['agent'];
        $model->created_on = date("Y-m-d H:i:s");
        $model->type = 'logout';
        $model->save();
    }


    public function actionLoginLogout()
    {
        $model = new Cdr();
        $model->agent = $_GET['agent'];
        $model->created_on = date("Y-m-d H:i:s");
        $model->type = $_GET['type'];
        $model->save();
    }



    public function beforeAction($action)
    {
        $this->enableCsrfValidation = false;
        return parent::beforeAction($action);
    }

    public function actionEvents()
    {
         
        
    
        $query = Leads::find()->where(['active'=>1])->andWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,lead_assign)'))->addParams([':cat_to_find' => Yii::$app->user->id])->all();
                

           foreach ($query as  $value) {
               $followquery=\app\models\FollowUp::find()->where(['lead_id'=>$value->id])->all();
                  foreach($followquery as $value1)
                  {

                     
                     $date=$value1->follow_date_time;
                   $newDate = date("Y-m-d", strtotime($date));
                    $contactresult=Leads::find()->where(['id'=>$value1->lead_id])->one();
                     $user=\app\models\User::find()->where(['id'=>$contactresult->lead_assign])->one();
                     $username=$user->first_name." ".$user->last_name;
                        $stringdata="lead ID: ".$contactresult->id." Assign to: ".$username." Note: ".$value1->note." with: ".$contactresult->contact_name."";

                     
                  $data[]=[
                    'title'=>''.$value1->note.'',
                    'start'=>''.$newDate.'',
                    'description'=>''.$stringdata.'',
                    'url'=>''.Yii::$app->homeUrl.'leads/view?id='.$value1->lead_id.''
                  ];
                  }
                  
                  }
                  return json_encode($data);
               
                  
              /*    $int=0;
               foreach($followquery as $key => $value)
               {
                  $originalDate =$value[0]['follow_date_time'];
                    $contactresult=Leads::find()->where(['id'=>$value[0]['lead_id']])->one();
                     $user=\app\models\User::find()->where(['id'=>$contactresult->lead_assign])->one();
                     $username=$user->first_name." ".$user->last_name;
                        $stringdata="lead ID: ".$contactresult->id." Assign to: ".$username." Note: ".$value[$int]['note']." with: ".$contactresult->contact_name."";

                 $newDate = date("Y-m-d", strtotime($originalDate));
                  $data[]=[
                    'title'=>''.$value[$int]['note'].'',
                    'start'=>$newDate,
                    'description'=>''.$stringdata.''

                      ];

                  
                }
                
               
                return json_encode($data);*/

            }



            public function actionEventsmyregister()
            {
                $query = Leads::find()->where([
                    'and',
                    ['active'=>1],
                    ['created_by'=>Yii::$app->user->id]
                ])->all();
                  
                  foreach ($query as  $value) {
               $followquery=\app\models\FollowUp::find()->where(['lead_id'=>$value->id])->all();
                  foreach($followquery as $value1)
                  {

                     
                     $date=$value1->follow_date_time;
                   $newDate = date("Y-m-d", strtotime($date));
                    $contactresult=Leads::find()->where(['id'=>$value1->lead_id])->one();
                     $user=\app\models\User::find()->where(['id'=>$contactresult->lead_assign])->one();
                     $username=$user->first_name." ".$user->last_name;
                        $stringdata="lead ID: ".$contactresult->id." Assign to: ".$username." Note: ".$value1->note." with: ".$contactresult->contact_name."";

                     
                  $data[]=[
                    'title'=>''.$value1->note.'',
                    'start'=>''.$newDate.'',
                    'description'=>''.$stringdata.'',
                    'url'=>''.Yii::$app->homeUrl.'leads/view?id='.$value1->lead_id.''
                  ];
                  }
                  
                  }
                  return json_encode($data);
               
              
               
                return json_encode($data);
            }


            public function actionAllevents()
            {

                $query = Leads::find()->where(['active'=>1])->all();
                  
                  foreach ($query as  $value) {
               $followquery=\app\models\FollowUp::find()->where(['lead_id'=>$value->id])->all();
                  foreach($followquery as $value1)
                  {

                     
                     $date=$value1->follow_date_time;
                   $newDate = date("Y-m-d", strtotime($date));
                    $contactresult=Leads::find()->where(['id'=>$value1->lead_id])->one();
                     $user=\app\models\User::find()->where(['id'=>$contactresult->lead_assign])->one();
                     $username=$user->first_name." ".$user->last_name;
                        $stringdata="lead ID: ".$contactresult->id." Assign to: ".$username." Note: ".$value1->note." with: ".$contactresult->contact_name."";

                     
                  $data[]=[
                    'title'=>''.$value1->note.'',
                    'start'=>''.$newDate.'',
                    'description'=>''.$stringdata.'',
                    'url'=>''.Yii::$app->homeUrl.'leads/view?id='.$value1->lead_id.''
                  ];
                  }
                  
                  }
                  return json_encode($data);
               
              
               
               
            }

          
          public function actionContactdetails()
          {
            $id=$_GET['id'];
            $result=User::find()->select('phone_no')->where(['id'=>$id])->one();
            echo json_encode($result->phone_no);
          }
          
           public function actionDeletefollowup()
   {
      
       $id=$_GET['id'];
         $leadid=FollowUp::find()->select('lead_id,note')->where(['id'=>$id])->one();
          $activities = new Activities();
                $username=\app\models\User::find()->select('first_name')->where(['id'=>Yii::$app->user->id])->one();
                $activities->activity=$username->first_name.' delete the follow up on '.date("Y-m-d H:i:s").' '.$leadid->note.'';
                $activities->created_on=date("Y-m-d H:i:s");
                $activities->created_by=Yii::$app->user->id;
                $activities->lead_id=$leadid->lead_id;
                $activities->save();
        FollowUp::findOne($id)->delete();

       

   }

 

   public function actionSendclient()
   {
      $agent=$_POST['client'];
      $message=$_POST['message'];
      $number=$_POST['number'];
      $location=$_POST['city'];

   
     /*$agent=$_POST['agentid'];
     $location=$_POST['location'];
     $message=$_POST['message'];
     $number=$_POST['number'];*/
     if($agent=="all" && !empty($location))
     {
        $cityid=City::find()->where(['=','city',$location])->one();
        $result=Clients::find()->where(['city'=>$cityid->id])->all();
        foreach ($result as $value) {
            $sendSms = Helper::sendSMS($value->mobile,$message);

            

             if($sendSms)
            {
                $smsHistory = new SmsHistory();
                $smsHistory->number = $value->mobile;
                $smsHistory->client_id = $value->id;
                $smsHistory->message =  $message;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->city=$cityid;
                $smsHistory->save();

            }
        }

        return $this->redirect(['sms-template/smspanal']);
     } 
     elseif($agent=="all" && empty($location))
     {
         
        $result=Clients::find()->all();
       
        foreach ($result as $value) 
        {
             $sendSms = Helper::sendSMS($value->mobile,$message);

            

             if($sendSms)
            {
                $smsHistory = new SmsHistory();
                $smsHistory->number = $value->mobile;
                $smsHistory->client_id = $value->id;
                $smsHistory->message =  $message;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->save();

            }
            
        }

        return $this->redirect(['sms-template/smspanal']);
       
     }elseif($agent!="all" && !empty($location))
     {    
        
         $cityid=City::find()->where(['=','city',$location])->one();
        $result=Clients::find()->where(['and',['city'=>$cityid->id],['created_by'=>$agent]])->orWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,show_to)'))->addParams([':cat_to_find' =>$agent])->all();
         
        foreach ($result as $value) {
              $sendSms = Helper::sendSMS($value->mobile,$message);

            

             if($sendSms)
            {
                $smsHistory = new SmsHistory();
                $smsHistory->number = $value->mobile;
                $smsHistory->client_id = $value->id;
                $smsHistory->message =  $message;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->city=$cityid;
                $smsHistory->save();

            }
        }

        return $this->redirect(['sms-template/smspanal']);
        

     }elseif(!empty($agent) && $agent!="all" && empty($location))
     {
         
        $result=Clients::find()->where(['created_by'=>$agent])->orWhere(new \yii\db\Expression('FIND_IN_SET(:cat_to_find,show_to)'))->addParams([':cat_to_find' =>$agent])->all();
         
        foreach ($result as $value) {
              $sendSms = Helper::sendSMS($value->mobile,$message);

            

             if($sendSms)
            {
                
                $smsHistory = new SmsHistory();
                $smsHistory->number = $value->mobile;
                $smsHistory->client_id = $value->id;
                $smsHistory->message =  $message;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->save();

            }
        }

         return $this->redirect(['sms-template/smspanal']);
    
     }
 
    else{
            
              $numbers = explode(',',$number);
              foreach ($numbers as $mobilenumber) {
              
              $sendSms = Helper::sendSMS($mobilenumber,$message);

             if($sendSms)
            {
                $smsHistory = new SmsHistory();
                $smsHistory->number =$mobilenumber;
                /*$smsHistory->client_id =$value->id;*/
                $smsHistory->message =  $message;
                $smsHistory->created_by = Yii::$app->user->id;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->save();
                 

            }
      

           }
           return $this->redirect(['sms-template/smspanal']);
     }
   }
   

   public function actionGetpropertyleads()
   {
     $id=$_GET['id'];
     $createdby=$_GET['createdby'];

     $propertyleads=Leads::find()->select('id,contact_name,contact_mobile,lead_assign,lead_status,created_by')->where(['property_id'=>$id])->all();
    $username=User::find()->select('first_name,last_name,phone_no')->where(['id'=>$createdby])->one();
    $result.="";
     $result.="<h3 style='margin-top:-14px;'>Property Added By</h3>";
     $result.="<h6 style='color:#85929E'><b> Name: ".$username->first_name." ".$username->last_name.",  Contact#: ".$username->phone_no."</b></h6>";
     $result.="<table class='table table-striped'>
                <thead>
                <th>LeadID</th>
                <th>Mobile</th>
                <th>Lead Assign</th>
                <th>Lead Status</th>
                </thead>
                 ";
     foreach($propertyleads as $value)
     {
        $user=User::find()->select('first_name,phone_no')->where(['id'=>$value->lead_assign])->one();

        $result.='<tr><td>'.$value->id.'</td><td>'.$user->phone_no.'</td><td>'
        .$user->first_name.'</td><td>'.$value->lead_status.'</td></tr>';
     }
     echo $result;exit;
   }



   public function actionTodayfollowupsendsms()
   {

    $todayfollowup=FollowUp::find()->where([
        'and',
        ['LIKE','follow_date_time',Date('Y-m-d')],
        ['smsstatus'=>0]
    ])->all();
   
  
    foreach ($todayfollowup as  $value) 
    {    
         
         $leaddetails=Leads::find()->where(['id' => $value->lead_id])->one();
         $userdata=User::find()->where(['id'=>$leaddetails->lead_assign])->one();
       
        $user= $userdata->first_name." ".$userdata->last_name;
          $mobileno=$userdata->phone_no;
          $email=$userdata->email;
        
         /*echo $value->follow_date_time;*/
         $newTime = strtotime($value->follow_date_time);

         $time = $newTime - (3 * 60);
         
          /* $smstime=date('Y-m-d H:i:s', $time);

         $message="".$user." your follow up note:".$value->note." has been set on Date: ".$value->follow_date_time." with client ".$leaddetails->contact_name." http://125.209.89.206:85/crm/web/leads/view?id=".$value->lead_id."";
    
         $time = $newTime - (30 * 60);*/
         
         $smstime=date('Y-m-d H:i:s', $time);
         $sendtime=date("d/m/Y h:i A", strtotime($value->follow_date_time));
         $message="".$user." your follow up ".$value->note." has been set on Date ".$sendtime." with client ".$leaddetails->contact_name." mobile no:".$leaddetails->contact_mobile."http://182.191.120.180:85/crm/web/leads/view?id=".$value->lead_id."";

       
        if(Date('Y-m-d H:i:s') >= $smstime)
         {
            
            $sndsms=Helper::sendSms($mobileno,$message);
            if($sndsms)
            {
                
               echo $sndsms;echo" sms send Successfully";
               $model=FollowUp::find()->where(['id'=>$value->id])->one();
               $model->smsstatus=1;
               $model->save();
             
                $smsHistory = new SmsHistory();
                $smsHistory->number = $mobileno;
                $smsHistory->message =$message;
                $smsHistory->lead_id = $value->lead_id;
                
                $smsHistory->created_by = $value->created_by;
                $smsHistory->created_on = date("Y-m-d H:i:s");
                $smsHistory->save();

                $activity = new Activities();
                $activity->activity ="Your follow up note:".$value->note." message hs been send on Date ".$sendtime."";
                $activity->lead_id = $value->lead_id;
                $activity->created_on = date("Y-m-d H:i:s");
                $activity->created_by=$value->created_by;
                $activity->save();


                  //send email to agent according to followup
            $emailresult=EmailTemplate::find()->where(['template_name'=>'Followup Email'])->one();

           
           
           
            $emailresult->template_body = str_replace("#Agent_name",$user,$emailresult->template_body);
            $emailresult->template_body = str_replace("#follupnote",$value->note,$emailresult->template_body);
            $emailresult->template_body = str_replace("#DateTime",$value->follow_date_time,$emailresult->template_body);
             $emailresult->template_body = str_replace("#Client Name",$leaddetails->contact_name,$emailresult->template_body);
             $emailresult->template_body = str_replace("#Cell Number",$leaddetails->contact_mobile,$emailresult->template_body);
             $emailresult->template_body = str_replace("#Email",$leaddetails->contact_email,$emailresult->template_body);
             $emailresult->template_body = str_replace("#Lead_ID",$value->lead_id,$emailresult->template_body);
              
              

             $emailresult->template_subject = str_replace("#Agent_name",$user,$emailresult->template_subject);
              $emailresult->template_subject = str_replace("#Lead_ID",$value->lead_id,$emailresult->template_subject);
          
             $emailsend=Helper::sendEmail($email,$emailresult->template_body,$emailresult->template_subject);

            } 




          
            
         }




    }



    


   }
  

   public function actionGetclientdetail()
   {
    $mobileno=$_POST['mobileno'];
    $client=Clients::find()->where(['mobile'=>$mobileno])->one();
        
        $resp[]['name'] = $client->name;
        $resp[]['email'] = $client->email;
        $resp[]['state'] = $client->state;
        echo json_encode($resp);
    

   }


   public function actionAdddesc()
   {
    $id=$_POST['id'];
    $desc=$_POST['desc'];
    $model =Property::findOne($id);
    $model->statusdesc=$desc;
    $model->save();

   }

   public function actionGetpropertyactivities()
   {
    $id=$_GET['id'];
     $activities=activities::find()->where(['property_id'=>$id])->all();
    $username=User::find()->select('first_name,last_name,phone_no')->where(['id'=>$created_by])->one();

    $result.="";
     $result.="<table class='table table-striped'>
                <thead>
                <th>Property Name</th>
                <th>Message</th>
                <th>Created On</th>
                <th>Created By</th>
                </thead>
                <tbody>
                 ";
     foreach($activities as $activity)
     {
        $user=User::find()->select('first_name,last_name')->where(['id'=>$activity->created_by])->one();
        $propertydetails=Property::find()->where(['id'=>$id])->one();

        $result.='<tr><td>'.$propertydetails->property_title.'</td><td>'.$activity->activity.'</td><td style="width:100px">'
        .$activity->created_on.'</td><td>'.$user->first_name.' '.$user->last_name.'</td></tr>';
     }
     $result.='</tbody>';
     echo $result;exit;

   }



     public function actionGetleadactivities()
   {
    $id=$_GET['id'];
     $activities=activities::find()->where(['lead_id'=>$id])->all();
    $username=User::find()->select('first_name,last_name,phone_no')->where(['id'=>$created_by])->one();

    $result.="";
     $result.="<table class='table table-striped'>
                <thead>
                <th>Sr#</th>
                <th>Activity</th>
                <th>Created On</th>
                <th>Created By</th>
                </thead>
                <tbody>
                 ";
                 $int=1;
     foreach($activities as $activity)
     {
        $user=User::find()->select('first_name,last_name')->where(['id'=>$activity->created_by])->one();
        $propertydetails=Property::find()->where(['id'=>$id])->one();

        $result.='<tr><td>'.$int.'</td><td>'.$activity->activity.'</td><td style="width:100px">'
        .$activity->created_on.'</td><td>'.$user->first_name.' '.$user->last_name.'</td></tr>';
        $int++;
     }
     $result.='</tbody>';
     echo $result;exit;

   }


   public function actionGetdesc()
   {
     $id=$_POST['id'];
     $description=Property::find()->select('statusdesc,property_title')->where(['id'=>$id])->one();
        $desc=$description->statusdesc;
        $title=$description->property_title;
        echo json_encode(array($desc,$title));
     
   }

   

    public function actionGetcity()
    {
        $countryid=$_POST['countryid'];
        $result=City::find()->where(['=','country_id',$countryid])->all();
      
        $option.="";
        foreach($result as $value)
        {
           $option.="<option value='$value->city' id='$value->id'></option>";
        }
        echo $option;exit;
       
    }



    public function actionClientsmshistory()
    {

         $id=$_GET['id'];
     $smshistory=SmsHistory::find()->where(['client_id'=>$id])->all();
    $username=User::find()->select('first_name,last_name,phone_no')->where(['id'=>$created_by])->one();

    $result.="";
     $result.="<table class='table table-striped'>
                <thead>
                <th>Mobile No</th>
                <th>Message</th>
                <th>Created On</th>
                <th>Created By</th>
                </thead>
                <tbody>
                 ";
     foreach($smshistory as $smshistory)
     {
        $user=User::find()->select('first_name,last_name')->where(['id'=>$smshistory->created_by])->one();

        $result.='<tr><td>'.$smshistory->number.'</td><td>'.$smshistory->message.'</td><td style="width:100px">'
        .$smshistory->created_on.'</td><td>'.$user->first_name.' '.$user->last_name.'</td></tr>';
     }
     $result.='</tbody>';
     echo $result;exit;


    }


    public function actionNotificationstatus()
    {
        $id=Yii::$app->user->id;
        $notification=Notification::find()->where(['user_id'=>$id])->all();
        foreach ($notification as $notif) {
            $note=Notification::findOne($notif->id);
            $note->status=1;
            $note->save();

        }


    }



    //getting blocksector aginats project
    


}
