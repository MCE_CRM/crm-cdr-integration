<nav class="navbar navbar-static-top" role="navigation" id="notdashboard">
    <div class="navbar-header">
        <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <i class="fa fa-reorder"></i>
        </button>

         <?php
            $query = \app\modules\cdr\models\Setting::find()->orderBy('id DESC')->limit(1)->offset(0)->one();
         ?>

       <a href="#" class=""><img src="<?= Yii::$app->homeUrl ?>logoimages/<?=$query->logo?>" style = "width: 80px; height: 45px;"></a>
    </div>
    <div class="navbar-collapse collapse" id="navbar">

        <ul class="nav navbar-nav">

            <li class="">
                <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>cdr/site/index">Dashboard</a>
            </li>


            <li class="">
                <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>cdr/site/cdr">CDR</a>
            </li>

            <li class="">
                <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>cdr/site/incoming">Incoming</a>
            </li>

            <li class="">
                <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>cdr/site/outgoing">Outgoing</a>
            </li>

            <li class="">
                <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>cdr/site/agent-status">Agent Status</a>
            </li>

            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Reports<span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu" style="border:1px solid;">
                    <?php //if(Yii::$app->user->can('site/adminDashboard')){?>
						<li ><a href="<?= Yii::$app->homeUrl?>cdr/calls-statistics-incoming/index"> Incoming Statistics Report</a></li>
                        <li ><a href="<?= Yii::$app->homeUrl?>cdr/calls-statistics-outgoing/index">Outgoing Statistics Report</a></li>

                        <li ><a href="<?= Yii::$app->homeUrl?>cdr/agent-session">Attendance report</a></li>
                        <li ><a href="<?= Yii::$app->homeUrl?>cdr/site/attended-report">Answered report</a></li>
                        <li ><a href="<?= Yii::$app->homeUrl?>cdr/site/disconnected-report">Disconnected report</a></li>
                        <li ><a href="<?= Yii::$app->homeUrl?>cdr/site/all-agent-report">All Agent-Wise CDR report</a></li>
                        <li ><a href="<?= Yii::$app->homeUrl?>cdr/site/user-deshboard">Agent-Wise DeshBoard</a></li>
                        
                    <?php// } ?>
                </ul>
            </li>

            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown">Setting<span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu" style="border:1px solid;">
                    <?php if(Yii::$app->user->can('cdr/logo')){?>
                    <li ><a href="<?= Yii::$app->homeUrl?>setting/index">logo upload</a></li>
                    <?php } ?>


                    <?php if(Yii::$app->user->can('user/management')){?>
                        <li class="">
                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>user/index">Users Management</a>
                        </li>
                    <?php } ?>

                    <?php if(Yii::$app->user->can('user/management')){?>
                        <li class="">
                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>rbac">Role Based Access Control</a>
                        </li>

                        <!--<li class="">
                            <a aria-expanded="false" role="button" href="<?/*= Yii::$app->homeUrl*/?>user/roles">User Settings</a>
                        </li>-->
                    <?php } ?>

                </ul>
            </li>
        </ul>

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a aria-expanded="false" role="button" href="#" class="dropdown-toggle" data-toggle="dropdown"><?= Yii::$app->user->identity->first_name?><span class="caret"></span></a>
                <ul role="menu" class="dropdown-menu">
                    <?php if(Yii::$app->user->can('user/profile')){?>
                    <li><a href="<?= Yii::$app->homeUrl?>user/profile">Profile</a></li>
                    <?php } ?>
                    <li><a href="<?= Yii::$app->homeUrl?>site/logout">Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</nav>