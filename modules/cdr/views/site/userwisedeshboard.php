

<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">

</script>
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/dataviz.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/kelly.js"></script>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>




<?php

/* @var $this yii\web\View */



\app\modules\cdr\assets\DashboardAsset::register($this);
$this->title = 'UserWise Dashboard';
$total_csr = 0;
$total_agents = 0;
$user = \app\models\User::find()->all();
foreach ($user as $data)
{
    $role = getUserRoles($data->id);
    if($role ==='CSR')
    {
        $total_csr++;
    }
    if($role =="Agent")
    {
        $total_agents++;
    }
}

function getUserRoles($user_id){
    $connection = \Yii::$app->db;
    $sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
    $command=$connection->createCommand($sql);
    $dataReader=$command->queryAll();
    $roles ='';
    if(count($dataReader) > 0){
        foreach($dataReader as $role){
            $roles= $role['name'];
        }
    }
    return $roles;
}

?>

<style>
.datepicker
{
    margin-top: -33px;
}

.card-header
{
    text-align:center;
}
.ibox {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}
.ibox-title {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 2px 0 0;
    color: inherit;
    margin-bottom: 0;
    padding: 15px 15px 7px;
    min-height: 48px;
}


.ibox-title .label {
    float: left;
    margin-left: 4px;
}
.label, .ibox .label {
    font-size: 10px;
    border-radius: 10px;
}
.label-success, .badge-success {
    background-color: #1c84c6;
    color: #FFFFFF;
}
.label {
    background-color:#2baab1;
    color:white;
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: 600;
    padding: 0px 8px;
    text-shadow: none;
}

.ibox-title h5 {
    display: inline-block;
    font-size: 14px;
    margin: 0 0 7px;
    padding: 0;
    text-overflow: ellipsis;
    float: left;
    font-weight: 600;
}
.ibox-content {
    clear: both;
}

.ibox-content {
    background-color: #ffffff;
    color: inherit;
    padding: 15px 20px 20px 20px;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 0;
}
small, .small {
    font-size: 85%;
}
.no-margins {
    margin: 0 !important;
}
.stat-percent {
    float: right;
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
}
#myChart
{
    height:100% !important;
    width:100% !important;

}
.amcharts-chart-div a {display:none !important;}
a.dlink:hover{
    text-decoration: none;
}



#notdashboard{
    display: none;
}

.topnav {
  overflow: hidden;
 /* background-color: white;*/
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;

  display: inline-block;
    border-radius: 4px;
    font-size: 12px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    padding: 10px;
    text-transform: uppercase;
    white-space: initial;
}

.active {
  background-color: white;
  color: black;
}

.topnav .icon {
  display: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 17px;    
  border: none;
  outline: none;
  color: black;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;

  display: inline-block;
    border-radius: 4px;
    font-size: 12px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    padding: 10px;
    text-transform: uppercase;
    white-space: initial;
}

.dropdown-content {
  display: none;
  position: fixed;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 9999;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.topnav a:hover, .dropdown:hover .dropbtn {
  background-color: #555;
  color: white;
}

.dropdown-content a:hover {
  background-color: #ddd;
  color: black;
}

.dropdown:hover .dropdown-content {
  display: block;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child), .dropdown .dropbtn {
    display: none;
  }
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .topnav.responsive .dropdown {float: none;}
  .topnav.responsive .dropdown-content {position: relative;}
  .topnav.responsive .dropdown .dropbtn {
    display: block;
    width: 100%;
    text-align: left;
  }
}

.top-navigation .wrapper.wrapper-content {
    padding: 0px;
}
.widget-summary .summary .amount{
  font-weight: 100 !important;
}

.widget-summary .summary .title{
  font-weight: bold;
}

    .tab1{
        background-color: #d0cfcf;
        color: white;
    }

    .tr1{
       border: 1px solid white;
    }

    .th1{
        border-right: 1px solid white;
    }

    .td1{
        border-right: 1px solid white;
    }

.circular-bar {
    margin-bottom: 0px;
}

.circular-bar {
    margin: 0px 0;
}

.tab2{
    background-color: #d0cfcf;
    color: white;
    width: 100%;
}
.panel-primary
{
	width:100% !important;
}
</style>

  <?php
   $query = \app\models\Setting::find()->orderBy('id DESC')->limit(1)->offset(0)->one();
  
  ?>
<!--<script src="<?/*= Yii::$app->homeUrl*/?>porto/js/theme.js"></script>
<script src="<?/*= Yii::$app->homeUrl*/?>porto/js/theme.init.js"></script>-->
<div class="row" style=" width: 95%;">
    <div class="col-lg-12">
        <div class="logo" style="float: left;">
        <a href="#" class=""><img src="<?= Yii::$app->homeUrl ?>logoimages/<?=$query->logo?>" style = "width: 80px; height: 45px;"></a>
    </div>
<div class="topnav" id="myTopnav">

<a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/index">Dashboard</a>
 <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/cdr">CDR</a>

    <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/incoming">Incoming</a>
    <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/outgoing">Outgoing</a>
            
  <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/agent-status">Agent Status</a>

  <div class="dropdown">
    <button class="dropbtn">Reports 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
        <?php if(Yii::$app->user->can('site/adminDashboard')){?>
		<a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>calls-statistics-incoming/index">Incoming Statistics Report</a>
            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>calls-statistics-outgoing/index">Outgoing Statistics Report</a>

            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>agent-session">Agent Attendance Report</a>
        <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/attended-report">Answered report</a>
        <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/disconnected-report">Disconnected report</a>
        <li ><a href="<?= Yii::$app->homeUrl?>site/all-agent-report">All Agent-Wise CDR report</a></li>
        <li ><a href="<?= Yii::$app->homeUrl?>site/user-deshboard">Agent-Wise DeshBoard</a></li>
        
        <?php } ?>
    </div>
  </div>
    <?php if(Yii::$app->user->can('cdr/logo')){?>
  <div class="dropdown">
    <button class="dropbtn">Setting 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">

                    <a href="<?= Yii::$app->homeUrl?>setting/index">logo upload</a>


                    <?php if(Yii::$app->user->can('user/management')){?>

                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>user/index">Users Managnment</a>

                    <?php } ?>
                    <?php if(Yii::$app->user->can('user/management')){?>

                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>rbac">Role Based Access Control</a>

                    <?php } ?>
    </div>
  </div>
    <?php } ?>
 
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>

  <div class="dropdown pull-right" >
    <button class="dropbtn"><?= Yii::$app->user->identity->first_name?>
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
        <?php if(Yii::$app->user->can('user/profile')){?>
       <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>rbac">Profile</a>
        <?php } ?>
    <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/logout">Logout</a>
    </div>
  </div> 
</div>
</div>
</div>
<br><br><br>
<?php
			if(isset($_GET['CdrSearch']['calldate']) && !empty($_GET['CdrSearch']['calldate']))
			 {
				 	list($start_date, $end_date) = explode(' - ',$_GET['CdrSearch']['calldate']);
					$start_date = DateTime::createFromFormat('d/m/Y h:i A', $start_date);
					$start_date = $start_date->format('Y-m-d H:i:s');
		
					$end_date = DateTime::createFromFormat('d/m/Y h:i A', $end_date);
					$end_date = $end_date->format('Y-m-d H:i:s');
			 }else
			 {
				$start_date = date('Y-m-d', strtotime("-1 months", strtotime("NOW")));
				$end_date = date('Y-m-d H:i:s');
			}
?>
<h3 style="align-content: center; margin: 0 auto;" >UserWise Daily Statistics (<?=$start_date.'-'.$end_date?>)</h3>
	<?php if(Yii::$app->user->can('site/adminDashboard')){?>
    	<div class="panel panel-primary">
            <div class="panel-body" > <?php  echo $this->render('_searchuserwise', ['model' => $searchModel]); ?>
        </div>
      <?php
      }
      ?>
<br><br><br>
<div class="row">

    <div class="col-lg-6">
        <div class="row mb-3">
            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Calls</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-random"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> <br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalcalls;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </a>
                </section>
            </div>
            <div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Incoming</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <a class="dlink" href="">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-secondary">
                                        <i class="fa fa-reply"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><br></h4>
                                        <div class="info">
                                            <strong class="amount"><?php echo $totalincomming;?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>


        </div>

        <div class="row">
        	<div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Outgoing</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fa fa-mail-forward"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> <br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totaloutgoing;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>
            <div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Answer Call By Customer </div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fas fa-microphone"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"> <br></h4>
                                        <div class="info">
                                            <strong class="amount"><?=$agent_Answer_outgoing?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
			<div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">SIP Status:</div>
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <a class="dlink" href="">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-quaternary">
                                        <i class="fa fa-globe"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">

                                        <div class="info">
                                            <strong class="amount" style="font-size: 12px !important;"><?php echo '<h3><b>'.$sipstatus.'</b></h3>'."";echo "ip:".$sipip;?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
            <div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Internal Transfer</div>
                  <section class="card card-featured-left card-featured-tertiary mb-3">
                      <a class="dlink" href="" >
                          <div class="card-body">
                              <div class="widget-summary">
                                  <div class="widget-summary-col widget-summary-col-icon">
                                      <div class="summary-icon bg-primary">
                                          <i class="fa fa-circle-o-notch"></i>
                                      </div>
                                  </div>
                                  <div class="widget-summary-col">
                                      <div class="summary">
                                          <h4 class="title"> <br></h4>
                                          <div class="info">
                                              <strong class="amount"><?php echo $totaltransfer;?></strong>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                          </div>
                      </a>
                  </section>
            </div>

            
        </div>
    </div>
    <div class="col-lg-6">

        <div class="row mb-3">
			<div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Answer By Agent</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-circle-o-notch"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"> <br></h4>
                                        <div class="info">
                                            <strong class="amount"><?php
                                                echo $agent_Answer_incoming;
                                                ?></strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>
            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">No Answer/dropped Calls by Agent</div>
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-quaternary">
                                    <i class="fa fa-tty"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"><br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $agent_drop_incoming;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        </div>
        <div class="row">
            <div class="col-xl-6">
               
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">No Answer Calls by Customers</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fas fa-user-tie"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"><br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $agent_drop_outgoing;?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>

                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">User System/Computer IP:</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fa fa-laptop"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"> <br></h4>
                                        <div class="info">
                                            <p class=""><?php 
														$hostname = gethostbyaddr($_SERVER['REMOTE_ADDR']);
														$REMOTE_ADDR=$hostname;
														$ip= $REMOTE_ADDR;
														$ip = GetHostByName($ip);
											echo 'IP: '.$ip;?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            	</div>
            
        </div>
    </div>

</div>


<br><br><br>
<h3 style="align-content: center; margin: 0 auto;" >Statistics (<?=$start_date.'-'.$end_date?>)</h3>
<br><br><br>
<div class="row" style="width: 100%;">

    <div class="col-lg-12">
        <div class="row mb-3">



            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly incoming</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">

                    <div class="circular-bar">
                        <div id="incomingchartContainer" style="height: 200px; width: 90%;"></div>
                    </div>
                </section>
            </div>

            <div class="col-xl-4">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly outgoing Calls</div>
                <section class="card card-featured-left card-featured-secondary mb-3">

                    <div class="circular-bar">
                        <div id="outgoingchartContainer" style="height: 200px; width: 90%;"></div>

                    </div>

                </section>
            
            </div>

            
            <div class="col-xl-4">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Transfer Call</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div id="transferContainer" style="height: 200px; width: 90%;"></div>
                </section>
            </div>
            
        </div>
        <div class="row">

            
            <div class="col-xl-4">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Dropped Calls/Not Answered</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div id="noanswerContainer" style="height: 200px; width: 90%;"></div>
                </section>
            </div>

        </div>
    </div>

  </div>



   

<?php
$trans =  Array();

//echo print_r($data);
//return  json_encode($data);

?>

<script type="text/html" src="https://cdnjs.com/libraries/Chart.js"></script>
<script  src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script>
function myFunction() {

  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}





        //Incoming Calls Graph
am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_dataviz);
    am4core.useTheme(am4themes_animated);
// Themes end



// Create chart instance

    var chart = am4core.create("incomingchartContainer", am4charts.PieChart);

// Add data
    chart.data = [

        { "sector": "M", "size": <?= $totalincomming ?>},
        { "sector": "D", "size": <?= $totalincomming ?> },

    ];

// Add label
    chart.innerRadius = 100;
    var label = chart.seriesContainer.createChild(am4core.Label);
    label.text = <?php echo $totalincomming  ?>;
    label.horizontalCenter = "middle";
    label.verticalCenter = "middle";
    label.fontSize = 50;

// Add and configure Series

    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "size";
    pieSeries.dataFields.category = "sector";

// Animate chart data




}); // end am4core.ready()

am4core.ready(function() {

// Themes begin

    am4core.useTheme(am4themes_animated);
// Themes end



// Create chart instance

    var chart = am4core.create("outgoingchartContainer", am4charts.PieChart);

// Add data
    chart.data = [

        { "sector": "M", "size": "<?= $totaloutgoing ?>"},
        { "sector": "D", "size": "<?= $totaloutgoing?>" },

    ];

// Add label
    chart.innerRadius = 100;
    var label = chart.seriesContainer.createChild(am4core.Label);
    label.text = <?php echo $totaloutgoing ?>;
    label.horizontalCenter = "middle";
    label.verticalCenter = "middle";
    label.fontSize = 50;

// Add and configure Series

    var pieSeries = chart.series.push(new am4charts.PieSeries());
    pieSeries.dataFields.value = "size";
    pieSeries.dataFields.category = "sector";

// Animate chart data




}); // end am4core.ready()

//transfer graph


//dropped/no answers calls

am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
    var chart = am4core.create("noanswerContainer", am4charts.XYChart);
    //chart.scrollbarX = new am4core.Scrollbar();

// Add data
    chart.data = [{
        "country": "",
        "visits": "<?=$agent_drop_incoming+$agent_drop_outgoing?>",
    },
    ];

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

    categoryAxis.dataFields.category = "country";

    //categoryAxis.renderer.labels.template.rotation = 270;
    categoryAxis.tooltip.disabled = true;
    //categoryAxis.renderer.minHeight = 110;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    // valueAxis.renderer.minWidth = 50;

    // Create series
    var series = chart.series.push(new am4charts.ColumnSeries());

    series.dataFields.valueY = "visits";
    series.dataFields.categoryX = "country";
    series.columns.template.strokeWidth = 0;

    series.calculatePercent = true;

    series.label = 1;

    series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: [bold]{valueY.percent}%[/] ({valueY})";

    series.legendSettings.itemValueText = "{valueY.percent}%";


    labelBullet.label.text = "{valueY.percent}%";


    // on hover, make corner radiuses bigger


    series.columns.template.adapter.add("fill", function(fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
    });

    // Cursor


}); // end am4core.ready()


am4core.ready(function() {

// Themes begin
    am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
    var chart = am4core.create("transferContainer", am4charts.XYChart);
    //chart.scrollbarX = new am4core.Scrollbar();

// Add data
    chart.data = [{
        "country": "",
        "visits": "<?=$totaltransfer?>",
    },
    ];

    // Create axes
    var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());

    categoryAxis.dataFields.category = "country";

    //categoryAxis.renderer.labels.template.rotation = 270;
    categoryAxis.tooltip.disabled = true;
    //categoryAxis.renderer.minHeight = 110;

    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    // valueAxis.renderer.minWidth = 50;

    // Create series
    var series = chart.series.push(new am4charts.ColumnSeries());

    series.dataFields.valueY = "visits";
    series.dataFields.categoryX = "country";
    series.columns.template.strokeWidth = 0;

    series.calculatePercent = true;

    series.label = 1;

    series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: [bold]{valueY.percent}%[/] ({valueY})";

    series.legendSettings.itemValueText = "{valueY.percent}%";


    labelBullet.label.text = "{valueY.percent}%";


    // on hover, make corner radiuses bigger


    series.columns.template.adapter.add("fill", function(fill, target) {
        return chart.colors.getIndex(target.dataItem.index);
    });

    // Cursor


}); // end am4core.ready()



//end dropped/no answer calls



$(document.body).on("click",".btn-default",function(){
		window.location.href  = "<?php echo Yii::$app->homeUrl?>site/index";
		
	});
</script>

<!-- HTML -->









