<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\VoiceMail */

$this->title = 'Create Voice Mail';
$this->params['breadcrumbs'][] = ['label' => 'Voice Mails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="voice-mail-create">



    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
