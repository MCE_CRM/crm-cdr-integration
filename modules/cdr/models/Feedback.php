<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "feedback".
 *
 * @property int $id
 * @property string $land_time
 * @property int $feedback_id
 * @property string $created_at
 * @property string $agent
 * @property string $phone
 * @property string $caller_id
 */
class Feedback extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'feedback';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['land_time', 'created_at'], 'safe'],
            [['feedback_id'], 'integer'],
            [['agent'], 'string', 'max' => 256],
            [['phone', 'caller_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'land_time' => 'Land Time',
            'feedback_id' => 'Feedback ID',
            'created_at' => 'Created At',
            'agent' => 'Agent',
            'phone' => 'Phone',
            'caller_id' => 'Caller ID',
        ];
    }
}
