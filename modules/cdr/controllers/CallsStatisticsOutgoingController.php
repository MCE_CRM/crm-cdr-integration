<?php

namespace app\modules\cdr\controllers;

use Yii;
use app\modules\cdr\models\CallsStatisticsOutgoing;

use app\modules\cdr\models\CallsStatisticsOutgoingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CallsStatisticsOutgoingController implements the CRUD actions for CallsStatisticsOutgoing model.
 */
class CallsStatisticsOutgoingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all CallsStatisticsOutgoing models.
     * @return mixed
     */
    public function actionIndex()
    {

        CallsStatisticsOutgoing::generateRecord();
        $searchModel = new CallsStatisticsOutgoingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CallsStatisticsOutgoing model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CallsStatisticsOutgoing model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CallsStatisticsOutgoing();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CallsStatisticsOutgoing model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CallsStatisticsOutgoing model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CallsStatisticsOutgoing model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CallsStatisticsOutgoing the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CallsStatisticsOutgoing::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionGenerate() {
        // Earliest date from (cdr) table
        $callDate = Yii::$app->db->createCommand(
            "SELECT calldate FROM cdr 
            WHERE calldate != '0000-00-00 00:00:00' ORDER BY calldate ASC LIMIT 1"
        )->queryScalar();

        // Getting rid of the time.
        $callDate = substr($callDate, 0, 10);

        // Saving previous execution time.
        $executionTime = ini_get('max_execution_time');

        // to get unlimited php script execution time
        ini_set('max_execution_time', 0);

        // !!! This will take a long time to execute if there are a lot of records.
        $generatedRows = CallsStatisticsOutgoing::generateRows($callDate);

        // back to old php script execution time
        ini_set('max_execution_time', $executionTime);

        echo $generatedRows;
    }

    public function actionOutgoing() {
        // Generate records in (cdr_calls_statistics)
        CallsStatisticsOutgoing::generateRecord();

        $searchModel = new CallsStatisticsOutgoingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, 'outgoing');

        return $this->render('outgoing', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



}
