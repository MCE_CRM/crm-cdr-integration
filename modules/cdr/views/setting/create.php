 <?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Setting */

$this->params['breadcrumbs'][] = ['label' => 'Settings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="setting-create">
    <div class="logo">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
<style type="text/css">
	.logo{
		width: 45%;
		margin: 0 auto;
	}
</style>
</div>
