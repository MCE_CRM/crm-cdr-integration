<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\PropertyCategory */

$this->title = 'Create Property Location Type';
$this->params['breadcrumbs'][] = ['label' => 'Property Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="property-category-create">

<!--    <h1><?/*= Html::encode($this->title) */?></h1>
-->
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
