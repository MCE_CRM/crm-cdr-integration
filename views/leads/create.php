<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\leads */

$this->title = 'Add New Lead';
$this->params['breadcrumbs'][] = ['label' => 'Leads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="leads-create">

    <!--<h1><?/*= Html::encode($this->title) */?></h1>-->

    <?= $this->render('_form_new', [
        'model' => $model,
    ]) ?>

</div>
