<?php
/**
 * Created by PhpStorm.
 * User: Multiline
 * Date: 4/24/2019
 * Time: 11:32 AM
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\Select2;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;
use kartik\grid\GridView;
use yii\widgets\Pjax;



$daterange = '';
$from ='';
$to='';
$agent = 'All';
        if (!empty($_GET['FollowUpSearch']['created_by'])) {
            $agentid = $_GET['FollowUpSearch']['created_by'];
            $branch=  \app\models\User::findOne($agentid);
            $agent =  $branch->first_name;
        }
		if (!empty($_GET['FollowUpSearch']['from'])) {
			$fromdate = $_GET['FollowUpSearch']['from'];
			$from = DateTime::createFromFormat('d/m/Y',$fromdate)->format('Y-m-d');
			$daterange = date('d-m-Y',strtotime($from));
		}else
		{
			//$from = date('Y-m-d',strtotime('-15 days'));
			//$daterange = date('d-m-Y',strtotime($from));
		}
		if (!empty($_GET['FollowUpSearch']['to'])) {
			$todate = $_GET['FollowUpSearch']['to'];
			$to = DateTime::createFromFormat('d/m/Y',$todate)->format('Y-m-d');
			$daterange .= ' - '.date('d-m-Y',strtotime($to));
		} else
		{
			$to = date('Y-m-d');
			$daterange .= ' - '.date('d-m-Y',strtotime($to));
		}
		if (empty($_GET['FollowUpSearch']['from']) && empty($_GET['FollowUpSearch']['to'])) {
			//$from = date('Y-m-d',strtotime('-15 days')).' 00:00:00';
			//$to = date('Y-m-d').' 23:59:59';
			$daterange = '';//date('d-m-Y',strtotime($from)).' - '.date('d-m-Y',strtotime($to));
		}
	
?>

<div class="card-body">

    <div class="follow-up-search">

        <?php $form = ActiveForm::begin([
            'action' => ['follow-user-wise'],
            'method' => 'get',
        ]); ?>
        <div class="row">
            <?php  if(Yii::$app->user->can('user_search_on_follow_up_report')){?>
           <div class="col-md-3">

                <?php 


                 if(Yii::$app->user->can('teamlead')){
                 $team_lead=Yii::$app->user->identity->lead_assign;
                        $split = preg_split('/(?:"[^"]*"|)\K\s*(,\s*|$)/', $team_lead);
                        $result = array_filter($split);

                        echo $form->field($model, 'created_by')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\User::find()->where(
                                ['id'=>$result])->all(),'id','username'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder'  => 'Select Users'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ])->label('Lead Assign'); 
                }else{
               
                echo $form->field($model, 'created_by')->widget(Select2::classname(), [
                'data' =>  app\helpers\Helper::assignLead2(),
                'theme' => Select2::THEME_BOOTSTRAP,
                'options' => ['multiple' => false, 'placeholder' => 'Select Users'],
                'pluginOptions' => [
                'allowClear' => true,
                ],
                ])->label('Lead Assign'); 
            
                 }
                ?>

            </div>
            <?php } ?>
            
                <div class="col-md-4">
            	<?php
                       echo $form->field($model, 'from')->widget(DatePicker::classname(), [
						'options' => ['autocomplete' => 'off',],
						'readonly' => true,
						'pluginOptions' => [
							'autoclose'=>true,
							'format' => 'dd/mm/yyyy',
							//'startDate'=> date('d-m-Y H:i'),
						]
					]);
				  ?>
                </div>
                <div class="col-md-4">
                        <?php
                       echo $form->field($model, 'to')->widget(DatePicker::classname(), [
						'options' => ['autocomplete' => 'off',],
						'readonly' => true,
						'pluginOptions' => [
							'autoclose'=>true,
							'format' => 'dd/mm/yyyy',
							//'startDate'=> date('d-m-Y H:i'),
						]
					]);
				  ?>
                </div>
        </div>
        <div class="row">
                <div class="col-md-3">

                    <?= $form->field($model, 'status')->widget(Select2::classname(), [
                        'data' =>  ArrayHelper::map(\app\models\FollowUpStatus::find()->all(), 'status', 'status'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['multiple' => false, 'placeholder' => 'Select Status'],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                    ]); ?>

            </div>
        
           <?php /*?> <div class="col-md-3">
                <?php
                echo '<label class="control-label">Follow Up Date Range</label>';
                echo '</br>';
                echo '<div class="drp-container">';
                echo DateRangePicker::widget([
                    'model'=>$model,
                    'attribute'=>'follow_date_time',

                    'convertFormat'=>true,

                    'pluginOptions'=>[
                        'opens'=>'left',
                        'ranges' => [

                            "Today" => ["moment().startOf('day')", "moment()"],
                            "Yesterday" => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                            "Last 7 Days" => ["moment().startOf('day').subtract(6, 'days')", "moment()"],

                        ],

                        'timePicker'=>true,
                        'timePickerIncrement'=>05,
                        'locale'=>['format'=>'d/m/Y h:i A']
                    ],
                    'presetDropdown'=>false,
                    'hideInput'=>true
                ]);
                echo '</div>'; ?>
            </div><?php */?>
        </div>

            
				
				


        </div>

        <div class="form-group">
              <?= Html::label( 'Page Size', 'pagesize', array( 'style' => 'margin-left:10px; margin-top:8px;' ) ) ?>

            <?= Html::dropDownList(
    'pagesize', 
    ( isset($_GET['pagesize']) ? $_GET['pagesize'] : 20 ),  // set the default value for the dropdown list
    // set the key and value for the drop down list
    array( 
        20=>20,
        50 => 50, 
        100 => 100,
        200=>200,
        1000=>'All'
    ),
    // add the HTML attritutes for the dropdown list
    // I add pagesize as an id of dropdown list. later on, I will add into the Gridview widget.
    // so when the form is submitted, I can get the $_POST value in InvoiceSearch model.
    array( 
        'id' => 'pagesize', 
        'style' => 'margin-left:5px; margin-top:8px;'
        )
    ) 
?>



            <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton('Reset', ['class' => 'btn btn-default obaid']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

    <div style="margin-top:20px">


 <?php
 $this->title = 'Follow Up UserWise Report('.$daterange.')';
     if(Yii::$app->user->can('followup/download'))
     {
        $show='';
     }
     else
     {
        $show=false;
     }

    ?>



    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'responsiveWrap' => false,
        'tableOptions' => [
            'class' => 'table table-condensed table-sm',
        ],
        'beforeHeader' => [
            [
                'columns' => [
                    ['content' => $agent, 'options' => ['colspan' => 5, 'class' => 'text-center success']],
                    ['content' => $daterange, 'options' => ['colspan' => 5, 'class' => 'text-center success']],
                ],
            ]
        ],
          'toolbar' =>  [
                    '{export}',
                    '{toggleData}',
                ],
                'export' => [
                'fontAwesome' => true
                ],
                'exportConfig' => [
                  
                    GridView::EXCEL => ['label' => 'Save as EXCEL'],
                   
                ],


        'panel' => [
            //'type' => GridView::TYPE_PRIMARY,
            //'heading' => '<h5 class="card-title"><i class="fa fa-th-list"></i> '.Yii::t ( 'app', 'List' ).' </h5>',
            //'before'=>$create.' '.$ordering.' ',
            'showFooter' => false,

        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			
			/*[
                //'attribute'=>'leads.lead_assign',
                'label' => 'User Name',
                'format'=>'raw',
                'value'=> function($model)
                {
                	$branch=  \app\models\User::findOne($model->lead->lead_assign);
                    return $branch->first_name;
                },
            ],*/
            [
                'label' => 'Lead ID',
                'format'=>'raw',
                'value' =>function($model)
                {
                    $id = $model->lead->id;
                    $url = Yii::$app->homeUrl.'leads/view?id='.$model->lead->id;
                    return "<a href='$url' target='_blank'>$id</a>";
                },
                'group'=>true

            ],
			
			
            [
                'label' => 'Name',
                'value'=>'lead.contact_name',
                'group'=>true

            ],

            [
                'label' => 'Mobile',
                'value'=>'lead.contact_mobile',
                'group'=>true,
                'hiddenFromExport' =>(Yii::$app->user->can('phonenumberdownload')),
               

            ],
           
            [
                'label' => 'Lead Status',
                'value'=>'lead.lead_status'

            ],
            'note',
			[
                'attribute'=>'created_on',
                'label'=>'Created Date',
                /*'format' => ['date', 'php:d/m/Y H:i a']*/
                'format'=>'raw',
                'value' => function($model){
                    if($model->created_on)
                    {
                        return date("d/m/Y h:i A", strtotime($model->created_on));

                    }else
                    {
                        return '';
                    }
                },


            ],
            /*[
                'attribute'=>'follow_date_time',
                'label'=>'Follow Up DateTime',
                /*'format' => ['date', 'php:d/m/Y H:i a']*/
                /*'format'=>'raw',
                'value' => function($model){
                    if($model->follow_date_time)
                    {
                        return date("d/m/Y h:i A", strtotime($model->follow_date_time));

                    }else
                    {
                        return '';
                    }
                },


            ],*/
			[
                'label' => 'New Note  & Occupation',
                'attribute'=>'new_note',
                'value'     => function ($model) {
                    if ($model->new_note != null) {
                        return $model->new_note; 
                  //or: return Html::encode($model->some_attribute)
                    } else {
                        return '';
                    }
                },

            ],
			[
				'attribute'=>'new_status',
                'label' => 'New Status',
                'format'=>'raw',
                'value'     => function ($model) {
                    if ($model->new_status != null) {
                        return $model->new_status; 
                  //or: return Html::encode($model->some_attribute)
                    } else {
                        return '';
                    }
                },
				//'value'=>' ',
            ],
            
            [
                'attribute'=>'new_follow_date_time',
                'label'=>'New Follow Up DateTime',
                'format'=>'raw',
                'value'     => function ($model) {
                    if ($model->new_follow_date_time != null) {
                        return $model->new_follow_date_time; 
                  //or: return Html::encode($model->some_attribute)
                    } else {
                        return '';
                    }
                },
            ],
            /*[
                'label' => 'Date Range',
				'attribute'=>'date_range',
				'format'=>'raw',
                'value' => function($model){
					if(!empty($this->$modeldaterange))
					{
                   	 return $this->$modeldaterange;
					}else
					{
						return 'abc';
					}
				},

            ],*/
           // 'status',
              

            //'created_by',
            //'updated_on',
            //'updated_by',


        ],
    ]); ?>

    </div>






<?php 
?>
</div>
<script type="text/javascript">
    $(".obaid").click(function(){

   var uri = window.location.toString();

    if (uri.indexOf("?") > 0) {
        var clean_uri = uri.substring(0, uri.indexOf("?"));
        window.history.replaceState({}, document.title, clean_uri);
       $('input.form-control.range-value').attr('value','');
      
    }
    location.reload();
});
   
</script>