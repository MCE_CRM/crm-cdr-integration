<?php

use app\models\DefaultValueModule;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use kartik\widgets\DepDrop;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;
use app\models\Leadresponse;
use kartik\widgets\DateTimePicker;

/* @var $this yii\web\View */ 
/* @var $model app\models\leads */
/* @var $form yii\widgets\ActiveForm */
?>
<style>

    .control-label{
        font-size: 0.9rem !important;
        color: black;
    }
    .form-control{
        padding:0.275rem 0.75rem;
    }

    .row{
        margin-bottom : -12px;
    }

    .coll{
        margin-bottom : -12px;
        margin-left: 0px;
        padding-right:0px;
        padding-left: 0px;
    }
    }
</style>

<?php

//Default Values

if($default_lead_status = DefaultValueModule::getDefaultValueId('lead-status'))
{
    $lead_status = \app\models\LeadStatus::findOne($default_lead_status);
    $model->lead_status = $lead_status->status;
}

if($default_lead_source = DefaultValueModule::getDefaultValueId('lead-source'))
{
    $lead_source = \app\models\LeadSource::findOne($default_lead_source);
    $model->lead_source = $lead_source->name;
}


    if($default_lead_response = DefaultValueModule::getDefaultValueId('lead-response'))
{

    $lead_response = \app\models\Leadresponse::findOne($default_lead_response);
    $model->lead_response = $lead_response->response;
}

   
    




if($default_lead_type = DefaultValueModule::getDefaultValueId('lead-type'))
{

    $lead_type = \app\models\LeadType::findOne($default_lead_type);
    $model->lead_type = $lead_type->type_name;
}





$country = '';
if($model->isNewRecord)
{
    if($default_country = DefaultValueModule::getDefaultValueId('country'))
    {
        $c= \app\models\Country::findOne($default_country);
        $country = $c->country;
         $model->contact_country = $default_country;
    }
}
else
{
    $c= \app\models\Country::findOne($model->country);
     $country = $c->country;

    $s = \app\models\State::findOne($model->state);
    $state = $s->state;

    $ci = \app\models\City::findOne($model->city);
    $city = $ci->city;

}


if(\Yii::$app->user->can('Agent'))
{
   $model->lead_assign  = Yii::$app->user->id;
}



?>


<div class="card-body">

    <?php

    if($_GET['id'])
    {
        $form = \yii\widgets\ActiveForm::begin([
            'id' => 'form',
            'enableAjaxValidation' => true,
            'validationUrl' => Yii::$app->homeUrl.'leads/validate?id='.$model->id.'',

        ]);
    }
    else
    {
        $form = \yii\widgets\ActiveForm::begin([
            'id' => 'form',
            'enableAjaxValidation' => true,
            'validationUrl' => Yii::$app->homeUrl.'leads/validate',
            'fieldConfig' => [
                'template' => "{label}{input}<label class=\"error\">{error}</label>",
                //'options' => ['class' => 'form-group'],
                'labelOptions' => ['class' => 'control-label'],
            ],
            'options' => [
                'class' => 'form-horizontal'
            ],
            'errorCssClass' => 'has-danger',
        ]);
    }
    
    if(isset($_GET['name']))
    {
        $model->contact_name=$_GET['name'];
        $model->contact_mobile=$_GET['mobile'];
        $model->contact_email=$_GET['email'];
        $model->contact_country=$_GET['country'];
        $model->contact_address=$_GET['address'];
        $model->contact_state=$_GET['state'];
        $model->contact_city=$_GET['city'];
        $model->project_id=$_GET['project'];
        $model->block_sector=$_GET['blocksector'];
        $model->property_id=$_GET['property_id'];


         $c= \app\models\Country::findOne($model->contact_country);
        $country = $c->country;

        $s = \app\models\State::findOne($model->contact_state);
        $state = $s->state;



        $ci = \app\models\City::findOne($model->contact_city);
        $city = $ci->city;
        

    }


    ?>


        <div class="card-body">

            <div class="row">
                <div class="col-lg-8 ">
                    <div class="row">
                        <div class="col-lg-4 ">
                            <?= $form->field($model, 'contact_name')->textInput(['maxlength' => true]) ?>

                            <?php if(isset($_GET['name']))
                            {

                                ?>
                                <input type="hidden" name="addleadclient" value="1">
                            <?php } ?>
    

                        </div>
                        <div class="col-lg-4 ">
                            <?= $form->field($model, 'contact_mobile')->textInput(['maxlength' => true]) ?>


                        </div>
                        <div class="col-lg-4 ">
                            <?= $form->field($model, 'contact_email')->textInput(['maxlength' => true]) ?>


                        </div>
                    </div>
                </div>
                <div class="col-lg-4 ">
                    <?php

                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'lead_status')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\LeadStatus::find()->where(['active'=>1])->orderBy([
                            'sort_order' => SORT_ASC,
                        ])->all(),'status','status'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Status ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);




                    ?>


                </div>
            </div>

            <div class="row">
                <div class="col-lg-8 ">
                    <div class="row">

                        <div class="col-lg-4 ">
                            <?=

                            $form->field($model, 'contact_country')->widget(Select2::classname(), [
                                'initValueText' => $country, // set the initial display text
                                'options' => ['placeholder' => 'Search for a Country ...'],
                                'theme' => Select2::THEME_BOOTSTRAP,
                                'pluginOptions' => [
                                    'allowClear' => true,
                                    'minimumInputLength' => 1,
                                    'language' => [
                                        'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                                    ],
                                    'ajax' => [
                                        'url' => \yii\helpers\Url::to(['ajax/country-list']),
                                        'dataType' => 'json',
                                        'data' => new JsExpression('function(params) { return {q:params.term}; }')
                                    ],
                                    'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                                    'templateResult' => new JsExpression('function(city) { return city.text; }'),
                                    'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                                ],
                            ]);



                            ?>



                        </div>
                        <div class="col-lg-4 ">
                            <?php
                                
                            echo $form->field($model, 'contact_state')->widget(DepDrop::classname(), [
                                'options'=> ['id'=>'subcat-id'],
                                'type'=>DepDrop::TYPE_SELECT2,
                                'data'=> [$model->contact_state =>$state],
                                'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],
                                'pluginOptions'=> [
                                    'depends'=>['leads-contact_country'],
                                    'initDepends' => ['leads-contact_country'],
                                    'initialize'=>true,
                                    'placeholder'=>'Select...',
                                    'url'=>Url::to(['/ajax/get-country-state'])
                                ]
                            ]);

                            ?>
                        </div>

                        <div class="col-lg-4">
                            <?php

                                 echo $form->field($model, 'contact_city')->widget(DepDrop::classname(), [
                                'options'=>['id'=>'subcat2-id'],
                                'type'=>DepDrop::TYPE_SELECT2,
                                 'data'=> [$model->contact_city =>$city],
                                'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP,'pluginOptions'=>['allowClear'=>true]],


                                'pluginOptions'=>[
                                    'depends'=>['subcat-id'],

                                    'placeholder'=>'Select...',
                                    'url'=>Url::to(['/ajax/get-state-city'])
                                ]
                            ]);


                            ?>
                        </div>

                        <div class="col-lg-6 ">
                            <?= $form->field($model, 'contact_address')->textarea(['rows' => '3']) ?>

                        </div>

                        <div class="col-lg-6 ">
                            <?= $form->field($model, 'lead_description')->textarea(['rows' => '3']) ?>

                        </div>

                    </div>
                </div>
                <div class="col-lg-4 ">

                    <div class="coll col-lg-12">
                        <?php

                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'lead_type')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\LeadType::find()->where(['active'=>1])->orderBy([
                                'sort_order' => SORT_ASC,
                            ])->all(),'type_name','type_name'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Select a Type ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);




                        ?>

                    </div>

                    <div class="coll col-lg-12">
                        <?php
                if(Yii::$app->user->can('teamlead')){
                
                        $team_lead=Yii::$app->user->identity->lead_assign;
                        $split = preg_split('/(?:"[^"]*"|)\K\s*(,\s*|$)/', $team_lead);
                        $result = array_filter($split);
     

                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\User::find()->where(
                                ['id'=>$result])->all(),'id','username'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => ''],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);

                        }else{
                             echo $form->field($model, 'lead_assign')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\User::find()->where(
                                ['!=','username','superadmin'])->all(),'id','username'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => ''],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);
                        }




                        ?>

                    </div>
                    <div class="coll col-lg-12">
                        <?php

                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'lead_source')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\LeadSource::find()->where(['active'=>1])->orderBy([
                                'sort_order' => SORT_ASC,
                            ])->all(),'name','name'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Select a Source ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);




                        ?>
                    </div>
                    <div class="coll col-lg-12">
                        <?php

                        // Normal select with ActiveForm & model
                        echo $form->field($model, 'lead_response')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(\app\models\Leadresponse::find()->where(['active'=>1])->all(),'response','response'),
                            'theme' => Select2::THEME_BOOTSTRAP,
                            'options' => ['placeholder' => 'Select a Response ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                        ]);




                        ?>
                    </div>

                </div>
            </div>
            <br>
            <br>

            <div class="row">
                <div class="col-lg-3 ">
                    <?php


                    // Normal select with ActiveForm & model
                    echo $form->field($model, 'project_id')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(\app\models\Projects::find()->where(['active'=>1])->all(),'id','name'),
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'options' => ['placeholder' => 'Select a Project ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                    ]);





                    ?>

                </div>
                <div class="col-lg-3 ">

                    <?php


                    echo $form->field($model, 'block_sector')->widget(DepDrop::classname(), [
                        'options'=> ['id'=>'block-sector1'],
                        'type'=>DepDrop::TYPE_SELECT2,
                        'select2Options'=>['theme' => Select2::THEME_BOOTSTRAP],
                        'pluginOptions'=> [
							'allowClear'=>true,
                            'depends'=>['leads-project_id'],
                            'initDepends' => ['leads-project_id'],
                            'initialize'=>true,
                            'placeholder'=>'Select...',
                            'url'=>Url::to(['/ajax/get-block-sector'])
                        ]
                    ]);


                    ?>
                </div>
                <div class="col-lg-3 ">
                    <?php



                    echo $form->field($model, 'property_id')->widget(Select2::classname(), [
                        //'initValueText' => $country, // set the initial display text
                        'options' => ['placeholder' => 'Search for a Property ...','id'=>'id_select'],
                        'theme' => Select2::THEME_BOOTSTRAP,
                        'pluginOptions' => [
							
                            'allowClear' => true,
                            'minimumInputLength' => 1,
                            'language' => [
                                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
                            ],
							
                            'ajax' => [
                                'url' => \yii\helpers\Url::to(['ajax/property-list-ava']),
                                'dataType' => 'json',
                                'data' => new JsExpression('function(params) { return {q:params.term,project: $("#leads-project_id").val(), block: $("#block-sector1").val()}; }')
                            ],
                            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
                            'templateResult' => new JsExpression('function(city) { return city.text; }'),
                            'templateSelection' => new JsExpression('function (city) { return city.text; }'),
                        ],
                    ]);

                    
                    ?>

                </div>

                <div class="col-lg-3 ">
                    <?php

                    // usage without model
                   /* echo '<label class="control-label" for="leads-follow_up">Follow Up</label>';
                    echo DatePicker::widget([
                        'name' => 'check_issue_date',
                        'value' => date('d-M-Y', strtotime('+2 days')),
                        'options' => ['placeholder' => 'Select issue date ...'],
                        'pluginOptions' => [
                            'format' => 'dd-M-yyyy',
                            'todayHighlight' => true
                        ]
                    ]);*/


                    ?>
                    <?= $form->field($model, 'sms_status')->dropDownList(['incoming'=>'Incoming Call','outgoing'=>'Outgoing Call']) ?>

                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-3 ">
                <?php
                echo $form->field($model, 'follow_date_time')->widget(DateTimePicker::classname(), [
                    'options' => ['autocomplete' => 'off',],
                    'readonly' => true,
                    'pluginOptions' => [
                        'autoclose'=>true,
                        'format' => 'dd/mm/yyyy h:ii',
                        'startDate'=> date('d-m-Y H:i',time()),
                    ]
                ]);
                ?>
            </div>
            <div class="col-lg-3 ">
                <?= $form->field($model, 'note')->textarea(['rows' => '3'])  ?>
            </div>
		</div>
    <a href="javascript:void(0)" class="btn btn-primary btn-sm" onclick="clickMe()">Add New Property</a>

</div>
<footer class="card-header text-center">


    <!-- <buttonb type="button" onclick="test()">test popup</button> -->
    <a href="<?= Yii::$app->homeUrl?>leads" class="mb-1 mt-1 mr-1 btn btn-danger">Cancel</a>

    <?= Html::submitButton('Save', ['class' => 'mb-1 mt-1 mr-1 btn btn-primary leadsave']) ?>

</footer>

<div class="property-form">


    <?php ActiveForm::end(); ?>


</div>
<script type="text/javascript">




  function clickMe()
  {
      //windowExternal('<?php echo Yii::$app->homeUrl?>property/create-popup',1000,1000);

          var url = '<?php echo Yii::$app->homeUrl?>property/create-popup';
          var dialog = bootbox.dialog({
              title: 'Add Open Property',
              message: '<p><i class="fa fa-spin fa-spinner"></i> Loading...</p>',
              size:'large',
              onEscape: function() {
                  // you can do anything here you want when the user dismisses dialog

              }

          });

          dialog.init(function(){
              var request = $.ajax({
                  url: url,
                  method: "GET",
              });

              request.done(function( msg ) {

                  dialog.find('.bootbox-body').html(msg);
              });

              $(document).on("submit", "#form2", function (event) {
                  event.preventDefault();
                  event.stopImmediatePropagation();

                  $(this).submit(function() {
                      return false;
                  });



                  $form = $('#form2'); //wrap this in jQuery

                  var url = $form.attr('action');

                  $form = $('#form2');

                  $.ajax({
                      type: "POST",
                      url: url,
                      data: $("#form2").serialize(),
                      // serializes the form's elements.
                      success: function(data)
                      {
                          var result  = data;
                          if(result.status=='true')
                          {



                              var $newOption = $("<option selected='selected'></option>").val(result.id).text(result.name);

                              $("#id_select").append($newOption).trigger('change');

                              bootbox.hideAll();
                          }else{
                              alert("Some Error Occur");
                          }


                      }
                  });
                  // avoid to execute the actual submit of the form.

              });

          });






  }


  function windowExternal(url,width,height)
  {


      popupWindow = window.open(
          url,'popUpWindow','height=30000,width=40000,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')

  }



</script>

