<?php

namespace app\modules\cdr\models;

use Yii;

/**
 * This is the model class for table "durations".
 *
 * @property int $id
 * @property string $land_time
 * @property string $attend_time
 * @property string $time_out
 * @property string $phone
 * @property string $caller_id
 * @property int $status
 * @property string $created_on
 */
class Durations extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'durations';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['land_time', 'attend_time', 'time_out', 'created_on'], 'safe'],
            [['status'], 'integer'],
            [['phone'], 'string', 'max' => 256],
            [['caller_id'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'land_time' => 'Land Time',
            'attend_time' => 'Attend Time',
            'time_out' => 'Time Out',
            'phone' => 'Phone',
            'caller_id' => 'Caller ID',
            'status' => 'Status',
            'created_on' => 'Created On',
        ];
    }
}
