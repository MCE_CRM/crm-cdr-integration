
<script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">

</script>
<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>


<?php

/* @var $this yii\web\View */

\app\modules\cdr\assets\DashboardAsset::register($this);
$this->title = 'Dashboard';
$total_csr = 0;
$total_agents = 0;
$user = \app\models\User::find()->all();
foreach ($user as $data)
{
    $role = getUserRoles($data->id);
    if($role ==='CSR')
    {
        $total_csr++;
    }
    if($role =="Agent")
    {
        $total_agents++;
    }
}

function getUserRoles($user_id){
    $connection = \Yii::$app->db;
    $sql="select auth_item.* from auth_item,auth_assignment where auth_item.type=1 and auth_assignment.user_id=$user_id and auth_assignment.item_name=auth_item.name";
    $command=$connection->createCommand($sql);
    $dataReader=$command->queryAll();
    $roles ='';
    if(count($dataReader) > 0){
        foreach($dataReader as $role){
            $roles= $role['name'];
        }
    }
    return $roles;
}


?>




<style>
.datepicker
{
    margin-top: -33px;
}

.card-header
{
    text-align:center;
}
.ibox {
    clear: both;
    margin-bottom: 25px;
    margin-top: 0;
    padding: 0;
}
.ibox-title {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background-color: #ffffff;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 2px 0 0;
    color: inherit;
    margin-bottom: 0;
    padding: 15px 15px 7px;
    min-height: 48px;
}


.ibox-title .label {
    float: left;
    margin-left: 4px;
}
.label, .ibox .label {
    font-size: 10px;
    border-radius: 10px;
}
.label-success, .badge-success {
    background-color: #1c84c6;
    color: #FFFFFF;
}
.label {
    background-color:#2baab1;
    color:white;
    font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
    font-weight: 600;
    padding: 0px 8px;
    text-shadow: none;
}

.ibox-title h5 {
    display: inline-block;
    font-size: 14px;
    margin: 0 0 7px;
    padding: 0;
    text-overflow: ellipsis;
    float: left;
    font-weight: 600;
}
.ibox-content {
    clear: both;
}

.ibox-content {
    background-color: #ffffff;
    color: inherit;
    padding: 15px 20px 20px 20px;
    border-color: #e7eaec;
    border-image: none;
    border-style: solid solid none;
    border-width: 1px 0;
}
small, .small {
    font-size: 85%;
}
.no-margins {
    margin: 0 !important;
}
.stat-percent {
    float: right;
}

.fa {
    display: inline-block;
    font: normal normal normal 14px/1;
    font-size: inherit;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
}
#myChart
{
    height:100% !important;
    width:100% !important;

}
.amcharts-chart-div a {display:none !important;}
a.dlink:hover{
    text-decoration: none;
}



#notdashboard{
    display: none;
}

.topnav {
  overflow: hidden;
 /* background-color: white;*/
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;

  display: inline-block;
    border-radius: 4px;
    font-size: 12px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    padding: 10px;
    text-transform: uppercase;
    white-space: initial;
}

.active {
  background-color: white;
  color: black;
}

.topnav .icon {
  display: none;
}

.dropdown {
  float: left;
  overflow: hidden;
}

.dropdown .dropbtn {
  font-size: 17px;    
  border: none;
  outline: none;
  color: black;
  padding: 14px 16px;
  background-color: inherit;
  font-family: inherit;
  margin: 0;

  display: inline-block;
    border-radius: 4px;
    font-size: 12px;
    font-style: normal;
    font-weight: 700;
    line-height: 20px;
    padding: 10px;
    text-transform: uppercase;
    white-space: initial;
}

.dropdown-content {
  display: none;
  position: fixed;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 9999;
}

.dropdown-content a {
  float: none;
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.topnav a:hover, .dropdown:hover .dropbtn {
  background-color: #555;
  color: white;
}

.dropdown-content a:hover {
  background-color: #ddd;
  color: black;
}

.dropdown:hover .dropdown-content {
  display: block;
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child), .dropdown .dropbtn {
    display: none;
  }
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
  .topnav.responsive .dropdown {float: none;}
  .topnav.responsive .dropdown-content {position: relative;}
  .topnav.responsive .dropdown .dropbtn {
    display: block;
    width: 100%;
    text-align: left;
  }
}



.top-navigation .wrapper.wrapper-content {
    padding: 0px;
}

.widget-summary .summary .amount{
  font-weight: 100 !important;
}

.widget-summary .summary .title{
  font-weight: bold;
}

.tab1{
    background-color: #d0cfcf;
    color: white;
}

.tr1{
    border: 1px solid white;
}

.th1{
    border-right: 1px solid white;
}

.td1{
    border-right: 1px solid white;
}

</style>

         <?php
            $query = \app\models\Setting::find()->orderBy('id DESC')->limit(1)->offset(0)->one();
  
  ?>
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.js"></script> 
<script src="<?= Yii::$app->homeUrl?>porto/js/theme.init.js"></script>
<div class="row" style=" width: 95%;">
    <div class="col-lg-12">
        <div class="logo" style="float: left;">
        <a href="#" class=""><img src="<?= Yii::$app->homeUrl ?>logoimages/<?=$query->logo?>" style = "width: 80px; height: 45px;"></a>
    </div>
<div class="topnav" id="myTopnav">
   
<a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/dashboard">Dashboard</a>
 <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/cdr">CDR</a>
            
  <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>site/agent-status">Agent Status</a>

  <div class="dropdown">
    <button class="dropbtn">Reports 
      <i class="fa fa-caret-down"></i>
    </button>
      <?php if(Yii::$app->user->can('cdr/logo')){?>
    <div class="dropdown-content">
       <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>agent-session">Agent Attendance Report</a>
    </div>
      <?php } ?>
  </div> 

  <div class="dropdown">
    <button class="dropbtn">Setting 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
       <?php if(Yii::$app->user->can('cdr/logo')){?>
                    <a href="<?= Yii::$app->homeUrl?>setting/index">logo upload</a>
                    <?php } ?>

                    <?php if(Yii::$app->user->can('user/management')){?>
                        
                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>user/index">Users Managnment</a>
                        
                    <?php } ?>
                    <?php if(Yii::$app->user->can('user/management')){?>
                        
                            <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>rbac">Role Based Access Control</a>
                        
                    <?php } ?>
    </div>
  </div> 

 
  <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="myFunction()">&#9776;</a>

  <div class="dropdown pull-right" >
    <button class="dropbtn">Admin 
      <i class="fa fa-caret-down"></i>
    </button>
    <div class="dropdown-content">
       <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>rbac">Profile</a>
    <a aria-expanded="false" role="button" href="<?= Yii::$app->homeUrl?>rbac">Logout</a>
    </div>
  </div> 
</div>
</div>
</div>
<br><br><br><br><br>
<h3 style="align-content: center; margin: 0 auto;" >Daily Statistics (<?=date('d-M-Y')?>)</h3>
<br><br><br>
<div class="row">

    <div class="col-lg-6">
        <div class="row mb-3">
            <div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Login/Active CSRs</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                   <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fas fa-sign-in-alt"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                 <div class="summary">
                                    <h4 class="title"><br></h4>
                                    <div class="info">
                                        <strong class="amount">
                                            <?php
                                            date_default_timezone_set("Asia/Karachi");

                                              exec('sudo /usr/sbin/asterisk -r -x "queue show" 2>&1',$online);
                                              /*echo $online;*/
                                              echo"2";

                                            ?>
                                            

                                        </strong>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                     </a>
                </section>
           
            </div>

            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Calls</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-tertiary">
                                    <i class="fa fa-exchange"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> <br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalcalls;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                   </a>
                </section>
            
            </div>
            
        </div>
        <div class="row">

            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">No Answer/dropped Calls</div>
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-quaternary">
                                    <i class="fa fa-tty"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"><br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalbusy;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Transfer</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fa fa-circle-o-notch"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> <br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totaltransfer;?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>

              <div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">User System/Computer IP:</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="fa fa-laptop"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"></h4>
                                    <div class="info">
                                        <strong class="amount" style="font-size: 12px"><?php echo 'User IP - '.$_SERVER['REMOTE_ADDR'];?></strong>
                                    </div>
                                </div>
                                <div class="summary-footer">
                                    <a class="text-muted text-uppercase" href="#"></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>


        </div>
    </div>
    <div class="col-lg-6">
      
        <div class="row mb-3">
            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Incoming</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-secondary">
                                    <i class="glyphicon glyphicon-copy"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"><br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totalincomming;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>
            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Outgoing</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="" >
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-primary">
                                    <i class="glyphicon glyphicon-paste"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> <br></h4>
                                    <div class="info">
                                        <strong class="amount"><?php echo $totaloutgoing;?></strong>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>

        </div>
        <div class="row">

            <div class="col-xl-6">
              <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">SIP STATUS</div>
                <section class="card card-featured-left card-featured-quaternary mb-3">
                    <a class="dlink" href="">
                    <div class="card-body">
                        <div class="widget-summary">
                            <div class="widget-summary-col widget-summary-col-icon">
                                <div class="summary-icon bg-quaternary">
                                    <i class="fa fa-globe"></i>
                                </div>
                            </div>
                            <div class="widget-summary-col">
                                <div class="summary">
                                    <h4 class="title"> <br></h4>
                                    <div class="info">
                                        <strong class="amount" style="font-size: 12px !important;"><?php echo $sipstatus;echo"<br>";echo "ip:".$sipip;?></strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
                </section>
            </div>
            <div class="col-xl-6">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Voicemails:</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <a class="dlink" href="">
                        <div class="card-body">
                            <div class="widget-summary">
                                <div class="widget-summary-col widget-summary-col-icon">
                                    <div class="summary-icon bg-primary">
                                        <i class="fas fa-microphone"></i>
                                    </div>
                                </div>
                                <div class="widget-summary-col">
                                    <div class="summary">
                                        <h4 class="title"><br></h4>
                                        <div class="info">
                                            <strong class="amount"></strong>
                                        </div>
                                    </div>
                                    <div class="summary-footer">
                                        <a class="text-muted text-uppercase" href="#"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </section>
            </div>

        </div>
    </div>

</div>

<br><br><br>
<h3 style="align-content: center; margin: 0 auto;" >Monthly Statistics (<?=date('M-Y')?>)</h3>
<br><br><br>

<div class="row" style="width: 100%;">

    <div class="col-lg-12">
        <div class="row mb-3">



            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly incoming</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">

                    <div class="circular-bar">
                        <div id="incomingchartContainer" style="height: 200px; width: 90%;"></div>
                    </div>
                </section>
            </div>

            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly outgoing Calls</div>
                <section class="card card-featured-left card-featured-secondary mb-3">

                    <div class="circular-bar">
                        <div id="outgoingchartContainer" style="height: 200px; width: 90%;"></div>

                    </div>

                </section>

            </div>

            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Top 5 Agents of Month</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <table class="tab1">

                        <tr class="tr1">

                            <th class="th1"> # </th>
                            <th class="th1"> Name </th>
                            <th class="th1"> Agent ID</th>
                            <th class="th1"> Calls </th>
                        </tr>

                        <?php $user = \app\models\User::find()->where('agent_id != "NULL"')->orWhere('agent_id != ""')->limit(5)->all();
                        $i =1;
                        foreach ($user as $value){ ?>
                            <tr class="tr1">
                                <td class="td1"> <?=$i?></td>
                                <td class="td1"> <?=$value->username?></td>
                                <td class="td1"> <?=$value->agent_id?></td>
                                <td class="td1"> 12 </td>
                            </tr>

                        <?php } ?>


                    </table>

                </section>

            </div>

        </div>
        <div class="row">

            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Transferred Calls</div>
                <section class="card card-featured-left card-featured-quaternary mb-3">

                    <div id="transferContainer" style="height: 200px; width: 90%;"></div>

                </section>
            </div>
            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Dropped Calls/Not Answered</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">
                    <div id="barchartContainer" style="height: 200px; width: 90%;"></div>
                </section>
            </div>

            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly Inquires</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">

                    <div id="inquirieschartdiv" style="height: 200px; width: 90%;">

                    </div>

                </section>
            </div>

        </div>
    </div>

</div>

<div class="row" style="width: 100%;">

    <div class="col-lg-12">
        <div class="row mb-3">
            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">CSR/Agent-wise Efficiency Graph</div>
                <section class="card card-featured-left card-featured-tertiary mb-3">

                    <div class="circular-bar">
                        <div id="incomingchartContainer" style="height: 200px; width: 90%;"></div>
                    </div>
                </section>

            </div>

            <!--<div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Total Monthly outgoing Calls</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    <div class="circular-bar">
                        <div id="outgoingchartContainer" style="height: 200px; width: 90%;"></div>

                    </div>
                </section>
            </div>

            <div class="col-xl-4">
                <div class="labelControlLabelCell" style="text-align: center; vertical-align: middle;background-color:  rgb(230, 231, 232); font-weight: bold; color: black">Top 5 Agents of Month</div>
                <section class="card card-featured-left card-featured-secondary mb-3">
                    jhjkhj

                </section>
            </div>-->

        </div>

    </div>
</div>





<br><br><br>
<h3 style="align-content: center; margin: 0 auto;" >Live Status of Agents/CSRs</h3>
<br><br><br>

<table class="tab2">
    <tr class="tr1">
        <th class="th1"> # </th>
        <th class="th1"> Name </th>
        <th class="th1"> Agent ID </th>
        <th class="th1"> Calls </th>
    </tr>

    <?php
    $i =1;
    foreach ($user as $value){ ?>
        <tr class="tr1">
            <td class="td1"> <?=$i?></td>
            <td class="td1"> <?=$value->username?> </td>
            <td class="td1"> <?=$value->agent_id?> </td>
            <td class="td1"> 12 </td>
        </tr>

    <?php } ?>

</table>

<script src="https://cdnjs.com/libraries/Chart.js"></script>
<script src="http://canvasjs.com/assets/script/canvasjs.min.js"></script>

<script>
    function myFunction() {
        var x = document.getElementById("myTopnav");
        if (x.className === "topnav") {
            x.className += " responsive";
        } else {
            x.className = "topnav";
        }
    }


    var barchart = new CanvasJS.Chart("barchartContainer", {
        animationEnabled: true,
        theme: "light2", // "light1", "light2", "dark1", "dark2"
        /*title:{
            text: "Top Oil Reserves"
        },
        axisY: {
            title: "Reserves(MMbbl)"
        },*/
        data: [{
            type: "column",
            showInLegend: true,
            legendMarkerColor: "grey",
            /*legendText: "MMbbl = one million barrels",*/
            dataPoints: [
                { y: '<?php echo $totalbusy;?>', label: "Dropped" },


            ]
        }]
    });
    barchart.render();
    barchart.destroy();


    var incomingchart = new CanvasJS.Chart("incomingchartContainer",
        {
            title: {
                verticalAlign: "center"
            },
            data: [
                {
                    type: "doughnut",
                    indexLabel: "#percent%",
                    //innerRadius: "60%",

                    indexLabelPlacement: "inside",
                    dataPoints: [
                        { label: "Daily", y: '<?=$totalincomming?>'},

                        { label: "B", y: 55 },
                    ]
                }
            ]
        });

    incomingchart.render();

    var total = 0;

    for(var i = 0; i < incomingchart.options.data[0].dataPoints.length; i++) {

        total += incomingchart.options.data[0].dataPoints[i].y;
    }
    incomingchart.title.set("text", total);

    var outgoingchart = new CanvasJS.Chart("outgoingchartContainer",
        {
            title: {
                verticalAlign: "center"
            },
            data: [
                {
                    type: "doughnut",
                    indexLabel: "#percent%",
                    //innerRadius: "60%",
                    indexLabelPlacement: "inside",
                    dataPoints: [
                        { label: "Daily", y: '<?php echo $totaloutgoing;?>' },
                        { label: "B", y: 56 },
                    ]
                }
            ]
        });

    outgoingchart.render();

    var total = 0;

    for(var i = 0; i < outgoingchart.options.data[0].dataPoints.length; i++) {

        total += outgoingchart.options.data[0].dataPoints[i].y;

    }

    outgoingchart.title.set("text", total);

    var transferContainer = new CanvasJS.Chart("transferContainer", {
        animationEnabled: true,
        /* title:{
            text: "Stock Price of BMW - August"
        },*/
        axisX:{

            //valueFormatString: "DD MMM",
            /*crosshair: {
                enabled: true,
                snapToDataPoint: true
            }*/
        },
        axisY: {

            // includeZero: false,
            // valueFormatString: "$#0.00",

            crosshair: {
                enabled: true,
                snapToDataPoint: true,
                labelFormatter: function(e) {
                    return  CanvasJS.formatNumber(e.value);
                }
            }
        },
        data: [{
            type: "area",
            dataPoints: [
                {  y: '<?php echo $totaltransfer;?>' },
            ]
        }]
    });

    transferContainer.render();

    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

// Create chart instance
        var chartdiv = am4core.create("chartdiv1", am4charts.XYChart);


// Add data
        chartdiv.data = [{
            "year": "2016",
            "europe": 2.5,
            "namerica": 2.5,
            "asia": 2.1,
            "lamerica": 0.3,
            "meast": 0.2,
            "africa": 0.1
        }, {
            "year": "2017",
            "europe": 2.6,
            "namerica": 2.7,
            "asia": 2.2,
            "lamerica": 0.3,
            "meast": 0.3,
            "africa": 0.1
        }, {
            "year": "2018",
            "europe": 2.8,
            "namerica": 2.9,
            "asia": 2.4,
            "lamerica": 0.3,
            "meast": 0.3,
            "africa": 0.1
        }];

// Create axes
        var categoryAxis = chartdiv.xAxes.push(new am4charts.CategoryAxis());
        categoryAxis.dataFields.category = "year";
        categoryAxis.renderer.grid.template.location = 0;
        var valueAxis = chartdiv.yAxes.push(new am4charts.ValueAxis());
        valueAxis.renderer.inside = true;
        valueAxis.renderer.labels.template.disabled = true;
        valueAxis.min = 0;

// Create series
        function createSeries(field, name) {
            // Set up series
            var series = chartdiv.series.push(new am4charts.ColumnSeries());
            series.name = name;
            series.dataFields.valueY = field;
            series.dataFields.categoryX = "year";
            series.sequencedInterpolation = true;
            // Make it stacked
            series.stacked = true;

            // Configure columns
            series.columns.template.width = am4core.percent(60);
            series.columns.template.tooltipText = "[bold]{name}[/]\n[font-size:14px]{categoryX}: {valueY}";

            // Add label
            var labelBullet = series.bullets.push(new am4charts.LabelBullet());
            labelBullet.label.text = "{valueY}";
            labelBullet.locationY = 0.5;
            return series;

        }



// Legend
        chartdiv.legend = new am4charts.Legend();

    }); // end am4core.ready()

    am4core.ready(function() {

// Themes begin
        am4core.useTheme(am4themes_animated);
// Themes end

        /**
         * Define data for each year
         */

        var chartData = {
            "10": [
                {
                    "sector": "Agriculture", "size": 6.6
                },
            ],
        };

// Create chart instance

        var chart = am4core.create("inquirieschartdiv", am4charts.PieChart);

// Add data
        chart.data = [
            { "sector": "Open", "size": 6},
            { "sector": "Closed", "size": 20 },

        ];

// Add label
        chart.innerRadius = 100;
        var label = chart.seriesContainer.createChild(am4core.Label);
        label.text = "10";
        label.horizontalCenter = "middle";
        label.verticalCenter = "middle";
        label.fontSize = 50;

// Add and configure Series

        var pieSeries = chart.series.push(new am4charts.PieSeries());
        pieSeries.dataFields.value = "size";
        pieSeries.dataFields.category = "sector";

// Animate chart data
        var currentYear = 10;
        function getCurrentData() {
            label.text = currentYear;
            var data = chartData[currentYear];
            currentYear++;
            if (currentYear > 14)
                currentYear = 10;
            return data;
        }

        function loop() {
            //chart.allLabels[0].text = currentYear;
            var data = getCurrentData();
            for(var i = 0; i < data.length; i++) {

                chart.data[i].size = data[i].size;
            }
            chart.invalidateRawData();
            chart.setTimeout( loop, 4000 );
        }

        loop();

    }); // end am4core.ready()


</script>

<!-- HTML -->











<!-- HTML -->









